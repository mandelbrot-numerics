mandelbrot-numerics
===================

Numerical algorithms related to the Mandelbrot set: ray tracing, nucleus
location, bond points, etc.


Dependencies
------------

For the C code, this may suffice on Debian-based systems:

    sudo aptitude install libmpc-dev


Build
-----

To install the C library and programs into ~/opt for example:

    make -C c/lib prefix=${HOME}/opt install
    make -C c/bin prefix=${HOME}/opt install


Run
---

To run the programs:

    LD_LIBRARY_PATH=${HOME}/opt/lib m-render # or other example program


Haskell
-------

To install the Haskell library with GHC:

    cabal install mandelbrot-numerics.cabal

To run with Hugs:

    hugs -P:hs/lib Mandelbrot.Numerics


Legal
-----

mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
Copyright (C) 2015-2023  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
