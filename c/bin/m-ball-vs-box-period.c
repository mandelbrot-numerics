#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define N 36
#define PI 3.141592653589793

static int sgn(double x)
{
  return (x > 0) - (0 > x);
}

static double cross(double _Complex a, double _Complex b) {
  return cimag(a) * creal(b) - creal(a) * cimag(b);
}

static int crosses_positive_real_axis(double _Complex a, double _Complex b) {
  if (sgn(cimag(a)) != sgn(cimag(b))) {
    double _Complex d = b - a;
    int s = sgn(cimag(d));
    int t = sgn(cross(d, a));
    return s == t;
  }
  return 0;
}

int main(int argc, char **argv)
{
  if (argc < 4) return 1;
  double _Complex c = atof(argv[1]) + I * atof(argv[2]);
  double r = atof(argv[3]);
  double _Complex c_ball = c;
  double _Complex z_ball = c;
  double r_ball = r;
  double _Complex c_box[N];
  double _Complex z_box[N];
  for (int i = 0; i < N; ++i)
  {
    double t = 2 * PI * (i + 0.5) / N;
    c_box[i] = z_box[i] = c + r * (cos(t) + I * sin(t));
  }
  int ball_found = 0;
  int box_found = 0;
  int both_found = 0;
  for (int p = 1; p < 100; ++p)
  {
    double a_box = 0;
    for (int i = 0; i < N; ++i)
    {
      a_box += creal(z_box[i]) * cimag(z_box[(i+1)%N]) - creal(z_box[(i+1)%N]) * cimag(z_box[i]);
    }
    a_box = 0.5 * fabs(a_box);
    double r_box = sqrt(a_box / PI);
    int p_ball = cabs(z_ball) <= r_ball;
    int count = 0;
    for (int i = 0; i < N; ++i)
    {
      count += crosses_positive_real_axis(z_box[i], z_box[(i+1)%N]);
    }
    int p_box = count & 1;
    ball_found |= p_ball;
    box_found |= p_box;
    both_found = ball_found && box_found;
    printf("%d\t%e\t%e\t%d\t%d\n", p, r_ball, r_box, p_ball, p_box);
    fflush(stdout);
    if (both_found) break;
    r_ball = (2 * cabs(z_ball) + r_ball) * r_ball;
    z_ball = z_ball * z_ball + c_ball;
    for (int i = 0; i < N; ++i)
    {
      z_box[i] = z_box[i] * z_box[i] + c_box[i];
    }
  }
  return 0;
}
