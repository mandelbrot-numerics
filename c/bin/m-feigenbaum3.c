// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <complex.h>
#include <stdio.h>

#include <mandelbrot-numerics.h>

#define twopi 6.283185307179586

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  if (argc != 4) return 1;
  complex double location = atof(argv[1]) + I * atof(argv[2]);
  int periodFactor = atoi(argv[3]);
  int period = 1;
  complex double nucleus = 0;
  complex double size = 1;
;
  for (int depth = 0; depth < 12; ++depth) {
    size = m_d_size(nucleus, period);
    complex double estimate = nucleus + location * size;
    period *= periodFactor;
    m_d_nucleus(&nucleus, estimate, period, 64);
  }
  complex double c0 = nucleus;
  nucleus = 0;
  size = 1;
  period = 1;
  for (int depth = 0; depth < 12; ++depth) {
    size = m_d_size(nucleus, period);
    complex double d0 = nucleus - c0;
    complex double estimate = nucleus + location * size;
    period *= periodFactor;
    m_d_nucleus(&nucleus, estimate, period, 64);
    complex double d1 = nucleus - c0;
    complex double r = d0 / d1;
    printf
      ( "#preset \nC = %.18e,%.18e\nZ = %.18e,%.18e\nT = %d\n#endpreset\n"
      , creal(c0), cimag(c0), creal(r), cimag(r), (int)round(25 * log10(cabs(r)))
      );
    fflush(stdout);
  }
  return 0;
}
