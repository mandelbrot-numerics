// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <complex.h>
#include <stdio.h>

#include <mandelbrot-numerics.h>

#define twopi 6.283185307179586

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  int count = 32;
  int period = 2;
  double size = 0.5;
  complex double nucleus = -1;
  printf("%.18e\n", -1.0);
  for (int depth = 1; depth < count; ++depth) {
    period *= 2;
    complex double estimate = nucleus - 1.25 * (size / 2);
    m_d_nucleus(&nucleus, estimate, period, 64);
    size = cabs(m_d_size(nucleus, period));
    printf("%.18e\n", creal(nucleus));
    fflush(stdout);
  }
  return 0;
}
