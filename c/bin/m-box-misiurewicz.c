// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>
#include "m-util.h"

static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage: %s precision center-re center-im radius maxpreperiod maxperiod\n"
    , progname
    );
}

extern int main(int argc, char **argv) {
  if (argc != 7) {
    usage(argv[0]);
    return 1;
  }
  bool native = true;
  int bits = 0;
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }
  if (native) {
    double cre = 0;
    double cim = 0;
    double radius = 0;
    int maxpreperiod = 0;
    int maxperiod = 0;
    if (! arg_double(argv[2], &cre)) { return 1; }
    if (! arg_double(argv[3], &cim)) { return 1; }
    if (! arg_double(argv[4], &radius)) { return 1; }
    if (! arg_int(argv[5], &maxpreperiod)) { return 1; }
    if (! arg_int(argv[6], &maxperiod)) { return 1; }
    int preperiod = -1;
    int period = 0;
    bool ok = m_d_box_misiurewicz_do(&preperiod, &period, cre + I * cim, radius, maxpreperiod, maxperiod);
    if (ok) {
      printf("%d %d\n", preperiod, period);
    }
    return ok ? 0 : 1;
  } else {
    mpc_t center;
    mpfr_t radius;
    int maxpreperiod = 0;
    int maxperiod = 0;
    mpc_init2(center, bits);
    mpfr_init2(radius, bits);
    if (! arg_mpc(argv[2], argv[3], center)) { return 1; }
    if (! arg_mpfr(argv[4], radius)) { return 1; }
    if (! arg_int(argv[5], &maxpreperiod)) { return 1; }
    if (! arg_int(argv[6], &maxperiod)) { return 1; }
    int preperiod = -1;
    int period = 0;
    bool ok = m_r_box_misiurewicz_do(&preperiod, &period, center, radius, maxpreperiod, maxperiod);
    if (ok) {
      printf("%d %d\n", preperiod, period);
    }
    mpc_clear(center);
    mpfr_clear(radius);
    return ok ? 0 : 1;
  }
  return 1;
}
