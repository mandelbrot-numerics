// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>
#include "m-util.h"

static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage: %s precision re im period\n"
    , progname
    );
}

extern int main(int argc, char **argv) {
  if (argc != 5) {
    usage(argv[0]);
    return 1;
  }
  bool native = true;
  int bits = 0;
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }
  if (native) {
    double cre = 0;
    double cim = 0;
    int period = 0;
    int verbose = 0;
    if (! arg_double(argv[2], &cre)) { return 1; }
    if (! arg_double(argv[3], &cim)) { return 1; }
    if (! arg_int(argv[4], &period)) { return 1; }
    double _Complex cs[4], c0 = cre + I * cim;
    int k = 0;
    for (int bifurcate = 0; bifurcate < 2; ++bifurcate)
    {
    for (int sign = -1; sign <= 1; sign += 2)
    {
    m_d_nucleus_homotopy *path = m_d_nucleus_homotopy_new(c0, 2 * period, sign);
    if (! path)
    {
      return 1;
    }
    while (m_stepped == m_d_nucleus_homotopy_step(path))
    {
      if (verbose)
      {
        double _Complex c = m_d_nucleus_homotopy_get(path);
        printf("%.16e %.16e\n", creal(c), cimag(c));
      }
    }
    double _Complex c = m_d_nucleus_homotopy_get(path);
    m_d_nucleus_homotopy_delete(path);
    printf("%.16e %.16e\n", creal(c), cimag(c));
    cs[k++] = c;
    }
    if (bifurcate == 0)
    {
      double _Complex z = 0, c = c0;
      m_d_interior(&z, &c, z, c, 1.25, period, 64);
      m_d_nucleus(&c, c, 2 * period, 64);
      c0 = c;
    }
    }
    return 0;
  } else {
    fprintf(stderr, "not yet implemented, sorry\n");
    return 1;
  }
  return 1;
}
