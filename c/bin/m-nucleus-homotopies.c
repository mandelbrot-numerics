// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>
#include "m-util.h"

double cnorm(double _Complex z)
{
  double x = creal(z);
  double y = cimag(z);
  return x * x + y * y;
}

double cross(double _Complex u, double _Complex v)
{
  double ux = creal(u);
  double uy = cimag(u);
  double vx = creal(v);
  double vy = cimag(v);
  return ux * vy - uy * vx;
}

static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage: %s precision re im period lo hi\n"
    , progname
    );
}

extern int main(int argc, char **argv) {
  if (argc != 7) {
    usage(argv[0]);
    return 1;
  }
  bool native = true;
  int bits = 0;
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }
  if (native) {
    double cre = 0;
    double cim = 0;
    int period = 0;
    mpq_t lo, hi;
    mpq_init(lo);
    mpq_init(hi);
    if (! arg_double(argv[2], &cre)) { return 1; }
    if (! arg_double(argv[3], &cim)) { return 1; }
    if (! arg_int(argv[4], &period)) { return 1; }
    if (! arg_rational(argv[5], lo)) { return 1; }
    if (! arg_rational(argv[6], hi)) { return 1; }
    double _Complex cs[4], c0 = cre + I * cim;
    int k = 0;
    for (int bifurcate = 0; bifurcate < 2; ++bifurcate)
    {
      for (int sign = -1; sign <= 1; sign += 2)
      {
        m_d_nucleus_homotopy *path = m_d_nucleus_homotopy_new(c0, 2 * period, sign);
        if (! path)
        {
          return 1;
        }
        while (m_stepped == m_d_nucleus_homotopy_step(path))
          ;
        cs[k++] = m_d_nucleus_homotopy_get(path);
        m_d_nucleus_homotopy_delete(path);
      }
      if (bifurcate == 0)
      {
        double _Complex z = 0, c = c0;
        m_d_interior(&z, &c, z, c, -1.25, period, 64);
        m_d_nucleus(&c, c, 2 * period, 64);
        c0 = c;
      }
    }
    const int sharpness = 8;
    j_d_exray_in *rays[4][2];
    for (int r = 0; r < 4; ++r)
    {
      rays[r][0] = j_d_exray_in_new(cs[r], lo, sharpness);
      rays[r][1] = j_d_exray_in_new(cs[r], hi, sharpness);
    }
    mpq_clear(lo);
    mpq_clear(hi);
    int p = 0;
    for (int pass = 1, passn = 0; pass <= 1 << 30; pass <<= 1, passn++)
    {
      for (; p < pass; ++p)
      {
        for (int r = 0; r < 4; ++r)
        {
          for (int w = 0; w < 2; ++w)
          {
            m_newton n = j_d_exray_in_step(rays[r][w], 64);
            if (n != m_stepped)
            {
              return 1;
            }
          }
        }
      }
      double d[4];
      double maxd = -1.0 / 0.0;
      for (int r = 0; r < 4; ++r)
      {
        d[r] = cnorm(j_d_exray_in_get(rays[r][0]) - j_d_exray_in_get(rays[r][1]));
        if (d[r] > maxd)
        {
          maxd = d[r];
        }
      }
      int smallcount = 0;
      int largecount = 0;
      const double threshold = 0.1;
      for (int r = 0; r < 4; ++r)
      {
        if (d[r] == maxd) largecount++;
        else if (d[r] < threshold * maxd) smallcount++;
      }
      if (largecount == 1 && smallcount + largecount == 4)
      {
        int order[4];
        for (int r = 0; r < 4; ++r)
        {
          if (d[r] == maxd)
          {
            order[0] = r;
            break;
          }
        }
        for (int o = 1; o < 4; ++o)
        {
          double m = -1.0 / 0.0;
          for (int r = 0; r < 4; ++r)
          {
            double x = cross(cs[order[o - 1]] - c0, cs[r] - c0);
            if (x > m)
            {
              order[o] = r;
              m = x;
            }
          }
        }
        printf("%.18f %.18f ---\n", creal(cs[order[0]]), cimag(cs[order[0]]));
        printf("%.18f %.18f 1/3\n", creal(cs[order[1]]), cimag(cs[order[1]]));
        printf("%.18f %.18f 1/2\n", creal(cs[order[2]]), cimag(cs[order[2]]));
        printf("%.18f %.18f 2/3\n", creal(cs[order[3]]), cimag(cs[order[3]]));
        return 0;
      }
    }
    return 1;
  } else {
    fprintf(stderr, "not yet implemented, sorry\n");
    return 1;
  }
  return 1;
}
