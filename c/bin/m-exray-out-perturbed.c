// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2019 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>
#include "m-util.h"

static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage: %s precision nucleus-re nucleus-im nucleus-preperiod nucleus-period delta-re delta-im sharpness maxdwell preperiod period\n"
    , progname
    );
}

extern int main(int argc, char **argv) {
  if (argc != 12) {
    usage(argv[0]);
    return 1;
  }
  bool native = true;
  int bits = 0;
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }
  if (native) {
    fprintf(stderr, "no native\n");
    return 1;
  } else {
    mpc_t nucleus, endpoint;
    int verbose = 0;
    int sharpness = 0;
    int maxdwell = 0;
    int preperiod = 0;
    int period = 0;
    int nperiod = 0;
    int npreperiod = 0;
    mpc_init2(nucleus, bits);
    mpc_init2(endpoint, bits);
    if (! arg_mpc(argv[2], argv[3], nucleus)) { return 1; }
    if (! arg_int(argv[4], &npreperiod)) { return 1; }
    if (! arg_int(argv[5], &nperiod)) { return 1; }
    if (! arg_mpc(argv[6], argv[7], endpoint)) { return 1; }
    if (! arg_int(argv[8], &sharpness)) { return 1; }
    if (! arg_int(argv[9], &maxdwell)) { return 1; }
    if (! arg_int(argv[10], &preperiod)) { return 1; }
    if (! arg_int(argv[11], &period)) { return 1; }
    if (! (npreperiod >= 0)) { return 1; }
    if (! (nperiod > 0)) { return 1; }
    if (! (preperiod >= 0)) { return 1; }
    if (! (period >= 0)) { return 1; }
    if (preperiod > 0 && ! (period > 0)) { return 1; }
    m_r_orbit_ld *orbit = m_r_orbit_ld_new(nucleus, npreperiod, nperiod);
    long double _Complex dc = mpc_get_ldc(endpoint, MPC_RNDNN);
    m_ld_exray_out_perturbed *ray = m_ld_exray_out_perturbed_new(orbit, dc, sharpness, maxdwell, 65536);
    if (! ray) { return 1; }
    char *sbits = malloc(maxdwell + 2);
    if (! sbits) { m_ld_exray_out_perturbed_delete(ray); return 1; }
    int n = 0;
    do {
      long double _Complex c = m_ld_exray_out_perturbed_get(ray);
      if (verbose) printf("%.18Le %.18Le", creall(c), cimagl(c));
      if (m_ld_exray_out_perturbed_have_bit(ray)) {
        int bit = m_ld_exray_out_perturbed_get_bit(ray);
        sbits[n++] = '0' + bit;
        if (verbose) printf(" %d\n", bit);
      } else {
        if (verbose) printf("\n");
      }
    } while (m_stepped == m_ld_exray_out_perturbed_step(ray, 64));
    sbits[n] = 0;
    m_ld_exray_out_perturbed_delete(ray);
    m_r_orbit_ld_delete(orbit);
    if (preperiod + period > 0) {
      printf(".");
      for (int i = 0; i < preperiod && 0 <= n - 1 - i; ++i) {
        putchar(sbits[n - 1 - i]);
      }
      putchar('(');
      for (int i = preperiod; i < preperiod + period && 0 <= n - 1 - i; ++i) {
        putchar(sbits[n - 1 - i]);
      }
      putchar(')');
      putchar('\n');
    }
    else
    {
      for (int i = 0; i < n; ++i)
      {
        putchar(sbits[n - 1 - i]);
      }
      putchar('\n');
    }
    free(sbits);
    return 0;
  }
  return 1;
}
