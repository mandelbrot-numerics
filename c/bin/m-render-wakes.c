// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2020 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <mandelbrot-numerics.h>
#include "m-util.h"


struct m_d_jexray_in {
  double _Complex c;
  mpq_t angle;
  mpq_t one;
  int sharpness;
  double er;
  double _Complex z;
  int j;
  int k;
};

typedef struct m_d_jexray_in m_d_jexray_in;

extern m_d_jexray_in *m_d_jexray_in_new(const double _Complex c, const mpq_t angle, int sharpness) {
  m_d_jexray_in *ray = malloc(sizeof(*ray));
  ray->c = c;
  mpq_init(ray->angle);
  mpq_set(ray->angle, angle);
  mpq_init(ray->one);
  mpq_set_ui(ray->one, 1, 1);
  ray->sharpness = sharpness;
  ray->er = 65536.0;
  double a = twopi * mpq_get_d(ray->angle);
  ray->z = ray->er * (cos(a) + I * sin(a));
  ray->k = 0;
  ray->j = 0;
  return ray;
}

extern void m_d_jexray_in_delete(m_d_jexray_in *ray) {
  if (! ray) {
    return;
  }
  mpq_clear(ray->angle);
  mpq_clear(ray->one);
  free(ray);
}

extern m_newton m_d_jexray_in_step(m_d_jexray_in *ray, int maxsteps) {
  double epsilon = nextafter(1, 1.0/0.0) - 1;
  if (ray->j >= ray->sharpness) {
    mpq_mul_2exp(ray->angle, ray->angle, 1);
    if (mpq_cmp_ui(ray->angle, 1, 1) >= 0) {
      mpq_sub(ray->angle, ray->angle, ray->one);
    }
    ray->k = ray->k + 1;
    ray->j = 0;
  }
  double r = pow(ray->er, pow(0.5, (ray->j + 0.5) / ray->sharpness));
  double a = twopi * mpq_get_d(ray->angle);
  double _Complex target = r * (cos(a) + I * sin(a));
  double _Complex c = ray->c;
  double _Complex z0 = ray->z;
  for (int i = 0; i < maxsteps; ++i) {
    double _Complex z = z0;
    double _Complex dz = 1;
    for (int p = 0; p < ray->k; ++p) {
      dz = 2 * z * dz;
      z = z * z + c;
    }
    double _Complex z_new = z0 - (z - target) / dz;
    double d = cabs(z_new - z0);
    if (cabs(z_new) < 1.0 / 0.0) {
      z0 = z_new;
    } else {
      break;
    }
    if (d <= epsilon) {
      break;
    }
  }
  ray->j = ray->j + 1;
  double d = cabs(z0 - ray->z);
  if (d <= epsilon) {
    ray->z = z0;
    return m_converged;
  }
  if (cabs(z0) < 1.0 / 0.0) {
    ray->z = z0;
    return m_stepped;
  } else {
    return m_failed;
  }
}

extern double _Complex m_d_jexray_in_get(const m_d_jexray_in *ray) {
  if (! ray) {
    return 0;
  }
  return ray->z;
}

extern double _Complex m_d_jexray_in_do(const double _Complex c, const mpq_t angle, int sharpness, int maxsteps, int maxnewtonsteps) {
  m_d_jexray_in *ray = m_d_jexray_in_new(c, angle, sharpness);
  if (! ray) {
    return 0;
  }
  for (int i = 0; i < maxsteps; ++i) {
    if (m_stepped != m_d_jexray_in_step(ray, maxnewtonsteps)) {
      break;
    }
  }
  double _Complex endpoint = m_d_jexray_in_get(ray);
  m_d_jexray_in_delete(ray);
  return endpoint;
}


int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  const int width = 512;
  const int height = 512;
  unsigned char *ppm = malloc(width * height * 3);
  const int ray_sharpness = 4;
  const int ray_steps = 256 * ray_sharpness;
  const int newton_steps = 64;
  const int maximum_iterations = 1024;
  const double escape_radius = 1 << 16;
  mpq_t s[6];
  for (int i = 0; i < 6; ++i)
  {
    mpq_init(s[i]);
    mpq_set_ui(s[i], i + 1, 7);
  }
  #pragma omp parallel for
  for (int j = 0; j < height; ++j)
  {
    for (int i = 0; i < width; ++i)
    {
      const double _Complex c = (((i + 0.5) / width - 0.5) * width / height * 2 * 2 - I * ((j + 0.5) / height - 0.5) * 2 * 2) + (-0.75 + I * 0);
      double de = 0;
      double _Complex z = 0;
      double _Complex dc = 0;
      for (int k = 0; k < maximum_iterations; ++k)
      {
        dc = 2 * z * dc + (2.0 * 2.0 / height);
        z = z * z + c;
        if (cabs(z) > escape_radius)
        {
          de = 255 * (1 - tanh(2.0 * cabs(z) * log(cabs(z)) / cabs(dc)));
          break;
        }
      }
      for (int k = 0; k < 3; ++k)
      {
        const double _Complex z0 = m_d_jexray_in_do(c, s[2 * k + 0], ray_sharpness, ray_steps, newton_steps);
        const double _Complex z1 = m_d_jexray_in_do(c, s[2 * k + 1], ray_sharpness, ray_steps, newton_steps);
        const double d = fmin(255, de + 1 / cabs(z0 - z1));
        ppm[(j * width + i) * 3 + k] = d;
      }
    }
  }
  fprintf(stdout, "P6\n%d %d\n255\n", width, height);
  fwrite(ppm, width * height * 3, 1, stdout);
  fflush(stdout);
  free(ppm);
  for (int i = 0; i < 6; ++i)
  {
    mpq_clear(s[i]);
  }
  return 0;
}
