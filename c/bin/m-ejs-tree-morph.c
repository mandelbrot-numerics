// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>
#include <mandelbrot-symbolics.h>

#define EXAMPLE_3

#define NCPUS 8

int main()
{

#ifdef EXAMPLE_1
  // embedded Julia set is 1_1/2->2_1/2->3_2/5->15_4/7->88
  // https://mathr.co.uk/mandelbrot/web/#!q=1_1/2-%3E2_1/2-%3E3_2/5-%3E15_4/7-%3E88
  // the ray goes to the tip of the first morphing in prepartion for the next
  const char *embedded_julia_ray = ".011100011100011011100011011100011100011100011011100011011100011100011100011011100011100001110001101110010010010010010010010001110001110001101110001101110001110001110001101110001101110001110001110001101110001110000111000110111(001)";
  int ray_preperiod = 225;
  int ray_period = 3;
  double _Complex ray_endpoint
    = -1.76525599938987623396492597243303e+00
    +  1.04485517375987067290733632798876e-02 * I;
  int influencing_island_period = 3;
  int embedded_julia_set_period = 88;
  int denominator_of_rotation = 5;
  // tree morphing
  int arm_length = 7;
  // how to scale the view
  double view_size_multiplier = 3600;
#endif

#ifdef EXAMPLE_2
  // embedded Julia set is 1_1/2->2_1/2->3_1/4->4_1/2->8_1/15->116_1/2->119
  // https://mathr.co.uk/mandelbrot/web/#!q=1_1/2-%3E2_1/2-%3E3_1/2-%3E4_1/2-%3E8_1/15-%3E116_1/2-%3E119
  // the ray goes to the tip of the first morphing in prepartion for the next
  const char *embedded_julia_ray = ".01111000011110000111100001111000011110000111100001111000011110000111100001111000011110000111100001111000100001110111011011110001000100010001000100010001000100010000111100001111000011110000111100001111000011110000111100001111000011110000111100001111000011110000111100010000111011101110000(1110)";
  int ray_preperiod = 287;
  int ray_period = 4;
  double _Complex ray_endpoint
    = -1.94155104121873375917685284273942e+00
    +  1.08484266852462027458038858364597e-04 * I;
  int influencing_island_period = 4;
  int embedded_julia_set_period = 119;
  int denominator_of_rotation = 2;
  // tree morphing
  int arm_length = 9;
  // how to scale the view
  double view_size_multiplier = 32768;
#endif

#ifdef EXAMPLE_3
  // embedded Julia set is 1_1/3->3_1/2->4_11/23->89
  // https://mathr.co.uk/mandelbrot/web/#!q=1_1/3-%3E3_1/2-%3E4_11/23-%3E89
  // the ray goes to the tip of the first morphing in prepartion for the next
  const char *embedded_julia_ray = ".001101000011010000110100001101000011010000110100001101000011010000110100001101000011010000100001100110011010000110100001101000011010000110100001101000011010000110100001101000011010000110100001000(0110)";
  int ray_preperiod = 195;
  int ray_period = 4;
  double _Complex ray_endpoint
    = -1.60671574317626674138601221936210e-01
    +  1.03694257152534946094999286804216e+00 * I;
  int influencing_island_period = 4;
  int embedded_julia_set_period = 89;
  int denominator_of_rotation = 2;
  // tree morphing
  int arm_length = 1;
  // how to scale the view
  double view_size_multiplier = 8;
#endif

  // how many times to iterate
  int number_of_morphs = 10;

  // ray tracing sharpness
  int ray_sharpness = 8;

  // maximum number of newton steps
  int newton_steps = 64;

  // current and previous periods of the nucleus
  int nperiod = 1;
  int nperiod_old = 1;
  int precision = 53 + 1.5 * nperiod; // number of bits
  mpc_t nucleus;
  mpc_init2(nucleus, precision);

  // current misiurewicz point
  int mpreperiod = 0;
  int mperiod = influencing_island_period;
  mpc_t misiurewicz;
  mpc_init2(misiurewicz, precision);

  // current guess for Misiurewicz point
  mpc_t guess;
  mpc_init2(guess, precision);

  // variables for atom domain coordinates and view size don't need to be so precise
  mpc_t atom_coord;
  mpfr_t atom_coord_size;
  mpc_init2(atom_coord, 53);
  mpfr_init2(atom_coord_size, 53);
  mpfr_set_si(atom_coord_size, 1, MPFR_RNDN);
  mpc_t view_delta;
  mpfr_t view_size;
  mpc_init2(view_delta, 53);
  mpfr_init2(view_size, 53);
  mpfr_set_si(view_size, 1, MPFR_RNDN);

  if (cabs(ray_endpoint) < 0.1)
  {
    // trace ray towards tip of first level morphed embedded Julia nucleus
    m_binangle binangle;
    m_binangle_init(&binangle);
    m_binangle_from_string(&binangle, embedded_julia_ray);
    mpreperiod = binangle.pre.length;
    mperiod = binangle.per.length;
    mpq_t angle;
    mpq_init(angle);
    m_binangle_to_rational(angle, &binangle);
    m_binangle_clear(&binangle);
    m_r_exray_in *ray = m_r_exray_in_new(angle, ray_sharpness);
    mpq_clear(angle);
    for (int i = 0; i < ray_sharpness * (mpreperiod + mperiod) * 3; ++i) // FIXME check if 3x is always far enough!
    {
      m_r_exray_in_step(ray, newton_steps);
    }
    m_r_exray_in_get(ray, guess); // updates precision too
    m_r_exray_in_delete(ray);
    precision = mpfr_get_prec(mpc_realref(guess));
    mpfr_printf("ray endpoint %dp%d %d %Re %Re\n", mpreperiod, mperiod, precision, mpc_realref(guess), mpc_imagref(guess));
  }
  else
  {
    // have the ray end point already, saves computation time
    mpreperiod = ray_preperiod;
    mperiod = ray_period;
    precision = 100; // FIXME don't hardcode this
    mpfr_set_prec(mpc_realref(guess), precision);
    mpfr_set_prec(mpc_imagref(guess), precision);
    mpc_set_dc(guess, ray_endpoint, MPC_RNDNN);
  }

  // Misiurewicz to tip
  precision = mpfr_get_prec(mpc_realref(guess));
  mpfr_set_prec(mpc_realref(misiurewicz), precision);
  mpfr_set_prec(mpc_imagref(misiurewicz), precision);
  m_r_misiurewicz(misiurewicz, guess, mpreperiod, mperiod, newton_steps);


  // Newton to center
  nperiod_old = embedded_julia_set_period;
  nperiod = embedded_julia_set_period + (arm_length + denominator_of_rotation) * influencing_island_period;
  precision = mpfr_get_prec(mpc_realref(guess));
  mpfr_set_prec(mpc_realref(nucleus), precision);
  mpfr_set_prec(mpc_imagref(nucleus), precision);
  m_r_nucleus(nucleus, misiurewicz, nperiod, newton_steps, NCPUS);

  // update period to new target
  nperiod_old = nperiod;
  nperiod = 2 * nperiod + arm_length * influencing_island_period;

  for (int morph = 2; morph <= number_of_morphs + 1; ++morph)
  {
    // invariant: at loop entry:
    //   nucleus points to a periodic nucleus at the center of a morphing
    //   guess points to the inital guess for misiurewicz finding
    //   misiurewicz points to the tree-wards tip of the morphing

    // compute view and output
    mpc_sub(view_delta, misiurewicz, nucleus, MPC_RNDNN);
    mpc_abs(view_size, view_delta, MPFR_RNDN);
    mpfr_mul_d(view_size, view_size, view_size_multiplier, MPFR_RNDN);
    mpfr_printf
      ( "size 1280 720\n"
        "view %d %Re %Re %Re\n"
        "text %d %Re %Re %d\n"
        "text %d %Re %Re %dp%d\n"
        "text %d %Re %Re GUESS\n"
        "\n"
      , precision, mpc_realref(nucleus), mpc_imagref(nucleus), view_size
      , precision, mpc_realref(nucleus), mpc_imagref(nucleus), nperiod_old
      , precision, mpc_realref(misiurewicz), mpc_imagref(misiurewicz), mpreperiod, mperiod
      , precision, mpc_realref(guess), mpc_imagref(guess)
      );
    fflush(stdout);
    if (morph == number_of_morphs + 1)
    {
      break;
    }

    // calculate atom domain coord of tip
    mpc_t z, zp, zq;
    mpfr_t mz2, z2;
    mpc_init2(z, precision);
    mpc_init2(zp, precision);
    mpc_init2(zq, precision);
    mpfr_init2(mz2, precision);
    mpfr_init2(z2, precision);
    mpc_set_si(z, 0, MPC_RNDNN);
    mpfr_set_d(mz2, 1.0 / 0.0, MPFR_RNDN);
    for (int i = 1; i <= nperiod_old; ++i)
    {
      mpc_sqr(z, z, MPC_RNDNN);
      mpc_add(z, z, misiurewicz, MPC_RNDNN);
      mpc_norm(z2, z, MPFR_RNDN);
      if (mpfr_less_p(z2, mz2))
      {
        mpfr_set(mz2, z2, MPFR_RNDN);
        mpc_set(zq, zp, MPC_RNDNN);
        mpc_set(zp, z, MPC_RNDNN);
      }
    }
    mpc_div(atom_coord, zp, zq, MPC_RNDNN);
    mpc_clear(z);
    mpc_clear(zp);

    // scale atom coord by **1.5 FIXME make this a settable variable?
    mpc_abs(atom_coord_size, atom_coord, MPC_RNDNN);
    mpc_div_fr(atom_coord, atom_coord, atom_coord_size, MPC_RNDNN);
    mpfr_sqrt(atom_coord_size, atom_coord_size, MPFR_RNDN);
    mpfr_pow_si(atom_coord_size, atom_coord_size, 3, MPFR_RNDN);
    mpc_mul_fr(atom_coord, atom_coord, atom_coord_size, MPC_RNDNN);

#ifdef EXAMPLE_3
    // FIXME HACK for example 3 - the guess is too off rotationally otherwise
    mpfr_swap(mpc_realref(atom_coord), mpc_imagref(atom_coord));
#endif

    // nucleus is in arm near misiurewicz tip
    m_r_nucleus(nucleus, misiurewicz, nperiod, newton_steps, NCPUS);

    // guess is atom coord relative to new nucleus
    m_r_domain_coord(guess, nucleus, atom_coord, nperiod_old, nperiod, newton_steps);

    // update period to new target
    nperiod_old = nperiod;
    nperiod = 2 * nperiod + arm_length * influencing_island_period;

    // set precision
    precision = 53 - 3 * mpfr_get_exp(view_size);
    mpfr_prec_round(mpc_realref(nucleus), precision, MPFR_RNDN);
    mpfr_prec_round(mpc_imagref(nucleus), precision, MPFR_RNDN);
    mpfr_prec_round(mpc_realref(guess), precision, MPFR_RNDN);
    mpfr_prec_round(mpc_imagref(guess), precision, MPFR_RNDN);
    mpfr_prec_round(mpc_realref(misiurewicz), precision, MPFR_RNDN);
    mpfr_prec_round(mpc_imagref(misiurewicz), precision, MPFR_RNDN);

    // misiurewicz is guess refined to new tip
    int d = influencing_island_period - 1 + arm_length * influencing_island_period * (morph + 1);
    mpreperiod = nperiod - d;
    mperiod = influencing_island_period;
    m_r_misiurewicz(misiurewicz, guess, mpreperiod, mperiod, newton_steps);

  }

  return 0;
}
