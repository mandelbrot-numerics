// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2021 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>
#include "m-util.h"

static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage:\n%s precision center-re center-im radius maxperiod\n"
      "%s precision period radius maxperiod <<EOF\n"
      "center-re\ncenter-im\nEOF\n"
    , progname, progname
    );
}

#define height 2160
#define width 3840
static unsigned char ppm[height][width][3];

static void plot(unsigned char *p, int period)
{
  int desat = period < 0;
  if (desat)
  {
    period = -period;
  }
  int r = 0, g = 0, b = 0;
  // deinterleaved bit reversal
  // 7
  r |= (period & (1 <<  0)) <<  7;
  g |= (period & (1 <<  1)) <<  6;
  b |= (period & (1 <<  2)) <<  5;
  // 6
  r |= (period & (1 <<  3)) <<  3;
  g |= (period & (1 <<  4)) <<  2;
  b |= (period & (1 <<  5)) <<  1;
  // 5
  r |= (period & (1 <<  6)) >>  1;
  g |= (period & (1 <<  7)) >>  2;
  b |= (period & (1 <<  8)) >>  3;
  // 4
  r |= (period & (1 <<  9)) >>  5;
  g |= (period & (1 << 10)) >>  6;
  b |= (period & (1 << 11)) >>  7;
  // 3
  r |= (period & (1 << 12)) >>  9;
  g |= (period & (1 << 13)) >> 10;
  b |= (period & (1 << 14)) >> 11;
  // 2
  r |= (period & (1 << 15)) >> 13;
  g |= (period & (1 << 16)) >> 14;
  b |= (period & (1 << 17)) >> 15;
  // 1
  r |= (period & (1 << 18)) >> 17;
  g |= (period & (1 << 19)) >> 18;
  b |= (period & (1 << 20)) >> 19;
  // 0
  r |= (period & (1 << 21)) >> 21;
  g |= (period & (1 << 22)) >> 22;
  b |= (period & (1 << 23)) >> 23;
  if (desat)
  {
    r += 256;
    g += 256;
    b += 256;
    r >>= 1;
    g >>= 1;
    b >>= 1;
  }
  p[0] = r;
  p[1] = g;
  p[2] = b;
}

extern int main(int argc, char **argv) {
  if (argc != 6 && argc != 4) {
    usage(argv[0]);
    return 1;
  }
  bool native = true;
  int bits = 0;
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }
  if (native)
  {
    double cre = 0;
    double cim = 0;
    double radius = 0;
    int max_period = 0;
    if (argc == 4)
    {
      if (! stdin_double(&cre)) { return 1; }
      if (! stdin_double(&cim)) { return 1; }
      if (! arg_double(argv[2], &radius)) { return 1; }
      if (! arg_int(argv[3], &max_period)) { return 1; }
    }
    else
    {
      if (! arg_double(argv[2], &cre)) { return 1; }
      if (! arg_double(argv[3], &cim)) { return 1; }
      if (! arg_double(argv[4], &radius)) { return 1; }
      if (! arg_int(argv[5], &max_period)) { return 1; }
    }
    #pragma omp parallel for schedule(dynamic, 1)
    for (int y = 0; y < height; ++y)
    {
      for (int x = 0; x < width; ++x)
      {
        double _Complex c = (cim + ((y + 0.5) / height - 0.5) * 2 * radius) * I
                          + (cre + ((x + 0.5) / width - 0.5) * width / height * 2 * radius);
        double _Complex z = 0;
        double _Complex dc = 0;
        double mz = 1.0 / 0.0;
        int atom_period = 1;
        double de = 0.0;
        for (int k = 1; k < max_period; ++k)
        {
          dc = 2 * z * dc + (radius * 2.0 / height);
          z = z * z + c;
          if (cabs(z) > 65536)
          {
            de = cabs(z) * log(cabs(z)) / cabs(dc);
            break;
          }
          if (cabs(z) < mz)
          {
            mz = cabs(z);
            atom_period = k;
          }
        }
        double _Complex nucleus = c;
        m_d_nucleus(&nucleus, nucleus, atom_period, 64);
        double domain_size = m_d_domain_size(nucleus, atom_period);
        if (atom_period == 1)
        {
          domain_size = 65536;
        }
        bool ok = false;
        plot(&ppm[y][x][0], atom_period);
        for (int n = 0; n < 9; ++n)
        {
          double r0 = 0.5 * cabs(c - nucleus);
          if (r0 > 0)
          {
            for (int k = 0; k < 64; ++k)
            {
              double r = exp(log(r0) + (k + 0.5) / 64 * (log(2 * domain_size) - log(r0)));
              m_d_ball_period *ball = m_d_ball_period_new(c, r);
              while (atom_period != m_d_ball_period_get_period(ball))
              {
                m_d_ball_period_step(ball);
              }
              ok = m_d_ball_period_have_period(ball) && m_d_ball_period_will_converge(ball);
              m_d_ball_period_delete(ball);
              if (ok)
              {
                break;
              }
            }
            if (ok)
            {
              break;
            }
            ppm[y][x][0] >>= 1;
            ppm[y][x][1] >>= 1;
            ppm[y][x][2] >>= 1;
            ppm[y][x][0] += 128;
            ppm[y][x][1] += 128;
            ppm[y][x][2] += 128;
            m_d_nucleus_step(&c, c, atom_period);
          }
        }
        if (de < 0.25)
        {
          ppm[y][x][0] = 0;
          ppm[y][x][1] = 0;
          ppm[y][x][2] = 0;
        }
      }
    }
    printf("P6\n%d %d\n255\n", width, height);
    fwrite(&ppm[0][0][0], width * height * 3, 1, stdout);
    return 0;
  }
  else
  {
    fprintf(stderr, "error: arbitrary precision not supported\n");
    return 1;
  }
  return 1;
}
