// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

// for strdup
#define _POSIX_C_SOURCE 200809L

#include <mandelbrot-numerics.h>
#include <mandelbrot-symbolics.h>

#include <stdio.h>
#include <string.h>

typedef struct
{
  int p;
  char *lo;
  char *hi;
} ray;

void initial(ray *r)
{
  r[0].p = 3;
  r[0].lo = strdup("011");
  r[0].hi = strdup("100");
#if 0
  r[1].p = 38;
  r[1].lo = strdup("01110001110001110001101110011100100100");
  r[1].hi = strdup("01110001110001110001101110100011011011");
#else
  r[1].p = 342;
  r[1].lo = strdup("100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100011100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100011100100100100");
  r[1].hi = strdup("100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100011100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011100100011011011011");
#endif
}

void step1(ray *r, int which, ray *seed, int n)
{
  r[0].p = 2 * r[-1].p + n * seed->p;
  r[0].lo = calloc(1, r[0].p + 1);
  r[0].hi = calloc(1, r[0].p + 1);
  if (which)
  {
    strncat(r[0].lo, r[-1].hi, r[0].p);
    strncat(r[0].hi, r[-1].hi, r[0].p);
    strncat(r[0].lo, r[-1].lo, r[0].p);
    strncat(r[0].hi, r[-1].hi, r[0].p);
  }
  else
  {
    strncat(r[0].lo, r[-1].lo, r[0].p);
    strncat(r[0].hi, r[-1].lo, r[0].p);
    strncat(r[0].lo, r[-1].hi, r[0].p);
    strncat(r[0].hi, r[-1].lo, r[0].p);
  }
  for (int i = 0; i < n; ++i)
  {
    strncat(r[0].lo, seed->hi, r[0].p);
    strncat(r[0].hi, seed->lo, r[0].p);
  }
}

void step2(ray *r, int which, ray *seed, int n)
{
  r[0].p = r[-1].p + r[-2].p + n * seed->p;
  r[0].lo = calloc(1, r[0].p + 1);
  r[0].hi = calloc(1, r[0].p + 1);
  if (which)
  {
    strncat(r[0].lo, r[-1].hi, r[0].p);
    strncat(r[0].hi, r[-1].hi, r[0].p);
    strncat(r[0].lo, r[-2].lo, r[0].p);
    strncat(r[0].hi, r[-2].hi, r[0].p);
  }
  else
  {
    strncat(r[0].lo, r[-1].lo, r[0].p);
    strncat(r[0].hi, r[-1].lo, r[0].p);
    strncat(r[0].lo, r[-2].hi, r[0].p);
    strncat(r[0].hi, r[-2].lo, r[0].p);
  }
  for (int i = 0; i < n; ++i)
  {
    strncat(r[0].lo, seed->hi, r[0].p);
    strncat(r[0].hi, seed->lo, r[0].p);
  }
}

void penultimate(ray *r, int which)
{
  r[0].p = r[-1].p + r[-2].p;
  r[0].lo = calloc(1, r[0].p + 1);
  r[0].hi = calloc(1, r[0].p + 1);
  strncat(r[0].lo, r[-1].lo, r[0].p);
  strncat(r[0].hi, r[-1].hi, r[0].p);
  if (which)
  {
    strncat(r[0].lo, r[-2].hi, r[0].p);
    strncat(r[0].hi, r[-2].lo, r[0].p);
  }
  else
  {
    strncat(r[0].lo, r[-2].lo, r[0].p);
    strncat(r[0].hi, r[-2].hi, r[0].p);
  }
}

void final(ray *r, int which)
{
  r[0].p = r[-1].p + r[-2].p + r[-3].p;
  r[0].lo = calloc(1, r[0].p + 1);
  r[0].hi = calloc(1, r[0].p + 1);
  strncat(r[0].lo, r[-1].lo, r[0].p);
  strncat(r[0].hi, r[-1].hi, r[0].p);
  if (which)
  {
    strncat(r[0].lo, r[-2].hi, r[0].p);
    strncat(r[0].hi, r[-2].lo, r[0].p);
    strncat(r[0].lo, r[-3].hi, r[0].p);
    strncat(r[0].hi, r[-3].lo, r[0].p);
  }
  else
  {
    strncat(r[0].lo, r[-2].lo, r[0].p);
    strncat(r[0].hi, r[-2].hi, r[0].p);
    strncat(r[0].lo, r[-3].lo, r[0].p);
    strncat(r[0].hi, r[-3].hi, r[0].p);
  }
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
#define N 6
  const int which = 1;
  const int variation = 1;
  const int tree = 2;
  ray rays[1 + N + 2];
  initial(&rays[0]);
  int n = 1;
  step1(&rays[++n], which, &rays[0], tree);
  while (n < N)
  {
    (variation ? step2 : step1)(&rays[++n], which, &rays[0], tree);
  }
  penultimate(&rays[++n], which);
  final(&rays[++n], which);
  for (int i = 0; i <= n; ++i)
  {
    fprintf(stderr, " %d", rays[i].p);
  }
  fprintf(stderr, "\n");
  // initialize
  m_binangle b;
  m_binangle_init(&b);
  m_block_from_string(&b.pre, "");
  m_block_from_string(&b.per, rays[n].lo);
  const int final_period = b.per.length;
  mpq_t angle;
  mpq_init(angle);
  m_binangle_to_rational(angle, &b);
  m_binangle_clear(&b);
  int prec = 24;
  mpc_t nucleus, new_nucleus;
  mpc_init2(new_nucleus, 2 * prec + 24);
  mpc_init2(nucleus, 2 * prec + 24);
  mpc_set_dc(nucleus, -1, MPC_RNDNN);
  int nucleus_period = 2;
  m_r_orbit_d *orbit = m_r_orbit_d_new(nucleus, 0, nucleus_period);
  int m = 0;
  int target_period = rays[m].p;
  int sharpness = 8;
  m_d_exray_in_perturbed *ray = m_d_exray_in_perturbed_new(orbit, angle, sharpness);
  if (! ray)
  {
    return 1;
  }
  mpq_clear(angle);
  mpc_t endpoint;
  mpc_init2(endpoint, 53);
  int depth = 0;
  while (nucleus_period < final_period)
  {
    // trace ray
    while (depth < target_period * sharpness)
    {
      if (m_stepped != m_d_exray_in_perturbed_step(ray, 64))
      {
        fprintf(stderr, "%s: exray in failed at depth = %d\n", argv[0], depth);
        return 1;
      }
      depth++;
    }
    // compute new reference
    mpfr_set_prec(mpc_realref(new_nucleus), 2 * prec + 24);
    mpfr_set_prec(mpc_imagref(new_nucleus), 2 * prec + 24);
    mpc_set_dc(endpoint, m_d_exray_in_perturbed_get(ray), MPC_RNDNN);
    mpc_add(new_nucleus, nucleus, endpoint, MPC_RNDNN);
    if (m_converged != m_r_nucleus(new_nucleus, new_nucleus, target_period, 64, 1))
    {
      fprintf(stderr, "%s: nucleus failed at period = %d\n", argv[0], target_period);
      return 1;
    }
    // rebase ray to new reference
    mpc_sub(endpoint, new_nucleus, nucleus, MPC_RNDNN);
    double _Complex delta_c = mpc_get_dc(endpoint, MPC_RNDNN);
    mpc_swap(new_nucleus, nucleus);
    nucleus_period = target_period;
    if (nucleus_period < final_period)
    {
      m_r_orbit_d_delete(orbit);
      orbit = m_r_orbit_d_new(nucleus, 0, nucleus_period);
      m_d_exray_in_perturbed_rebase(ray, delta_c, orbit);
      // prepare for next stage
      target_period = rays[++m].p;
      prec = prec * 3 / 2;
    }
    // output
    mpfr_printf("%d %Re %Re\n", nucleus_period, mpc_realref(nucleus), mpc_imagref(nucleus));
    fflush(stdout);
  }
  // compute viewing parameters
  mpfr_t radius;
  mpfr_init2(radius, 53);
  m_r_domain_size(radius, nucleus, nucleus_period);
  mpfr_prec_round(radius, 53, MPFR_RNDN);
  mpfr_pow_ui(radius, radius, 9, MPFR_RNDN);
  mpfr_sqrt(radius, radius, MPFR_RNDN);
  mpfr_sqrt(radius, radius, MPFR_RNDN);
  mpfr_sqrt(radius, radius, MPFR_RNDN);
  mpfr_mul_d(radius, radius, 0.1, MPFR_RNDN);
  mpfr_set_prec(mpc_realref(new_nucleus), mpfr_get_prec(mpc_realref(nucleus)));
  mpfr_set_prec(mpc_imagref(new_nucleus), mpfr_get_prec(mpc_imagref(nucleus)));
  mpc_set(new_nucleus, nucleus, MPC_RNDNN);
  mpfr_add(mpc_realref(new_nucleus), mpc_realref(new_nucleus), radius, MPFR_RNDN);
  m_r_nucleus(new_nucleus, new_nucleus, rays[m].p + rays[m-1].p, 64, 1);
  mpc_sub(endpoint, new_nucleus, nucleus, MPC_RNDNN);
  mpc_abs(radius, endpoint, MPFR_RNDN);
  mpfr_mul_d(radius, radius, 2, MPFR_RNDN);
  mpfr_printf("radius = %Re\n", radius);
  mpc_arg(radius, endpoint, MPFR_RNDN);
  printf("degrees = %f\n", mpfr_get_d(radius, MPFR_RNDN) * 360 / (2 * 3.141592653589793));
  fflush(stdout);
  // cleanup
  m_d_exray_in_perturbed_delete(ray);
  mpc_clear(nucleus);
  mpc_clear(new_nucleus);
  mpc_clear(endpoint);
  mpfr_clear(radius);
  m_r_orbit_d_delete(orbit);
  return 0;
}
