#include <complex.h>
#include <math.h>
#include <stdio.h>

#include <mandelbrot-numerics.h>

#include "m-util.h"

static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage: %s angle... | ffmpeg -r 10 -i - output.gif\n"
    , progname
    );
}

extern int main(int argc, char **argv) {
  if (! (argc > 1)) {
    usage(argv[0]);
    return 1;
  }
  const int maxsteps = 64;
  const int sharpness = 64;
  mpq_t q;
  mpq_init(q);
  double dperiod = 1;
  for (int arg = 1; arg < argc; ++arg)
  {
    if (! arg_rational(argv[arg], q)) { mpq_clear(q); return 1; }
    if (! (0 < mpq_cmp_si(q, 0, 1) && mpq_cmp_si(q, 1, 1) < 1)) { mpq_clear(q); return 1; }
    dperiod *= mpz_get_d(mpq_denref(q));
  }
  if (dperiod > INT_MAX) { mpq_clear(q); return 1; }
  int height = 2 * sharpness * (argc - 1);
  int width  = height;
  unsigned char *ppm = malloc(width * height * 3);
  for (int angle = 0; angle < 360; angle += 5)
  {
    memset(ppm, 0, width * height * 3);
    int period = 1;
    double _Complex c = 0;
    double _Complex z = 0;
    double _Complex dc = 0;
    double _Complex dz = 0;
    double _Complex rotation = cexp(I * twopi * angle / 360.0);
    double _Complex left  = cexp(I * twopi *  2.0 / 360) * rotation;
    double _Complex right = cexp(I * twopi * -2.0 / 360) * rotation;
    int x = 0;
    for (int arg = 1; arg < argc; ++arg)
    {
      arg_rational(argv[arg], q);
      m_d_nucleus(&c, c, period, maxsteps);
      z = 0;
      for (int step = 0; step < sharpness; ++step)
      {
        int y = (2 * arg - 1) * sharpness - 1 - step;
        m_d_interior(&z, &c, z, c, (step + 0.5) / sharpness, period, maxsteps);
        double _Complex w = z;
        for (int att = 0; att < period; ++att)
        {
          x = cimag(right * w) / cabs(right * w - 10) * 4 * width + width / 2;
          if (x < 0) x = 0;
          if (x >= width) x = width - 1;
          ppm[3 * width * y + 3 * x + 0] = 255;
          x = cimag(left * w) / cabs(left * w - 10) * 4 * width + width / 2;
          if (x < 0) x = 0;
          if (x >= width) x = width - 1;
          ppm[3 * width * y + 3 * x + 1] = 255;
          ppm[3 * width * y + 3 * x + 2] = 255;
          w = w * w + c;
        }
      }
      double t = twopi * mpq_get_d(q);
      double _Complex v = cexp(I * t);
      m_d_nucleus(&c, c, period, maxsteps);
      z = 0;
      for (int step = 0; step < sharpness; ++step)
      {
        int y = (2 * arg - 1) * sharpness + step;
        double _Complex z1, c1;
        m_d_interior(&z1, &c1, z, c, (step + 0.5) / sharpness * v, period, maxsteps);
        dc = c1 - c;
        dz = z1 - z;
        c = c1;
        z = z1;
        double _Complex w = z;
        for (int att = 0; att < period; ++att)
        {
          x = cimag(right * w) / cabs(right * w - 10) * 4 * width + width / 2;
          if (x < 0) x = 0;
          if (x >= width) x = width - 1;
          ppm[3 * width * y + 3 * x + 0] = 255;
          x = cimag(left * w) / cabs(left * w - 10) * 4 * width + width / 2;
          if (x < 0) x = 0;
          if (x >= width) x = width - 1;
          ppm[3 * width * y + 3 * x + 1] = 255;
          ppm[3 * width * y + 3 * x + 2] = 255;
          w = w * w + c;
        }
        ++y;
      }
      int p = mpz_get_ui(mpq_denref(q));
      double f = 0.5 * (1 + (period == 1 ? sin(0.5 * p) : 1.0) / (p * p));
      c += dc * f;
      z += dz * f;
      period *= p;
    }
    printf("P6\n%d %d\n255\n", width, height);
    fwrite(ppm, width * height * 3, 1, stdout);
    fflush(stdout);
  }
  mpq_clear(q);
  free(ppm);
  return 0;
}
