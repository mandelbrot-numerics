#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <mandelbrot-numerics.h>

#define U (rand()/(double) RAND_MAX)
#define pi 3.141592653589793
#define twopi (2 * pi)

double cnorm(double _Complex z)
{
  double x = creal(z);
  double y = cimag(z);
  return x * x + y * y;
}

void wheel(const mpc_t c, const mpfr_t r, int p)
{
  mpfr_t zoom;
  mpfr_init2(zoom, 64);
  mpfr_ui_div(zoom, 1, r, MPFR_RNDN);
  mpfr_printf
    ( // location
      "Re: %Re\r\n"
      "Im: %Re\r\n"
      "Zoom: %Re\r\n"
      "Iterations: 10100100\r\n"
      "IterDiv: 0.010000\r\n"
      "SmoothMethod: 0\r\n"
      "ColorMethod: 7\r\n"
      "Differences: 3\r\n"
      "ColorOffset: 0\r\n"
      "Rotate: 0.000000\r\n"
      "Ratio: 360.000000\r\n"
      "Colors: 255,255,255,128,0,64,160,0,0,192,128,0,64,128,0,0,255,255,64,128,255,0,0,255,\r\n"
      "InteriorColor: 0,0,0,\r\n"
      "Smooth: 1\r\n"
      "MultiColor: 0\r\n"
      "BlendMC: 0\r\n"
      "MultiColors: \r\n"
      "Power: 2\r\n"
      "FractalType: 0\r\n"
      "Slopes: 1\r\n"
      "SlopePower: 50\r\n"
      "SlopeRatio: 20\r\n"
      "SlopeAngle: 45\r\n"
      "imag: 1\r\n"
      "real: 1\r\n"
      "SeedR: 0\r\n"
      "SeedI: 0\r\n"
      "FactorAR: 1\r\n"
      "FactorAI: 0\r\n"
      "Period: %d\r\n"
      // settings
      "ZoomSize: 2\r\n"
      "MaxReferences: 10000\r\n"
      "GlitchLowTolerance: 0\r\n"
      "ApproxLowTolerance: 0\r\n"
      "AutoApproxTerms: 1\r\n"
      "ImageWidth: 256\r\n"
      "ImageHeight: 256\r\n"
      "ThreadsPerCore: 1\r\n"
      "ArbitrarySize: 1\r\n"
      "ReuseReference: 0\r\n"
      "AutoSolveGlitches: 1\r\n"
      "Guessing: 1\r\n"
      "SolveGlitchNear: 0\r\n"
      "NoApprox: 0\r\n"
      "Mirror: 0\r\n"
      "LongDoubleAlways: 0\r\n"
      "FloatExpAlways: 0\r\n"
      "AutoIterations: 1\r\n"
      "ShowGlitches: 1\r\n"
      "NoReuseCenter: 1\r\n"
      "IsolatedGlitchNeighbourhood: 4\r\n"
      "JitterSeed: 1\r\n"
      "JitterShape: 0\r\n"
      "JitterScale: 1\r\n"
      "Derivatives: 0\r\n"
      "ShowCrossHair: 0\r\n"
      "UseNanoMB1: 0\r\n"
      "UseNanoMB2: 1\r\n"
      "OrderM: 16\r\n"
      "OrderN: 16\r\n"
      "InteriorChecking: 0\r\n"
      "RadiusScale: 0.1\r\n"
    , mpc_realref(c)
    , mpc_imagref(c)
    , zoom
    , p
    );
  mpfr_clear(zoom);
}

int gcd(int a, int b)
{
  if (a <= 0) return b;
  if (a > b) return gcd(b, a);
  return gcd(b % a, a);
}

int main(int argc, char **argv)
{
  if (argc > 1)
    srand(atoi(argv[1]));
  else
    srand(time(0));
  int kind = 1 * U;  
  switch (kind)
  {
#if 0
    case -1: // shoreline
    {
      double _Complex c = 0;
      int period = 1;
      double size = cabs(m_d_size(c, period));
      do
      {
        int denominator = 2 + 12 * U;
        int numerator = 1 + (denominator - 1) * U;
        int g = gcd(numerator, denominator);
        numerator /= g;
        denominator /= g;
        fprintf(stderr, "q/p = %d/%d\n", numerator, denominator);
        double _Complex z_new = 0;
        double _Complex c_new = 0;
        double _Complex z_guess = 0;
        double _Complex c_guess = c;
        double _Complex interior = cexp(twopi * I * numerator / denominator);
        m_d_interior(&z_new, &c_new, z_guess, c_guess, interior, period, 64);
        c_guess = c_new + (c_new - c_guess) * (period == 1 ? sin(pi * numerator / denominator) : 1) / (denominator * denominator);
        period *= denominator;
        m_d_nucleus(&c, c_guess, period, 64);
        size = cabs(m_d_size(c, period));
      } while (size > 1e-10);
      double _Complex z_new = 0;
      double _Complex c_new = 0;
      double _Complex z_guess = 0;
      double _Complex c_guess = c;
      double _Complex interior = cexp(twopi * I * U);
      m_d_interior(&z_new, &c_new, z_guess, c_guess, interior, period, 64);
      size /= 100;
      //output(c_new, size, period);
      break;
    }
#endif
    default:
    case 0: // wheels
    {
      mpc_t c_guess, c_out;
      mpc_t z_guess, z_out;
      mpc_init2(c_guess, 1024);
      mpc_init2(c_out, 1024);
      mpc_init2(z_guess, 1024);
      mpc_init2(z_out, 1024);
      mpc_t c_outer, c_inner, c_new;
      mpc_init2(c_outer, 1024);
      mpc_init2(c_inner, 1024);
      mpc_init2(c_new, 1024);
      mpc_set_ui(c_outer, 0, MPC_RNDNN);
      mpc_set_dc(c_inner, -2, MPC_RNDNN);
      int outer_period = 1, inner_period = 3 + 10 * U;
      m_r_nucleus(c_inner, c_inner, inner_period, 64, 1);
      //mpfr_fprintf(stderr, "%Re + %Re i\n", mpc_realref(c_inner), mpc_imagref(c_inner));
      mpc_t size, inner_size;
      mpc_init2(size, 64);
      mpc_init2(inner_size, 64);
      mpfr_t outer_r, inner_r, new_r;
      mpfr_init2(outer_r, 64);
      mpfr_init2(inner_r, 64);
      mpfr_init2(new_r, 64);
      m_r_size(size, c_outer, outer_period);
      mpc_abs(outer_r, size, MPFR_RNDN);
      m_r_size(inner_size, c_inner, inner_period);
      mpc_abs(inner_r, inner_size, MPFR_RNDN);
      mpfr_t o, i;
      mpfr_init2(o, 64);
      mpfr_init2(i, 64);
      mpc_t t;
      mpc_init2(t, 64);
      int depth;
      for (depth = 0; depth < 16; ++depth)
      {
        mpfr_prec_t p = 53 - 2 * mpfr_get_exp(inner_r);
        mpfr_prec_round(mpc_realref(c_inner), p, MPFR_RNDN);
        mpfr_prec_round(mpc_imagref(c_inner), p, MPFR_RNDN);
        mpfr_set_prec(mpc_realref(c_guess), p);
        mpfr_set_prec(mpc_imagref(c_guess), p);
        mpfr_set_prec(mpc_realref(c_out), p);
        mpfr_set_prec(mpc_imagref(c_out), p);
        mpfr_set_prec(mpc_realref(z_guess), p);
        mpfr_set_prec(mpc_imagref(z_guess), p);
        mpfr_set_prec(mpc_realref(z_out), p);
        mpfr_set_prec(mpc_imagref(z_out), p);
        mpfr_set_prec(mpc_realref(c_new), 3 * p);
        mpfr_set_prec(mpc_imagref(c_new), 3 * p);
        mpfr_pow_ui(i, inner_r, 3, MPFR_RNDN);
        mpfr_fprintf(stderr, "%d @ %Re\n", inner_period, inner_r);
        if (inner_period >= 10000)
        {
          break;
        }
        for (int try = 0; try < 10000; ++try)
        {
          // pick a point outside M_1
          double _Complex c = (4 * U - 2) + I * (4 * U - 2) - 0.75;
          double _Complex z = 0;
          int k;
          for (k = 0; k < 100; ++k)
          {
            z = z * z + c;
            if (cnorm(z) > 4) break;
          }
          if (! (cnorm(z) > 4)) continue;
          if (k < 10) continue;
#if 0
          // compute atom coordinate
          z = 0;
          for (k = 0; k < 64; ++k)
            m_d_attractor(&z, z, (k + 0.5)/64 * c, 1, 64);
          m_d_attractor(&z, z, c, 1, 64);
          double _Complex a = 2 * z;
#endif
          //fprintf(stderr, "%e + %e i\n", creal(a), cimag(a));
          // compute coordinate relative to inner atom
          mpc_set_dc(t, c, MPC_RNDNN);
          mpc_mul(t, t, inner_size, MPC_RNDNN);
          mpc_add(c_out, c_inner, t, MPC_RNDNN);
#if 0
          mpc_set_dc(z_guess, 0, MPC_RNDNN);
          mpc_set(c_guess, c_inner, MPC_RNDNN);
          for (int k = 0; k < 16; ++k)
          {
            mpc_set_dc(t, (k + 0.5)/16 * a, MPC_RNDNN);
            m_r_interior(z_out, c_out, z_guess, c_guess, t, inner_period, 64);
            mpc_swap(z_out, z_guess);
            mpc_swap(c_out, c_guess);
          }
          mpc_set_dc(t, a, MPC_RNDNN);
          m_r_interior(z_out, c_out, z_guess, c_guess, t, inner_period, 64);
#endif
          // compute period
          mpfr_mul_d(new_r, inner_r, 1e-4, MPFR_RNDN);
          int new_period = m_r_ball_period_do(c_out, new_r, inner_period * 100);
          if (new_period <= inner_period)
            continue;
          if (new_period % inner_period == 0)
            continue;
          // compute nucleus
          m_newton result = m_r_nucleus(c_new, c_out, new_period, 256, 1);
          if (result == m_failed)
            continue;
          m_r_size(size, c_new, new_period);
          mpc_abs(new_r, size, MPFR_RNDN);
          if (! mpfr_greater_p(new_r, i))
            continue;
          if (! mpfr_less_p(new_r, inner_r))
            continue;
          // step
          mpc_swap(inner_size, size);
          mpc_swap(c_inner, c_outer);
          mpc_swap(c_inner, c_new);
          outer_period = inner_period;
          inner_period = new_period;
          mpfr_swap(inner_r, outer_r);
          mpfr_swap(inner_r, new_r);
          break;
        }
      }
      mpfr_prec_t p = 53 - 2 * mpfr_get_exp(inner_r);
      mpfr_prec_round(mpc_realref(c_inner), p, MPFR_RNDN);
      mpfr_prec_round(mpc_imagref(c_inner), p, MPFR_RNDN);
      if (depth == 16)
        exit(1);
      int fold = 7 * U;
      switch (fold)
      {
        case 0: // wheel
          fprintf(stderr, "wheel\n");
          mpfr_mul_d(new_r, inner_r, 100, MPFR_RNDN);
          break;
        case 1: // 2-fold
        case 2: // 4-fold
        case 3: // 8-fold
          fprintf(stderr, "%d-fold\n", 1 << fold);
          //int new_period = outer_period + fold * inner_period;
          mpfr_set(new_r, outer_r, MPFR_RNDN);
          for (int k = 0; k < fold; ++k)
          {
            mpfr_mul(new_r, new_r, inner_r, MPFR_RNDN);
            mpfr_sqrt(new_r, new_r, MPFR_RNDN);
          }
#if 0
          for (int k = 0; k < 100; ++k)
          {
            double _Complex a = cexp(twopi * I * U);
            mpc_set_dc(t, a, MPC_RNDNN);
            mpc_mul_fr(t, t, new_r, MPC_RNDNN);
            mpc_add(c_guess, c_inner, t, MPC_RNDNN);
            m_newton result = m_r_nucleus(c_new, c_guess, new_period, 64, 1);
            if (result == m_failed)
              continue;
            mpc_sub(t, c_new, c_inner, MPC_RNDNN);
            mpc_abs(o, t, MPFR_RNDN);
            if (mpfr_greater_p(inner_r, o))
              continue;
            if (mpfr_greater_p(o, outer_r))
              continue;
            mpfr_swap(o, new_r);
            break;
          }
#endif
//          mpfr_mul_2ui(new_r, new_r, 1, MPFR_RNDN);
          break;
        case 4: // minibrot
          fprintf(stderr, "minibrot\n");
          mpc_set_dc(z_out, 0, MPC_RNDNN);
          mpc_set_dc(t, -1, MPC_RNDNN);
          m_r_interior(z_out, c_inner, z_out, c_inner, t, inner_period, 64);
          mpfr_mul_d(new_r, inner_r, 1, MPFR_RNDN);
          break;
        case 5: // shoreline
        case 6: // texture
          fprintf(stderr, fold == 5 ? "shoreline\n" : "texture\n");
          // pick a point outside M_1
          double _Complex c, z;
          for (int j = 0; j < 10000; ++j)
          {
            c = (4 * U - 2) + I * (4 * U - 2) - 0.75;
            z = 0;
            int k;
            for (k = 0; k < 100; ++k)
            {
              z = z * z + c;
              if (cnorm(z) > 4) break;
            }
            if (! (cnorm(z) > 4)) continue;
            if (k < 10) continue;
          }
          // compute atom coordinate
#if 0
          z = 0;
          for (int k = 0; k < 64; ++k)
            m_d_attractor(&z, z, (k + 0.5)/64 * c, 1, 64);
          m_d_attractor(&z, z, c, 1, 64);
          double _Complex a = 2 * z;
#endif
          //fprintf(stderr, "%e + %e i\n", creal(a), cimag(a));
          // compute coordinate relative to inner atom
          mpc_set_dc(t, c, MPC_RNDNN);
          mpc_mul(t, t, inner_size, MPC_RNDNN);
          mpc_add(c_inner, c_inner, t, MPC_RNDNN);
          mpfr_mul_d(new_r, inner_r, fold == 5 ? 0.1 : 0.01, MPFR_RNDN);
#if 0
          int new_period = inner_period;
          int count = 4 * U;
          for (int k = 0; k < count; ++k)
          {
            int denominator = 2 + 11 * U;
            int numerator = 1 + (denominator - 1) * U;
            int g = gcd(numerator, denominator);
            numerator /= g;
            denominator /= g;
            fprintf(stderr, "q/p = %d/%d\n", numerator, denominator);
            mpc_set_dc(t, cexp(twopi * I * numerator / denominator), MPC_RNDNN);
            mpc_set_dc(z_out, 0, MPC_RNDNN);
            m_r_interior(z_out, c_new, z_out, c_inner, t, new_period, 64);
            mpc_sub(t, c_new, c_inner, MPC_RNDNN);
            mpfr_set_d(i, (k == 0 ? sin(pi * numerator / denominator) : 1) / (denominator * denominator), MPFR_RNDN);
            mpc_mul_fr(t, t, i, MPC_RNDNN);
            mpc_add(c_inner, c_new, t, MPC_RNDNN);
            new_period *= denominator;
          }
          m_r_nucleus(c_inner, c_inner, new_period, 64, 1);
          m_r_size(size, c_inner, new_period);
          mpc_abs(new_r, size, MPFR_RNDN);
          mpfr_mul_d(new_r, new_r, 4, MPFR_RNDN);
#endif
          break;
      }
      wheel(c_inner, new_r, inner_period);
      break;
    }   
  }
  return 0;
}
