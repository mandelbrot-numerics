// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>
#include "m-util.h"

static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage:\n%s precision center-re center-im radius maxperiod\n"
      "%s precision radius maxperiod <<EOF\n"
      "center-re\ncenter-im\nEOF\n"
    , progname, progname
    );
}

extern int main(int argc, char **argv) {
  if (argc != 4 && argc != 6) {
    usage(argv[0]);
    return 1;
  }
  bool native = true;
  int bits = 0;
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }
  if (native) {
    double cre = 0;
    double cim = 0;
    double radius = 0;
    int maxperiod = 0;
    if (argc == 4)
    {
      if (! stdin_double(&cre)) { return 1; }
      if (! stdin_double(&cim)) { return 1; }
      if (! arg_double(argv[2], &radius)) { return 1; }
      if (! arg_int(argv[3], &maxperiod)) { return 1; }
    }
    else
    {
      if (! arg_double(argv[2], &cre)) { return 1; }
      if (! arg_double(argv[3], &cim)) { return 1; }
      if (! arg_double(argv[4], &radius)) { return 1; }
      if (! arg_int(argv[5], &maxperiod)) { return 1; }
    }
    int period = m_d_ball_period_do(cre + I * cim, radius, maxperiod);
    if (period > 0) {
      printf("%d\n", period);
      return 0;
    }
  } else {
    mpc_t center;
    mpfr_t radius;
    int maxperiod = 0;
    mpc_init2(center, bits);
    mpfr_init2(radius, 53);
    if (argc == 4)
    {
      if (! stdin_mpfr(mpc_realref(center))) { return 1; }
      if (! stdin_mpfr(mpc_imagref(center))) { return 1; }
      if (! arg_mpfr(argv[2], radius)) { return 1; }
      if (! arg_int(argv[3], &maxperiod)) { return 1; }
    }
    else
    {
      if (! arg_mpc(argv[2], argv[3], center)) { return 1; }
      if (! arg_mpfr(argv[4], radius)) { return 1; }
      if (! arg_int(argv[5], &maxperiod)) { return 1; }
    }
    int period = m_r_ball_period_do(center, radius, maxperiod);
    if (period > 0) {
      printf("%d\n", period);
    }
    mpc_clear(center);
    mpfr_clear(radius);
    return period <= 0;
  }
  return 1;
}
