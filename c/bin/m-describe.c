// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mandelbrot-numerics.h>
#include "m-util.h"

static const char *compass[16] =
  { "east"
  , "east-north-east"
  , "north-east"
  , "north-north-east"
  , "north"
  , "north-north-west"
  , "north-west"
  , "west-north-west"
  , "west"
  , "west-south-west"
  , "south-west"
  , "south-south-west"
  , "south"
  , "south-south-east"
  , "south-east"
  , "east-south-east"
  };

static inline double cabs2(double _Complex z) {
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

int main(int argc, char **argv)
{
  if (! (argc >= 6))
  {
    fprintf(stderr, "usage: %s precision maxperiod maxiters re im [ppperiod ...]\n", argv[0]);
    return 1;
  }
  bool native = true;
  int bits = 0;
  int minrayperiod = 0; // FIXME hardcoded
  int maxrayperiod = 100000; // FIXME hardcoded
  int maxppperiod = 0;
  int maxperiod = 0;
  int maxiters = 0;
  int ncpus = 1;
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }
  if (! arg_int(argv[2], &maxperiod)) { return 1; }
  if (! arg_int(argv[3], &maxiters)) { return 1; }
  if (native) {
    double cre = 0;
    double cim = 0;
    if (! arg_double(argv[4], &cre)) { return 1; }
    if (! arg_double(argv[5], &cim)) { return 1; }

    double _Complex c = cre + I * cim;
    double _Complex z = 0;
    double _Complex dc = 0;
    printf("the input point was %.18f + %.18f i\n", creal(c), cimag(c));
    bool escaped = false;
    for (int i = 0; i < maxiters; ++i)
    {
      dc = 2 * z * dc + 1;
      z = z * z + c;
      if (!escaped && cabs2(z) > 1e10)
      {
        double mu = i + 1 - log2(log(cabs(z)));
        double de = 2 * cabs(z) * log(cabs(z)) / cabs(dc);
        printf("the point escaped with dwell %.5f\nand exterior distance estimate %.5g\n", mu, de);
        escaped = true;
      }
    }
    if (! escaped)
    {
      printf("the point didn't escape after %d iterations\n", maxiters);
    }
    printf("\nnearby hyperbolic components to the input point:\n");
    double _Complex cperiodic = 0;
    double periodic = 1.0 / 0.0;
    int lastlastperiod = 1;
    int lastperiod = 1;
    bool interior = false;
    z = 0;
    for (int period = 1; period <= maxperiod && !interior; ++period)
    {
      z = z * z + c;
      double m = cabs(z);
      if (m <= periodic)
      {
        periodic = m;
        double _Complex q;
        if (m_failed != m_d_nucleus(&q, c, period, 64))
        {
          double _Complex w = 0;
          for (int i = 0; i <= 64; ++i)
          {
            m_d_attractor(&w, w, (cperiodic * (64 - i) + i * q) / 64, lastperiod, 64);
          }
          double _Complex dw = 1;
          for (int i = 0; i < lastperiod; ++i)
          {
            dw *= 2 * w;
            w = w * w + q;
          }
          double _Complex nucleus_domain_coordinate = dw;
          double _Complex u = 0, v = 0;
          for (int i = 1; i <= lastperiod; ++i)
          {
            u = u * u + q;
            if (i == lastlastperiod)
              v = u;
          }
          double _Complex atom_domain_coordinate = u / v;
          cperiodic = q;
          lastlastperiod = lastperiod;
          lastperiod = period;
          int dir = round(carg(q - c) / 6.283185307179586 * 16);
          dir += 16;
          dir %= 16;
          double _Complex r = m_d_size(q, period);
          int sdir = round(carg(r) / 6.283185307179586 * 16);
          sdir += 16;
          sdir += 8;
          sdir %= 16;
          int cardioid = m_d_shape_discriminant(m_d_shape_estimate(q, period)) == m_cardioid;
          printf(
            "\n- a period %d %s\n"
            "  with nucleus at %.18f + %.18f i\n"
            "  the component has size %.5g and is pointing %s\n"
            "  the atom domain has size %.5g\n"
            "  the nucleus domain coordinate is %.5g at turn %.18f\n"
            "  the atom    domain coordinate is %.5g at turn %.18f\n"
            "  the nucleus is %.5g to the %s of the input point\n"
            , period, cardioid ? "cardioid" : "circle"
            , creal(q), cimag(q)
            , cabs(r), compass[sdir]
            , m_d_domain_size(q, period)
            , cabs(nucleus_domain_coordinate), fmod(carg(nucleus_domain_coordinate) / 6.283185307179586 + 1, 1)
            , cabs(atom_domain_coordinate), fmod(carg(atom_domain_coordinate) / 6.283185307179586 + 1, 1)
            , cabs(q - c), compass[dir]
            );
          if (m_failed != m_d_attractor(&w, z, c, period, 64))
          {
            double _Complex w0 = w;
            double _Complex dw = 1;
            for (int i = 0; i < period; ++i)
            {
              dw = 2 * w * dw;
              w = w * w + c;
            }
            double t = carg(dw) / 6.283185307179586;
            t -= floor(t);
            interior = cabs(dw) <= 1;
            printf(
              "  the input point is %s to this component at\n"
              "  radius %.5g and angle %.5f (in turns)\n"
              "  a point in the attractor is %.18f + %.18f i\n"
              , interior ? "interior" : "exterior"
              , cabs(dw), t
              , creal(w0), cimag(w0)
              );
            if (interior)
            {
              double ide = 0;
              double _Complex dz = 0;
              if (m_d_interior_de(&ide, &dz, w0, c, period, 64))
              {
                printf(
                  "  the interior distance estimate is %.5g\n"
                  , ide
                  );
              }
            }
          }
          char *lo = 0, *hi = 0;
          m_d_external_angles(&lo, &hi, q, period, 0.9 - 0.8 / sqrt(period), 64);
          if (lo && hi)
            printf("  external angles of this component are:\n  %s\n  %s\n", lo, hi);
          if (lo)
            free(lo);
          if (hi)
            free(hi);
        }
      }
    }

    printf("\nnearby Misiurewicz points to the input point:\n");
    double de = 1.0 / 0.0;
    double d = 1.0 / 0.0;
    int period = -1;
    int preperiod = -1;
    for (int m = 1; m <= maxperiod; ++m)
    {
      z = c;
      double _Complex w = c;
      for (int n = 0; n < m; ++n)
      {
        z = z * z + c;
      }
      for (int n = 1; n <= maxppperiod; ++n)
      {
        z = z * z + c;
        w = w * w + c;
        double z2 = cabs2(z - w);
        if (z2 < de)
        {
          de = z2;
          period = m;
          preperiod = n + 1;

          double _Complex z2 = c;
          m_d_misiurewicz(&z2, z2, preperiod, period, 64);
          m_d_misiurewicz_naive(&z2, z2, preperiod, period, 64);
          double d2 = cabs2(z2 - c);
          if (d2 < d)
          {
            d = d2;
            int dir = round(carg(z2 - c) / 6.283185307179586 * 16);
            dir += 16;
            dir %= 16;
            double _Complex w = z2;
            double _Complex dw = 1;
            for (int i = 0; i < preperiod; ++i)
            {
              w = w * w + z2;
            }
            for (int i = 0; i < period; ++i)
            {
              dw = 2 * w * dw;
              w = w * w + z2;
            }
            double t = carg(dw) / 6.283185307179586;
            if (cabs(dw) > 1)
            {
              printf(
                "\n- %dp%d\n"
                "  the strength is %.5g\n",
                preperiod, period,
                de
                );
              printf(
                "  the center is at %.18f + %.18f i\n"
                "  the center is %.5g to the %s of the input point\n"
                "  the multiplier has radius %.5g and angle %.5f (in turns)\n",
                creal(z2), cimag(z2),
                cabs(z2 - c), compass[dir],
                cabs(dw), t
                );
            }
          }
        }
      }
    }

  } else {
    mpc_t c;
    mpc_init2(c, bits);
    if (! arg_mpc(argv[4], argv[5], c)) { return 1; }

    mpc_t z, dzdc, dc, q_old, q, r, w, w0, dz, dw, zw, mc, periodicz, a, wc, wc0, wc1;
    mpfr_t z2,da,  er2, escapeRadius2, de, periodic;
    mpc_init2(z, bits);
    mpc_init2(dzdc, bits);
    mpc_init2(dc, bits);
    mpc_init2(q_old, bits);
    mpc_init2(q, bits);
    mpc_init2(r, bits);
    mpc_init2(w, bits);
    mpc_init2(w0, bits);
    mpc_init2(dz, bits);
    mpc_init2(dw, bits);
    mpc_init2(zw, bits);
    mpc_init2(mc, bits);
    mpc_init2(wc, bits);
    mpc_init2(wc0, bits);
    mpc_init2(wc1, bits);
    mpc_init2(periodicz, bits);
    mpc_init2(a, bits);
    mpfr_init2(z2, bits);
    mpfr_init2(da, bits);
    mpfr_init2(er2, bits);
    mpfr_init2(de, bits);
    mpfr_init2(periodic, bits);
    mpfr_init2(escapeRadius2, 53);
    mpfr_set_d(er2, 1e100, MPFR_RNDN);
    mpfr_set_d(escapeRadius2, 1e100, MPFR_RNDN);
    mpc_set_ui_ui(z, 0, 0, MPC_RNDNN);
    mpc_set_ui_ui(dc, 0, 0, MPC_RNDNN);
    mpfr_printf("the input point was %Re + %Re i\n", mpc_realref(c), mpc_imagref(c));
    printf("nearby hyperbolic components to the input point:\n");
    mpfr_set_d(periodic, 1.0 / 0.0, MPFR_RNDN);
    int periodicp = 0;
    bool escaped = false;
    bool interior = false;
    mpc_set_ui_ui(z, 0, 0, MPC_RNDNN);
    for (int period = 1; period <= maxiters && !interior; ++period)
    {
#if 0
      // dc = 2 * z * dc + 1;
      mpc_mul(dzdc, dzdc, z, MPC_RNDNN);
      mpfr_mul_2exp(mpc_realref(dzdc), mpc_realref(dzdc), 1, MPFR_RNDN);
      mpfr_mul_2exp(mpc_imagref(dzdc), mpc_imagref(dzdc), 1, MPFR_RNDN);
      mpfr_add_ui(mpc_realref(dzdc), mpc_realref(dzdc), 1, MPFR_RNDN);
#endif
      // z = z * z + c;
      mpc_sqr(z, z, MPC_RNDNN);
      mpc_add(z, z, c, MPC_RNDNN);
      mpc_norm(z2, z, MPFR_RNDN);
      if (mpfr_greater_p(z2, escapeRadius2))
      {
        double dz2 = mpfr_get_d(z2, MPFR_RNDN);
        double mu = period + 1 - log2(log(sqrt(dz2)));
#if 0
        mpfr_set_d(de, 2 * sqrt(dz2) * log(sqrt(dz2)), MPFR_RNDN);
        mpc_abs(z2, dzdc, MPFR_RNDN);
        mpfr_div(de, de, z2, MPFR_RNDN);
        mpfr_printf("the point escaped with dwell %.5f\nand exterior distance estimate %.5Re\n", mu, de);
#endif
        mpfr_printf("the point escaped with dwell %.5f\n", mu);
        escaped = true;
        break;
      }
      if (period <= maxperiod && ! mpfr_greater_p(z2, periodic))
      {
        mpc_div(a, z, periodicz, MPC_RNDNN);
        mpc_set(periodicz, z, MPC_RNDNN);
        mpfr_set(periodic, z2, MPFR_RNDN);
        periodicp = period;
        if (period < minrayperiod) continue; // FIXME temporary hack
        if (period > maxrayperiod) break; // FIXME temporary hack
        double _Complex atom = mpc_get_dc(a, MPC_RNDNN);
        int adir = round(carg(atom) / 6.283185307179586 * 16);
        adir += 16;
        adir %= 16;
        if (m_failed != m_r_nucleus(q, c, period, 64, ncpus))
        {
          mpc_sub(dc, q, c, MPC_RNDNN);
          mpc_arg(z2, dc, MPFR_RNDN);
          mpc_abs(er2, dc, MPFR_RNDN);
          int dir = round(mpfr_get_d(z2, MPFR_RNDN) / 6.283185307179586 * 16);
          dir += 16;
          dir %= 16;
          m_r_size(r, q, period);
          mpc_div(dc, dc, r, MPC_RNDNN);
          double _Complex inter = - mpc_get_dc(dc, MPC_RNDNN);
          int idir = round(carg(inter) / 6.283185307179586 * 16);
          idir += 16;
          idir %= 16;
          mpc_arg(z2, r, MPFR_RNDN);
          int sdir = round(mpfr_get_d(z2, MPFR_RNDN) / 6.283185307179586 * 16);
          sdir += 16;
          sdir += 8;
          sdir %= 16;
          mpc_abs(z2, r, MPFR_RNDN);
          mpfr_exp_t e = mpfr_get_exp(z2);
          mpfr_prec_t prec = 16 - e;
          mpfr_prec_round(mpc_realref(q), prec, MPFR_RNDN);
          mpfr_prec_round(mpc_imagref(q), prec, MPFR_RNDN);
          m_r_domain_size(de, q, period);
          mpfr_printf(
            "\n- a period %d %s\n"
            "  with nucleus at %Re + %Re i\n"
            "  the component has size %.5Re and is pointing %s\n"
            "  the atom domain has size %.5Re\n"
            "  the atom domain coordinates of the input point are %.5g + %.5g i\n"
            "  the atom domain coordinates in polar form are %.5g to the %s\n"
            "  the atom coordinates of the input point are %.5g + %.5g i\n"
            "  the atom coordinates in polar form are %.5g to the %s\n"
            "  the nucleus is %.5Re to the %s of the input point\n"
            , period, m_r_shape(q, period) == m_cardioid ? "cardioid" : "circle"
            , mpc_realref(q), mpc_imagref(q)
            , z2, compass[sdir]
            , de
            , creal(atom), cimag(atom)
            , cabs(atom), compass[adir]
            , creal(inter), cimag(inter)
            , cabs(inter), compass[idir]
            , er2, compass[dir]
            );
#if 0
          if (! interior)
          {
            if (m_failed != m_r_attractor(w, z, c, period, 64))
            {
              mpc_set_prec(w0, prec);
              mpc_set(w0, w, MPC_RNDNN);
              mpc_set_ui_ui(dw, 1, 0, MPC_RNDNN);
              for (int i = 0; i < period; ++i)
              {
                // dw = 2 * w * dw;
                mpc_mul(dw, w, dw, MPC_RNDNN);
                mpfr_mul_2exp(mpc_realref(dw), mpc_realref(dw), 1, MPFR_RNDN);
                mpfr_mul_2exp(mpc_imagref(dw), mpc_imagref(dw), 1, MPFR_RNDN);
                // w = w * w + c;
                mpc_sqr(w, w, MPC_RNDNN);
                mpc_add(w, w, c, MPC_RNDNN);
              }
              mpc_arg(z2, dw, MPFR_RNDN);
              double t = mpfr_get_d(z2, MPFR_RNDN) / 6.283185307179586;
              t -= floor(t);
              mpc_abs(z2, dw, MPFR_RNDN);
              interior = mpfr_get_d(z2, MPFR_RNDN) <= 1;
              mpfr_printf(
                "  the input point is %s to this component at\n"
                "  radius %.5Re and angle %.18f (in turns)\n"
                "  the multiplier is %.5Re + %.5Re i\n"
                "  a point in the attractor is %Re + %Re i\n"
                , interior ? "interior" : "exterior"
                , z2, t
                , mpc_realref(dw), mpc_imagref(dw)
                , mpc_realref(w0), mpc_imagref(w0)
                );
            }
          }
#endif
          fflush(stdout);
#if 1
          if (mpfr_get_d(de, MPFR_RNDN) > 1e-10)
          {
            char *lo = 0, *hi = 0;
            m_d_external_angles(&lo, &hi, mpc_get_dc(q, MPC_RNDNN), period, 0.75, 32);
            if (lo && hi)
              printf("  external angles of this component are:\n  %s\n  %s\n", lo, hi);
            if (lo)
              free(lo);
            if (hi)
              free(hi);
          }
#if 0
          else if (period < 200)
          {
            char *rays[2] = { 0, 0 };
            m_r_external_angles(2, &rays[0], q, 0, period, 0.75, 32);
            if (rays[0] && rays[1])
              printf("  external angles of this component are:\n  %s\n  %s\n", rays[0], rays[1]);
            if (rays[0])
              free(rays[0]);
            if (rays[1])
              free(rays[1]);
          }
#endif
          else if (0 && mpfr_get_d(de, MPFR_RNDN) > 1e-300)
          {
            m_r_orbit_d *orbit = m_r_orbit_d_new(q, 0, period);
            char *rays = 0;
            m_d_external_angles_perturbed(1, &rays, orbit, 0, period, 0.75, 32);
            m_r_orbit_d_delete(orbit);
            if (rays)
            {
              printf("  external angles of this component are:\n");
              printf("  %s\n", rays);
              fflush(stdout);
              FILE *o = popen("m-conjugate | sed 's/^/  /g'", "w");
              if (o)
              {
                fprintf(o, "%s\n", rays);
                fflush(o);
                pclose(o);
              }
              else
              {
                printf("  ?\n");
              }
            }
            if (rays)
              free(rays);
          }
          else if (mpfr_get_ld(de, MPFR_RNDN) > 1e-4920L)
          {
            m_r_orbit_ld *orbit = m_r_orbit_ld_new(q, 0, period);
            char *rays = 0;
            m_ld_external_angles_perturbed(1, &rays, orbit, 0, period, 0.75, 32);
            m_r_orbit_ld_delete(orbit);
            if (rays)
            {
              printf("  external angles of this component are:\n");
              printf("  %s\n", rays);
              fflush(stdout);
              FILE *o = popen("m-conjugate | sed 's/^/  /g'", "w");
              if (o)
              {
                fprintf(o, "%s\n", rays);
                fflush(o);
                pclose(o);
              }
              else
              {
                printf("  ?\n");
              }
            }
            if (rays)
              free(rays);
          }
          fflush(stdout);
#endif
        }
      }
    }
    if (! escaped && ! interior)
    {
      printf("\nthe point didn't escape after %d iterations\n", maxiters);
    }

    if (6 < argc)
    {
      printf("\nnearby Misiurewicz points to the input point:\n");
      for (int i = 6; i < argc; ++i)
      {
        int p = 0;
        if (! arg_int(argv[i], &p)) { continue; }
        m_r_orbit_diff_l *orbit = m_r_orbit_diff_l_new(c, 0, maxperiod, p);
        m_l_nearby_misiurewicz_orbit *ball = m_l_nearby_misiurewicz_orbit_new(orbit);
        for (int q = 0; q < maxperiod; ++q)
        {
          if (m_l_nearby_misiurewicz_orbit_check(ball))
          {
            int preperiod = m_l_nearby_misiurewicz_orbit_get_preperiod(ball);
            int period = m_l_nearby_misiurewicz_orbit_get_period(ball);
            long double distance = m_l_nearby_misiurewicz_orbit_get_distance(ball);
            long double size = m_l_nearby_misiurewicz_orbit_get_size(ball);
            if (distance > 0 && size > 0)
            {
              if (preperiod > 0)
              {
                int prec = 53 - log2l(distance);
                mpc_t m;
                mpc_init2(m, prec);
                mpc_set(m, c, MPC_RNDNN);
                if (m_converged == m_r_misiurewicz(m, m, preperiod, period, 64))
                {
                  if (m_converged == m_r_misiurewicz_naive(m, m, preperiod, period, 64))
                  {
                    mpc_sub(zw, m, c, MPC_RNDNN);
                    mpc_abs(z2, zw, MPC_RNDNN);
                    mpc_arg(da, zw, MPFR_RNDN);
                    int dir = round(mpfr_get_d(da, MPFR_RNDN) / 6.283185307179586 * 16);
                    dir += 16;
                    dir %= 16;
                    m_r_misiurewicz_size(da, m, preperiod, period);
                    const long double _Complex *z_ptr = m_r_orbit_diff_l_get_z_ptr(orbit);
                    long double _Complex dzdz = 1;
                    for (int j = 0; j < period; ++j)
                    {
                      dzdz = 2 * z_ptr[preperiod + j] * dzdz;
                    }
                    mpfr_printf(
                      "\n"
                      "- %dp%d\n"
                      "  with center at %Re + %Re i\n"
                      "  the Misiurewicz domain has size %.5Re\n"
                      "  the Misiurewicz domain coordinate radius is %.5Lg\n"
                      "  the center is %.5Re to the %s of the input point\n"
                      "  the multiplier has radius %.18Le and angle %.18Lf (in turns)\n",
                      preperiod, period,
                      mpc_realref(m), mpc_imagref(m),
                      da,
                      distance / size,
                      z2, compass[dir],
                      cabsl(dzdz), cargl(dzdz) / (2 * 3.14159265358979)
                    );
                    fflush(stdout);
                  }
                }
                mpc_clear(m);
              }
            }
            else
            {
              break;
            }
          }
          if (! m_l_nearby_misiurewicz_orbit_step(ball))
          {
            break;
          }
        }
        m_l_nearby_misiurewicz_orbit_delete(ball);
        m_r_orbit_diff_l_delete(orbit);
      }
    }

    mpc_clear(c);
    mpc_clear(z);
    mpc_clear(dzdc);
    mpc_clear(dc);
    mpc_clear(q);
    mpc_clear(r);
    mpc_clear(w);
    mpc_clear(w0);
    mpc_clear(dz);
    mpc_clear(dw);
    mpc_clear(zw);
    mpc_clear(mc);
    mpfr_clear(z2);
    mpfr_clear(da);
    mpfr_clear(er2);
    mpfr_clear(de);
    mpfr_clear(periodic);
  }

  return 0;
}
