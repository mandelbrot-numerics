// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2020 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>
#include <mandelbrot-symbolics.h>
#include "m-util.h"

static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage: %s precision angle sharpness maxsteps\n"
    , progname
    );
}

extern int main(int argc, char **argv) {
  if (argc != 5) {
    usage(argv[0]);
    return 1;
  }
  bool parallel = false;
  bool verbose = false;
  bool native = true;
  int bits = 0;
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }
  mpq_t angle;
  mpq_init(angle);
  int sharpness = 0;
  int maxsteps = 0;
  m_binangle ba;
  m_binangle_init(&ba);
  if (! arg_rational(argv[2], angle)) {
    const char *s = m_binangle_from_string(&ba, argv[2]);
    if ((!s) || *s)
    {
      mpq_clear(angle);
      m_binangle_clear(&ba);
      return 1;
    }
  }
  else
  {
    m_binangle_from_rational(&ba, angle);
  }
  m_binangle_to_rational(angle, &ba);
  int preperiod = ba.pre.length;
  int period = ba.per.length;
  m_binangle_clear(&ba);
  if (! arg_int(argv[3], &sharpness)) { mpq_clear(angle); return 1; }
  if (! arg_int(argv[4], &maxsteps)) { mpq_clear(angle); return 1; }
  int retval = 0;
  if (native)
  {
    m_d_spider_path *path = m_d_spider_path_new(angle, preperiod, period, sharpness);
    mpq_clear(angle);
    if (! path)
    {
      return 1;
    }
    for (int i = 0; i < maxsteps; ++i)
    {
      if (verbose)
      {
        for (int j = 0; j <= preperiod + period; ++j)
        {
          double _Complex c = m_d_spider_path_get(path, j);
          printf("%.16e %.16e\t", creal(c), cimag(c));
        }
        printf("\n");
      }
      m_d_spider_path_step(path);
    }
    if (! verbose)
    {
      double _Complex c = m_d_spider_path_get(path, 1);
      printf("%.16e %.16e\n", creal(c), cimag(c));
    }
    m_d_spider_path_delete(path);
  }
  else
  {
    m_r_spider_path *path = m_r_spider_path_new(angle, preperiod, period, sharpness, bits);
    mpq_clear(angle);
    if (! path)
    {
      return 1;
    }
    mpc_t c;
    mpc_init2(c, bits);
    for (int i = 0; i < maxsteps; ++i)
    {
      if (verbose)
      {
        for (int j = 0; j <= preperiod + period; ++j)
        {
          m_r_spider_path_get(path, j, c);
          mpfr_printf("%Re %Re\t", mpc_realref(c), mpc_imagref(c));
        }
        printf("\n");
      }
      m_r_spider_path_step(path, parallel);
    }
    if (! verbose)
    {
      m_r_spider_path_get(path, 1, c);
      mpfr_printf("%Re %Re\n", mpc_realref(c), mpc_imagref(c));
    }
    mpc_clear(c);
    m_r_spider_path_delete(path);
  }
  return retval;
}
