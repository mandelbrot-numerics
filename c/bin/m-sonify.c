#include <mandelbrot-numerics.h>
#include <math.h>
#include <sndfile.h>

#define SR 48000
#define FPS 60
#define PI 3.141592653589793

// http://burtleburtle.net/bob/hash/integer.html
static uint32_t burtle_hash(uint32_t a)
{
    a = (a+0x7ed55d16) + (a<<12);
    a = (a^0xc761c23c) ^ (a>>19);
    a = (a+0x165667b1) + (a<<5);
    a = (a+0xd3a2646c) ^ (a<<9);
    a = (a+0xfd7046c5) + (a<<3);
    a = (a^0xb55a4f09) ^ (a>>16);
    return a;
}

// pseudo-random uniform number in [0,1)
static float uniform(uint32_t c)
{
//  return rand() / (double) RAND_MAX;
  return burtle_hash(c) / (float) (0x100000000LL);
}

float buffer[SR][4];

int main(int argc, char **argv)
{
  SF_INFO info = { 0, 48000, 4, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
  SNDFILE *sndfile = sf_open("m-sonify.wav", SFM_WRITE, &info);
  mpq_t angle;
  mpq_init(angle);
  int sharpness = 8;
  int j = 0;
  for (int period = 1; period < 8; ++period)
  {
    int denominator = (1 << period) - 1;
    for (int numerator = 1; numerator < denominator; numerator += 2)
    {
      mpq_set_ui(angle, numerator, denominator);
      mpq_canonicalize(angle);
      if (mpz_get_ui(mpq_denref(angle)) != denominator) continue;
      m_d_exray_in *ray = m_d_exray_in_new(angle, sharpness);
      for (int i = 0; i < period * sharpness * 2; ++i)
        m_d_exray_in_step(ray, 16);
      double _Complex endpoint = m_d_exray_in_get(ray);
      m_d_exray_in_delete(ray);
      double _Complex nucleus;
      m_d_nucleus(&nucleus, endpoint, period, 16);
      int k = 0;
      double _Complex z = 0;
      double _Complex c = nucleus;
      for (int t = 0; t < SR; ++t)
      {
        m_d_interior(&z, &c, z, c, cexp(I * 2 * PI * (t + 0.5) / SR), period, 16);

  buffer[k][0] = 0;
  buffer[k][1] = 0;
  buffer[k][2] = 0;
  buffer[k][3] = 0;
  float omni = (uniform(j++) - 0.5f) / period;
        for (int i = 0; i < period; ++i)
        {
  double X = creal(z);
  double Y = cimag(z);
  double R = X * X + Y * Y + 1;
  double u = 2 * X / R;
  double v = 2 * Y / R;
  double w = (R - 2) / R;
  float left  = -u * omni;
  float front = -w * omni;
  float up    =  v * omni;
  buffer[k][0] += omni;
  buffer[k][1] += left;
  buffer[k][2] += up;
  buffer[k][3] += front;
          z = z * z + c;
        }
          if (++k == SR)
          {
            k = 0;
            sf_writef_float(sndfile, &buffer[0][0], SR);
          }

      }
    }
  }
  mpq_clear(angle);
  sf_close(sndfile);
  return 0;
}
