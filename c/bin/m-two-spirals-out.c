// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>

int main()
{
  int n = 64;
  double _Complex c = -1.7687402565964134e+00 + I * 3.0387357058642636e-03;
  int p = 3;
  int P = 56;
  int arms = 2;
  // period 56 nucleus at center
  m_d_nucleus(&c, c, P, n);
  // a
  m_d_misiurewicz(&c, c, P + 1, p, n);
  // b
  m_d_nucleus(&c, c, P = P + arms * p, n);
  // c
  m_d_misiurewicz(&c, c, P + 1, p, n);
  // d
  m_d_nucleus(&c, c, P = P + p, n);
  // e
  m_d_misiurewicz(&c, c, P + 1, p, n);
  // f
  m_d_nucleus(&c, c, P = P + p, n);
  // period 68 nucleus two spirals out
  printf("%.18f %.18f 1e-8\n", creal(c), cimag(c));
  return 0;
}
