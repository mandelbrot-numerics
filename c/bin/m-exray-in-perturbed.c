// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>
#include <mandelbrot-symbolics.h>
#include "m-util.h"

static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage: %s precision re im preperiod period angle sharpness steps verbose\n"
    , progname
    );
}

extern int main(int argc, char **argv) {
  if (argc != 10) {
    usage(argv[0]);
    return 1;
  }
  bool native = true;
  int bits = 0;
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }
  if (native) {
    fprintf(stderr, "no native\n");
    return 1;
  } else {
    mpc_t nucleus, endpoint;
    mpc_init2(nucleus, bits);
    mpc_init2(endpoint, bits);
    int preperiod = 0;
    int period = 0;
    mpq_t angle;
    mpq_init(angle);
    int sharpness = 0;
    int steps = 0;
    int verbose = 0;
    if (! arg_mpc(argv[2], argv[3], nucleus)) { return 1; }
    if (! arg_int(argv[4], &preperiod)) { return 1; }
    if (! arg_int(argv[5], &period)) { return 1; }
    if (! arg_rational(argv[6], angle)) {
      m_binangle ba;
      m_binangle_init(&ba);
      const char *s = m_binangle_from_string(&ba, argv[6]);
      if ((!s) || *s) return 1;
      m_binangle_to_rational(angle, &ba);
      m_binangle_clear(&ba);
    }
    if (! arg_int(argv[7], &sharpness)) { return 1; }
    if (! arg_int(argv[8], &steps)) { return 1; }
    if (! arg_int(argv[9], &verbose)) { return 1; }
    if (! (preperiod >= 0)) { return 1; }
    if (! (period > 0)) { return 1; }

    m_r_orbit_d *orbit = m_r_orbit_d_new(nucleus, preperiod, period);
    if (! orbit) { return 1; }
    m_d_exray_in_perturbed *ray = m_d_exray_in_perturbed_new(orbit, angle, sharpness);
    if (! ray) { return 1; }
    int retval = 0;
    double _Complex c = 0./0.;
    for (int i = 0; i < steps; ++i)
    {
      if (m_stepped != m_d_exray_in_perturbed_step(ray, 64))
      {
        retval = 1;
        break;
      }
      c = m_d_exray_in_perturbed_get(ray);
      if (verbose)
      {
        mpc_set_dc(endpoint, c, MPC_RNDNN);
        mpc_add(endpoint, endpoint, nucleus, MPC_RNDNN);
        mpfr_printf("%Re %Re\n", mpc_realref(endpoint), mpc_imagref(endpoint));
      }
    }
    if (! verbose)
    {
      mpc_set_dc(endpoint, c, MPC_RNDNN);
      mpc_add(endpoint, endpoint, nucleus, MPC_RNDNN);
      mpfr_printf("%Re %Re\n", mpc_realref(endpoint), mpc_imagref(endpoint));
    }
    m_d_exray_in_perturbed_delete(ray);
    m_r_orbit_d_delete(orbit);
    return retval;
  }
  return 1;
}
