// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>
#include "m-util.h"

static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage: %s parent-period nucleus-re nucleus-im internal-angle external-angle\n"
    , progname
    );
}

extern int main(int argc, char **argv) {
  if (argc != 6) {
    usage(argv[0]);
    return 1;
  }
  int period = 0;
  double nucleusre, nucleusim;
  mpq_t inangle, exangle;
  mpq_init(inangle);
  mpq_init(exangle);
  arg_int(argv[1], &period);
  arg_double(argv[2], &nucleusre);
  arg_double(argv[3], &nucleusim);
  arg_rational(argv[4], inangle);
  arg_rational(argv[5], exangle);
  complex double pnucleus;
  m_d_nucleus(&pnucleus, nucleusre + I * nucleusim, period, 256);
  complex double z, bond;
  m_d_interior(&z, &bond, pnucleus, pnucleus, cexp(I * 2 * 3.141592653589793 * mpq_get_d(inangle)), period, 256);
  complex double cnucleus;
  int den = mpz_get_si(mpq_denref(inangle));
  m_d_nucleus(&cnucleus, bond + (bond - pnucleus) / (den * den) , period * den, 256);
  double s = cabs(cnucleus - bond);
  int sharpness = 4;
  m_d_exray_in *ray = m_d_exray_in_new(exangle, sharpness);
  if (! ray) { mpq_clear(inangle); mpq_clear(exangle); return 1; }
  int n = 0;
  for (int j = 4; j < 17; ++j) {
    for (int i = 0; i < 1 << j; ++i) {
      m_d_exray_in_step(ray, 4);
    }
    complex double c = m_d_exray_in_get(ray);
    double eps = cabs(c - bond);
    z = 0;
    for (n = 0; ; ++n) {
      z = z * z + c;
      if (cabs(z) > 2) { break; }
    }
    printf("%d %.16e %.16e\n", n, eps, eps * n / (2 * s * period * den));
    fflush(stdout);
  }
  m_d_exray_in_delete(ray);
  mpq_clear(inangle);
  mpq_clear(exangle);  
  return 0;
}
