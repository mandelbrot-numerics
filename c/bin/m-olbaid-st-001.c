// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

// for strdup
#define _POSIX_C_SOURCE 200809L

#include <mandelbrot-numerics.h>
#include <mandelbrot-symbolics.h>

#include <stdio.h>
#include <string.h>

typedef struct
{
  int p;
  char *lo;
  char *hi;
} ray;

void structure(ray *r, int j, int d, int n, int k)
{
  r[0].p = 3;
  r[0].lo = strdup("011");
  r[0].hi = strdup("100");
  // p38
  r[1].p = j * r[0].p + 2;;
  r[1].lo = calloc(1, r[1].p + 1);
  r[1].hi = calloc(1, r[1].p + 1);
  for (int i = 0; i < j; ++i)
  {
    strncat(r[1].lo, r[0].lo, r[1].p);
    strncat(r[1].hi, r[0].hi, r[1].p);
  }
  strncat(r[1].lo, "01", r[1].p);
  strncat(r[1].hi, "10", r[1].p);
  // p215
  r[2].p = d * r[1].p + k * r[0].p + 1;
  r[2].lo = calloc(1, r[2].p + 1);
  r[2].hi = calloc(1, r[2].p + 1);
  for (int i = d - 1; i >= 0; --i)
  {
    strncat(r[2].lo, ((n+0) & (1 << i)) ? r[1].hi : r[1].lo, r[2].p);
    strncat(r[2].hi, ((n+1) & (1 << i)) ? r[1].hi : r[1].lo, r[2].p);
  }
  for (int i = 0; i < k; ++i)
  {
    strncat(r[2].lo, r[0].hi, r[2].p);
    strncat(r[2].hi, r[0].lo, r[2].p);
  }
  strncat(r[2].lo, "1", r[2].p);
  strncat(r[2].hi, "0", r[2].p);
  if ((n + 1) == (1 << d))
  {
    char *tmp = r[2].lo; r[2].lo = r[2].hi; r[2].hi = tmp;
  }
  // p291
  r[3].p = r[2].p + 2 * r[1].p;
  r[3].lo = calloc(1, r[3].p + 1);
  r[3].hi = calloc(1, r[3].p + 1);
  if ((n + 1) == (1 << d)) // in cusp, arbitrary choice ?
  {
    strncat(r[3].lo, r[2].lo, r[3].p);
    strncat(r[3].hi, r[2].lo, r[3].p);
    strncat(r[3].lo, r[1].hi, r[3].p);
    strncat(r[3].hi, r[1].hi, r[3].p);
    strncat(r[3].lo, r[1].lo, r[3].p);
    strncat(r[3].hi, r[1].hi, r[3].p);
  }
  else if ((3 * n) < (1 << d)) // before 1/2 limb
  {
    strncat(r[3].lo, r[2].lo, r[3].p);
    strncat(r[3].hi, r[2].lo, r[3].p);
    strncat(r[3].lo, r[1].lo, r[3].p);
    strncat(r[3].hi, r[1].hi, r[3].p);
    strncat(r[3].lo, r[1].hi, r[3].p);
    strncat(r[3].hi, r[1].lo, r[3].p);
  }
  else if ((3 * n) < 2 * (1 << d)) // in 1/2 limb
  {
    strncat(r[3].lo, r[2].lo, r[3].p);
    strncat(r[3].hi, r[2].hi, r[3].p);
    strncat(r[3].lo, r[1].hi, r[3].p);
    strncat(r[3].hi, r[1].lo, r[3].p);
    strncat(r[3].lo, r[1].lo, r[3].p);
    strncat(r[3].hi, r[1].hi, r[3].p);
  }
  else // after 1/2 limb
  {
    strncat(r[3].lo, r[2].hi, r[3].p);
    strncat(r[3].hi, r[2].hi, r[3].p);
    strncat(r[3].lo, r[1].lo, r[3].p);
    strncat(r[3].hi, r[1].hi, r[3].p);
    strncat(r[3].lo, r[1].hi, r[3].p);
    strncat(r[3].hi, r[1].lo, r[3].p);
  }
  // p658
  r[4].p = 2 * r[3].p + 2 * r[1].p;
  r[4].lo = calloc(1, r[4].p + 1);
  r[4].hi = calloc(1, r[4].p + 1);
  strncat(r[4].lo, r[3].lo, r[4].p);
  strncat(r[4].hi, r[3].lo, r[4].p);
  strncat(r[4].lo, r[3].lo, r[4].p);
  strncat(r[4].hi, r[3].hi, r[4].p);
  strncat(r[4].lo, r[1].hi, r[4].p);
  strncat(r[4].hi, r[1].lo, r[4].p);
  strncat(r[4].lo, r[1].lo, r[4].p);
  strncat(r[4].hi, r[1].hi, r[4].p);
  // p1025
  r[5].p = r[4].p + r[3].p + 2 * r[1].p;
  r[5].lo = calloc(1, r[5].p + 1);
  r[5].hi = calloc(1, r[5].p + 1);
  strncat(r[5].lo, r[4].hi, r[5].p);
  strncat(r[5].hi, r[4].hi, r[5].p);
  strncat(r[5].lo, r[3].lo, r[5].p);
  strncat(r[5].hi, r[3].hi, r[5].p);
  strncat(r[5].lo, r[1].hi, r[5].p);
  strncat(r[5].hi, r[1].lo, r[5].p);
  strncat(r[5].lo, r[1].lo, r[5].p);
  strncat(r[5].hi, r[1].hi, r[5].p);
  // p2708 FIXME verify
  r[6].p = 2 * r[5].p + r[4].p;
  r[6].lo = calloc(1, r[6].p + 1);
  r[6].hi = calloc(1, r[6].p + 1);
  strncat(r[6].lo, r[5].hi, r[6].p);
  strncat(r[6].hi, r[5].hi, r[6].p);
  strncat(r[6].lo, r[5].lo, r[6].p);
  strncat(r[6].hi, r[5].hi, r[6].p);
  strncat(r[6].lo, r[4].hi, r[6].p);
  strncat(r[6].hi, r[4].lo, r[6].p);
  // p3733
  r[7].p = r[6].p + r[5].p;
  r[7].lo = calloc(1, r[7].p + 1);
  r[7].hi = calloc(1, r[7].p + 1);
  strncat(r[7].lo, r[6].lo, r[7].p);
  strncat(r[7].hi, r[6].hi, r[7].p);
  strncat(r[7].lo, r[5].hi, r[7].p);
  strncat(r[7].hi, r[5].lo, r[7].p);
  // p7466
  r[8].p = r[7].p + r[6].p + r[5].p;
  r[8].lo = calloc(1, r[8].p + 1);
  r[8].hi = calloc(1, r[8].p + 1);
  strncat(r[8].lo, r[7].lo, r[8].p);
  strncat(r[8].hi, r[7].lo, r[8].p);
  strncat(r[8].lo, r[6].lo, r[8].p);
  strncat(r[8].hi, r[6].hi, r[8].p);
  strncat(r[8].lo, r[5].lo, r[8].p);
  strncat(r[8].hi, r[5].hi, r[8].p);
  // p11199
  r[9].p = r[8].p + r[7].p;
  r[9].lo = calloc(1, r[9].p + 1);
  r[9].hi = calloc(1, r[9].p + 1);
  strncat(r[9].lo, r[8].lo, r[9].p);
  strncat(r[9].hi, r[8].hi, r[9].p);
  strncat(r[9].lo, r[7].lo, r[9].p);
  strncat(r[9].hi, r[7].hi, r[9].p);
}

int main(int argc, char **argv)
{
  if (argc != 5)
  {
    fprintf(stderr, "usage: %s j d n k\n", argv[0]);
    return 1;
  }
  ray rays[10];
  structure(rays, atoi(argv[1]), atoi(argv[2]), atoi(argv[3]), atoi(argv[4]));
  for (int i = 0; i < 10; ++i)
  {
    fprintf(stderr, " %d", rays[i].p);
  }
  fprintf(stderr, "\n");
  // initialize
  m_binangle b;
  m_binangle_init(&b);
  m_block_from_string(&b.pre, "");
  m_block_from_string(&b.per, rays[9].lo);
  const int final_period = b.per.length;
  mpq_t angle;
  mpq_init(angle);
  m_binangle_to_rational(angle, &b);
  m_binangle_clear(&b);
  int prec = 24;
  mpc_t nucleus, new_nucleus;
  mpc_init2(new_nucleus, 2 * prec + 24);
  mpc_init2(nucleus, 2 * prec + 24);
  mpc_set_dc(nucleus, -1, MPC_RNDNN);
  int nucleus_period = 2;
  m_r_orbit_d *orbit = m_r_orbit_d_new(nucleus, 0, nucleus_period);
  int m = 0;
  int target_period = rays[m].p;
  int sharpness = 4;
  m_d_exray_in_perturbed *ray = m_d_exray_in_perturbed_new(orbit, angle, sharpness);
  if (! ray)
  {
    return 1;
  }
  mpq_clear(angle);
  mpc_t endpoint;
  mpc_init2(endpoint, 53);
  double _Complex delta_c = 0;
  int depth = 0;
  printf("program = \"m-perturbator-gtk\"\nannotations = [\n");
  while (nucleus_period < final_period)
  {
    // trace ray
    while (depth < 1.125 * target_period * sharpness)
    {
      if (m_stepped != m_d_exray_in_perturbed_step(ray, 64))
      {
        fprintf(stderr, "%s: exray in failed at depth = %d\n", argv[0], depth);
        break;
        return 1;
      }
      depth++;
    }
    // compute new reference
    mpfr_set_prec(mpc_realref(new_nucleus), 2 * prec + 24);
    mpfr_set_prec(mpc_imagref(new_nucleus), 2 * prec + 24);
    mpc_set_dc(endpoint, m_d_exray_in_perturbed_get(ray), MPC_RNDNN);
    mpc_add(new_nucleus, nucleus, endpoint, MPC_RNDNN);
    if (m_converged != m_r_nucleus(new_nucleus, new_nucleus, target_period, 64, 1))
    {
      fprintf(stderr, "%s: nucleus failed at period = %d\n", argv[0], target_period);
      return 1;
    }
    // rebase ray to new reference
    mpc_sub(endpoint, new_nucleus, nucleus, MPC_RNDNN);
    delta_c = mpc_get_dc(endpoint, MPC_RNDNN);
    mpc_swap(new_nucleus, nucleus);
    nucleus_period = target_period;
    m_r_orbit_d_delete(orbit);
    orbit = m_r_orbit_d_new(nucleus, 0, nucleus_period);
    m_d_exray_in_perturbed_rebase(ray, delta_c, orbit);
    double domain_size = m_d_domain_size_orbit(orbit);
    double size = cabs(m_d_size_orbit(orbit));
    // output
    mpfr_printf("  { style = { rgb = [ 0.000000, 0.000000, 0.000000 ], fill_type = -1, line_type = 0 }, label = \"%d\", type = \"nucleus\", precision = %d, re = \"%Re\", im = \"%Re\", period = %d, domain_size = \"%.16e\", size = \"%.16e\" },\n"
    , nucleus_period
    , mpfr_get_prec(mpc_realref(nucleus))
    , mpc_realref(nucleus)
    , mpc_imagref(nucleus)
    , nucleus_period
    , domain_size
    , size
    );
    fflush(stdout);
    // prepare for next stage
    if (++m < 10)
    {
      target_period = rays[m].p;
      prec = prec * 3 / 2;
    }
  }
  mpfr_printf("]\nview.default = { precision = %d, re = \"%Re\", im = \"%Re\", radius = \"%.16e\", iterations = 65536 }\n"
  , mpfr_get_prec(mpc_realref(new_nucleus))
  , mpc_realref(new_nucleus)
  , mpc_imagref(new_nucleus)
  , 2 * cabs(delta_c)
  );
/*
  // compute viewing parameters
  mpfr_printf(
    "location.real = \"%Re\"\n"
    "location.imag = \"%Re\"\n"
    "location.zoom = \"%.16e\"\n"
    "reference.period = %d\n"
    "transform.rotate = %.16f\n"
  , mpc_realref(new_nucleus)
  , mpc_imagref(new_nucleus)
  , 2 / cabs(delta_c)
  , rays[8].p
  , carg(delta_c) * 360 / (2 * 3.141592653589793)
  );
*/
  fflush(stdout);
  // cleanup
  m_d_exray_in_perturbed_delete(ray);
  mpc_clear(nucleus);
  mpc_clear(new_nucleus);
  mpc_clear(endpoint);
  m_r_orbit_d_delete(orbit);
  return 0;
}
