// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2021 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>
#include "m-util.h"

static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage:\n%s precision center-re center-im radius maxperiod pixelradius\n"
      "%s precision period radius maxperiod pixelradius <<EOF\n"
      "center-re\ncenter-im\nEOF\n"
    , progname, progname
    );
}

#define height 360
#define width 640
static unsigned char ppm[4][height][width][3];

static void plot(unsigned char *p, int period)
{
  int r = 0, g = 0, b = 0;
  // deinterleaved bit reversal
  // 7
  r |= (period & (1 <<  0)) <<  7;
  g |= (period & (1 <<  1)) <<  6;
  b |= (period & (1 <<  2)) <<  5;
  // 6
  r |= (period & (1 <<  3)) <<  3;
  g |= (period & (1 <<  4)) <<  2;
  b |= (period & (1 <<  5)) <<  1;
  // 5
  r |= (period & (1 <<  6)) >>  1;
  g |= (period & (1 <<  7)) >>  2;
  b |= (period & (1 <<  8)) >>  3;
  // 4
  r |= (period & (1 <<  9)) >>  5;
  g |= (period & (1 << 10)) >>  6;
  b |= (period & (1 << 11)) >>  7;
  // 3
  r |= (period & (1 << 12)) >>  9;
  g |= (period & (1 << 13)) >> 10;
  b |= (period & (1 << 14)) >> 11;
  // 2
  r |= (period & (1 << 15)) >> 13;
  g |= (period & (1 << 16)) >> 14;
  b |= (period & (1 << 17)) >> 15;
  // 1
  r |= (period & (1 << 18)) >> 17;
  g |= (period & (1 << 19)) >> 18;
  b |= (period & (1 << 20)) >> 19;
  // 0
  r |= (period & (1 << 21)) >> 21;
  g |= (period & (1 << 22)) >> 22;
  b |= (period & (1 << 23)) >> 23;
  p[0] = r;
  p[1] = g;
  p[2] = b;
}

extern int main(int argc, char **argv) {
  if (argc != 7 && argc != 5) {
    usage(argv[0]);
    return 1;
  }
  bool native = true;
  int bits = 0;
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }
  if (native) {
    double cre = 0;
    double cim = 0;
    double radius = 0;
    double pixelradius = 0;
    int maxperiod = 0;
    if (argc == 5)
    {
      if (! stdin_double(&cre)) { return 1; }
      if (! stdin_double(&cim)) { return 1; }
      if (! arg_double(argv[2], &radius)) { return 1; }
      if (! arg_int(argv[3], &maxperiod)) { return 1; }
      if (! arg_double(argv[4], &pixelradius)) { return 1; }
    }
    else
    {
      if (! arg_double(argv[2], &cre)) { return 1; }
      if (! arg_double(argv[3], &cim)) { return 1; }
      if (! arg_double(argv[4], &radius)) { return 1; }
      if (! arg_int(argv[5], &maxperiod)) { return 1; }
      if (! arg_double(argv[6], &pixelradius)) { return 1; }
    }
    double r = 2 * radius * pixelradius / height;
    for (int y = 0; y < height; ++y)
    {
      for (int x = 0; x < width; ++x)
      {
        double _Complex c = (cim + ((y + 0.5) / height - 0.5) * 2 * radius) * I
                          + (cre + ((x + 0.5) / width - 0.5) * width / height * 2 * radius);
        plot(&ppm[0][y][x][0], m_d_box_period_do(c, r, maxperiod));
        plot(&ppm[1][y][x][0], m_d_near_period_do(c, r, maxperiod));
        int p = m_d_ball_period_do(c, r, maxperiod);
        plot(&ppm[2][y][x][0], p < 0 ? -p : p);
        plot(&ppm[3][y][x][0], p < 0 ?  0 : p);
      }
    }
    printf("P6\n%d %d\n255\n", width, 4 * height);
    fwrite(&ppm[0][0][0][0], width * height * 4 * 3, 1, stdout);
    return 0;
  } else {
    fprintf(stderr, "error: arbitrary precision not supported\n");
    return 1;
  }
  return 1;
}
