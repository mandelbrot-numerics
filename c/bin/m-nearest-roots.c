#include <mandelbrot-numerics.h>

#define NCPUS 8

int main()
{
  mpfr_t distance;
  mpfr_init2(distance, 53);
  for (int period = 4; period <= 65536; period <<= 1)
  {
    mpc_t nucleus[3];
    mpc_t delta;
    mpc_init2(nucleus[0], 4 * period);
    mpc_init2(nucleus[1], 4 * period);
    mpc_init2(nucleus[2], 4 * period);
    mpc_init2(delta, 4 * period);
    mpc_set_si(nucleus[0], -2, MPC_RNDNN);
    mpc_set_si(nucleus[1], -2, MPC_RNDNN);
    m_r_nucleus(nucleus[0], nucleus[0], period, 64, NCPUS);
    m_r_nucleus(nucleus[1], nucleus[1], period + 1, 64, NCPUS);
    mpc_sub(delta, nucleus[0], nucleus[1], MPC_RNDNN);
    mpc_add(nucleus[2], nucleus[0], delta, MPC_RNDNN);
    m_r_nucleus(nucleus[2], nucleus[2], period + 1, 64, NCPUS);
    mpc_sub(delta, nucleus[1], nucleus[2], MPC_RNDNN);
    mpc_abs(distance, delta, MPFR_RNDN);
    mpfr_log2(distance, distance, MPFR_RNDN);
    mpfr_div_si(distance, distance, period + 1, MPFR_RNDN);
    mpfr_printf("%6d\t%Re\n", period + 1, distance);
    mpc_clear(nucleus[0]);
    mpc_clear(nucleus[1]);
    mpc_clear(nucleus[2]);
    mpc_clear(delta);
  }
  return 0;
}
