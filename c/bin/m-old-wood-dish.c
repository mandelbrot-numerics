// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include <mandelbrot-symbolics.h>

#include <stdio.h>

// compute external angle
#define L "101010101010101010101010101010011"
#define L1(l) l "1"
#define L2(l) l l "1"
#define L9214 ".(" L1(L2(L1(L2(L1(L2(L1(L2(L1(L2(L1(L2(L1(L2(L1(L2(L1(L))))))))))))))))) ")"

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  // initialize
  m_binangle b;
  m_binangle_init(&b);
  m_binangle_from_string(&b, L9214);
  const int final_period = b.per.length;
  mpq_t angle;
  mpq_init(angle);
  m_binangle_to_rational(angle, &b);
  m_binangle_clear(&b);
  int prec = 24;
  mpc_t nucleus, new_nucleus;
  mpc_init2(new_nucleus, 2 * prec + 24);
  mpc_init2(nucleus, 2 * prec + 24);
  mpc_set_dc(nucleus, -1, MPC_RNDNN);
  int nucleus_period = 2;
  m_r_orbit_d *orbit = m_r_orbit_d_new(nucleus, 0, nucleus_period);
  int target_period = 34;
  int sharpness = 4;
  m_d_exray_in_perturbed *ray = m_d_exray_in_perturbed_new(orbit, angle, sharpness);
  if (! ray)
  {
    return 1;
  }
  mpq_clear(angle);
  mpc_t endpoint;
  mpc_init2(endpoint, 53);
  int depth = 0;
  while (nucleus_period < final_period)
  {
    // trace ray
    while (depth < target_period * sharpness)
    {
      if (m_stepped != m_d_exray_in_perturbed_step(ray, 64))
      {
        fprintf(stderr, "%s: exray in failed at depth = %d\n", argv[0], depth);
        return 1;
      }
      depth++;
    }
    // compute new reference
    mpfr_set_prec(mpc_realref(new_nucleus), 2 * prec + 24);
    mpfr_set_prec(mpc_imagref(new_nucleus), 2 * prec + 24);
    mpc_set_dc(endpoint, m_d_exray_in_perturbed_get(ray), MPC_RNDNN);
    mpc_add(new_nucleus, nucleus, endpoint, MPC_RNDNN);
    if (m_converged != m_r_nucleus(new_nucleus, new_nucleus, target_period, 64, 1))
    {
      fprintf(stderr, "%s: nucleus failed at period = %d\n", argv[0], target_period);
      return 1;
    }
    // rebase ray to new reference
    mpc_sub(endpoint, new_nucleus, nucleus, MPC_RNDNN);
    double _Complex delta_c = mpc_get_dc(endpoint, MPC_RNDNN);
    mpc_swap(new_nucleus, nucleus);
    nucleus_period = target_period;
    if (nucleus_period < final_period)
    {
      m_r_orbit_d_delete(orbit);
      orbit = m_r_orbit_d_new(nucleus, 0, nucleus_period);
      m_d_exray_in_perturbed_rebase(ray, delta_c, orbit);
    }
    // prepare for next stage
    target_period = 2 * target_period + 1 + 1;
    prec = prec * 3 / 2;
    // output
    mpfr_printf("%d %Re %Re\n", nucleus_period, mpc_realref(nucleus), mpc_imagref(nucleus));
    fflush(stdout);
  }
  // compute viewing parameters
  mpfr_t radius;
  mpfr_init2(radius, 53);
  m_r_domain_size(radius, nucleus, nucleus_period);
  mpfr_prec_round(radius, 53, MPFR_RNDN);
  double r = pow(mpfr_get_d(radius, MPFR_RNDN), 1.125);
  mpfr_set_prec(mpc_realref(new_nucleus), mpfr_get_prec(mpc_realref(nucleus)));
  mpfr_set_prec(mpc_imagref(new_nucleus), mpfr_get_prec(mpc_imagref(nucleus)));
  mpc_set(new_nucleus, nucleus, MPC_RNDNN);
  mpfr_add_d(mpc_realref(new_nucleus), mpc_realref(new_nucleus), 0.1 * r, MPFR_RNDN);
  m_r_nucleus(new_nucleus, new_nucleus, nucleus_period + nucleus_period / 2 - 1, 64, 1);
  mpc_sub(endpoint, new_nucleus, nucleus, MPC_RNDNN);
  mpc_abs(radius, endpoint, MPFR_RNDN);
  printf("radius = %g\n", 10 * mpfr_get_d(radius, MPFR_RNDN));
  mpc_arg(radius, endpoint, MPFR_RNDN);
  printf("degrees = %f\n", mpfr_get_d(radius, MPFR_RNDN) * 360 / (2 * 3.141592653589793));
  fflush(stdout);
  // cleanup
  m_d_exray_in_perturbed_delete(ray);
  mpc_clear(nucleus);
  mpc_clear(new_nucleus);
  mpc_clear(endpoint);
  mpfr_clear(radius);
  m_r_orbit_d_delete(orbit);
  return 0;
}
