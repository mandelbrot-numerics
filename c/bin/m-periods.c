// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2019 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mandelbrot-numerics.h>
#include "m-util.h"

static inline double cabs2(double _Complex z) {
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

int main(int argc, char **argv)
{
  if (argc < 5)
  {
    fprintf(stderr, "usage: %s precision maxperiod re im\n", argv[0]);
    return 1;
  }
  bool native = true;
  int bits = 0;
  int maxperiod = 0;
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }
  if (! arg_int(argv[2], &maxperiod)) { return 1; }
  if (native)
  {
    double cre = 0;
    double cim = 0;
    if (! arg_double(argv[3], &cre)) { return 1; }
    if (! arg_double(argv[4], &cim)) { return 1; }
    double mz2 = 1.0 / 0.0;
    double _Complex c = cre + I * cim;
    double _Complex z = 0;
    for (int i = 1; i <= maxperiod; ++i)
    {
      z = z * z + c;
      double z2 = cabs2(z);
      if (z2 > 1e10)
      {
        break;
      }
      if (z2 < mz2)
      {
        mz2 = z2;
        printf("%d\n", i);
        fflush(stdout);
      }
    }
  }
  else
  {
    mpc_t c;
    mpc_init2(c, bits);
    if (! arg_mpc(argv[3], argv[4], c)) { return 1; }
    mpc_t z;
    mpfr_t z2, mz2, er2;
    mpc_init2(z, bits);
    mpfr_init2(z2, 53);
    mpfr_init2(mz2, 53);
    mpfr_init2(er2, 53);
    mpfr_set_d(mz2, 1.0 / 0.0, MPFR_RNDN);
    mpfr_set_d(er2, 1e10, MPFR_RNDN);
    mpc_set_ui_ui(z, 0, 0, MPC_RNDNN);
    for (int i = 1; i <= maxperiod; ++i)
    {
      // z = z * z + c;
      mpc_sqr(z, z, MPC_RNDNN);
      mpc_add(z, z, c, MPC_RNDNN);
      mpc_norm(z2, z, MPFR_RNDN);
      if (mpfr_greater_p(z2, er2))
      {
        break;
      }
      if (mpfr_less_p(z2, mz2))
      {
        mpfr_set(mz2, z2, MPFR_RNDN);
        printf("%d\n", i);
        fflush(stdout);
      }
    }
    mpc_clear(c);
    mpc_clear(z);
    mpfr_clear(z2);
    mpfr_clear(mz2);
    mpfr_clear(er2);
  }
  return 0;
}
