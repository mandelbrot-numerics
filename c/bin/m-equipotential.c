// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2019 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>
#include <mandelbrot-symbolics.h>
#include "m-util.h"

static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage: %s precision count sharpness\n"
    , progname
    );
}

extern int main(int argc, char **argv) {
  if (argc != 4) {
    usage(argv[0]);
    return 1;
  }
  bool native = true;
  int bits = 0;
  int count = 0;
  int sharpness = 0;
  int newton = 64;
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }
  if (! arg_int(argv[2], &count)) { return 1; }
  if (! arg_int(argv[3], &sharpness)) { return 1; }
  int retval = 0;
  if (native)
  {
    mpq_t angle;
    mpq_init(angle);
    mpq_set_si(angle, 0, 1);
    mpq_canonicalize(angle);
    double _Complex c = m_d_exray_in_do(angle, sharpness, sharpness * count, newton);
    mpq_clear(angle);
    m_d_equipotential *ray = m_d_equipotential_new(c, sharpness, count);
    int64_t steps = ((int64_t) sharpness) << count;
    for (int64_t i = 0; i < steps; ++i)
    {
      printf("%.16e %.16e\n", creal(c), cimag(c));
      if (m_stepped != m_d_equipotential_step(ray, newton))
      {
        retval = 1;
        break;
      }
      c = m_d_equipotential_get(ray);
    }
    m_d_equipotential_delete(ray);
  }
  else
  {
    fprintf(stderr, "non-native equipotential not yet implemented\n");
    retval = 1;
  }
  return retval;
}
