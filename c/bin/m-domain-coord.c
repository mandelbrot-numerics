// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>
#include "m-util.h"

static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage: %s precision guess-re guess-im coord-re coord-im loperiod hiperiod maxsteps\n"
    , progname
    );
}

extern int main(int argc, char **argv) {
  if (argc != 9) {
    usage(argv[0]);
    return 1;
  }
  bool native = true;
  int bits = 0;
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }
  if (native) {
    double cre = 0;
    double cim = 0;
    double are = 0;
    double aim = 0;
    int loperiod = 0;
    int hiperiod = 0;
    int maxsteps = 0;
    if (! arg_double(argv[2], &cre)) { return 1; }
    if (! arg_double(argv[3], &cim)) { return 1; }
    if (! arg_double(argv[4], &are)) { return 1; }
    if (! arg_double(argv[5], &aim)) { return 1; }
    if (! arg_int(argv[6], &loperiod)) { return 1; }
    if (! arg_int(argv[7], &hiperiod)) { return 1; }
    if (! arg_int(argv[8], &maxsteps)) { return 1; }
    double _Complex c = 0;
    m_d_domain_coord(&c, cre + I * cim, are + I * aim, loperiod, hiperiod, maxsteps);
    printf("%.16e %.16e\n", creal(c), cimag(c));
    return 0;
  } else {
    mpc_t c_guess;
    mpc_t domain_coord;
    int loperiod = 0;
    int hiperiod = 0;
    int maxsteps = 0;
    mpc_init2(c_guess, bits);
    mpc_init2(domain_coord, bits);
    if (! arg_mpc(argv[2], argv[3], c_guess)) { return 1; }
    if (! arg_mpc(argv[4], argv[5], domain_coord)) { return 1; }
    if (! arg_int(argv[6], &loperiod)) { return 1; }
    if (! arg_int(argv[7], &hiperiod)) { return 1; }
    if (! arg_int(argv[8], &maxsteps)) { return 1; }
    mpc_t c_out;
    mpc_init2(c_out, bits);
    m_r_domain_coord(c_out, c_guess, domain_coord, loperiod, hiperiod, maxsteps);
    mpfr_printf("%Re %Re\n", mpc_realref(c_out), mpc_imagref(c_out));
    mpc_clear(c_out);
    mpc_clear(domain_coord);
    mpc_clear(c_guess);
    return 0;
  }
  return 1;
}
