#include <assert.h>
#include <stdio.h>
#include <mandelbrot-numerics.h>
#include <mandelbrot-symbolics.h>
#include "m-util.h"

int main(int argc, char **argv)
{
  if (! (argc == 3))
  {
    fprintf(stderr, "usage: %s preperiod period\n", argv[0]);
    return 1;
  }
  int preperiod = 0;
  int period = 1;
  int sharpness = 16;
  int maxsteps = 100;
  int ncpus = 0;
  arg_int(argv[1], &preperiod);
  assert(preperiod >= 0);
  assert(preperiod < 64);
  arg_int(argv[2], &period);
  assert(period > 0);
  assert(period < 64);
  mpfr_prec_t precision = 4 * (preperiod + period);
  int depth = (precision + 16) * sharpness;
  int digits = log10(2) * precision;
  m_binangle angle;
  m_binangle_init(&angle);
  mpq_t q;
  mpq_init(q);
  mpc_t endpoint, root, output, delta;
  mpc_init2(endpoint, precision + 16);
  mpc_init2(root, precision + 16);
  mpc_init2(output, precision);
  mpc_init2(delta, 16);
  mpfr_t distance;
  mpfr_init2(distance, 16);
  for (uint64_t pre = 0; pre < ((uint64_t)1) << preperiod; ++pre)
  {
    for (uint64_t per = 0; per < ((uint64_t)1) << period; ++per)
    {
      mpz_set_ui(angle.pre.bits, pre);
      angle.pre.length = preperiod;
      mpz_set_ui(angle.per.bits, per);
      angle.per.length = period;
      m_binangle_to_rational(q, &angle);
      m_binangle_from_rational(&angle, q);
      if (angle.pre.length == preperiod && angle.per.length == period &&
          mpz_get_ui(angle.pre.bits) == pre && mpz_get_ui(angle.per.bits) == per &&
          mpq_cmp_ui(q, 1, 2) <= 0)
      {
        m_r_exray_in *ray = m_r_exray_in_new(q, sharpness);
        m_newton r;
        for (int i = 0; i < depth; ++i)
        {
          r = m_r_exray_in_step(ray, maxsteps);
          if (r == m_failed)
          {
            fprintf(stderr, "FAIL %lu %lu %d\n", pre, per, i);
            abort();
          }
        }
        m_r_exray_in_get(ray, endpoint);
        mpc_set(root, endpoint, MPC_RNDNN);
        if (preperiod == 0)
        {
          r = m_r_nucleus(root, root, period, maxsteps, ncpus);
        }
        else
        {
          r = m_r_misiurewicz(root, root, preperiod + 1, period, maxsteps);
          if (r != m_failed)
          {
            r = m_r_misiurewicz_naive(root, root, preperiod + 1, period, maxsteps);
          }
        }
        if (r == m_failed)
        {
          fprintf(stderr, "FAIL %lu %lu\n", pre, per);
          abort();
        }
        mpc_sub(delta, root, endpoint, MPC_RNDNN);
        mpc_norm(distance, delta, MPFR_RNDN);
        mpfr_exp_t e = -mpfr_get_exp(distance);
        if (1 || (e > 2 * precision))
        {
          mpc_set(output, root, MPC_RNDNN);
          mpfr_abs(mpc_imagref(output), mpc_imagref(output), MPFR_RNDN);
          char *s = malloc(m_binangle_strlen(&angle));
          m_binangle_to_string(s, &angle);
          mpfr_printf("%s\t%Qd\t%.*Rf\t%.*Rf\n", s, q, digits, mpc_realref(output), digits, mpc_imagref(output));
          free(s);
          fflush(stdout);
        }
        else
        {
          fprintf(stderr, "FAIL %lu %lu e=%ld\n", pre, per, e);
          abort();
        }
      }
    }
  }
  return 0;
}
