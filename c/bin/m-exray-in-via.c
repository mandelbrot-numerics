// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#define MPFR_WANT_FLOAT128

#include <mandelbrot-numerics.h>
#include <mandelbrot-symbolics.h>

#include <string.h>
#include <time.h>

_Float128 _Complex mpc_get_qdc(const mpc_t z, mpc_rnd_t rnd)
{
  (void) rnd; // FIXME
  return mpfr_get_float128(mpc_realref(z), MPFR_RNDN) + I * mpfr_get_float128(mpc_imagref(z), MPFR_RNDN);
}

void mpc_set_qdc(mpc_t z, _Float128 _Complex s, mpc_rnd_t rnd)
{
  (void) rnd; // FIXME
  mpfr_set_float128(mpc_realref(z), crealf128(s), MPFR_RNDN);
  mpfr_set_float128(mpc_imagref(z), cimagf128(s), MPFR_RNDN);
}

int main(int argc, char **argv)
{
  if (! (argc > 1))
  {
    fprintf(stderr, "usage: echo $angle | %s p p p ...\n", argv[0]); // FIXME TODO support preperiodic
    return 1;
  }
  // initialize
  m_binangle b;
  m_binangle_init(&b);
  char *line = 0;
  size_t line_length = 0;
  getline(&line, &line_length, stdin);
  if (line[strlen(line) - 1] == '\n' || line[strlen(line) - 1] == '\r')
  {
    line[strlen(line) - 1] = '\0';
  }
  m_binangle_from_string(&b, line);
  free(line);
  mpq_t angle;
  mpq_init(angle);
  m_binangle_to_rational(angle, &b);
  m_binangle_clear(&b);
  int prec = 200;
  mpc_t nucleus, new_nucleus;
  mpc_init2(new_nucleus, 2 * prec + 24);
  mpc_init2(nucleus, 2 * prec + 24);
  mpc_set_dc(nucleus, 0, MPC_RNDNN);
  int nucleus_period = 1;
  m_r_orbit_qd *orbit = m_r_orbit_qd_new(nucleus, 0, nucleus_period);
  int m = 1;
  int target_period = atoi(argv[m]);
  int sharpness = 4;
  int maxsteps = 8;
  m_qd_exray_in_perturbed *ray = m_qd_exray_in_perturbed_new(orbit, angle, sharpness);
  if (! ray)
  {
    return 1;
  }
  mpc_t endpoint;
  mpc_init2(endpoint, 113);
  _Float128 _Complex delta_c = 0;
  int depth = 0;
  while (m < argc)
  {
    // trace ray
    time_t then = time(0);
    while (depth < 1.05 * target_period * sharpness + 24)
    {
      time_t now = time(0);
      if (now >= then + 10)
      {
        fprintf(stderr, "%f%%\r", depth * 100 / (1.05 * target_period * sharpness + 24));
        then = now;
      }
      if (m_stepped != m_qd_exray_in_perturbed_step(ray, maxsteps))
      {
        //fprintf(stderr, "%s: warning: exray failed at depth = %d\n", argv[0], depth);
        break;
      }
      depth++;
    }
    // compute new reference
    mpfr_set_prec(mpc_realref(new_nucleus), 2 * prec + 24);
    mpfr_set_prec(mpc_imagref(new_nucleus), 2 * prec + 24);
    mpc_set_qdc(endpoint, m_qd_exray_in_perturbed_get(ray), MPC_RNDNN);
    mpc_add(new_nucleus, nucleus, endpoint, MPC_RNDNN);
    if (m_failed == m_r_nucleus(new_nucleus, new_nucleus, target_period, 64, 1))
    {
      fprintf(stderr, "%s: error: nucleus failed at period = %d (depth = %d)\n", argv[0], target_period, depth);
      return 1;
    }

    // adaptive precision
    m_r_size(endpoint, new_nucleus, target_period);
    prec = 53 - (int) log2f128(cabsf128(mpc_get_qdc(endpoint, MPC_RNDNN)));
fprintf(stderr, "%d\n", prec);
    if (prec < 0) prec = 1000;
    mpfr_prec_round(mpc_realref(new_nucleus), prec, MPFR_RNDN);
    mpfr_prec_round(mpc_imagref(new_nucleus), prec, MPFR_RNDN);

    // output
    mpfr_printf("%d %Re %Re %.16Le %.16Lf\n", target_period, mpc_realref(new_nucleus), mpc_imagref(new_nucleus), (long double) cabsf128(delta_c), (long double) cargf128(delta_c) * 360 / (2 * 3.14159265358979));
    fflush(stdout);

    // rebase ray to new reference
    mpc_sub(endpoint, new_nucleus, nucleus, MPC_RNDNN);
    delta_c = mpc_get_qdc(endpoint, MPC_RNDNN);
    mpc_swap(new_nucleus, nucleus);
    nucleus_period = target_period;
    m_r_orbit_qd_delete(orbit);
    orbit = m_r_orbit_qd_new(nucleus, 0, nucleus_period);
    if (! orbit)
    {
      fprintf(stderr, "%s: error: orbit failed at period = %d\n", argv[0], nucleus_period);
      return 1;
    }
#if 0
    // rebase
    m_qd_exray_in_perturbed_rebase(ray, delta_c, orbit);
#else
    // restart
    m_qd_exray_in_perturbed_delete(ray);
    ray = m_qd_exray_in_perturbed_new(orbit, angle, sharpness);
    depth = 0;
#endif
    // prepare for next stage
    if (++m < argc)
    {
      target_period = atoi(argv[m]);
    }
  }
  // cleanup
  m_qd_exray_in_perturbed_delete(ray);
  mpc_clear(nucleus);
  mpc_clear(new_nucleus);
  mpc_clear(endpoint);
  m_r_orbit_qd_delete(orbit);
  mpq_clear(angle);
  return 0;
}
