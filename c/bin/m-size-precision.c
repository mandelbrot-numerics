// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*

for antenna:
need nucleus precision at least 2x period to compute size and domain size
need nucleus precision at least 4x period to ensure nucleus within atom
atom size ~ A/16^period
domain size ~ B/4^period ~ sqrt(atom size)

*/
#include <stdio.h>
#include <mandelbrot-numerics.h>

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  for (int period = 1; period < 64; period += 1)
  {
    printf("# period %d\n", period);
    fflush(stdout);
    mpfr_prec_t prec = 16 * period + 8;
    mpc_t guess, nucleus, size;
    mpc_init2(guess, prec);
    mpc_init2(nucleus, prec);
    mpc_init2(size, prec);
    mpc_set_d(guess, -2, MPC_RNDNN);
    while (prec > 2)
    {
      mpfr_prec_round(mpc_realref(guess), prec, MPFR_RNDN);
      mpfr_prec_round(mpc_imagref(guess), prec, MPFR_RNDN);
      m_r_nucleus(nucleus, guess, period, 64, 8);
      printf("%ld ", prec);
      fflush(stdout);
      m_r_size(size, nucleus, period);
      mpc_norm(mpc_realref(size), size, MPFR_RNDN);
      mpfr_log2(mpc_realref(size), mpc_realref(size), MPFR_RNDN);
      mpfr_div_2ui(mpc_realref(size), mpc_realref(size), 1, MPFR_RNDN);
      mpfr_printf("%Re ", mpc_realref(size));
      fflush(stdout);
      m_r_domain_size(mpc_realref(size), nucleus, period);
      mpfr_prec_round(mpc_realref(size), 53, MPFR_RNDN);
      mpfr_log2(mpc_realref(size), mpc_realref(size), MPFR_RNDN);
      mpfr_printf("%Re\n", mpc_realref(size));
      fflush(stdout);
      prec--;
      mpfr_prec_round(mpc_realref(nucleus), prec, MPFR_RNDN);
      mpfr_prec_round(mpc_imagref(nucleus), prec, MPFR_RNDN);
    }
    printf("\n\n");
    fflush(stdout);
  }
  return 0;
}
