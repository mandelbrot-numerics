// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <mandelbrot-numerics.h>
#include <mandelbrot-symbolics.h>

#define pi 3.141592653589793

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  const int sharpness = 8;
  for (int period = 4; period < 12; ++period)
  {
    printf("period %d\n", period);
    mpq_t lo, hi;
    mpq_init(lo);
    mpq_init(hi);
    uint64_t num = 1ull << (period - 2);
    uint64_t den = (1ull << period) - 1;
    printf("(%ld/%ld, %ld/%ld)\n", num - 1, den, num, den);
    mpq_set_ui(lo, num - 1, den);
    mpq_set_ui(hi, num, den);
    mpq_canonicalize(lo);
    mpq_canonicalize(hi);

    char filename[100];
    snprintf(filename, sizeof(filename), "p%d.log", period);
    FILE *f = fopen(filename, "wb");
    double _Complex c;
    for (int j = 0; j < 2; ++j)
    {
      m_d_exray_in *ray = m_d_exray_in_new(j ? lo : hi, sharpness);
      for (int i = 0; i < sharpness * period * 100; ++i)
      {
        m_newton result = m_d_exray_in_step(ray, 64);
        c = m_d_exray_in_get(ray);
        fprintf(f, "%.16e %.16e\n", creal(c), cimag(c));
        if (result != m_stepped)
        {
          break;
        }
      }
      printf("%.16e %.16e\n", creal(c), cimag(c));
      m_d_exray_in_delete(ray);
      fprintf(f, "\n");
    }
    double _Complex cp;
    m_d_nucleus(&cp, c, period, 64);
    printf("root %.16e %.16e\n", creal(cp), cimag(cp));
    double _Complex cp2;
    m_d_nucleus(&cp2, cp, period * 2, 64);
    printf("bulb %.16e %.16e\n", creal(cp2), cimag(cp2));
    double _Complex cp21[4] = {0,0,0,0};
    double _Complex z = 0;
    c = cp;
    for (int i = 0; i <= 360; ++i)
    {
      double a = 2 * pi * (i + 0.5) / 360;
      m_d_interior(&z, &c, z, c, cexp(I * a), period, 64);
      fprintf(f, "%.16e %.16e\n", creal(c), cimag(c));
    }
    fprintf(f, "\n");
    z = 0;
    c = cp2;
    for (int i = 0; i <= 360; ++i)
    {
      double a = 2 * pi * (i + 0.5) / 360;
      m_d_interior(&z, &c, z, c, cexp(I * a), period * 2, 64);
      fprintf(f, "%.16e %.16e\n", creal(c), cimag(c));
    }
    fprintf(f, "\n");
    for (int which = 0; which < 2; ++which)
    {
      m_d_nucleus_homotopy *path = m_d_nucleus_homotopy_new(cp, 2 * period, which);
      if (! path) abort();
      while (m_stepped == m_d_nucleus_homotopy_step(path))
      {
        c = m_d_nucleus_homotopy_get(path);
        fprintf(f, "%.16e %.16e\n", creal(c), cimag(c));
      }
      m_d_nucleus_homotopy_delete(path);
      fprintf(f, "\n");
      m_d_nucleus(&cp21[which], c, 2 * period + 1, 64);
    }
    for (int which = 0; which < 2; ++which)
    {
      m_d_nucleus_homotopy *path = m_d_nucleus_homotopy_new(cp2, 2 * period, which);
      if (! path) abort();
      while (m_stepped == m_d_nucleus_homotopy_step(path))
      {
        c = m_d_nucleus_homotopy_get(path);
        fprintf(f, "%.16e %.16e\n", creal(c), cimag(c));
      }
      m_d_nucleus_homotopy_delete(path);
      fprintf(f, "\n");
      m_d_nucleus(&cp21[2 + which], c, 2 * period + 1, 64);
    }
    double R = 1.5 * fmax(fmax(fmax(cabs(cp21[0] - cp), cabs(cp21[1] - cp)), cabs(cp21[2] - cp)), cabs(cp21[3] - cp));
    double r = 2.5 * cabs(m_d_size(cp, period));
    double x = creal(cp);
    double y = cimag(cp);

    j_d_exray_in *ray[4][2] = {{0,0},{0,0},{0,0},{0,0}};
    for (int i = 0; i < 4; ++i)
    {
      ray[i][0] = j_d_exray_in_new(cp21[i], lo, sharpness);
      ray[i][1] = j_d_exray_in_new(cp21[i], hi, sharpness);
    }
    bool ok = true; 
    int which = -1;
    for (int i = 0; ok; ++i)
    {
      double _Complex e[4][2];
      double d[4];
      for (int j = 0; ok && j < 4; ++j)
      {
        for (int k = 0; ok && k < 2; ++k)
        {
          m_newton result = j_d_exray_in_step(ray[j][k], 64);
          ok &= result == m_stepped;
          e[j][k] = j_d_exray_in_get(ray[j][k]);
        }
        d[j] = cabs(e[j][0] - e[j][1]);
      }
      if (! ok)
      {
        printf("FAILED after %d dynamic ray steps\n", i);
        for (int j = 0; j < 4; ++j)
        {
          printf("j = %d ; d = %g ; e[0] = %.16e %.16e ; e[1] = %.16e %.16e\n", j, d[j], creal(e[j][0]), cimag(e[j][0]), creal(e[j][1]), cimag(e[j][1]));
        }
        break;
      }
      double minimum_d = fmin(fmin(fmin(d[0], d[1]), d[2]), d[3]);
      double next_d = 1.0/0.0;
      for (int j = 0; j < 4; ++j)
      {
        if (d[j] > minimum_d)
        {
          next_d = fmin(next_d, d[j]);
        }
      }
      if (minimum_d < 0.1 && minimum_d < 0.5 * next_d)
      {
        for (int j = 0; j < 4; ++j)
        {
          if (d[j] == minimum_d)
          {
            which = j;
          }
        }
        printf("%d dynamic ray steps\n", i);
        break;
      }
    }
    for (int i = 0; i < 4; ++i)
    {
      for (int j = 0; j < 2; ++j)
      {
        j_d_exray_in_delete(ray[i][j]);
      }
    }

    double _Complex root[4] = {0,0,0,0};
    if (which >= 0)
    {
      double _Complex d0 = cp21[which] - cp;
      for (int i = 0; i < 4; ++i)
      {
        double _Complex d1 = cp21[i] - cp;
        double cos_theta = (creal(d0) * creal(d1) + cimag(d0) * cimag(d1)) / cabs(d0 * d1);
        double sgn_theta = creal(d0) * cimag(d1) >= cimag(d0) * creal(d1) ? 1 : -1;
        double theta = sgn_theta * acos(fmin(fmax(cos_theta, -1), 1)) * 360 / (2 * pi);
        int quadrant = 2;
        if (-45 <= theta && theta < 45)
        {
          quadrant = 0;
        }
        if (45 <= theta && theta < 135)
        {
          quadrant = 1;
        }
        if (-135 <= theta && theta < -45)
        {
          quadrant = 3;
        }
        printf("quadrant %d root %.16f %.16f angle %.3f\n", quadrant, creal(cp21[i]), cimag(cp21[i]), theta);
        root[quadrant] = cp21[i];
      }
      for (int quadrant = 0; quadrant < 4; ++quadrant)
      {
        for (int j = 0; j <= quadrant; ++j)
        {
          double radius = 0.05 * pow(2, (j + 0.5) / 4) * R;
          for (int i = 0; i <= 360; ++i)
          {
            double a = 2 * pi * i / 360;
            c = root[quadrant] + radius * (cos(a) + I * sin(a));
            fprintf(f, "%.16e %.16e\n", creal(c), cimag(c));
          }
          fprintf(f, "\n");
        }
      }
    }

    fclose(f);
    snprintf(filename, sizeof(filename), "p%d.gnuplot", period);
    f = fopen(filename, "wb");
    fprintf
      ( f
      , "set term pngcairo enhanced size 1024,1024\n"
        "set size square\n"
        "set grid\n"
        "unset key\n"
        "set title \"Period %d homotopy paths near period %d root\n"
        "set output \"p%d-near.png\"\n"
        "plot [%.16e:%.16e] [%.16e:%.16e] \"p%d.log\" u 1:2:-1 w l lc variable\n"
        "set output \"p%d-far.png\"\n"
        "plot [%.16e:%.16e] [%.16e:%.16e] \"p%d.log\" u 1:2:-1 w l lc variable\n"
      , 2 * period + 1, period
      , period, x - r, x + r, y - r, y + r, period
      , period, x - R, x + R, y - R, y + R, period
      );
    fclose(f);
    m_binangle blo;
    m_binangle bhi;
    m_binangle_init(&blo);
    m_binangle_init(&bhi);
    m_binangle_from_rational(&blo, lo);
    m_binangle_from_rational(&bhi, hi);
    char slo[100];
    char shi[100];
    m_binangle_to_string(slo, &blo);
    m_binangle_to_string(shi, &bhi);
    m_binangle_clear(&blo);
    m_binangle_clear(&bhi);
    double radius[2] = { r, R };
    for (int i = 0; i < 2; ++i)
    {
      snprintf(filename, sizeof(filename), "p%d-%s-m.p", period, i ? "far" : "near");
      f = fopen(filename, "wb");
      fprintf
        ( f
        , "program = \"m-perturbator-gtk\"\n"
          "version =\"v0.2-3-g57bae2f\"\n"
          "width = 1024\n"
          "height = 1024\n"
          "theme = \"light\"\n"
          "colour = \"low\"\n"
          "key = \"none\"\n"
          "view.default = { precision = 53, re = \"%.16e\", im = \"%.16e\", radius = \"%g\", iterations = 65536 }\n"
          "annotations = [\n"
          "  { style = { rgb = [ 0.000000, 0.000000, 0.000000 ], fill_type = -1, line_type = 0 }, label = \"%d\", type = \"nucleus\", precision = 53, re = \"%.16e\", im = \"%.16e\", period = %d, domain_size = \"1\", size = \"1\" },\n"
          "  { style = { rgb = [ 0.000000, 0.000000, 0.000000 ], fill_type = -1, line_type = 0 }, label = \"%d\", type = \"nucleus\", precision = 53, re = \"%.16e\", im = \"%.16e\", period = %d, domain_size = \"1\", size = \"1\" },\n"
          "  { style = { rgb = [ 0.000000, 0.000000, 0.000000 ], fill_type = -1, line_type = 0 }, label = \"%d (%s)\", type = \"nucleus\", precision = 53, re = \"%.16e\", im = \"%.16e\", period = %d, domain_size = \"1\", size = \"1\" },\n"
          "  { style = { rgb = [ 0.000000, 0.000000, 0.000000 ], fill_type = -1, line_type = 0 }, label = \"%d (%s)\", type = \"nucleus\", precision = 53, re = \"%.16e\", im = \"%.16e\", period = %d, domain_size = \"1\", size = \"1\" },\n"
          "  { style = { rgb = [ 0.000000, 0.000000, 0.000000 ], fill_type = -1, line_type = 0 }, label = \"%d (%s)\", type = \"nucleus\", precision = 53, re = \"%.16e\", im = \"%.16e\", period = %d, domain_size = \"1\", size = \"1\" },\n"
          "  { style = { rgb = [ 0.000000, 0.000000, 0.000000 ], fill_type = -1, line_type = 0 }, label = \"%d (%s)\", type = \"nucleus\", precision = 53, re = \"%.16e\", im = \"%.16e\", period = %d, domain_size = \"1\", size = \"1\" },\n"
          "  { style = { rgb = [ 1.000000, 0.000000, 0.000000 ], fill_type = -1, line_type = 0 }, label = \"%s\", type = \"ray_in\", depth = %d, angle = \"%s\" },\n"
          "  { style = { rgb = [ 1.000000, 0.000000, 0.000000 ], fill_type = -1, line_type = 0 }, label = \"%s\", type = \"ray_in\", depth = %d, angle = \"%s\" }\n"
          "]\n"
        , x, y, radius[i]
        , period, creal(cp), cimag(cp), period
        , 2 * period, creal(cp2), cimag(cp2), 2 * period
        , 2 * period + 1, "cusp", creal(root[0]), cimag(root[0]), 2 * period + 1
        , 2 * period + 1, "1/3",  creal(root[1]), cimag(root[1]), 2 * period + 1
        , 2 * period + 1, "1/2",  creal(root[2]), cimag(root[2]), 2 * period + 1
        , 2 * period + 1, "2/3",  creal(root[3]), cimag(root[3]), 2 * period + 1
        , slo, 10 * period, slo
        , shi, 10 * period, shi
        );
      fclose(f);
    }
    mpq_clear(lo);
    mpq_clear(hi);
    printf("\n");
  }
  return 0;
}
