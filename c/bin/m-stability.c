#include <math.h>
#include <stdint.h>
#include <stdio.h>

#include <mpfr.h>

int main(int argc, char **argv)
{
  uint64_t exact = 0;
  uint64_t histogram[256];
  for (uint64_t k = 0; k < 256; ++k)
  {
    histogram[k] = 0;
  }
  uint64_t otherwise = 0;
  uint64_t total = 0;
  const uint64_t depth = 11;
  const mpfr_prec_t prec = depth + 1;
  {
    const uint64_t range = 1 << depth;
    #pragma omp parallel for
    for (uint64_t j = 0; j < range; ++j)
    {
      mpfr_t fx, fy, fp, fm, fr, fi;
      mpfr_init2(fx, prec);
      mpfr_init2(fy, prec);
      mpfr_init2(fp, prec);
      mpfr_init2(fm, prec);
      mpfr_init2(fr, prec);
      mpfr_init2(fi, prec);
      mpfr_set_d(fy, (((j << 1) + 1.) / (range << 1)) * 4. - 2., MPFR_RNDN);
      const float dy = mpfr_get_d(fy, MPFR_RNDN);
      for (uint64_t i = 0; i < range; ++i)
      {
        mpfr_set_d(fx, (((i << 1) + 1.) / (range << 1)) * 4. - 2., MPFR_RNDN);
        const double dx = mpfr_get_d(fx, MPFR_RNDN);

        mpfr_add(fp, fx, fy, MPFR_RNDN);
        mpfr_add(fm, fx, fy, MPFR_RNDN);
        mpfr_mul(fr, fp, fm, MPFR_RNDN);
        mpfr_mul(fi, fx, fy, MPFR_RNDN);
        mpfr_mul_2exp(fi, fi, 1, MPFR_RNDN);

        const double ex = (dx + dy) * (dx - dy);
        const double ey = 2. * dx * dy;

        const double hx = mpfr_get_d(fr, MPFR_RNDN);
        const double hy = mpfr_get_d(fi, MPFR_RNDN);

        const double cx = hx - ex;
        const double cy = hy - ey;
        const double c = cx * cx + cy * cy;
        
        int b = 0;
        frexp(c, &b);
        const int k = -b;
        if (c == 0.0)
        {
          #pragma omp atomic
          exact++;
        }
        else if (0 <= k && k < 256)
        {
          #pragma omp atomic
          histogram[k]++;
        }
        else
        {
          #pragma omp atomic
          otherwise++;
        }
      }
      mpfr_clear(fx);
      mpfr_clear(fy);
      mpfr_clear(fp);
      mpfr_clear(fm);
      mpfr_clear(fr);
      mpfr_clear(fi);
    }
    total += range * range;
    for (uint64_t k = 0; k < 256; ++k)
    {
      printf("%g\n", histogram[k] / (double) total);
    }
    printf("\n\n");
    fflush(stdout);
  }
  return 0;
}
