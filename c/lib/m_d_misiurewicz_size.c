// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

extern double m_d_misiurewicz_size(double _Complex c, int preperiod, int period)
{
  double _Complex z = 0;
  double _Complex dc = 0;
  double _Complex zp = 0;
  double _Complex dcp = 0;
  for (int p = 0; p < period; ++p)
  {
    dcp = 2 * zp * dcp + 1;
    zp = zp * zp + c;
  }
  double zq2 = cabs2(zp - z);
  for (int q = 1; q <= preperiod; ++q)
  {
    dcp = 2 * zp * dcp + 1;
    zp = zp * zp + c;
    dc = 2 * z * dc + 1;
    z = z * z + c;
    double zp2 = cabs2(zp - z);
    if (q < preperiod && zp2 < zq2)
      zq2 = zp2;
  }
  return sqrt(zq2) / cabs(dcp - dc);
}

extern double m_d_misiurewicz_size_orbit(const m_r_orbit_d *orbit)
{
  const double _Complex *z_ptr = m_r_orbit_d_get_z_ptr(orbit);
  const int preperiod = m_r_orbit_d_get_preperiod(orbit);
  const int period = m_r_orbit_d_get_period(orbit);
  double _Complex dc = 0;
  double _Complex dcp = 0;
  for (int p = 0; p < period; ++p)
  {
    dcp = 2 * z_ptr[p] * dcp + 1;
  }
  double zq2 = cabs(z_ptr[period]);
  for (int q = 1; q <= preperiod; ++q)
  {
    dcp = 2 * z_ptr[period + q - 1] * dcp + 1;
    dc = 2 * z_ptr[q - 1] * dc + 1;
    if (q < preperiod)
    {
      double zp2 = cabs(z_ptr[period + q] - z_ptr[q]);
      if (zp2 < zq2)
        zq2 = zp2;
    }
  }
  return zq2 / cabs(dcp - dc);
}
