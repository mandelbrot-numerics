// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

static double cross(double _Complex a, double _Complex b) {
  return cimag(a) * creal(b) - creal(a) * cimag(b);
}

static bool crosses_positive_real_axis(double _Complex a, double _Complex b) {
  if (sgn(cimag(a)) != sgn(cimag(b))) {
    double _Complex d = b - a;
    int s = sgn(cimag(d));
    int t = sgn(cross(d, a));
    return s == t;
  }
  return false;
}

static bool surrounds_origin(double _Complex a, double _Complex b, double _Complex c, double _Complex d) {
  return odd
    ( crosses_positive_real_axis(a, b)
    + crosses_positive_real_axis(b, c)
    + crosses_positive_real_axis(c, d)
    + crosses_positive_real_axis(d, a)
    );
}

struct m_d_box_misiurewicz {
  double _Complex center;
  double _Complex w, wp;
  double _Complex c[4];
  double _Complex z[4];
  int preperiod;
  int period;
};

extern m_d_box_misiurewicz *m_d_box_misiurewicz_new(double _Complex center, double radius) {
  m_d_box_misiurewicz *box = (m_d_box_misiurewicz *) malloc(sizeof(*box));
  if (! box) {
    return 0;
  }
  box->center = center;
  box->w = center;
  box->z[0] = box->c[0] = center + ((-radius) + I * (-radius));
  box->z[1] = box->c[1] = center + (( radius) + I * (-radius));
  box->z[2] = box->c[2] = center + (( radius) + I * ( radius));
  box->z[3] = box->c[3] = center + ((-radius) + I * ( radius));
  box->preperiod = 1;
  box->period = 0;
  return box;
}

extern void m_d_box_misiurewicz_delete(m_d_box_misiurewicz *box) {
  if (box) {
    free(box);
  }
}

extern bool m_d_box_misiurewicz_step(m_d_box_misiurewicz *box) {
  if (! box) {
    return false;
  }
  box->w = box->w * box->w + box->center;
  bool ok = cisfinite(box->w);
  for (int i = 0; i < 4; ++i) {
    box->z[i] = box->z[i] * box->z[i] + box->c[i];
    ok = ok && cisfinite(box->z[i]);
  }
  box->preperiod++;
  return ok;
}

extern bool m_d_box_misiurewicz_check_periods(m_d_box_misiurewicz *box, int maxperiod) {
  if (! box) {
    return true;
  }
  double _Complex z = box->w;
  for (int period = 1; period <= maxperiod; ++period)
  {
    z = z * z + box->center;
    if (surrounds_origin(box->z[0] - z, box->z[1] - z, box->z[2] - z, box->z[3] - z))
    {
      box->period = period;
      return true;
    }
  }
  return false;
}

extern int m_d_box_misiurewicz_get_period(const m_d_box_misiurewicz *box) {
  if (! box) {
    return 0;
  }
  return box->period;
}

extern int m_d_box_misiurewicz_get_preperiod(const m_d_box_misiurewicz *box) {
  if (! box) {
    return -1;
  }
  return box->preperiod;
}

extern bool m_d_box_misiurewicz_do(int *preperiod, int *period, double _Complex center, double radius, int maxpreperiod, int maxperiod) {
  m_d_box_misiurewicz *box = m_d_box_misiurewicz_new(center, radius);
  if (! box) {
    return false;
  }
  for (int i = 0; i < maxpreperiod; ++i) {
    if (m_d_box_misiurewicz_check_periods(box, maxperiod)) {
      *preperiod = m_d_box_misiurewicz_get_preperiod(box);
      *period = m_d_box_misiurewicz_get_period(box);
      m_d_box_misiurewicz_delete(box);
      return true;
    }
    if (! m_d_box_misiurewicz_step(box)) {
      break;
    }
  }
  m_d_box_misiurewicz_delete(box);
  return false;
}
