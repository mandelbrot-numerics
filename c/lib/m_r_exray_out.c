// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

static inline bool mpc_finite_p(const mpc_t z) {
  return mpfr_number_p(mpc_realref(z)) && mpfr_number_p(mpc_imagref(z));
}

static double dwell(double loger2, int n, double zmag2) {
  return n - log2(log(zmag2) / loger2);
}

struct m_r_exray_out {
  int sharpness;
  mpfr_t er;
  mpfr_t er2;
  double loger2;
  mpc_t c;
  mpc_t z;
  double d;
  int n;
  int bit;
  mpfr_t z2;
  double erd;
  mpc_t k[2];
  mpc_t cn[2];
  mpc_t zn[2];
  mpc_t dcn[2];
  mpc_t c_new[2];
  mpfr_t d2[2];
};

extern m_r_exray_out *m_r_exray_out_new(const mpc_t c, int sharpness, int maxdwell, double er) {
  m_r_exray_out *ray = malloc(sizeof(*ray));
  if (! ray) {
    return 0;
  }
  mpfr_prec_t precr = mpfr_get_prec(mpc_realref(c));
  mpfr_prec_t preci = mpfr_get_prec(mpc_imagref(c));
  mpfr_prec_t prec = precr > preci ? precr : preci;
  mpfr_init2(ray->er, 53);
  mpfr_init2(ray->er2, 53);
  mpc_init2(ray->c, prec);
  mpc_init2(ray->z, prec);
  mpfr_init2(ray->z2, 53);
  mpc_init2(ray->k    [0], 53);
  mpc_init2(ray->cn   [0], prec);
  mpc_init2(ray->zn   [0], prec);
  mpc_init2(ray->dcn  [0], prec);
  mpc_init2(ray->c_new[0], prec);
  mpfr_init2(ray->d2  [0], 53);
  mpc_init2(ray->k    [1], 53);
  mpc_init2(ray->cn   [1], prec);
  mpc_init2(ray->zn   [1], prec);
  mpc_init2(ray->dcn  [1], prec);
  mpc_init2(ray->c_new[1], prec);
  mpfr_init2(ray->d2  [1], 53);

  double er2 = er * er;
  mpfr_set_d(ray->er, er, MPFR_RNDN);
  mpfr_set_d(ray->er2, er2, MPFR_RNDN);
  ray->erd = er;
  ray->loger2 = log(er2);
  int n = 0;
  mpc_set_ui_ui(ray->z, 0, 0, MPC_RNDNN);
  for (int i = 0; i < maxdwell; ++i) {
    n = n + 1;
    mpc_sqr(ray->z, ray->z, MPC_RNDNN);
    mpc_add(ray->z, ray->z, c, MPC_RNDNN);
    mpc_norm(ray->z2, ray->z, MPFR_RNDN);
    if (mpfr_greater_p(ray->z2, ray->er2)) {
      break;
    }
  }
  if (! (mpfr_greater_p(ray->z2, ray->er2))) {
    // FIXME cleanup
    return 0;
  }
  ray->sharpness = sharpness;
  mpc_set(ray->c, c, MPC_RNDNN);
  ray->d = dwell(ray->loger2, n, mpfr_get_d(ray->z2, MPFR_RNDN));
  ray->n = n;
  ray->bit = -1;
  return ray;
}

extern void m_r_exray_out_delete(m_r_exray_out *ray) {
  if (ray) {
    mpfr_clear(ray->er);
    mpfr_clear(ray->er2);
    mpc_clear(ray->c);
    mpc_clear(ray->z);
    mpfr_clear(ray->z2);
    mpc_clear(ray->k    [0]);
    mpc_clear(ray->cn   [0]);
    mpc_clear(ray->zn   [0]);
    mpc_clear(ray->dcn  [0]);
    mpc_clear(ray->c_new[0]);
    mpfr_clear(ray->d2  [0]);
    mpc_clear(ray->k    [1]);
    mpc_clear(ray->cn   [1]);
    mpc_clear(ray->zn   [1]);
    mpc_clear(ray->dcn  [1]);
    mpc_clear(ray->c_new[1]);
    mpfr_clear(ray->d2  [1]);
    free(ray);
  }
}

extern m_newton m_r_exray_out_step(m_r_exray_out *ray) {
  if (! ray) {
    return m_failed;
  }
  ray->bit = -1;
  ray->d -= 1.0 / ray->sharpness;
  if (ray->d <= 0) {
    return m_converged;
  }
  int m = ceil(ray->d);
  double r = pow(ray->erd, pow(2, m - ray->d));
  mpc_arg(ray->z2, ray->z, MPFR_RNDN);
  double a = mpfr_get_d(ray->z2, MPFR_RNDN) / twopi;
  double t = a - floor(a);

  if (m == ray->n) {
    mpc_set_dc(ray->k[0], r * cexp(I * twopi *  t), MPC_RNDNN);
    mpc_set(ray->cn[0], ray->c, MPC_RNDNN);
    for (int i = 0; i < 8; ++i) { // FIXME arbitrary limit
      mpc_set_ui_ui(ray->zn[0], 0, 0, MPC_RNDNN);
      mpc_set_ui_ui(ray->dcn[0], 0, 0, MPC_RNDNN);
      for (int p = 0; p < m; ++p) {
        // dc = 2 * z * dc + 1;
        mpc_mul(ray->dcn[0], ray->zn[0], ray->dcn[0], MPC_RNDNN);
        mpfr_mul_2exp(mpc_realref(ray->dcn[0]), mpc_realref(ray->dcn[0]), 1, MPFR_RNDN);
        mpfr_mul_2exp(mpc_imagref(ray->dcn[0]), mpc_imagref(ray->dcn[0]), 1, MPFR_RNDN);
        mpfr_add_ui(mpc_realref(ray->dcn[0]), mpc_realref(ray->dcn[0]), 1, MPFR_RNDN);
        // z = z * z + c;
        mpc_sqr(ray->zn[0], ray->zn[0], MPC_RNDNN);
        mpc_add(ray->zn[0], ray->zn[0], ray->cn[0], MPC_RNDNN);
      }
      // c_new = c - (z - k) / dc;
      mpc_sub(ray->c_new[0], ray->zn[0], ray->k[0], MPC_RNDNN);
      mpc_div(ray->c_new[0], ray->c_new[0], ray->dcn[0], MPC_RNDNN);
      mpc_sub(ray->c_new[0], ray->cn[0], ray->c_new[0], MPC_RNDNN);
      // c = c_new;
      mpc_set(ray->cn[0], ray->c_new[0], MPC_RNDNN);
/*
      double d2 = cabs2(c_new - c);
      if (mpc_finite_p(c_new)) {
        c = c_new;
      } else {
        break;
      }
      if (d2 <= epsilon2) {
        break;
      }
*/
    }
    if (mpc_finite_p(ray->cn[0])) {
      mpc_set(ray->c, ray->cn[0], MPC_RNDNN);
      mpc_set(ray->z, ray->zn[0], MPC_RNDNN);
      mpc_norm(ray->z2, ray->z, MPFR_RNDN);
      ray->d = dwell(ray->loger2, m, mpfr_get_d(ray->z2, MPFR_RNDN));
      return m_stepped;
    }
    return m_failed;

  } else {
    mpc_set_dc(ray->k[0], r * cexp(I * pi *  t     ), MPC_RNDNN);
    mpc_set_dc(ray->k[1], r * cexp(I * pi * (t + 1)), MPC_RNDNN);
    mpc_set(ray->cn[0], ray->c, MPC_RNDNN);
    mpc_set(ray->cn[1], ray->c, MPC_RNDNN);
    for (int i = 0; i < 8; ++i) { // FIXME arbitrary limit
      mpc_set_ui_ui(ray->zn[0], 0, 0, MPC_RNDNN);
      mpc_set_ui_ui(ray->zn[1], 0, 0, MPC_RNDNN);
      mpc_set_ui_ui(ray->dcn[0], 0, 0, MPC_RNDNN);
      mpc_set_ui_ui(ray->dcn[1], 0, 0, MPC_RNDNN);
      for (int p = 0; p < m; ++p) {
        for (int w = 0; w < 2; ++w) {
          // dc = 2 * z * dc + 1;
          mpc_mul(ray->dcn[w], ray->zn[w], ray->dcn[w], MPC_RNDNN);
          mpfr_mul_2exp(mpc_realref(ray->dcn[w]), mpc_realref(ray->dcn[w]), 1, MPFR_RNDN);
          mpfr_mul_2exp(mpc_imagref(ray->dcn[w]), mpc_imagref(ray->dcn[w]), 1, MPFR_RNDN);
          mpfr_add_ui(mpc_realref(ray->dcn[w]), mpc_realref(ray->dcn[w]), 1, MPFR_RNDN);
          // z = z * z + c;
          mpc_sqr(ray->zn[w], ray->zn[w], MPC_RNDNN);
          mpc_add(ray->zn[w], ray->zn[w], ray->cn[w], MPC_RNDNN);
        }
      }
      for (int w = 0; w < 2; ++w) {
        // c_new = c - (z - k) / dc;
        mpc_sub(ray->c_new[w], ray->zn[w], ray->k[w], MPC_RNDNN);
        mpc_div(ray->c_new[w], ray->c_new[w], ray->dcn[w], MPC_RNDNN);
        mpc_sub(ray->c_new[w], ray->cn[w], ray->c_new[w], MPC_RNDNN);
        // c = c_new;
        mpc_set(ray->cn[w], ray->c_new[w], MPC_RNDNN);
        // d2[w] = cabs2(c_new[w] - ray->c);
        mpc_sub(ray->dcn[w], ray->c_new[w], ray->c, MPC_RNDNN);
        mpc_norm(ray->d2[w], ray->dcn[w], MPFR_RNDN);
      }
 /*
      if (! (e2[0] > epsilon2 && e2[1] > epsilon2)) {
        break;
      }
*/
    }
    if ((mpc_finite_p(ray->cn[0]) && mpc_finite_p(ray->cn[1]) && mpfr_lessequal_p(ray->d2[0], ray->d2[1]))
     || (mpc_finite_p(ray->cn[0]) && ! mpc_finite_p(ray->cn[1]))) {
      ray->bit = 0;
      mpc_set(ray->c, ray->cn[0], MPC_RNDNN);
      mpc_set(ray->z, ray->zn[0], MPC_RNDNN);
      mpc_norm(ray->z2, ray->z, MPFR_RNDN);
      ray->n = m;
      ray->d = dwell(ray->loger2, m, mpfr_get_d(ray->z2, MPFR_RNDN));
      return m_stepped;
    }
    if ((mpc_finite_p(ray->cn[0]) && mpc_finite_p(ray->cn[1]) && mpfr_lessequal_p(ray->d2[1], ray->d2[0]))
     || (! mpc_finite_p(ray->cn[0]) && mpc_finite_p(ray->cn[1]))) {
      ray->bit = 1;
      mpc_set(ray->c, ray->cn[1], MPC_RNDNN);
      mpc_set(ray->z, ray->zn[1], MPC_RNDNN);
      mpc_norm(ray->z2, ray->z, MPFR_RNDN);
      ray->n = m;
      ray->d = dwell(ray->loger2, m, mpfr_get_d(ray->z2, MPFR_RNDN));
      return m_stepped;
    }
    return m_failed;
  }
}

extern bool m_r_exray_out_have_bit(const m_r_exray_out * ray) {
  if (! ray) {
    return false;
  }
  return 0 <= ray->bit;
}

extern bool m_r_exray_out_get_bit(const m_r_exray_out *ray) {
  if (! ray) {
    return false;
  }
  return ray->bit;
}

extern void m_r_exray_out_get(const m_r_exray_out *ray, mpc_t c) {
  if (! ray) {
    return;
  }
  mpfr_prec_t precr = mpfr_get_prec(mpc_realref(ray->c));
  mpfr_prec_t preci = mpfr_get_prec(mpc_imagref(ray->c));
  mpfr_prec_t prec = precr > preci ? precr : preci;
  mpfr_set_prec(mpc_realref(c), prec);
  mpfr_set_prec(mpc_imagref(c), prec);
  mpc_set(c, ray->c, MPC_RNDNN);
}

extern char *m_r_exray_out_do(const mpc_t c, int sharpness, int maxdwell, double er) {
  m_r_exray_out *ray = m_r_exray_out_new(c, sharpness, maxdwell, er);
  if (! ray) {
    return 0;
  }
  char *bits = malloc(maxdwell + 2);
  if (! bits) {
    m_r_exray_out_delete(ray);
    return 0;
  }
  int n = 0;
  while (n <= maxdwell) {
    if (m_r_exray_out_have_bit(ray)) {
      bits[n++] = '0' + m_r_exray_out_get_bit(ray);
    }
    if (m_stepped != m_r_exray_out_step(ray)) {
      break;
    }
  }
  bits[n] = 0;
  m_r_exray_out_delete(ray);
  return bits;
}
