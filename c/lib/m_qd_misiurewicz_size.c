// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_qd_util.h"

extern _Float128 m_qd_misiurewicz_size(_Float128 _Complex c, int preperiod, int period)
{
  _Float128 _Complex z = 0;
  _Float128 _Complex dc = 0;
  _Float128 _Complex zp = 0;
  _Float128 _Complex dcp = 0;
  for (int p = 0; p < period; ++p)
  {
    dcp = 2 * zp * dcp + 1;
    zp = zp * zp + c;
  }
  _Float128 zq2 = cabs2f128(zp - z);
  for (int q = 1; q <= preperiod; ++q)
  {
    dcp = 2 * zp * dcp + 1;
    zp = zp * zp + c;
    dc = 2 * z * dc + 1;
    z = z * z + c;
    _Float128 zp2 = cabs2f128(zp - z);
    if (q < preperiod && zp2 < zq2)
      zq2 = zp2;
  }
  return sqrtf128(zq2) / cabsf128(dcp - dc);
}

extern _Float128 m_qd_misiurewicz_size_orbit(const m_r_orbit_qd *orbit)
{
  const _Float128 _Complex *z_ptr = m_r_orbit_qd_get_z_ptr(orbit);
  const int preperiod = m_r_orbit_qd_get_preperiod(orbit);
  const int period = m_r_orbit_qd_get_period(orbit);
  _Float128 _Complex dc = 0;
  _Float128 _Complex dcp = 0;
  for (int p = 0; p < period; ++p)
  {
    dcp = 2 * z_ptr[p] * dcp + 1;
  }
  _Float128 zq2 = cabsf128(z_ptr[period]);
  for (int q = 1; q <= preperiod; ++q)
  {
    dcp = 2 * z_ptr[period + q - 1] * dcp + 1;
    dc = 2 * z_ptr[q - 1] * dc + 1;
    if (q < preperiod)
    {
      _Float128 zp2 = cabsf128(z_ptr[period + q] - z_ptr[q]);
      if (zp2 < zq2)
        zq2 = zp2;
    }
  }
  return zq2 / cabsf128(dcp - dc);
}
