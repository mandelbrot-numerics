// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>

struct m_r_orbit_ld
{
  int preperiod;
  int period;
  mpc_t c;
  long double _Complex *orbit;
};

extern m_r_orbit_ld *m_r_orbit_ld_new(const mpc_t c, int preperiod, int period)
{
  m_r_orbit_ld *o = calloc(1, sizeof(*o));
  if (! o)
  {
    return 0;
  }
  o->preperiod = preperiod;
  o->period = period;
  o->orbit = malloc(sizeof(*o->orbit) * (preperiod + period));
  if (! o->orbit)
  {
    free(o);
    return 0;
  }
  mpc_init2(o->c, mpfr_get_prec(mpc_realref(c)));
  mpc_set_ldc(o->c, 0, MPC_RNDNN);
  for (int i = 0; i < preperiod + period; ++i)
  {
    o->orbit[i] = mpc_get_ldc(o->c, MPC_RNDNN);
    mpc_sqr(o->c, o->c, MPC_RNDNN);
    mpc_add(o->c, o->c, c, MPC_RNDNN);
  }
  mpc_set(o->c, c, MPC_RNDNN);
  return o;
}

extern void m_r_orbit_ld_delete(m_r_orbit_ld *o)
{
  if (o)
  {
    if (o->orbit)
    {
      free(o->orbit);
    }
    mpc_clear(o->c);
    free(o);
  }
}

extern int m_r_orbit_ld_get_preperiod(const m_r_orbit_ld *o)
{
  return o->preperiod;
}

extern int m_r_orbit_ld_get_period(const m_r_orbit_ld *o)
{
  return o->period;
}

extern void m_r_orbit_ld_get_c(const m_r_orbit_ld *o, mpc_t c)
{
  mpfr_set_prec(mpc_realref(c), mpfr_get_prec(mpc_realref(o->c)));
  mpfr_set_prec(mpc_imagref(c), mpfr_get_prec(mpc_imagref(o->c)));
  mpc_set(c, o->c, MPC_RNDNN);
}

extern long double _Complex m_r_orbit_ld_get_z(const m_r_orbit_ld *o, int n)
{
  if (n >= o->preperiod)
  {
    n -= o->preperiod;
    n %= o->period;
    n += o->preperiod;
  }
  return o->orbit[n];
}

extern const long double _Complex *m_r_orbit_ld_get_z_ptr(const m_r_orbit_ld *o)
{
  return o->orbit;
}
