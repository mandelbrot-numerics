// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

extern double m_d_domain_size(double _Complex nucleus, int period)
{
  double _Complex z = nucleus;
  double _Complex dc = 1;
  double zq2 = cabs2(z);
  for (int q = 2; q <= period; ++q)
  {
    dc = 2 * z * dc + 1;
    z = z * z + nucleus;
    double zp2 = cabs2(z);
    if (q < period && zp2 < zq2)
      zq2 = zp2;
  }
  return sqrt(zq2) / cabs(dc);
}

extern double m_d_filtered_domain_size(double _Complex nucleus, int period, m_period_filter_t *filter)
{
  double _Complex z = 0;
  double _Complex dc = 0;
  double zq = 1.0/0.0;
  for (int q = 1; q <= period; ++q)
  {
    dc = 2 * z * dc + 1;
    z = z * z + nucleus;
    double zp = cabs(z);
    if (q < period && zp < zq && filter->accept(filter, q))
      zq = zp;
  }
  return zq / cabs(dc);
}

extern double m_d_domain_size_orbit(const m_r_orbit_d *orbit)
{
  const double _Complex *z_ptr = m_r_orbit_d_get_z_ptr(orbit);
  const int period = m_r_orbit_d_get_period(orbit);
  double _Complex z = z_ptr[1];
  double _Complex dc = 1;
  double zq2 = cabs2(z);
  for (int q = 2; q <= period; ++q)
  {
    dc = 2 * z * dc + 1;
    if (q < period)
    {
      z = z_ptr[q];
      double zp2 = cabs2(z);
      if (q < period && zp2 < zq2)
        zq2 = zp2;
    }
  }
  return sqrt(zq2) / cabs(dc);
}
