// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

extern m_newton m_d_domain_coord_step(double _Complex *c_out, double _Complex c_guess, double _Complex domain_coord, int loperiod, int hiperiod) {
  double _Complex zp = 0;
  double _Complex dcp = 0;
  double _Complex z = 0;
  double _Complex dc = 0;
  for (int i = 1; i <= hiperiod; ++i) {
    dc = 2 * z * dc + 1;
    z = z * z + c_guess;
    if (i == loperiod)
    {
      zp = z;
      dcp = dc;
    }
  }
  double _Complex f = z / zp - domain_coord;
  double _Complex df = (dc * zp - z * dcp) / (zp * zp);
  if (cabs2(df) <= epsilon2) {
    *c_out = c_guess;
    return m_converged;
  }
  double _Complex c_new = c_guess - f / df;
  double _Complex d = c_new - c_guess;
  if (cabs2(d) <= epsilon2) {
    *c_out = c_new;
    return m_converged;
  }
  if (cisfinite(d)) {
    *c_out = c_new;
    return m_stepped;
  } else {
    *c_out = c_guess;
    return m_failed;
  }
}

extern m_newton m_d_domain_coord(double _Complex *c_out, double _Complex c_guess, double _Complex domain_coord, int loperiod, int hiperiod, int maxsteps) {
  m_newton result = m_failed;
  double _Complex c = c_guess;
  for (int i = 0; i < maxsteps; ++i) {
    if (m_stepped != (result = m_d_domain_coord_step(&c, c, domain_coord, loperiod, hiperiod))) {
      break;
    }
  }
  *c_out = c;
  return result;
}
