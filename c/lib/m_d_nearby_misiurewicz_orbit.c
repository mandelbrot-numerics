// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

struct m_d_nearby_misiurewicz_orbit
{
  // orbit
  const m_r_orbit_diff_d *o;
  const double _Complex *z_ptr, *d_ptr;
  int n;
  // iterates
  double _Complex dcp, dc, ddcp, ddc;
  double zq, zq_new;
  // results
  double min_distance;
  double distance;
  double size;
  int preperiod;
  int period;
};

extern m_d_nearby_misiurewicz_orbit *m_d_nearby_misiurewicz_orbit_new(const m_r_orbit_diff_d *o)
{
  m_d_nearby_misiurewicz_orbit *ball = (m_d_nearby_misiurewicz_orbit *) malloc(sizeof(*ball));
  if (! ball)
  {
    return 0;
  }
  ball->o = o;
  ball->z_ptr = m_r_orbit_diff_d_get_z_ptr(o);
  ball->d_ptr = m_r_orbit_diff_d_get_d_ptr(o);
  ball->n = m_r_orbit_diff_d_get_preperiod(o) + m_r_orbit_diff_d_get_period(o);
  ball->period = m_r_orbit_diff_d_get_offset(o);
  ball->preperiod = 0;
  ball->ddcp = 0;
  ball->dcp = 0;
  for (int i = 0; i < ball->period; ++i)
  {
    ball->ddcp = 2 * (ball->z_ptr[i] * ball->ddcp + ball->dcp * ball->dcp);
    ball->dcp = 2 * ball->z_ptr[i] * ball->dcp + 1;
  }
  ball->ddc = 0;
  ball->dc = 0;
  ball->zq = 1.0 / 0.0;
  ball->zq_new = 1.0 / 0.0;
  ball->min_distance = 1.0 / 0.0;
  ball->distance = 1.0 / 0.0;
  ball->size = -1.0 / 0.0;
  return ball;
}

extern void m_d_nearby_misiurewicz_orbit_delete(m_d_nearby_misiurewicz_orbit *ball)
{
  if (ball)
  {
    free(ball);
  }
}

extern bool m_d_nearby_misiurewicz_orbit_step(m_d_nearby_misiurewicz_orbit *ball)
{
  if (! ball)
  {
    return false;
  }
  if (ball->preperiod + ball->period < ball->n)
  {
    ball->ddcp = 2 * (ball->z_ptr[ball->preperiod + ball->period] * ball->ddcp + ball->dcp * ball->dcp);
    ball->dcp = 2 * ball->z_ptr[ball->preperiod + ball->period] * ball->dcp + 1;
    ball->ddc = 2 * (ball->z_ptr[ball->preperiod] * ball->ddc + ball->dc * ball->dc);
    ball->dc = 2 * ball->z_ptr[ball->preperiod] * ball->dc + 1;
    ball->zq = ball->zq_new;
    ball->zq_new = fmin(ball->zq, cabs(ball->d_ptr[ball->preperiod]));
    ball->preperiod++;
    bool ok = cisfinite(ball->ddcp) && cisfinite(ball->ddc) && cisfinite(ball->dcp) && cisfinite(ball->dc);
    return ok;
  }
  else
  {
    return false;
  }
}

extern bool m_d_nearby_misiurewicz_orbit_check(m_d_nearby_misiurewicz_orbit *ball)
{
  if (! ball)
  {
    return false;
  }
  // distance is increment of root-finding methods for finding Misiurewicz point
  double z = cabs(ball->d_ptr[ball->preperiod]);
  // double d1 = cabs(z / (ball->dcp - ball->dc)); // Householder 1 aka Newton (not accurate enough)
  double d2 = cabs(1 / ((ball->dcp - ball->dc) / z - 0.5 * (ball->ddcp - ball->ddc) / (ball->dcp - ball->dc))); // Householder 2
  ball->distance = d2;
  double coord = z / ball->zq; // Misiurewicz domain coord
  ball->size = ball->distance / coord;
  bool ok = coord < 4 && ball->distance < 0.25 * ball->min_distance;
  ball->min_distance = fmin(ball->min_distance, ball->distance);
  return ok;
;
}

extern double m_d_nearby_misiurewicz_orbit_get_distance(const m_d_nearby_misiurewicz_orbit *ball)
{
  if (! ball) {
    return 0;
  }
  return ball->distance;
}

extern double m_d_nearby_misiurewicz_orbit_get_size(const m_d_nearby_misiurewicz_orbit *ball)
{
  if (! ball) {
    return 0;
  }
  return ball->size;
}

extern int m_d_nearby_misiurewicz_orbit_get_period(const m_d_nearby_misiurewicz_orbit *ball)
{
  if (! ball) {
    return 0;
  }
  return ball->period;
}

extern int m_d_nearby_misiurewicz_orbit_get_preperiod(const m_d_nearby_misiurewicz_orbit *ball)
{
  if (! ball)
  {
    return -1;
  }
  return ball->preperiod;
}
