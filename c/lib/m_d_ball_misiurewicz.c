// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

struct m_d_ball_misiurewicz {
  double _Complex c;
  double _Complex w;
  double _Complex dwdc;
  double r;
  int preperiod;
  int period;
};

extern m_d_ball_misiurewicz *m_d_ball_misiurewicz_new(double _Complex center, double radius) {
  m_d_ball_misiurewicz *ball = (m_d_ball_misiurewicz *) malloc(sizeof(*ball));
  if (! ball) {
    return 0;
  }
  ball->c = center;
  ball->r = radius;
  ball->w = ball->c;
  ball->dwdc = 1;
  ball->preperiod = 1;
  ball->period = 0;
  return ball;
}

extern void m_d_ball_misiurewicz_delete(m_d_ball_misiurewicz *ball) {
  if (ball) {
    free(ball);
  }
}

extern bool m_d_ball_misiurewicz_step(m_d_ball_misiurewicz *ball) {
  if (! ball) {
    return false;
  }
  ball->dwdc = 2 * ball->w * ball->dwdc + 1;
  ball->w = ball->w * ball->w + ball->c;
  ball->preperiod++;
  bool ok = cisfinite(ball->dwdc) && cisfinite(ball->w);
  return ok;
}

extern bool m_d_ball_misiurewicz_check_periods(m_d_ball_misiurewicz *ball, int maxperiod) {
  if (! ball) {
    return true;
  }
  double _Complex z = ball->w;
  for (int period = 1; period <= maxperiod; ++period)
  {
    z = z * z + ball->c;
    if (cabs(z - ball->w) < ball->r * cabs(ball->dwdc))
    {
      ball->period = period;
      return true;
    }
  }
  return false;
}

extern int m_d_ball_misiurewicz_get_period(const m_d_ball_misiurewicz *ball) {
  if (! ball) {
    return 0;
  }
  return ball->period;
}

extern int m_d_ball_misiurewicz_get_preperiod(const m_d_ball_misiurewicz *ball) {
  if (! ball) {
    return -1;
  }
  return ball->preperiod;
}

extern bool m_d_ball_misiurewicz_do(int *preperiod, int *period, double _Complex center, double radius, int maxpreperiod, int maxperiod) {
  m_d_ball_misiurewicz *ball = m_d_ball_misiurewicz_new(center, radius);
  if (! ball) {
    return false;
  }
  for (int i = 0; i < maxpreperiod; ++i) {
    if (m_d_ball_misiurewicz_check_periods(ball, maxperiod)) {
      *preperiod = m_d_ball_misiurewicz_get_preperiod(ball);
      *period = m_d_ball_misiurewicz_get_period(ball);
      m_d_ball_misiurewicz_delete(ball);
      return true;
    }
    if (! m_d_ball_misiurewicz_step(ball)) {
      break;
    }
  }
  m_d_ball_misiurewicz_delete(ball);
  return false;
}
