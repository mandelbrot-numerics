// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>

extern m_newton m_r_misiurewicz_naive_step_raw(mpc_t c_out, const mpc_t c_guess, int preperiod, int period, mpc_t z, mpc_t dc, mpc_t zp, mpc_t dcp, mpc_t c_new, mpc_t d, mpfr_t d2, mpfr_t epsilon2) {
  mpc_set_ui_ui(z, 0, 0, MPC_RNDNN);
  mpc_set_ui_ui(dc, 0, 0, MPC_RNDNN);
  mpc_set_ui_ui(zp, 0, 0, MPC_RNDNN);
  mpc_set_ui_ui(dcp, 0, 0, MPC_RNDNN);
  for (int i = 0; i < preperiod + period; ++i) {
    if (i == preperiod) {
      mpc_set(zp, z, MPC_RNDNN);
      mpc_set(dcp, dc, MPC_RNDNN);
    }
    // dc = 2 * z * dc + 1;
    mpc_mul(dc, z, dc, MPC_RNDNN);
    mpc_mul_2si(dc, dc, 1, MPC_RNDNN);
    mpc_add_ui(dc, dc, 1, MPC_RNDNN);
    // z = z * z + c_guess;
    mpc_sqr(z, z, MPC_RNDNN);
    mpc_add(z, z, c_guess, MPC_RNDNN);
  }
  mpc_sub(z, z, zp, MPC_RNDNN);
  mpc_sub(dc, dc, dcp, MPC_RNDNN);
  mpc_div(zp, z, dc, MPC_RNDNN);
  mpc_sub(c_new, c_guess, zp, MPC_RNDNN);
  mpc_sub(d, c_new, c_guess, MPC_RNDNN);
  mpc_norm(d2, d, MPFR_RNDN);
  if (mpfr_less_p(d2, epsilon2)) {
    mpc_set(c_out, c_new, MPC_RNDNN);
    return m_converged;
  }
  if (mpfr_number_p(mpc_realref(d)) && mpfr_number_p(mpc_imagref(d))) {
    mpc_set(c_out, c_new, MPC_RNDNN);
    return m_stepped;
  } else {
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_failed;
  }
}

extern m_newton m_r_misiurewicz_naive_step(mpc_t c_out, const mpc_t c_guess, int preperiod, int period) {
  // prec
  mpfr_prec_t precr, preci, prec;
  mpc_get_prec2(&precr, &preci, c_guess);
  prec = precr > preci ? precr : preci;
  mpc_set_prec(c_out, prec); // FIXME might trash when c_out = c_guess
  // init
  mpc_t z, dc, zp, dcp, c_new, d;
  mpfr_t d2, epsilon2;
  mpc_init2(z, prec);
  mpc_init2(dc, prec);
  mpc_init2(zp, prec);
  mpc_init2(dcp, prec);
  mpc_init2(c_new, prec);
  mpc_init2(d, prec);
  mpfr_init2(d2, prec);
  mpfr_init2(epsilon2, prec);
  // epsilon
  mpfr_set_si(epsilon2, 2, MPFR_RNDN);
  mpfr_nextabove(epsilon2);
  mpfr_sub_si(epsilon2, epsilon2, 2, MPFR_RNDN);
  mpfr_sqr(epsilon2, epsilon2, MPFR_RNDN);
  // step raw
  m_newton retval = m_r_misiurewicz_naive_step_raw(c_out, c_guess, preperiod, period, z, dc, zp, dcp, c_new, d, d2, epsilon2);
  // cleanup
  mpc_clear(z);
  mpc_clear(dc);
  mpc_clear(zp);
  mpc_clear(dcp);
  mpc_clear(c_new);
  mpc_clear(d);
  mpfr_clear(d2);
  mpfr_clear(epsilon2);
  return retval;
}

extern m_newton m_r_misiurewicz_naive(mpc_t c_out, const mpc_t c_guess, int preperiod, int period, int maxsteps) {
  // prec
  mpfr_prec_t precr, preci, prec;
  mpc_get_prec2(&precr, &preci, c_guess);
  prec = precr > preci ? precr : preci;
  // init
  mpc_t c, z, dc, zp, dcp, c_new, d;
  mpfr_t d2, epsilon2;
  mpc_init2(c, prec);
  mpc_init2(z, prec);
  mpc_init2(dc, prec);
  mpc_init2(zp, prec);
  mpc_init2(dcp, prec);
  mpc_init2(c_new, prec);
  mpc_init2(d, prec);
  mpfr_init2(d2, prec);
  mpfr_init2(epsilon2, prec);
  // epsilon
  mpfr_set_si(epsilon2, 2, MPFR_RNDN);
  mpfr_nextabove(epsilon2);
  mpfr_sub_si(epsilon2, epsilon2, 2, MPFR_RNDN);
  mpfr_sqr(epsilon2, epsilon2, MPFR_RNDN);
  // c = c_guess
  m_newton retval = m_failed;
  mpc_set(c, c_guess, MPC_RNDNN);
  for (int i = 0; i < maxsteps; ++i) {
    if (m_stepped != (retval = m_r_misiurewicz_naive_step_raw(c, c, preperiod, period, z, dc, zp, dcp, c_new, d, d2, epsilon2))) {
      break;
    }
  }
  // c_out = c;
  mpc_set_prec(c_out, prec);
  mpc_set(c_out, c, MPC_RNDNN);
  // cleanup
  mpc_clear(c);
  mpc_clear(z);
  mpc_clear(dc);
  mpc_clear(zp);
  mpc_clear(dcp);
  mpc_clear(c_new);
  mpc_clear(d);
  mpfr_clear(d2);
  mpfr_clear(epsilon2);
  return retval;
}

extern m_newton m_r_misiurewicz_step_raw(mpc_t c_out, const mpc_t c_guess, int preperiod, int period, mpc_t z, mpc_t dc, mpc_t zp, mpc_t dcp, mpc_t f, mpc_t df, mpc_t g, mpc_t dg, mpc_t h, mpc_t dh, mpc_t k, mpc_t l, mpfr_t epsilon2) {
  // iteration
  // z = 0; dc = 0; zp = 0; dcp = 0; h = 1; dh = 0;
  mpc_set_si(z, 0, MPC_RNDNN);
  mpc_set_si(dc, 0, MPC_RNDNN);
  mpc_set_si(zp, 0, MPC_RNDNN);
  mpc_set_si(dcp, 0, MPC_RNDNN);
  mpc_set_si(h, 1, MPC_RNDNN);
  mpc_set_si(dh, 0, MPC_RNDNN);
  for (int i = 0; i < period; ++i) {
    // dc = 2 * z * dc + 1;
    mpc_mul(dc, z, dc, MPC_RNDNN);
    mpc_mul_2si(dc, dc, 1, MPC_RNDNN);
    mpc_add_ui(dc, dc, 1, MPC_RNDNN);
    // z = z * z + c_guess;
    mpc_sqr(z, z, MPC_RNDNN);
    mpc_add(z, z, c_guess, MPC_RNDNN);
  }
  for (int i = 0; i < preperiod; ++i) {
    // reject lower preperiods
    // k = z - zp;
    mpc_sub(k, z, zp, MPC_RNDNN);
    // h = h * k;
    mpc_mul(h, h, k, MPC_RNDNN);
    // dh = dh + (dc - dcp) / k;
    mpc_sub(l, dc, dcp, MPC_RNDNN);
    mpc_div(l, l, k, MPC_RNDNN);
    mpc_add(dh, dh, l, MPC_RNDNN);
    // iterate
    // dc = 2 * z * dc + 1;
    mpc_mul(dc, z, dc, MPC_RNDNN);
    mpc_mul_2si(dc, dc, 1, MPC_RNDNN);
    mpc_add_ui(dc, dc, 1, MPC_RNDNN);
    // z = z * z + c_guess;
    mpc_sqr(z, z, MPC_RNDNN);
    mpc_add(z, z, c_guess, MPC_RNDNN);
    // dcp = 2 * zp * dcp + 1;
    mpc_mul(dcp, zp, dcp, MPC_RNDNN);
    mpc_mul_2si(dcp, dcp, 1, MPC_RNDNN);
    mpc_add_ui(dcp, dcp, 1, MPC_RNDNN);
    // zp = zp * zp + c_guess;
    mpc_sqr(zp, zp, MPC_RNDNN);
    mpc_add(zp, zp, c_guess, MPC_RNDNN);
  }
  // build function
  // dh = dh * h;
  mpc_mul(dh, dh, h, MPC_RNDNN);
  // g = z - zp;
  mpc_sub(g, z, zp, MPC_RNDNN);
  // dg = dc - dcp;
  mpc_sub(dg, dc, dcp, MPC_RNDNN);
  // f = g / h;
  mpc_div(f, g, h, MPC_RNDNN);
  // df = (dg * h - g * dh) / (h * h);
  mpc_mul(dg, dg, h, MPC_RNDNN);
  mpc_mul(dh, dh, g, MPC_RNDNN);
  mpc_sub(df, dg, dh, MPC_RNDNN);
  mpc_sqr(h, h, MPC_RNDNN);
  mpc_div(df, df, h, MPC_RNDNN);
  // newton step
  // h = c_guess - f / df;
  mpc_div(g, f, df, MPC_RNDNN);
  mpc_sub(h, c_guess, g, MPC_RNDNN);
  // check convergence
  // g = h - c_guess;
  mpc_sub(g, h, c_guess, MPC_RNDNN);
  mpc_norm(mpc_realref(f), g, MPFR_RNDN);
  if (mpfr_less_p(mpc_realref(f), epsilon2)) {
    // *c_out = h;
    mpc_set(c_out, h, MPC_RNDNN);
    return m_converged;
  }
  if (mpfr_number_p(mpc_realref(f))) {
    // *c_out = h;
    mpc_set(c_out, h, MPC_RNDNN);
    return m_stepped;
  } else {
    // *c_out = c_guess;
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_failed;
  }
}

extern m_newton m_r_misiurewicz(mpc_t c_out, const mpc_t c_guess, int preperiod, int period, int maxsteps) {
  // prec
  mpfr_prec_t precr, preci, prec;
  mpc_get_prec2(&precr, &preci, c_guess);
  prec = precr > preci ? precr : preci;
  // init
  mpc_t c, z, dc, f, df, g, dg, h, dh, k, l, zp, dcp;
  mpfr_t epsilon2;
  mpc_init2(c, prec);
  mpc_init2(z, prec);
  mpc_init2(dc, prec);
  mpc_init2(zp, prec);
  mpc_init2(dcp, prec);
  mpc_init2(f, prec);
  mpc_init2(df, prec);
  mpc_init2(g, prec);
  mpc_init2(dg, prec);
  mpc_init2(h, prec);
  mpc_init2(dh, prec);
  mpc_init2(k, prec);
  mpc_init2(l, prec);
  mpfr_init2(epsilon2, prec);
  // epsilon
  mpfr_set_si(epsilon2, 2, MPFR_RNDN);
  mpfr_nextabove(epsilon2);
  mpfr_sub_si(epsilon2, epsilon2, 2, MPFR_RNDN);
  mpfr_sqr(epsilon2, epsilon2, MPFR_RNDN);
  // c = c_guess
  m_newton retval = m_failed;
  mpc_set(c, c_guess, MPC_RNDNN);
  for (int i = 0; i < maxsteps; ++i) {
    if (m_stepped != (retval = m_r_misiurewicz_step_raw(c, c, preperiod, period, z, dc, zp, dcp, f, df, g, dg, h, dh, k, l, epsilon2))) {
      break;
    }
  }
  // c_out = c;
  mpc_set_prec(c_out, prec);
  mpc_set(c_out, c, MPC_RNDNN);
  // cleanup
  mpc_clear(c);
  mpc_clear(z);
  mpc_clear(dc);
  mpc_clear(zp);
  mpc_clear(dcp);
  mpc_clear(f);
  mpc_clear(df);
  mpc_clear(g);
  mpc_clear(dg);
  mpc_clear(h);
  mpc_clear(dh);
  mpc_clear(k);
  mpc_clear(l);
  mpfr_clear(epsilon2);
  return retval;
}
