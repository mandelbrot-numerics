// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

static void cross(mpfr_t out, const mpc_t a, const mpc_t b, mpfr_t t) {
  mpfr_mul(out, mpc_imagref(a), mpc_realref(b), MPFR_RNDN);
  mpfr_mul(t, mpc_realref(a), mpc_imagref(b), MPFR_RNDN);
  mpfr_sub(out, out, t, MPFR_RNDN);
}

static bool crosses_positive_real_axis(const mpc_t a, const mpc_t b, mpc_t d, mpfr_t t1, mpfr_t t2) {
  if (mpfr_sgn(mpc_imagref(a)) != mpfr_sgn(mpc_imagref(b))) {
    // d = b - a;
    mpc_sub(d, b, a, MPC_RNDNN);
    // s = sgn(cimag(d));
    int s = mpfr_sgn(mpc_imagref(d));
    // t = sgn(cross(d, a));
    cross(t1, d, a, t2);
    int t = mpfr_sgn(t1);
    return s == t;
  }
  return false;
}

static bool surrounds_origin(const mpc_t a, const mpc_t b, const mpc_t c, const mpc_t d, mpc_t e, mpfr_t t1, mpfr_t t2) {
  return odd
    ( crosses_positive_real_axis(a, b, e, t1, t2)
    + crosses_positive_real_axis(b, c, e, t1, t2)
    + crosses_positive_real_axis(c, d, e, t1, t2)
    + crosses_positive_real_axis(d, a, e, t1, t2)
    );
}

struct m_r_box_misiurewicz {
  mpc_t center;
  mpc_t w, wp;
  mpc_t c[4];
  mpc_t z[4];
  mpc_t d[4];
  int preperiod;
  int period;
  mpc_t e;
  mpfr_t t1;
  mpfr_t t2;
};

extern m_r_box_misiurewicz *m_r_box_misiurewicz_new(const mpc_t center, const mpfr_t radius) {
  m_r_box_misiurewicz *box = (m_r_box_misiurewicz *) malloc(sizeof(*box));
  if (! box) {
    return 0;
  }
  // prec
  mpfr_prec_t precr, preci, prec;
  mpc_get_prec2(&precr, &preci, center);
  prec = precr > preci ? precr : preci;
  // init
  mpc_init2(box->center, prec);
  mpc_init2(box->w, prec);
  mpc_init2(box->wp, prec);
  for (int i = 0; i < 4; ++i) {
    mpc_init2(box->c[i], prec);
    mpc_init2(box->z[i], prec);
    mpc_init2(box->d[i], prec);
  }
  mpc_init2(box->e, prec);
  mpfr_init2(box->t1, prec);
  mpfr_init2(box->t2, prec);
  // vars
  mpc_set(box->center, center, MPC_RNDNN);
  mpc_set(box->w, center, MPC_RNDNN);
  box->preperiod = 1;
  box->period = 0;
  // box
  mpfr_sub(mpc_realref(box->c[0]), mpc_realref(center), radius, MPFR_RNDN);
  mpfr_sub(mpc_imagref(box->c[0]), mpc_imagref(center), radius, MPFR_RNDN);
  mpfr_add(mpc_realref(box->c[1]), mpc_realref(center), radius, MPFR_RNDN);
  mpfr_sub(mpc_imagref(box->c[1]), mpc_imagref(center), radius, MPFR_RNDN);
  mpfr_add(mpc_realref(box->c[2]), mpc_realref(center), radius, MPFR_RNDN);
  mpfr_add(mpc_imagref(box->c[2]), mpc_imagref(center), radius, MPFR_RNDN);
  mpfr_sub(mpc_realref(box->c[3]), mpc_realref(center), radius, MPFR_RNDN);
  mpfr_add(mpc_imagref(box->c[3]), mpc_imagref(center), radius, MPFR_RNDN);
  mpc_set(box->z[0], box->c[0], MPC_RNDNN);
  mpc_set(box->z[1], box->c[1], MPC_RNDNN);
  mpc_set(box->z[2], box->c[2], MPC_RNDNN);
  mpc_set(box->z[3], box->c[3], MPC_RNDNN);
  return box;
}

extern void m_r_box_misiurewicz_delete(m_r_box_misiurewicz *box) {
  if (box) {
    mpc_clear(box->center);
    mpc_clear(box->w);
    for (int i = 0; i < 4; ++i) {
      mpc_clear(box->c[i]);
      mpc_clear(box->z[i]);
      mpc_clear(box->d[i]);
    }
    mpc_clear(box->e);
    mpfr_clear(box->t1);
    mpfr_clear(box->t2);
    free(box);
  }
}

extern bool m_r_box_misiurewicz_step(m_r_box_misiurewicz *box) {
  if (! box) {
    return false;
  }
  bool ok = true;
  mpc_sqr(box->w, box->w, MPC_RNDNN);
  mpc_add(box->w, box->w, box->center, MPC_RNDNN);
  for (int i = 0; i < 4; ++i) {
    // box->z[i] = box->z[i] * box->z[i] + box->c[i];
    mpc_sqr(box->z[i], box->z[i], MPC_RNDNN);
    mpc_add(box->z[i], box->z[i], box->c[i], MPC_RNDNN);
    ok = ok && mpfr_number_p(mpc_realref(box->z[i])) && mpfr_number_p(mpc_imagref(box->z[i]));
  }
  box->preperiod++;
  return ok;
}


extern bool m_r_box_misiurewicz_check_periods(m_r_box_misiurewicz *box, int maxperiod) {
  // FIXME cache w orbit?  time/space trade-off...
  if (! box) {
    return true;
  }
  // wp = w
  mpc_set(box->wp, box->w, MPC_RNDNN);
  for (int period = 1; period <= maxperiod; ++period)
  {
    // wp = wp * wp + box->center;
    mpc_sqr(box->wp, box->wp, MPC_RNDNN);
    mpc_add(box->wp, box->wp, box->center, MPC_RNDNN);
    // d = z - wp
    mpc_sub(box->d[0], box->z[0], box->wp, MPC_RNDNN);
    mpc_sub(box->d[1], box->z[1], box->wp, MPC_RNDNN);
    mpc_sub(box->d[2], box->z[2], box->wp, MPC_RNDNN);
    mpc_sub(box->d[3], box->z[3], box->wp, MPC_RNDNN);
    // if d surround 0, found (pre)period
    if (surrounds_origin(box->d[0], box->d[1], box->d[2], box->d[3], box->e, box->t1, box->t2))
    {
      box->period = period;
      return true;
    }
  }
  return false;
}


extern int m_r_box_misiurewicz_get_period(const m_r_box_misiurewicz *box) {
  if (! box) {
    return 0;
  }
  return box->period;
}


extern int m_r_box_misiurewicz_get_preperiod(const m_r_box_misiurewicz *box) {
  if (! box) {
    return -1;
  }
  return box->preperiod;
}


extern bool m_r_box_misiurewicz_do(int *preperiod, int *period, const mpc_t center, const mpfr_t radius, int maxpreperiod, int maxperiod) {
  m_r_box_misiurewicz *box = m_r_box_misiurewicz_new(center, radius);
  if (! box) {
    return false;
  }
  for (int i = 0; i < maxpreperiod; ++i) {
    if (m_r_box_misiurewicz_check_periods(box, maxperiod)) {
      *preperiod = m_r_box_misiurewicz_get_preperiod(box);
      *period = m_r_box_misiurewicz_get_period(box);
      m_r_box_misiurewicz_delete(box);
      return true;
    }
    if (! m_r_box_misiurewicz_step(box)) {
      break;
    }
  }
  m_r_box_misiurewicz_delete(box);
  return false;
}
