// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2020 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

/*
See appendix of <http://arxiv.org/abs/1706.04177v1>
*/

struct m_d_spider_path {
  int preperiod;
  int period;
  int sharpness;
  double _Complex x0; // 0
  double _Complex *x; // (preperiod + period) * sharpness
  int n; // t = n + (s + 0.5) / sharpness
  int s; // [0 .. sharpness)
};

static double _Complex *X(m_d_spider_path *s, int i, int j)
{
  if (i == 0) return &s->x0;
  else return s->x + (j * (s->preperiod + s->period) + (i - 1));
}

extern m_d_spider_path *m_d_spider_path_new(const mpq_t angle0, int preperiod, int period, int sharpness)
{
  m_d_spider_path *path = malloc(sizeof(*path));
  if (! path)
  {
    return 0;
  }
  path->preperiod = preperiod;
  path->period = period;
  path->sharpness = sharpness;
  path->x0 = 0;
  path->x = malloc(sizeof(*path->x) * sharpness * (preperiod + period));
  if (! path->x)
  {
    free(path);
    return 0;
  }
  mpq_t angle;
  mpq_init(angle);
  mpq_set(angle, angle0);
  mpq_t one;
  mpq_init(one);
  mpq_set_ui(one, 1, 1);
  // x_j = exp(2 \pi i 2^{j-1} \theta)
  for (int i = 1; i <= preperiod + period; ++i)
  {
    *X(path, i, 0) = cexp(twopi * I * mpq_get_d(angle));
    for (int j = 1; j < sharpness; ++j)
    {
      *X(path, i, j) = *X(path, i, 0);
    }
    // double angle
    mpq_mul_2exp(angle, angle, 1);
    if (mpq_cmp_ui(angle, 1, 1) >= 0) {
      mpq_sub(angle, angle, one);
    }
  }
  mpq_clear(angle);
  mpq_clear(one);
  for (int j = 0; j < sharpness; ++j)
  {
    double t = (j + 0.5) / sharpness;
    *X(path, 1, j) *= t;
    if (preperiod == 0)
    {
      *X(path, period, j) *= (1 - t);
    }
  }
  path->n = 0;
  path->s = 0;
  return path;
}

extern void m_d_spider_path_delete(m_d_spider_path *path)
{
  if (! path)
  {
    return;
  }
  if (path->x)
  {
    free(path->x);
  }
  free(path);
}

extern void m_d_spider_path_step(m_d_spider_path *path)
{
  // next values
  double _Complex x[path->preperiod + path->period];
  int s0 = path->s;
  // for continuity check
  int s1 = (path->s - 1 + path->sharpness) % path->sharpness;
  // pull back path
  for (int i = 0; i < path->preperiod + path->period; ++i)
  {
    int j = i + 1;
    if (j >= path->preperiod + path->period)
    {
      j -= path->period;
    }
    x[i] = csqrt(*X(path, j + 1, s0) - *X(path, 1, s0));
    // continuity
    double _Complex y = *X(path, i + 1, s1);
    if (cabs2(-x[i] - y) < cabs2(x[i] - y))
    {
      x[i] = -x[i];
    }
  }
  // copy
  for (int i = 0; i < path->preperiod + path->period; ++i)
  {
    *X(path, i + 1, s0) = x[i];
  }
  path->s++;
  if (path->s == path->sharpness)
  {
    path->s = 0;
    path->n++;
  }
}

extern double _Complex m_d_spider_path_get(const m_d_spider_path *path, int i)
{
  return *X((m_d_spider_path *) path, i, path->s); // const cast
}

extern double _Complex m_d_spider_path_do(const mpq_t angle, int preperiod, int period, int sharpness, int maxsteps)
{
  m_d_spider_path *path = m_d_spider_path_new(angle, preperiod, period, sharpness);
  if (! path)
  {
    return 0;
  }
  for (int i = 0; i < maxsteps; ++i)
  {
    m_d_spider_path_step(path);
  }
  double _Complex endpoint = m_d_spider_path_get(path, 1);
  m_d_spider_path_delete(path);
  return endpoint;
}
