// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_ld_util.h"

#include <stdio.h>

// BLA_FACTOR 2 - 64 is 20% faster than no BLA
#define USE_BLA
#define BLA_FACTOR 2

struct m_ld_exray_in_perturbed
{
  const m_r_orbit_ld *orbit;
#ifdef USE_BLA
  m_ld_bla *bla;
#endif
  mpq_t angle;
  mpq_t one;
  int sharpness;
  long double er;
  long double _Complex c;
  int j;
  int k;
};

extern m_ld_exray_in_perturbed *m_ld_exray_in_perturbed_new(const m_r_orbit_ld *orbit, const mpq_t angle, int sharpness)
{
  if (! orbit)
  {
    return 0;
  }
  m_ld_exray_in_perturbed *ray = malloc(sizeof(*ray));
  if (! ray)
  {
    return 0;
  }
  ray->er = 65536.0;
#ifdef USE_BLA
  ray->bla = m_ld_bla_new(orbit, 64, ray->er);
  if (! ray->bla)
  {
    free(ray);
    return 0;
  }
#endif
  ray->orbit = orbit;
  mpq_init(ray->angle);
  mpq_set(ray->angle, angle);
  mpq_init(ray->one);
  mpq_set_ui(ray->one, 1, 1);
  ray->sharpness = sharpness;
  long double a = twopil * mpq_get_d(ray->angle);
  ray->c = ray->er * (cosl(a) + I * sinl(a));
  if (m_r_orbit_ld_get_preperiod(orbit) + m_r_orbit_ld_get_period(orbit) > 1)
  {
    ray->c -= m_r_orbit_ld_get_z(orbit, 1);
  }
  ray->k = 0;
  ray->j = 0;
  return ray;
}

extern void m_ld_exray_in_perturbed_delete(m_ld_exray_in_perturbed *ray) {
  if (! ray) {
    return;
  }
#ifdef USE_BLA
  m_ld_bla_delete(ray->bla);
#endif
  mpq_clear(ray->angle);
  mpq_clear(ray->one);
  free(ray);
}

extern m_newton m_ld_exray_in_perturbed_step(m_ld_exray_in_perturbed *ray, int maxsteps) {
  if (! ray)
  {
    return m_failed;
  }
  long double epsilon16 = nextafterl(16, 32) - 16;
  if (ray->j >= ray->sharpness) {
    mpq_mul_2exp(ray->angle, ray->angle, 1);
    if (mpq_cmp_ui(ray->angle, 1, 1) >= 0) {
      mpq_sub(ray->angle, ray->angle, ray->one);
    }
    ray->k = ray->k + 1;
    ray->j = 0;
  }
  long double r = powl(ray->er, powl(0.5, (ray->j + 0.5) / ray->sharpness));
  long double a = twopil * mpq_get_d(ray->angle);
  long double _Complex target = r * (cosl(a) + I * sinl(a));
  long double _Complex c = ray->c, c_old = 0.0/0.0;
  long double _Complex z = 0, dc = 0, Z = 0;
  const int preperiod = m_r_orbit_ld_get_preperiod(ray->orbit);
  const int period = m_r_orbit_ld_get_period(ray->orbit);
  const int length = preperiod + period;
  const long double * restrict orbit = (const long double * restrict) m_r_orbit_ld_get_z_ptr(ray->orbit);
  for (int i = 0; i < maxsteps; ++i)
  {
#ifdef USE_BLA
    z = 0;
    dc = 0;
    int n = 0, m = 0;
    m_ld_bla_iterate_z_dc(ray->bla, 1ull << 48, ray->k + 1, c, &z, &dc, &n, &m);
    if (n != ray->k + 1)
    {
      fprintf(stderr, "unexpected iteraton count mismatch\ngot = %d ; expected = %d\nc = %Le %Le ; z = %Le %Le\n", n, ray->k + 1, creall(c), cimagl(c), creall(z), cimagl(z));
      abort();
    }
#else
    // this version is a few % faster than using _Complex
    long double zx = 0, zy = 0, dcx = 0, dcy = 0, cx = creall(c), cy = cimagl(c);
    int m = 0;
    int k = ray->k;
    for (int p = 0; p <= k; ++p)
    {
      long double Zx = orbit[2 * m];
      long double Zy = orbit[2 * m + 1];
      long double Zzx = Zx + zx;
      long double Zzy = Zy + zy;
      long double Zz2 = Zzx * Zzx + Zzy * Zzy;
      long double z2 = zx * zx + zy * zy;
      if (Zz2 < z2)
      {
        zx = Zzx;
        zy = Zzy;
        m = 0;
        Zx = 0;
        Zy = 0;
      }
      long double Z2zx = 2 * Zx + zx;
      long double Z2zy = 2 * Zy + zy;
      long double dcxn = 2 * (Zzx * dcx - Zzy * dcy) + 1;
      long double dcyn = 2 * (Zzx * dcy + Zzy * dcx);
      long double zxn = Z2zx * zx - Z2zy * zy + cx;
      long double zyn = Z2zx * zy + Z2zy * zx + cy;
      dcx = dcxn;
      dcy = dcyn;
      zx = zxn;
      zy = zyn;
      if (++m == length)
      {
        m = preperiod;
      }
    }
    z = zx + I * zy;
    dc = dcx + I * dcy;
#endif
    Z = orbit[2 * m] + I * orbit[2 * m + 1];
    c_old = c;
    c -= (Z + z - target) / dc;
    if (cabsl(c_old - c) == 0 || ! cisfinitel(c))
    {
      break;
    }
  }
  if (cabsl(c - ray->c) <= epsilon16 * cabsl(ray->c))
  {
    fprintf(stderr, "exray failed: c too close to previous: |c| = %Lg ; j = %d; k = %d\n", cabsl(c), ray->j, ray->k);
    return m_converged;
  }
  if (cisfinitel(c))
  {
    ray->c = c;
    ray->j = ray->j + 1;
#ifdef USE_BLA
    long double rc = m_ld_bla_get_rc(ray->bla);
    if (cabsl(ray->c) * BLA_FACTOR * 2 < rc || rc < cabsl(c))
    {
      m_ld_bla_update(ray->bla, 64, cabsl(c) * 2);
    }
#endif
    return m_stepped;
  }
  else
  {
    fprintf(stderr, "exray failed: c not finite: j = %d ; k = %d ; c_old = %Lg %Lg ; z = %Lg %Lg ; target = %Lg %Lg ; Z = %Lg %Lg\n", ray->j, ray->k, creall(c_old), cimagl(c_old), creall(z), cimagl(z), creall(target), cimagl(target), creall(Z), cimagl(Z));
    return m_failed;
  }
}

extern long double _Complex m_ld_exray_in_perturbed_get(const m_ld_exray_in_perturbed *ray)
{
  if (! ray) {
    return 0.0/0.0;
  }
  return ray->c;
}

extern m_ld_exray_in_perturbed *m_ld_exray_in_perturbed_rebase(m_ld_exray_in_perturbed *ray, long double _Complex delta_c, const m_r_orbit_ld *orbit)
{
  if (! ray)
  {
    return 0;
  }
  if (! orbit)
  {
    return 0;
  }
  ray->c -= delta_c;
  ray->orbit = orbit;
#ifdef USE_BLA
  m_ld_bla_delete(ray->bla);
  ray->bla = m_ld_bla_new(orbit, 64, cabsl(ray->c) * 2);
  if (! ray->bla)
  {
    free(ray);
    return 0;
  }
#endif
  return ray;
}
