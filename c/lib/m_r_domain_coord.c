// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>

extern m_newton m_r_domain_coord_step_raw(mpc_t c_out, const mpc_t c_guess, const mpc_t domain_coord, int loperiod, int hiperiod, mpc_t z, mpc_t dc, mpc_t zp, mpc_t dcp, mpc_t f, mpc_t df, mpc_t c_new, mpc_t d, mpfr_t d2, mpfr_t epsilon2) {
  // z = 0; dc = 0; zp = 0; dcp = 0;
  mpc_set_si(z, 0, MPC_RNDNN);
  mpc_set_si(dc, 0, MPC_RNDNN);
  mpc_set_si(zp, 0, MPC_RNDNN);
  mpc_set_si(dcp, 0, MPC_RNDNN);
  for (int i = 1; i <= hiperiod; ++i) {
    // dc = 2 * z * dc + 1;
    mpc_mul(dc, z, dc, MPC_RNDNN);
    mpc_mul_2si(dc, dc, 1, MPC_RNDNN);
    mpc_add_ui(dc, dc, 1, MPC_RNDNN);
    // z = z * z + c_guess;
    mpc_sqr(z, z, MPC_RNDNN);
    mpc_add(z, z, c_guess, MPC_RNDNN);
    if (i == loperiod)
    {
      // zp = z; dcp = dc;
      mpc_set(zp, z, MPC_RNDNN);
      mpc_set(dcp, dc, MPC_RNDNN);
    }
  }
  // f = z / zp - domain_coord;
  mpc_div(f, z, zp, MPC_RNDNN);
  mpc_sub(f, f, domain_coord, MPC_RNDNN);
  // df = (dc * zp - z * dcp) / (zp * zp);
  mpc_mul(dc, dc, zp, MPC_RNDNN);
  mpc_mul(dcp, dcp, z, MPC_RNDNN);
  mpc_sqr(zp, zp, MPC_RNDNN);
  mpc_sub(z, dc, dcp, MPC_RNDNN);
  mpc_div(df, z, zp, MPC_RNDNN);
  // check |df|^2 < epsilon2
  mpc_norm(d2, df, MPFR_RNDN);
  if (mpfr_lessequal_p(d2, epsilon2)) {
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_converged;
  }
  // c_new = c_guess - f / df;
  mpc_div(f, f, df, MPC_RNDNN);
  mpc_sub(c_new, c_guess, f, MPC_RNDNN);
  // d = c_new - c_guess;
  mpc_sub(d, c_new, c_guess, MPC_RNDNN);
  mpc_norm(d2, d, MPFR_RNDN);
  if (mpfr_lessequal_p(d2, epsilon2)) {
    mpc_set(c_out, c_new, MPC_RNDNN);
    return m_converged;
  }
  if (mpfr_number_p(mpc_realref(d)) && mpfr_number_p(mpc_imagref(d))) {
    mpc_set(c_out, c_new, MPC_RNDNN);
    return m_stepped;
  } else {
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_failed;
  }
}

extern m_newton m_r_domain_coord_step(mpc_t c_out, const mpc_t c_guess, const mpc_t domain_coord, int loperiod, int hiperiod) {
  // prec
  mpfr_prec_t precr, preci, prec;
  mpc_get_prec2(&precr, &preci, c_guess);
  prec = precr > preci ? precr : preci;
  mpc_set_prec(c_out, prec); // FIXME might trash when c_out = c_guess
  // init
  mpc_t z, dc, c_new, d, zp, dcp, f, df;
  mpfr_t d2, epsilon2;
  mpc_init2(z, prec);
  mpc_init2(dc, prec);
  mpc_init2(c_new, prec);
  mpc_init2(d, prec);
  mpc_init2(zp, prec);
  mpc_init2(dcp, prec);
  mpc_init2(f, prec);
  mpc_init2(df, prec);
  mpfr_init2(d2, prec);
  mpfr_init2(epsilon2, prec);
  // epsilon
  mpfr_set_si(epsilon2, 2, MPFR_RNDN);
  mpfr_nextabove(epsilon2);
  mpfr_sub_si(epsilon2, epsilon2, 2, MPFR_RNDN);
  mpfr_sqr(epsilon2, epsilon2, MPFR_RNDN);
  // step raw
  m_newton retval = m_r_domain_coord_step_raw(c_out, c_guess, domain_coord, loperiod, hiperiod, z, dc, zp, dcp, f, df, c_new, d, d2, epsilon2);
  // cleanup
  mpc_clear(z);
  mpc_clear(dc);
  mpc_clear(c_new);
  mpc_clear(d);
  mpc_clear(zp);
  mpc_clear(dcp);
  mpc_clear(f);
  mpc_clear(df);
  mpfr_clear(d2);
  mpfr_clear(epsilon2);
  return retval;
}

extern m_newton m_r_domain_coord(mpc_t c_out, const mpc_t c_guess, const mpc_t domain_coord, int loperiod, int hiperiod, int maxsteps) {
  m_newton result = m_failed;
  // prec
  mpfr_prec_t precr, preci, prec;
  mpc_get_prec2(&precr, &preci, c_guess);
  prec = precr > preci ? precr : preci;
  // init
  mpc_t c, z, dc, c_new, d, zp, dcp, f, df;
  mpfr_t d2, epsilon2;
  mpc_init2(c, prec);
  mpc_init2(z, prec);
  mpc_init2(dc, prec);
  mpc_init2(c_new, prec);
  mpc_init2(d, prec);
  mpc_init2(zp, prec);
  mpc_init2(dcp, prec);
  mpc_init2(f, prec);
  mpc_init2(df, prec);
  mpfr_init2(d2, prec);
  mpfr_init2(epsilon2, prec);
  // epsilon
  mpfr_set_si(epsilon2, 2, MPFR_RNDN);
  mpfr_nextabove(epsilon2);
  mpfr_sub_si(epsilon2, epsilon2, 2, MPFR_RNDN);
  mpfr_sqr(epsilon2, epsilon2, MPFR_RNDN);
  // c = c_guess
  mpc_set(c, c_guess, MPC_RNDNN);
  for (int i = 0; i < maxsteps; ++i) {
    if (m_stepped != (result = m_r_domain_coord_step_raw(c, c, domain_coord, loperiod, hiperiod, z, dc, zp, dcp, f, df, c_new, d, d2, epsilon2))) {
      break;
    }
  }
  // c_out = c;
  mpc_set_prec(c_out, prec);
  mpc_set(c_out, c, MPC_RNDNN);
  // cleanup
  mpc_clear(c);
  mpc_clear(z);
  mpc_clear(dc);
  mpc_clear(c_new);
  mpc_clear(d);
  mpc_clear(zp);
  mpc_clear(dcp);
  mpc_clear(f);
  mpc_clear(df);
  mpfr_clear(d2);
  mpfr_clear(epsilon2);
  return result;
}

extern m_newton m_r_domain_coord_dynamic_step_raw(mpc_t c_out, const mpc_t c_guess, const mpc_t domain_coord, int hiperiod, mpc_t z, mpc_t dc, mpc_t zp, mpc_t dcp, mpc_t f, mpc_t df, mpc_t c_new, mpc_t d, mpfr_t d2, mpfr_t epsilon2) {
  // z = 0; dc = 0; zp = 0; dcp = 0;
  mpc_set_si(z, 0, MPC_RNDNN);
  mpc_set_si(dc, 0, MPC_RNDNN);
  mpc_set_si(zp, 0, MPC_RNDNN);
  mpc_set_si(dcp, 0, MPC_RNDNN);
  mpfr_set_d(mpc_imagref(f), 1.0/0.0, MPFR_RNDN);
  for (int i = 1; i <= hiperiod; ++i) {
    // dc = 2 * z * dc + 1;
    mpc_mul(dc, z, dc, MPC_RNDNN);
    mpc_mul_2si(dc, dc, 1, MPC_RNDNN);
    mpc_add_ui(dc, dc, 1, MPC_RNDNN);
    // z = z * z + c_guess;
    mpc_sqr(z, z, MPC_RNDNN);
    mpc_add(z, z, c_guess, MPC_RNDNN);
    mpc_norm(mpc_realref(f), z, MPFR_RNDN);
    if (mpfr_less_p(mpc_realref(f), mpc_imagref(f)) && i < hiperiod)
    {
      // zp = z; dcp = dc;
      mpfr_set(mpc_imagref(f), mpc_realref(f), MPFR_RNDN);
      mpc_set(zp, z, MPC_RNDNN);
      mpc_set(dcp, dc, MPC_RNDNN);
    }
  }
  // f = z / zp - domain_coord;
  mpc_div(f, z, zp, MPC_RNDNN);
  mpc_sub(f, f, domain_coord, MPC_RNDNN);
  // df = (dc * zp - z * dcp) / (zp * zp);
  mpc_mul(dc, dc, zp, MPC_RNDNN);
  mpc_mul(dcp, dcp, z, MPC_RNDNN);
  mpc_sqr(zp, zp, MPC_RNDNN);
  mpc_sub(z, dc, dcp, MPC_RNDNN);
  mpc_div(df, z, zp, MPC_RNDNN);
  // check |df|^2 < epsilon2
  mpc_norm(d2, df, MPFR_RNDN);
  if (mpfr_lessequal_p(d2, epsilon2)) {
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_converged;
  }
  // c_new = c_guess - f / df;
  mpc_div(f, f, df, MPC_RNDNN);
  mpc_sub(c_new, c_guess, f, MPC_RNDNN);
  // d = c_new - c_guess;
  mpc_sub(d, c_new, c_guess, MPC_RNDNN);
  mpc_norm(d2, d, MPFR_RNDN);
  if (mpfr_lessequal_p(d2, epsilon2)) {
    mpc_set(c_out, c_new, MPC_RNDNN);
    return m_converged;
  }
  if (mpfr_number_p(mpc_realref(d)) && mpfr_number_p(mpc_imagref(d))) {
    mpc_set(c_out, c_new, MPC_RNDNN);
    return m_stepped;
  } else {
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_failed;
  }
}

extern m_newton m_r_domain_coord_dynamic_step(mpc_t c_out, const mpc_t c_guess, const mpc_t domain_coord, int hiperiod) {
  // prec
  mpfr_prec_t precr, preci, prec;
  mpc_get_prec2(&precr, &preci, c_guess);
  prec = precr > preci ? precr : preci;
  mpc_set_prec(c_out, prec); // FIXME might trash when c_out = c_guess
  // init
  mpc_t z, dc, c_new, d, zp, dcp, f, df;
  mpfr_t d2, epsilon2;
  mpc_init2(z, prec);
  mpc_init2(dc, prec);
  mpc_init2(c_new, prec);
  mpc_init2(d, prec);
  mpc_init2(zp, prec);
  mpc_init2(dcp, prec);
  mpc_init2(f, prec);
  mpc_init2(df, prec);
  mpfr_init2(d2, prec);
  mpfr_init2(epsilon2, prec);
  // epsilon
  mpfr_set_si(epsilon2, 2, MPFR_RNDN);
  mpfr_nextabove(epsilon2);
  mpfr_sub_si(epsilon2, epsilon2, 2, MPFR_RNDN);
  mpfr_sqr(epsilon2, epsilon2, MPFR_RNDN);
  // step raw
  m_newton retval = m_r_domain_coord_dynamic_step_raw(c_out, c_guess, domain_coord, hiperiod, z, dc, zp, dcp, f, df, c_new, d, d2, epsilon2);
  // cleanup
  mpc_clear(z);
  mpc_clear(dc);
  mpc_clear(c_new);
  mpc_clear(d);
  mpc_clear(zp);
  mpc_clear(dcp);
  mpc_clear(f);
  mpc_clear(df);
  mpfr_clear(d2);
  mpfr_clear(epsilon2);
  return retval;
}

extern m_newton m_r_domain_coord_dynamic(mpc_t c_out, const mpc_t c_guess, const mpc_t domain_coord, int hiperiod, int maxsteps) {
  m_newton result = m_failed;
  // prec
  mpfr_prec_t precr, preci, prec;
  mpc_get_prec2(&precr, &preci, c_guess);
  prec = precr > preci ? precr : preci;
  // init
  mpc_t c, z, dc, c_new, d, zp, dcp, f, df;
  mpfr_t d2, epsilon2;
  mpc_init2(c, prec);
  mpc_init2(z, prec);
  mpc_init2(dc, prec);
  mpc_init2(c_new, prec);
  mpc_init2(d, prec);
  mpc_init2(zp, prec);
  mpc_init2(dcp, prec);
  mpc_init2(f, prec);
  mpc_init2(df, prec);
  mpfr_init2(d2, prec);
  mpfr_init2(epsilon2, prec);
  // epsilon
  mpfr_set_si(epsilon2, 2, MPFR_RNDN);
  mpfr_nextabove(epsilon2);
  mpfr_sub_si(epsilon2, epsilon2, 2, MPFR_RNDN);
  mpfr_sqr(epsilon2, epsilon2, MPFR_RNDN);
  // c = c_guess
  mpc_set(c, c_guess, MPC_RNDNN);
  for (int i = 0; i < maxsteps; ++i) {
    if (m_stepped != (result = m_r_domain_coord_dynamic_step_raw(c, c, domain_coord, hiperiod, z, dc, zp, dcp, f, df, c_new, d, d2, epsilon2))) {
      break;
    }
  }
  // c_out = c;
  mpc_set_prec(c_out, prec);
  mpc_set(c_out, c, MPC_RNDNN);
  // cleanup
  mpc_clear(c);
  mpc_clear(z);
  mpc_clear(dc);
  mpc_clear(c_new);
  mpc_clear(d);
  mpc_clear(zp);
  mpc_clear(dcp);
  mpc_clear(f);
  mpc_clear(df);
  mpfr_clear(d2);
  mpfr_clear(epsilon2);
  return result;
}
