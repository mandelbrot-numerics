// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_ld_util.h"
#include <stdio.h>

struct m_ld_bla_level
{
  long double _Complex *dz;
  long double _Complex *dc;
  long double *rz;
  int skip_last;
  int length;
};

struct m_ld_bla
{
  const m_r_orbit_ld *orbit;
  struct m_ld_bla_level *level;
  int levels;
  long double rc;
};

void m_ld_bla_update(m_ld_bla *bla, int accuracy, long double rc)
{
  long double epsilon = ldexpl(1.0L, -accuracy);
  int n = bla->level[0].length;
  for (int i = 0; i < n; ++i)
  {
    bla->level[0].rz[i] = epsilon * cabsmaxl(bla->level[0].dz[i]);
  }
  int m = n;
  n = (n + 1) / 2;
  for (int l = 1; l < bla->levels; ++l, m = n, n = (n + 1) / 2)
  {
    for (int i = 0; i < m / 2; ++i)
    {
      long double _Complex dz1 = bla->level[l - 1].dz[2 * i];
      long double _Complex dc1 = bla->level[l - 1].dc[2 * i];
      long double rz1 = bla->level[l - 1].rz[2 * i];
      long double rz2 = bla->level[l - 1].rz[2 * i + 1];
      bla->level[l].rz[i] = fminl(rz1, fmaxl(0, (rz2 - rc * cabsmaxl(dc1)) / cabsmaxl(dz1)));
    }
    if (2 * n > m)
    {
      bla->level[l].rz[n - 1] = bla->level[l - 1].rz[2 * (n - 1)];
    }
  }
}

void m_ld_bla_delete(m_ld_bla *bla)
{
  if (bla)
  {
    if (bla->level)
    {
      for (int l = 0; l < bla->levels; ++l)
      {
        if (bla->level[l].dz) free(bla->level[l].dz);
        if (bla->level[l].dc) free(bla->level[l].dc);
        if (bla->level[l].rz) free(bla->level[l].rz);
      }
      free(bla->level);
    }
    free(bla);
  }
}

m_ld_bla *m_ld_bla_new(const m_r_orbit_ld *orbit, int accuracy, long double _Complex rc)
{
  int length = m_r_orbit_ld_get_preperiod(orbit) + m_r_orbit_ld_get_period(orbit) - 1;
  int levels = 1;
  for (int n = length; n > 1; n = (n + 1) / 2)
  {
     ++levels;
  }
  m_ld_bla *bla = calloc(1, sizeof(*bla));
  if (! bla)
  {
    return 0;
  }
  bla->orbit = orbit;
  bla->levels = levels;
  bla->level = calloc(1, sizeof(*bla->level) * levels);
  if (! bla->level)
  {
    m_ld_bla_delete(bla);
    return 0;
  }
  bool ok = true;
  int n;
  for (int l = 0, n = length; ok && l < levels; ++l, n = (n + 1) / 2)
  {
    ok &=
      (bla->level[l].dz = malloc(sizeof(*bla->level[l].dz) * n)) &&
      (bla->level[l].dc = malloc(sizeof(*bla->level[l].dc) * n)) &&
      (bla->level[l].rz = malloc(sizeof(*bla->level[l].rz) * n));
  }
  if (! ok)
  {
    m_ld_bla_delete(bla);
    return 0;
  }
  n = length;
  const long double _Complex *Z = m_r_orbit_ld_get_z_ptr(orbit);
  for (int i = 0; i < n; ++i)
  {
    bla->level[0].dz[i] = 2 * Z[i + 1];
    bla->level[0].dc[i] = 1;
  }
  bla->level[0].skip_last = 1;
  bla->level[0].length = n;
  int m = n;
  n = (n + 1) / 2;
  for (int l = 1; l < levels; ++l, m = n, n = (n + 1) / 2)
  {
    for (int i = 0; i < m / 2; ++i)
    {
      long double _Complex dz1 = bla->level[l - 1].dz[2 * i];
      long double _Complex dz2 = bla->level[l - 1].dz[2 * i + 1];
      long double _Complex dc1 = bla->level[l - 1].dc[2 * i];
      long double _Complex dc2 = bla->level[l - 1].dc[2 * i + 1];
      bla->level[l].dz[i] = dz2 * dz1;
      bla->level[l].dc[i] = dz2 * dc1 + dc2;
    }
    if (2 * n > m)
    {
      bla->level[l].dz[n - 1] = bla->level[l - 1].dz[2 * (n - 1)];
      bla->level[l].dc[n - 1] = bla->level[l - 1].dc[2 * (n - 1)];
      bla->level[l].skip_last = bla->level[l - 1].skip_last;
    }
    else
    {
      bla->level[l].skip_last = bla->level[l - 1].skip_last + (1 << (l - 1));
    }
    bla->level[l].length = n;
  }
  m_ld_bla_update(bla, accuracy, rc);
  return bla;
}

bool m_ld_bla_iterate_z_dc(const m_ld_bla *bla, long double er, int N, long double _Complex c, long double _Complex *zp, long double _Complex *dcp, int *np, int *mp)
{
  const long double _Complex *Zp = m_r_orbit_ld_get_z_ptr(bla->orbit);
  long double _Complex z = *zp;
  long double _Complex dc = *dcp;
  int n = *np;
  int m = *mp;
  int length = bla->level[0].length + 1;
  long double er2 = er * er;
  while (n < N)
  {
    long double _Complex Z = Zp[m];
    long double _Complex Zz = Z + z;
    long double mZz = cabsmaxl(Zz);
    long double mz = cabsmaxl(z);
    if (mZz < mz)
    {
      z = Zz;
      m = 0;
      Z = 0;
//fprintf(stderr, " r");
    }
    if (cabs2l(Zz) > er2)
    {
      *zp = z;
      *dcp = dc;
      *np = n;
      *mp = m;
//fprintf(stderr, "\n");
      return true;
    }
    int level = -1;
    if (m > 0)
    {
      for (int l = 0, k = m - 1; l < bla->levels && (k << l) == m - 1; ++l, k >>= 1)
      {
        if (mz < bla->level[l].rz[k])
        {
          level = l;
        }
        else
        {
          break;
        }
      }
    }
    if (level >= 0)
    {
      int k = (m - 1) >> level;
      dc = bla->level[level].dz[k] * dc + bla->level[level].dc[k];
      z = bla->level[level].dz[k] * z + bla->level[level].dc[k] * c;
      int s = k + 1 == bla->level[level].length ? bla->level[level].skip_last : 1 << level;
      n += s;
      m += s;
//fprintf(stderr, " b%d", s);
    }
    else
    {
      dc = 2 * Zz * dc + 1;
      z = (2 * Z + z) * z + c;
      n += 1;
      m += 1;
//fprintf(stderr, " p1");
    }
    if (m == length)
    {
      m = 0;//preperiod;
    }
  }
  *zp = z;
  *dcp = dc;
  *np = n;
  *mp = m;
//fprintf(stderr, "\n");
  return false;
}

long double m_ld_bla_get_rc(const m_ld_bla *bla)
{
  return bla->rc;
}
