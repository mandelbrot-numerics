// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>

// https://en.wikipedia.org/wiki/Talk:Logistic_map#Very_old_discussions
extern double m_d_from_logistic(double r)
{
  double t = r - 1;
  return (1 - t * t) / 4;
}
