// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2020 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

extern void m_r_misiurewicz_size(mpfr_t size, const mpc_t c, int preperiod, int period)
{
  mpfr_prec_t prec = mpfr_get_prec(mpc_realref(c));
  mpc_t z, dc, zp, dcp;
  mpc_init2(z, prec);
  mpc_init2(dc, prec);
  mpc_init2(zp, prec);
  mpc_init2(dcp, prec);
  mpc_set_dc(z, 0, MPC_RNDNN);
  mpc_set_dc(dc, 0, MPC_RNDNN);
  mpc_set_dc(zp, 0, MPC_RNDNN);
  mpc_set_dc(dcp, 0, MPC_RNDNN);
  for (int p = 0; p < period; ++p)
  {
    // dcp = 2 * zp * dcp + 1;
    mpc_mul(dcp, dcp, zp, MPC_RNDNN);
    mpfr_mul_2si(mpc_realref(dcp), mpc_realref(dcp), 1, MPFR_RNDN);
    mpfr_mul_2si(mpc_imagref(dcp), mpc_imagref(dcp), 1, MPFR_RNDN);
    mpfr_add_ui(mpc_realref(dcp), mpc_realref(dcp), 1, MPFR_RNDN);
    // zp = zp * zp + c;
    mpc_sqr(zp, zp, MPC_RNDNN);
    mpc_add(zp, zp, c, MPC_RNDNN);
  }
  mpfr_t zq2;
  mpfr_init2(zq2, prec);
  mpfr_t zp2;
  mpfr_init2(zp2, prec);
  mpc_t d;
  mpc_init2(d, prec);
  // zq2 = cabs2(zp - z);
  mpc_sub(d, zp, z, MPC_RNDNN);
  mpc_norm(zq2, d, MPFR_RNDN);
  for (int q = 1; q <= preperiod; ++q)
  {
    // dcp = 2 * zp * dcp + 1;
    mpc_mul(dcp, dcp, zp, MPC_RNDNN);
    mpfr_mul_2si(mpc_realref(dcp), mpc_realref(dcp), 1, MPFR_RNDN);
    mpfr_mul_2si(mpc_imagref(dcp), mpc_imagref(dcp), 1, MPFR_RNDN);
    mpfr_add_ui(mpc_realref(dcp), mpc_realref(dcp), 1, MPFR_RNDN);
    // zp = zp * zp + c;
    mpc_sqr(zp, zp, MPC_RNDNN);
    mpc_add(zp, zp, c, MPC_RNDNN);
    // dc = 2 * z * dc + 1;
    mpc_mul(dc, dc, z, MPC_RNDNN);
    mpfr_mul_2si(mpc_realref(dc), mpc_realref(dc), 1, MPFR_RNDN);
    mpfr_mul_2si(mpc_imagref(dc), mpc_imagref(dc), 1, MPFR_RNDN);
    mpfr_add_ui(mpc_realref(dc), mpc_realref(dc), 1, MPFR_RNDN);
    // z = z * z + c;
    mpc_sqr(z, z, MPC_RNDNN);
    mpc_add(z, z, c, MPC_RNDNN);
    // zp2 = cabs2(zp - z);
    mpc_sub(d, zp, z, MPC_RNDNN);
    mpc_norm(zp2, d, MPFR_RNDN);
    if (q < preperiod && mpfr_less_p(zp2, zq2))
      mpfr_set(zq2, zp2, MPFR_RNDN);
  }
  // size = sqrt(zq2) / cabs(dcp - dc);
  mpc_sub(d, dcp, dc, MPC_RNDNN);
  mpc_abs(zp2, d, MPFR_RNDN);
  mpfr_sqrt(zq2, zq2, MPFR_RNDN);
  mpfr_div(size, zq2, zp2, MPFR_RNDN);
}
