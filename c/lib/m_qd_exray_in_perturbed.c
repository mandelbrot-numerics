// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"
#include "m_qd_util.h"

#include <stdio.h>

// BLA_FACTOR 2 - 64 is 20% faster than no BLA
#define USE_BLA
#define BLA_FACTOR 2

struct m_qd_exray_in_perturbed
{
  const m_r_orbit_qd *orbit;
#ifdef USE_BLA
  m_qd_bla *bla;
#endif
  mpq_t angle;
  mpq_t one;
  int sharpness;
  _Float128 er;
  _Float128 _Complex c;
  int j;
  int k;
};

extern m_qd_exray_in_perturbed *m_qd_exray_in_perturbed_new(const m_r_orbit_qd *orbit, const mpq_t angle, int sharpness)
{
  if (! orbit)
  {
    return 0;
  }
  m_qd_exray_in_perturbed *ray = malloc(sizeof(*ray));
  if (! ray)
  {
    return 0;
  }
  ray->er = 65536.0;
#ifdef USE_BLA
  ray->bla = m_qd_bla_new(orbit, 113, ray->er);
  if (! ray->bla)
  {
    free(ray);
    return 0;
  }
#endif
  ray->orbit = orbit;
  mpq_init(ray->angle);
  mpq_set(ray->angle, angle);
  mpq_init(ray->one);
  mpq_set_ui(ray->one, 1, 1);
  ray->sharpness = sharpness;
  double a = twopi * mpq_get_d(ray->angle);
  ray->c = ray->er * (cos(a) + I * sin(a));
  if (m_r_orbit_qd_get_preperiod(orbit) + m_r_orbit_qd_get_period(orbit) > 1)
  {
    ray->c -= m_r_orbit_qd_get_z(orbit, 1);
  }
  ray->k = 0;
  ray->j = 0;
  return ray;
}

extern void m_qd_exray_in_perturbed_delete(m_qd_exray_in_perturbed *ray) {
  if (! ray) {
    return;
  }
#ifdef USE_BLA
  m_qd_bla_delete(ray->bla);
#endif
  mpq_clear(ray->angle);
  mpq_clear(ray->one);
  free(ray);
}

extern m_newton m_qd_exray_in_perturbed_step(m_qd_exray_in_perturbed *ray, int maxsteps) {
  if (! ray)
  {
    return m_failed;
  }
  _Float128 epsilon16 = nextafterf128(16, 32) - 16;
  if (ray->j >= ray->sharpness) {
    mpq_mul_2exp(ray->angle, ray->angle, 1);
    if (mpq_cmp_ui(ray->angle, 1, 1) >= 0) {
      mpq_sub(ray->angle, ray->angle, ray->one);
    }
    ray->k = ray->k + 1;
    ray->j = 0;
  }
  _Float128 r = pow(ray->er, pow(0.5, (ray->j + 0.5) / ray->sharpness));
  double a = twopi * mpq_get_d(ray->angle);
  _Float128 _Complex target = r * (cos(a) + I * sin(a));
  _Float128 _Complex c = ray->c, c_old = 0.0/0.0;
  _Float128 _Complex z = 0, dc = 0, Z = 0;
  const int preperiod = m_r_orbit_qd_get_preperiod(ray->orbit);
  const int period = m_r_orbit_qd_get_period(ray->orbit);
  const int length = preperiod + period;
  const _Float128 * restrict orbit = (const _Float128 * restrict) m_r_orbit_qd_get_z_ptr(ray->orbit);
  for (int i = 0; i < maxsteps; ++i)
  {
#ifdef USE_BLA
    z = 0;
    dc = 0;
    int n = 0, m = 0;
    m_qd_bla_iterate_z_dc(ray->bla, 1ull << 48, ray->k + 1, c, &z, &dc, &n, &m);
    if (n != ray->k + 1)
    {
      fprintf(stderr, "%d %d %Le %Le %Le %Le\n", n, ray->k + 1, (long double) crealf128(c), (long double) cimagf128(c), (long double) crealf128(z), (long double) cimagf128(z));
      abort();
    }
#else
    // this version is a few % faster than using _Complex
    _Float128 zx = 0, zy = 0, dcx = 0, dcy = 0, cx = crealf128(c), cy = cimagf128(c);
    int m = 0;
    int k = ray->k;
    for (int p = 0; p <= k; ++p)
    {
      _Float128 Zx = orbit[2 * m];
      _Float128 Zy = orbit[2 * m + 1];
      _Float128 Zzx = Zx + zx;
      _Float128 Zzy = Zy + zy;
      _Float128 Zz2 = Zzx * Zzx + Zzy * Zzy;
      _Float128 z2 = zx * zx + zy * zy;
      if (Zz2 < z2)
      {
        zx = Zzx;
        zy = Zzy;
        m = 0;
        Zx = 0;
        Zy = 0;
      }
      _Float128 Z2zx = 2 * Zx + zx;
      _Float128 Z2zy = 2 * Zy + zy;
      _Float128 dcxn = 2 * (Zzx * dcx - Zzy * dcy) + 1;
      _Float128 dcyn = 2 * (Zzx * dcy + Zzy * dcx);
      _Float128 zxn = Z2zx * zx - Z2zy * zy + cx;
      _Float128 zyn = Z2zx * zy + Z2zy * zx + cy;
      dcx = dcxn;
      dcy = dcyn;
      zx = zxn;
      zy = zyn;
      if (++m == length)
      {
        m = preperiod;
      }
    }
    z = zx + I * zy;
    dc = dcx + I * dcy;
#endif
    Z = orbit[2 * m] + I * orbit[2 * m + 1];
    c_old = c;
    c -= (Z + z - target) / dc;
    if (cabsf128(c_old - c) == 0 || ! cisfinitef128(c))
    {
      break;
    }
  }
  if (cabsf128(c - ray->c) <= epsilon16 * cabsf128(ray->c))
  {
    fprintf(stderr, "exray failed: c too close to previous: |c| = %Lg ; j = %d; k = %d\n", (long double) cabsf128(c), ray->j, ray->k);
    return m_converged;
  }
  if (cisfinitef128(c))
  {
    ray->c = c;
    ray->j = ray->j + 1;
#ifdef USE_BLA
    _Float128 rc = m_qd_bla_get_rc(ray->bla);
    if (cabsf128(ray->c) * BLA_FACTOR * 2 < rc || rc < cabsf128(c))
    {
      m_qd_bla_update(ray->bla, 113, cabsf128(c) * 2);
    }
#endif
    return m_stepped;
  }
  else
  {
    fprintf(stderr, "exray failed: c not finite: j = %d ; k = %d ; c_old = %Lg %Lg ; z = %Lg %Lg ; target = %Lg %Lg ; Z = %Lg %Lg\n", ray->j, ray->k, creall(c_old), cimagl(c_old), creall(z), cimagl(z), creall(target), cimagl(target), creall(Z), cimagl(Z));
    return m_failed;
  }
}

extern _Float128 _Complex m_qd_exray_in_perturbed_get(const m_qd_exray_in_perturbed *ray)
{
  if (! ray) {
    return 0.0/0.0;
  }
  return ray->c;
}

extern m_qd_exray_in_perturbed *m_qd_exray_in_perturbed_rebase(m_qd_exray_in_perturbed *ray, _Float128 _Complex delta_c, const m_r_orbit_qd *orbit)
{
  if (! ray)
  {
    return 0;
  }
  if (! orbit)
  {
    return 0;
  }
  ray->c -= delta_c;
  ray->orbit = orbit;
#ifdef USE_BLA
  m_qd_bla_delete(ray->bla);
  ray->bla = m_qd_bla_new(orbit, 113, cabsf128(ray->c) * 2);
  if (! ray->bla)
  {
    free(ray);
    return 0;
  }
#endif
  return ray;
}
