// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

#include <assert.h>

struct m_d_bla
{
  double _Complex A;
  double _Complex B;
  double rz;
  double rc;
  // FIXME TODO don't store these
  int n;
  int l;
  double _pad;
};

struct m_d_blas
{
  int N;
  int L;
  m_d_bla **b;
};

extern m_d_blas *m_d_bla_new(const double _Complex *Z, int length)
{
  int N = length - 1;
  int count = 1;
  int m = N - 1;
  for ( ; m > 1; m = (m + 1) >> 1)
  {
    count++;
  }
  int L = count;
  m_d_blas *state = calloc(1, sizeof(*state));
  if (! state)
  {
    return 0;
  }
  m_d_bla **b = calloc(1, sizeof(*b) * count);
  if (! b)
  {
    free(state);
    return 0;
  }
  m = N - 1;
  int total = 0;
  int ok = 1;
  for (int ix = 0; ix < count; ++ix, m = (m + 1) >> 1)
  {
    b[ix] = malloc(sizeof(*b[ix]) * m);
    ok &= !! b[ix];
    total += m;
  }
  if (! ok)
  {
    for (int ix = 0; ix < count; ++ix)
    {
      if (b[ix])
      {
        free(b[ix]);
      }
    }
    free(b);
    free(state);
    return 0;
  }
  double e = nextafter(1, 2) - 1;
  // one-step
  for (int m = 1; m < N; ++m)
  {
    //    z_{m-1} << Z_m/A_{m-1} && c << Z_m/B_{m-1}
    // => Z_m >> A_{m-1} z_{m-1} && Z_m >> B_{m-1} c
    // => 2 Z_m >> A_{m-1} z_{m-1} + B_{m-1} c
    // => 2 Z_m >> z_m
    // => 2 Z_m z_m >> z_m^2
    // => BLA from m-1 to m is valid
    const int n = m;
    const int l = 1;
    const double _Complex A = 2 * Z[n];
    const double _Complex B = 1;
    const double rz = e * cabs(Z[n + l]) / (cabs(A));
    const double rc = e * cabs(Z[n + l]) / (cabs(B));
    m_d_bla b1 = { A, B, rz, rc, n, l, 0 };
    b[0][m - 1] = b1;
  }
  // merge
  int src = 0;
  for (int msrc = N - 1; msrc > 1; msrc = (msrc + 1) >> 1)
  {
    int dst = src + 1;
    int mdst = (msrc + 1) >> 1;
    for (int m = 0; m < mdst; ++m)
    {
      const int mx = m * 2;
      const int my = m * 2 + 1;
      if (my < msrc)
      {
        const m_d_bla x = b[src][mx];
        const m_d_bla y = b[src][my];
        const int n = x.n;
        const int l = x.l + y.l;
        const double _Complex A = y.A * x.A;
        const double _Complex B = y.A * x.B + y.B;
        const double rz0 = x.rz;
        const double rc0 = x.rc;
        const double rz1 = 0.5 * y.rz / cabs(x.A);
        const double rc1 = fmin(0.5 * y.rz / cabs(x.B), y.rc);
        const double rz2 = e * cabs(Z[n + l]) / (cabs(A));
        const double rc2 = e * cabs(Z[n + l]) / (cabs(B));
        const double rz = fmin(fmin(rz0, rz1), rz2);
        const double rc = fmin(fmin(rc0, rc1), rc2);
        m_d_bla xy = { A, B, rz, rc, n, l, 0 };
        b[dst][m] = xy;
      }
      else
      {
        b[dst][m] = b[src][mx];
      }
    }
    src++;
  }
  state->N = N;
  state->L = L;
  state->b = b;
  return state;
}

extern void m_d_bla_delete(m_d_blas *state)
{
  if (state)
  {
    if (state->b)
    {
      for (int ix = 0; ix < state->L; ++ix)
      {
        if (state->b[ix])
        {
          free(state->b[ix]);
        }
      }
      free(state->b);
    }
    free(state);
  }
}

extern const m_d_bla *m_d_bla_lookup(const m_d_blas *B, int m, double rz, double rc)
{
  if (m <= 0)
  {
    return 0;
  }
  if (! (m < B->N))
  {
    return 0;
  }
  const m_d_bla *ret = 0;
  int ix = m - 1;
  for (int level = 0; level < B->L; ++level)
  {
    int ixm = (ix << level) + 1;
    if (m == ixm && rz < B->b[level][ix].rz && rc < B->b[level][ix].rc)
    {
      assert(B->b[level][ix].n == m);
      ret = &B->b[level][ix];
    }
    else
    {
      break;
    }
    ix = ix >> 1;
  }
  return ret;
}

extern int m_d_bla_apply(const m_d_bla *b, double _Complex c, double _Complex *z, double _Complex *dc)
{
  if (! b)
  {
    return 0;
  }
  if (*dc)
  {
    *dc = b->A * *dc + b->B;
  }
  if (*z)
  {
    *z = b->A * *z + b->B * c;
  }
  return b->l;
}
