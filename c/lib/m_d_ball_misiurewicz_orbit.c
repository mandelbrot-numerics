// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

struct m_d_ball_misiurewicz_orbit
{
  const m_r_orbit_diff_d *o;
  const double _Complex *z_ptr, *d_ptr;
  int n;
  double _Complex dwdc;
  double r;
  int preperiod;
  int period;
};

extern m_d_ball_misiurewicz_orbit *m_d_ball_misiurewicz_orbit_new(const m_r_orbit_diff_d *o, double radius)
{
  m_d_ball_misiurewicz_orbit *ball = (m_d_ball_misiurewicz_orbit *) malloc(sizeof(*ball));
  if (! ball)
  {
    return 0;
  }
  ball->o = o;
  ball->z_ptr = m_r_orbit_diff_d_get_z_ptr(o);
  ball->d_ptr = m_r_orbit_diff_d_get_d_ptr(o);
  ball->n = m_r_orbit_diff_d_get_preperiod(o) + m_r_orbit_diff_d_get_period(o);
  ball->r = radius;
  ball->dwdc = radius;
  ball->preperiod = 1;
  ball->period = 0;
  return ball;
}

extern void m_d_ball_misiurewicz_orbit_delete(m_d_ball_misiurewicz_orbit *ball)
{
  if (ball)
  {
    free(ball);
  }
}

extern bool m_d_ball_misiurewicz_orbit_step(m_d_ball_misiurewicz_orbit *ball)
{
  if (! ball)
  {
    return false;
  }
  if (ball->preperiod < ball->n)
  {
    ball->dwdc = 2 * ball->z_ptr[ball->preperiod++] * ball->dwdc + ball->r;
    bool ok = cisfinite(ball->dwdc);
    return ok;
  }
  else
  {
    return false;
  }
}

extern bool m_d_ball_misiurewicz_orbit_check_periods(m_d_ball_misiurewicz_orbit *ball)
{
  if (! ball)
  {
    return true;
  }
  if (cabs(ball->d_ptr[ball->preperiod]) < cabs(ball->dwdc))
  {
    ball->period = m_r_orbit_diff_d_get_offset(ball->o);
    return true;
  }
  return false;
}

extern int m_d_ball_misiurewicz_orbit_get_period(const m_d_ball_misiurewicz_orbit *ball)
{
  if (! ball) {
    return 0;
  }
  return ball->period;
}

extern int m_d_ball_misiurewicz_orbit_get_preperiod(const m_d_ball_misiurewicz_orbit *ball)
{
  if (! ball)
  {
    return -1;
  }
  return ball->preperiod;
}

extern bool m_d_ball_misiurewicz_orbit_do(int *preperiod, int *period, const m_r_orbit_diff_d *o, double radius, int maxpreperiod)
{
  m_d_ball_misiurewicz_orbit *ball = m_d_ball_misiurewicz_orbit_new(o, radius);
  if (! ball)
  {
    return false;
  }
  for (int i = 0; i < maxpreperiod; ++i)
  {
    if (m_d_ball_misiurewicz_orbit_check_periods(ball))
    {
      *preperiod = m_d_ball_misiurewicz_orbit_get_preperiod(ball);
      *period = m_d_ball_misiurewicz_orbit_get_period(ball);
      m_d_ball_misiurewicz_orbit_delete(ball);
      return true;
    }
    if (! m_d_ball_misiurewicz_orbit_step(ball))
    {
      break;
    }
  }
  m_d_ball_misiurewicz_orbit_delete(ball);
  return false;
}
