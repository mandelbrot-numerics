// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef M_QD_UTIL_H
#define M_QD_UTIL_H 1

#include <math.h>
#include <complex.h>

static inline int sgnf128(_Float128 z) {
  if (z > 0) { return  1; }
  if (z < 0) { return -1; }
  return 0;
}

static inline _Float128 cabs2f128(_Float128 _Complex z) {
  return crealf128(z) * crealf128(z) + cimagf128(z) * cimagf128(z);
}

static inline _Float128 cabsmaxf128(_Float128 _Complex z) {
  return fmaxf128(fabsf128(crealf128(z)), fabsf128(cimagf128(z)));
}

static inline bool cisfinitef128(_Float128 _Complex z) {
  return isfinite(crealf128(z)) && isfinite(cimagf128(z));
}

static const _Float128 piq = 3.1415926535897932385L; // FIXME
static const _Float128 twopiq = 6.283185307179586477L; // FIXME

// last . takeWhile (\x -> 2 /= 2 + x) . iterate (/2) $ 1 :: Float128
static const _Float128 epsilonq = 3.851859888774471706111955885169855e-34L; // FIXME

// epsilon^2
static const _Float128 epsilon2q = 1.4836824602749685542926941046190558e-67L; // FIXME

#endif
