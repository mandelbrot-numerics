// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2020 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

/*
<https://mathr.co.uk/blog/2017-11-21_misiurewicz_domain_coordinates_and_size_estimates.html>
\[G(c, p, q, r) = \frac{F^{q + p}(0, c) - F^{q}(0, c)}{F^{r + p}(0, c) - F^{r}(0, c)}\]
*/

extern m_newton m_d_misiurewicz_coord_step(double _Complex *c_out, double _Complex c_guess, double _Complex misiurewicz_coord, int period, int lopreperiod, int hipreperiod) {
  double _Complex num = 0;
  double _Complex den = 0;
  double _Complex dnum = 0;
  double _Complex dden = 0;
  double _Complex z = 0;
  double _Complex dc = 0;
  for (int i = 1; i <= hipreperiod + period; ++i) {
    dc = 2 * z * dc + 1;
    z = z * z + c_guess;
    if (i == lopreperiod)
    {
      den -= z;
      den -= dc;
    }
    if (i == lopreperiod + period)
    {
      den += z;
      den += dc;
    }
    if (i == hipreperiod)
    {
      num -= z;
      num -= dc;
    }
    if (i == hipreperiod + period)
    {
      num += z;
      num += dc;
    }
  }
  double _Complex f = num / den - misiurewicz_coord;
  double _Complex df = (dnum * den - num * dden) / (den * den);
  double _Complex c_new = c_guess - f / df;
  double _Complex d = c_new - c_guess;
  if (cabs2(d) <= epsilon2) {
    *c_out = c_new;
    return m_converged;
  }
  if (cisfinite(d)) {
    *c_out = c_new;
    return m_stepped;
  } else {
    *c_out = c_guess;
    return m_failed;
  }
}

extern m_newton m_d_misiurewicz_coord(double _Complex *c_out, double _Complex c_guess, double _Complex misiurewicz_coord, int period, int lopreperiod, int hipreperiod, int maxsteps) {
  m_newton result = m_failed;
  double _Complex c = c_guess;
  for (int i = 0; i < maxsteps; ++i) {
    if (m_stepped != (result = m_d_misiurewicz_coord_step(&c, c, misiurewicz_coord, period, lopreperiod, hipreperiod))) {
      break;
    }
  }
  *c_out = c;
  return result;
}
