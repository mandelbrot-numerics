// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

struct j_d_exray_in {
  mpq_t angle;
  mpq_t one;
  int sharpness;
  double er;
  double _Complex c;
  double _Complex z;
  int j;
  int k;
};

extern j_d_exray_in *j_d_exray_in_new(double _Complex c, const mpq_t angle, int sharpness) {
  j_d_exray_in *ray = malloc(sizeof(*ray));
  mpq_init(ray->angle);
  mpq_set(ray->angle, angle);
  mpq_init(ray->one);
  mpq_set_ui(ray->one, 1, 1);
  ray->sharpness = sharpness;
  ray->er = 65536.0;
  double a = twopi * mpq_get_d(ray->angle);
  ray->c = c;
  ray->z = ray->er * (cos(a) + I * sin(a));
  ray->k = 0;
  ray->j = 0;
  return ray;
}

extern void j_d_exray_in_delete(j_d_exray_in *ray) {
  if (! ray) {
    return;
  }
  mpq_clear(ray->angle);
  mpq_clear(ray->one);
  free(ray);
}

extern m_newton j_d_exray_in_step(j_d_exray_in *ray, int maxsteps) {
  if (ray->j >= ray->sharpness) {
    mpq_mul_2exp(ray->angle, ray->angle, 1);
    if (mpq_cmp_ui(ray->angle, 1, 1) >= 0) {
      mpq_sub(ray->angle, ray->angle, ray->one);
    }
    ray->k = ray->k + 1;
    ray->j = 0;
  }
  double r = pow(ray->er, pow(0.5, (ray->j + 0.5) / ray->sharpness));
  double a = twopi * mpq_get_d(ray->angle);
  double _Complex target = r * (cos(a) + I * sin(a));
  double _Complex c = ray->c;
  double _Complex z0 = ray->z;
  for (int i = 0; i < maxsteps; ++i) {
    double _Complex z = z0;
    double _Complex dz = 1;
    for (int p = 0; p < ray->k; ++p) {
      dz = 2 * z * dz;
      z = z * z + c;
    }
    double _Complex z_new = z0 - (z - target) / dz;
    double d2 = cabs2(z_new - z0);
    if (cisfinite(z_new)) {
      z0 = z_new;
    } else {
      break;
    }
    if (d2 <= epsilon2) {
      break;
    }
  }
  ray->j = ray->j + 1;
  double d2 = cabs2(z0 - ray->z);
  if (d2 <= epsilon2) {
    ray->z = z0;
    return m_converged;
  }
  if (cisfinite(c)) {
    ray->z = z0;
    return m_stepped;
  } else {
    return m_failed;
  }
}

extern double _Complex j_d_exray_in_get(const j_d_exray_in *ray) {
  if (! ray) {
    return 0;
  }
  return ray->z;
}

extern double _Complex j_d_exray_in_do(double _Complex c, const mpq_t angle, int sharpness, int maxsteps, int maxnewtonsteps) {
  j_d_exray_in *ray = j_d_exray_in_new(c, angle, sharpness);
  if (! ray) {
    return 0;
  }
  for (int i = 0; i < maxsteps; ++i) {
    if (m_stepped != j_d_exray_in_step(ray, maxnewtonsteps)) {
      break;
    }
  }
  double _Complex endpoint = j_d_exray_in_get(ray);
  j_d_exray_in_delete(ray);
  return endpoint;
}
