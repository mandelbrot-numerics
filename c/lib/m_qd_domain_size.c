// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_qd_util.h"

extern _Float128 m_qd_domain_size(_Float128 _Complex nucleus, int period)
{
  _Float128 _Complex z = nucleus;
  _Float128 _Complex dc = 1;
  _Float128 zq2 = cabs2f128(z);
  for (int q = 2; q <= period; ++q)
  {
    dc = 2 * z * dc + 1;
    z = z * z + nucleus;
    _Float128 zp2 = cabs2f128(z);
    if (q < period && zp2 < zq2)
      zq2 = zp2;
  }
  return sqrtf128(zq2) / cabsf128(dc);
}

extern _Float128 m_qd_filtered_domain_size(_Float128 _Complex nucleus, int period, m_period_filter_t *filter)
{
  _Float128 _Complex z = 0;
  _Float128 _Complex dc = 0;
  _Float128 zq = 1.0/0.0;
  for (int q = 1; q <= period; ++q)
  {
    dc = 2 * z * dc + 1;
    z = z * z + nucleus;
    _Float128 zp = cabsf128(z);
    if (q < period && zp < zq && filter->accept(filter, q))
      zq = zp;
  }
  return zq / cabsf128(dc);
}

extern _Float128 m_qd_domain_size_orbit(const m_r_orbit_qd *orbit)
{
  const _Float128 _Complex *z_ptr = m_r_orbit_qd_get_z_ptr(orbit);
  const int period = m_r_orbit_qd_get_period(orbit);
  _Float128 _Complex z = z_ptr[1];
  _Float128 _Complex dc = 1;
  _Float128 zq2 = cabs2f128(z);
  for (int q = 2; q <= period; ++q)
  {
    dc = 2 * z * dc + 1;
    if (q < period)
    {
      z = z_ptr[q];
      _Float128 zp2 = cabs2f128(z);
      if (q < period && zp2 < zq2)
        zq2 = zp2;
    }
  }
  return sqrtf128(zq2) / cabsf128(dc);
}
