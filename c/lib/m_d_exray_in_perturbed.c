// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

#include <stdio.h>

struct m_d_exray_in_perturbed
{
  const m_r_orbit_d *orbit;
  mpq_t angle;
  mpq_t one;
  int sharpness;
  double er;
  double _Complex c;
  int j;
  int k;
};

extern m_d_exray_in_perturbed *m_d_exray_in_perturbed_new(const m_r_orbit_d *orbit, const mpq_t angle, int sharpness)
{
  if (! orbit)
  {
    return 0;
  }
  m_d_exray_in_perturbed *ray = malloc(sizeof(*ray));
  if (! ray)
  {
    return 0;
  }
  ray->orbit = orbit;
  mpq_init(ray->angle);
  mpq_set(ray->angle, angle);
  mpq_init(ray->one);
  mpq_set_ui(ray->one, 1, 1);
  ray->sharpness = sharpness;
  ray->er = 65536.0;
  double a = twopi * mpq_get_d(ray->angle);
  ray->c = ray->er * (cos(a) + I * sin(a));
  if (m_r_orbit_d_get_preperiod(orbit) + m_r_orbit_d_get_period(orbit) > 1)
  {
    ray->c -= m_r_orbit_d_get_z(orbit, 1);
  }
  ray->k = 0;
  ray->j = 0;
  return ray;
}

extern void m_d_exray_in_perturbed_delete(m_d_exray_in_perturbed *ray) {
  if (! ray) {
    return;
  }
  mpq_clear(ray->angle);
  mpq_clear(ray->one);
  free(ray);
}

extern m_newton m_d_exray_in_perturbed_step(m_d_exray_in_perturbed *ray, int maxsteps) {
  if (! ray)
  {
    return m_failed;
  }
  double epsilon16 = nextafter(16, 32) - 16;
  if (ray->j >= ray->sharpness) {
    mpq_mul_2exp(ray->angle, ray->angle, 1);
    if (mpq_cmp_ui(ray->angle, 1, 1) >= 0) {
      mpq_sub(ray->angle, ray->angle, ray->one);
    }
    ray->k = ray->k + 1;
    ray->j = 0;
  }
  double r = pow(ray->er, pow(0.5, (ray->j + 0.5) / ray->sharpness));
  double a = twopi * mpq_get_d(ray->angle);
  double _Complex target = r * (cos(a) + I * sin(a));
  double _Complex c = ray->c, c_old = 0.0/0.0;
  double _Complex z = 0, dc = 0, Z = 0;
  const int preperiod = m_r_orbit_d_get_preperiod(ray->orbit);
  const int period = m_r_orbit_d_get_period(ray->orbit);
  const int length = preperiod + period;
  const double * restrict orbit = (const double * restrict) m_r_orbit_d_get_z_ptr(ray->orbit);
  for (int i = 0; i < maxsteps; ++i) {
    // this version is a few % faster than using _Complex
    double zx = 0, zy = 0, dcx = 0, dcy = 0, cx = creal(c), cy = cimag(c);
    int m = 0;
    int k = ray->k;
    for (int p = 0; p <= k; ++p)
    {
      double Zx = orbit[2 * m];
      double Zy = orbit[2 * m + 1];
      double Zzx = Zx + zx;
      double Zzy = Zy + zy;
      double Zz2 = Zzx * Zzx + Zzy * Zzy;
      double z2 = zx * zx + zy * zy;
      if (Zz2 < z2)
      {
        zx = Zzx;
        zy = Zzy;
        m = 0;
        Zx = 0;
        Zy = 0;
      }
      double Z2zx = 2 * Zx + zx;
      double Z2zy = 2 * Zy + zy;
      double dcxn = 2 * (Zzx * dcx - Zzy * dcy) + 1;
      double dcyn = 2 * (Zzx * dcy + Zzy * dcx);
      double zxn = Z2zx * zx - Z2zy * zy + cx;
      double zyn = Z2zx * zy + Z2zy * zx + cy;
      dcx = dcxn;
      dcy = dcyn;
      zx = zxn;
      zy = zyn;
      if (++m == length)
      {
        m = preperiod;
      }
    }
    z = zx + I * zy;
    dc = dcx + I * dcy;
    Z = orbit[2 * m] + I * orbit[2 * m + 1];
    c_old = c;
    c -= (Z + z - target) / dc;
    if (cabs(c_old - c) == 0 || ! cisfinite(c))
    {
      break;
    }
  }
  if (cabs(c - ray->c) <= epsilon16 * cabs(ray->c))
  {
    fprintf(stderr, "exray failed: c too close to previous: |c| = %g ; j = %d; k = %d\n", cabs(c), ray->j, ray->k);
    return m_converged;
  }
  if (cisfinite(c))
  {
    ray->c = c;
    ray->j = ray->j + 1;
    return m_stepped;
  }
  else
  {
    fprintf(stderr, "exray failed: c not finite: j = %d ; k = %d ; c_old = %g %g ; z = %g %g ; target = %g %g ; Z = %g %g\n", ray->j, ray->k, creal(c_old), cimag(c_old), creal(z), cimag(z), creal(target), cimag(target), creal(Z), cimag(Z));
    return m_failed;
  }
}

extern double _Complex m_d_exray_in_perturbed_get(const m_d_exray_in_perturbed *ray)
{
  if (! ray) {
    return 0.0/0.0;
  }
  return ray->c;
}

extern void m_d_exray_in_perturbed_rebase(m_d_exray_in_perturbed *ray, double _Complex delta_c, const m_r_orbit_d *orbit)
{
  if (! ray)
  {
    return;
  }
  if (! orbit)
  {
    return;
  }
  ray->c -= delta_c;
  ray->orbit = orbit;
}
