// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

/*

Homotopy algorithm adapted from:

Chan, Eunice Y. S.,
"A comparison of solution methods for Mandelbrot-like polynomials"
Thesis, MSc (Applied Mathematics) (2016).
The University of Western Ontario,
Electronic Thesis and Dissertation Repository. 4028.
<https://ir.lib.uwo.ca/etd/4028/>

Algorithm details adapted from:

Piers Lawrence
"The Mandelbrot Polynomial Roots Challenge"
Fractal Forums post (2015)
attachment: mandel.zip
<https://www.fractalforums.com/theory/the-mandelbrot-polynomial-roots-challenge/msg88565/#msg88565>
License BSD-style;
----8<----
Copyright 2015, Piers Lawrence, 
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    * Neither the name of The University of Western Ontario, KU
      Leuven, Université catholique de Louvain, nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY PIERS LAWRENCE ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL PIERS LAWRENCE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----8<----

RKF45 solver adapted from:

GNU Scientific Library
gsl/ode-initval/rkf45.c
Copyright (C) 2001, 2004, 2007 Brian Gough
License GPL3+

Rate control adapted from:

GNU Scientific Library
gsl/ode-initval/cstd.c
Copyright (C) 1996, 1997, 1998, 1999, 2000 Gerard Jungman
License GPL3+

*/

static const double c[6] =
  { 0
  , 1./4.
  , 3./8.
  , 12./13.
  , 1.
  , 1./2.
  };

static const double a[5][5] =
  { { 1./4., 0, 0, 0, 0 }
  , { 3./32., 9./32., 0, 0, 0 }
  , { 1932./2197., -7200./2197., 7296./2197., 0, 0 }
  , { 8341./4104., -32832./4104., 29440./4104., -845./4104., 0 }
  , { -6080./20520., 41040./20520., -28352./20520., 9295./20520., -5643./20520. }
  };

static const double b5[6] =
  { 902880.0/7618050.0
  , 0
  , 3953664.0/7618050.0
  , 3855735.0/7618050.0
  , -1371249.0/7618050.0
  , 277020.0/7618050.0
  };

static const double ec[6] =
  { 1./360.
  , 0
  , -128.0/4275.
  , -2197./75240.
  , 1./5.
  , 2./55.
  };

static const double _Complex g = 0.98078528040323 + I * 0.195090322016128;

struct m_d_nucleus_homotopy
{
  int period;
  double t;
  double h;
  double _Complex c;
};

extern m_d_nucleus_homotopy *m_d_nucleus_homotopy_new(double _Complex c, int period, int sign)
{
  sign = sign <= 0 ? -1 : 1;
  double _Complex p = 0;
  double _Complex dp = 0;
  double _Complex ddp = 0;
  double _Complex dddp = 0;
  for (int i = 0; i < period; ++i)
  {
    dddp = 6 * dp * dp + 6 * p * ddp + 6 * c * dp * ddp + 2 * c * p * dddp;
    ddp = 4 * p * dp + 2 * c * dp * dp + 2 * c * p * ddp;
    dp = p * p + 2 * c * p * dp;
    p = c * p * p + 1;
  }
  // FIXME TODO figure out what this series is doing
  double _Complex beta[3];
  beta[0] = sign * csqrt(-1 / c) / dp;
  beta[1] = - beta[0] * beta[0] * (dp + c * ddp) / (2 * c * dp);
  beta[2] = - beta[0] * beta[0] * beta[0] * (-15 * dp * dp - 18 * c * dp * ddp - 12 * c * c * ddp + 4 * c * c * dp * dddp) / (24 * c * c * dp * dp);
  double _Complex c0;
  double h = 1./64., residue;
  do
  {
    h /= 2;
    if (! (h > 1e-16))
    {
      return 0;
    }
    double _Complex t = h * g / (1 + (g - 1) * h);
    c0 = c + (beta[0] + (beta[1] + beta[2] * t) * t) * t;
    p = 0;
    for (int j = 0; j < period; ++j)
    {
      p = c0 * p * p + 1;
    }
    residue = cabs(c0 * p * p + t * t);
  } while (! (residue < 1e-8));
  m_d_nucleus_homotopy *path = malloc(sizeof(*path));
  if (! path)
  {
    return 0;
  }
  path->period = period + 1;
  path->t = h;
  path->h = h;
  path->c = c0;
  return path;
}

extern void m_d_nucleus_homotopy_delete(m_d_nucleus_homotopy *path)
{
  free(path);
}

m_newton m_d_nucleus_homotopy_step(m_d_nucleus_homotopy *path)
{
  int period = path->period;
  double t = path->t;
  double h = path->h;
  double _Complex x = path->c;
  if (t >= 1)
  {
    return m_converged;
  }

  // Newton refinement
  for (int s = 0; s < 64; ++s)
  {
    double _Complex p = 0;
    double _Complex dp = 0;
    for (int i = 0; i < period - 1; ++i)
    {
      dp = p * (p + 2 * x * dp);
      p = x * p * p + 1;
    }
    double _Complex tg = (t * g) / (1 + (g - 1) * t);
    dp = p * (p + 2 * x * dp);
    p = x * p * p + tg * tg;
    double _Complex dx = -p / dp;
    x += dx;
    if (cabs(dx) < 1e-16)
    {
      break;
    }
  }

  // KRF45 step
  double _Complex k[6];
  for (int s = 0; s < 6; ++s)
  {
    double _Complex xin = x;
    for (int i = 0; i < s; ++i)
    {
      xin += a[s - 1][i] * k[i] * h;
    }
    double tin = t + c[s] * h;
    double _Complex p = 0;
    double _Complex dp = 0;
    for (int i = 0; i < period; ++i)
    {
      dp = p * (p + 2 * xin * dp);
      p = xin * p * p + 1;
    }
    double _Complex tg = (tin * g) / (1 + (g - 1) * tin);
    k[s] = -2 * tg / dp;
  }
  double _Complex e = 0;
  double _Complex dx = 0;
  for (int i = 0; i < 6; ++i)
  {
    dx += b5[i] * k[i];
    e += ec[i] * k[i];
  }
  double err = cabs(e * h);
  x += dx * h;

  // adaptive step size control
  const double eps_rel = 1e-14;
  const double eps_abs = 1e-14;
  const double d = eps_rel * cabs(x) + eps_abs;
  const double r = d / err;
  if (r > 1.1)
  {
    double q = 0.9 * pow(r, 1. / 4);
    if (q < 0.2) q = 0.2;
    h *= q;
  }
  else if (r < 0.5)
  {
    double q = 0.9 * pow(r, 1. / 5);
    if (q > 5) q = 5;
    if (q < 1) q = 1;
    h *= q;
  }

  double t_old = t;
  t += h;
  path->t = t;
  path->h = h;
  path->c = x;
  if (t == t_old)
  {
    return m_failed;
  }
  return m_stepped;
}

double _Complex m_d_nucleus_homotopy_get(const m_d_nucleus_homotopy *path)
{
  return path->c;
}

double _Complex m_d_nucleus_homotopy_do(double _Complex c, int period, int sign)
{
  m_d_nucleus_homotopy *path = m_d_nucleus_homotopy_new(c, period, sign);
  if (! path)
  {
    return 0./0.;
  }
  while (m_stepped == m_d_nucleus_homotopy_step(path))
  {
    // nop
  }
  double _Complex ret = m_d_nucleus_homotopy_get(path);
  m_d_nucleus_homotopy_delete(path);
  return ret;
}
