// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2020 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>

/*
<https://mathr.co.uk/blog/2017-11-21_misiurewicz_domain_coordinates_and_size_estimates.html>
\[G(c, p, q, r) = \frac{F^{q + p}(0, c) - F^{q}(0, c)}{F^{r + p}(0, c) - F^{r}(0, c)}\]
*/

extern m_newton m_r_misiurewicz_coord_step_raw(mpc_t c_out, const mpc_t c_guess, const mpc_t misiurewicz_coord, int period, int lopreperiod, int hipreperiod, mpc_t z, mpc_t dc, mpc_t num, mpc_t dnum, mpc_t den, mpc_t dden, mpc_t f, mpc_t df, mpc_t c_new, mpc_t d, mpfr_t d2, mpfr_t epsilon2) {
  // z = 0; dc = 0; num = 0; dnum = 0; den = 0; dden = 0;
  mpc_set_si(z, 0, MPC_RNDNN);
  mpc_set_si(dc, 0, MPC_RNDNN);
  mpc_set_si(num, 0, MPC_RNDNN);
  mpc_set_si(dnum, 0, MPC_RNDNN);
  mpc_set_si(den, 0, MPC_RNDNN);
  mpc_set_si(dden, 0, MPC_RNDNN);
  for (int i = 1; i <= hipreperiod + period; ++i) {
    // dc = 2 * z * dc + 1;
    mpc_mul(dc, z, dc, MPC_RNDNN);
    mpc_mul_2si(dc, dc, 1, MPC_RNDNN);
    mpc_add_ui(dc, dc, 1, MPC_RNDNN);
    // z = z * z + c_guess;
    mpc_sqr(z, z, MPC_RNDNN);
    mpc_add(z, z, c_guess, MPC_RNDNN);
    if (i == lopreperiod)
    {
      mpc_sub(den, den, z, MPC_RNDNN);
      mpc_sub(dden, dden, dc, MPC_RNDNN);
    }
    if (i == lopreperiod + period)
    {
      mpc_add(den, den, z, MPC_RNDNN);
      mpc_add(dden, dden, dc, MPC_RNDNN);
    }
    if (i == hipreperiod)
    {
      mpc_sub(num, num, z, MPC_RNDNN);
      mpc_sub(dnum, dnum, dc, MPC_RNDNN);
    }
    if (i == hipreperiod + period)
    {
      mpc_add(num, num, z, MPC_RNDNN);
      mpc_add(dnum, dnum, dc, MPC_RNDNN);
    }

  }
  // f = num / den - misiurewicz_coord;
  mpc_div(f, num, den, MPC_RNDNN);
  mpc_sub(f, f, misiurewicz_coord, MPC_RNDNN);
  // df = (dnum * den - num * dden) / (den * den);
  mpc_mul(dnum, dnum, den, MPC_RNDNN);
  mpc_mul(dden, dden, num, MPC_RNDNN);
  mpc_sqr(den, den, MPC_RNDNN);
  mpc_sub(num, dnum, dden, MPC_RNDNN);
  mpc_div(df, num, den, MPC_RNDNN);
  // c_new = c_guess - f / df;
  mpc_div(f, f, df, MPC_RNDNN);
  mpc_sub(c_new, c_guess, f, MPC_RNDNN);
  // d = c_new - c_guess;
  mpc_sub(d, c_new, c_guess, MPC_RNDNN);
  mpc_norm(d2, d, MPFR_RNDN);
  if (mpfr_lessequal_p(d2, epsilon2)) {
    mpc_set(c_out, c_new, MPC_RNDNN);
    return m_converged;
  }
  if (mpfr_number_p(mpc_realref(d)) && mpfr_number_p(mpc_imagref(d))) {
    mpc_set(c_out, c_new, MPC_RNDNN);
    return m_stepped;
  } else {
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_failed;
  }
}

extern m_newton m_r_misiurewicz_coord_step(mpc_t c_out, const mpc_t c_guess, const mpc_t misiurewicz_coord, int period, int lopreperiod, int hipreperiod) {
  // prec
  mpfr_prec_t precr, preci, prec;
  mpc_get_prec2(&precr, &preci, c_guess);
  prec = precr > preci ? precr : preci;
  mpc_set_prec(c_out, prec); // FIXME might trash when c_out = c_guess
  // init
  mpc_t z, dc, c_new, d, num, dnum, den, dden, f, df;
  mpfr_t d2, epsilon2;
  mpc_init2(z, prec);
  mpc_init2(dc, prec);
  mpc_init2(c_new, prec);
  mpc_init2(d, prec);
  mpc_init2(num, prec);
  mpc_init2(dnum, prec);
  mpc_init2(den, prec);
  mpc_init2(dden, prec);
  mpc_init2(f, prec);
  mpc_init2(df, prec);
  mpfr_init2(d2, prec);
  mpfr_init2(epsilon2, prec);
  // epsilon
  mpfr_set_si(epsilon2, 2, MPFR_RNDN);
  mpfr_nextabove(epsilon2);
  mpfr_sub_si(epsilon2, epsilon2, 2, MPFR_RNDN);
  mpfr_sqr(epsilon2, epsilon2, MPFR_RNDN);
  // step raw
  m_newton retval = m_r_misiurewicz_coord_step_raw(c_out, c_guess, misiurewicz_coord, period, lopreperiod, hipreperiod, z, dc, num, dnum, den, dden, f, df, c_new, d, d2, epsilon2);
  // cleanup
  mpc_clear(z);
  mpc_clear(dc);
  mpc_clear(c_new);
  mpc_clear(d);
  mpc_clear(num);
  mpc_clear(dnum);
  mpc_clear(den);
  mpc_clear(dden);
  mpc_clear(f);
  mpc_clear(df);
  mpfr_clear(d2);
  mpfr_clear(epsilon2);
  return retval;
}

extern m_newton m_r_misiurewicz_coord(mpc_t c_out, const mpc_t c_guess, const mpc_t misiurewicz_coord, int period, int lopreperiod, int hipreperiod, int maxsteps) {
  m_newton result = m_failed;
  // prec
  mpfr_prec_t precr, preci, prec;
  mpc_get_prec2(&precr, &preci, c_guess);
  prec = precr > preci ? precr : preci;
  // init
  mpc_t z, dc, c, c_new, d, num, dnum, den, dden, f, df;
  mpfr_t d2, epsilon2;
  mpc_init2(z, prec);
  mpc_init2(dc, prec);
  mpc_init2(c, prec);
  mpc_init2(c_new, prec);
  mpc_init2(d, prec);
  mpc_init2(num, prec);
  mpc_init2(dnum, prec);
  mpc_init2(den, prec);
  mpc_init2(dden, prec);
  mpc_init2(f, prec);
  mpc_init2(df, prec);
  mpfr_init2(d2, prec);
  mpfr_init2(epsilon2, prec);
  // epsilon
  mpfr_set_si(epsilon2, 2, MPFR_RNDN);
  mpfr_nextabove(epsilon2);
  mpfr_sub_si(epsilon2, epsilon2, 2, MPFR_RNDN);
  mpfr_sqr(epsilon2, epsilon2, MPFR_RNDN);
  // c = c_guess
  mpc_set(c, c_guess, MPC_RNDNN);
  for (int i = 0; i < maxsteps; ++i) {
    if (m_stepped != (result = m_r_misiurewicz_coord_step_raw(c, c, misiurewicz_coord, period, lopreperiod, hipreperiod, z, dc, num, dnum, den, dden, f, df, c_new, d, d2, epsilon2))) {
      break;
    }
  }
  // c_out = c;
  mpc_set_prec(c_out, prec);
  mpc_set(c_out, c, MPC_RNDNN);
  // cleanup
  mpc_clear(z);
  mpc_clear(dc);
  mpc_clear(c);
  mpc_clear(c_new);
  mpc_clear(d);
  mpc_clear(num);
  mpc_clear(dnum);
  mpc_clear(den);
  mpc_clear(dden);
  mpc_clear(f);
  mpc_clear(df);
  mpfr_clear(d2);
  mpfr_clear(epsilon2);
  return result;
}
