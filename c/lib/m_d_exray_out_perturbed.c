// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

#include <stdio.h>

static inline bool mpc_finite_p(const mpc_t z) {
  return mpfr_number_p(mpc_realref(z)) && mpfr_number_p(mpc_imagref(z));
}

static double dwell(double loger2, int n, double zmag2) {
  return n - log2(log(zmag2) / loger2);
}

struct m_d_exray_out_perturbed
{
  const m_r_orbit_d *orbit;
  int sharpness;
  double d;
  double er;
  double loger2;
  double _Complex delta_c;
  double _Complex z;
  int n;
  int bit;
};

extern m_d_exray_out_perturbed *m_d_exray_out_perturbed_new(const m_r_orbit_d *orbit, double _Complex endpoint, int sharpness, int maxdwell, double er)
{
  m_d_exray_out_perturbed *ray = malloc(sizeof(*ray));
  if (! ray) {
    return 0;
  }
  ray->orbit = orbit;
  ray->delta_c = endpoint;
  ray->er = er;
  ray->loger2 = log(er * er);
  int n = 0;
  double _Complex z = 0;
  int m = 0;
  const int preperiod = m_r_orbit_d_get_preperiod(ray->orbit);
  const int period = m_r_orbit_d_get_period(ray->orbit);
  const double _Complex *z_ptr = m_r_orbit_d_get_z_ptr(orbit);
  for (int i = 0; i < maxdwell; ++i) {
    double _Complex Z = z_ptr[m];
    if (cabs(Z + z) < cabs(z))
    {
      z = Z + z;
      m = 0;
      Z = z_ptr[m];
    }
    if (cabs(Z + z) > ray->er)
    {
      break;
    }
    z = (2 * Z + z) * z + ray->delta_c;
    if (++m == preperiod + period)
      m = preperiod;
    n = n + 1;
  }
  double _Complex Z = z_ptr[m];
  if (! (cabs(Z + z) > ray->er)) {
    free(ray);
    return 0;
  }
  ray->sharpness = sharpness;
  ray->z = Z + z;
  ray->d = dwell(ray->loger2, n, cabs2(ray->z));
  ray->n = n;
  ray->bit = -1;
  return ray;
}

extern void m_d_exray_out_perturbed_delete(m_d_exray_out_perturbed *ray)
{
  if (ray)
  {
    free(ray);
  }
}

extern m_newton m_d_exray_out_perturbed_step(m_d_exray_out_perturbed *ray, int maxsteps)
{
  if (! ray) {
    return m_failed;
  }
  ray->bit = -1;
  ray->d -= 1.0 / ray->sharpness;
  if (ray->d <= 0) {
    return m_converged;
  }
  int m = ceil(ray->d);
  double r = pow(ray->er, pow(2, m - ray->d));
  double a = carg(ray->z) / twopi;
  double t = a - floor(a);
  const int preperiod = m_r_orbit_d_get_preperiod(ray->orbit);
  const int period = m_r_orbit_d_get_period(ray->orbit);
  const int length = preperiod + period;
  const double * restrict orbit = (const double * restrict) m_r_orbit_d_get_z_ptr(ray->orbit);
  if (m == ray->n) {
    double _Complex target = r * cexp(I * twopi *  t);
    double _Complex c = ray->delta_c;
    double _Complex Z = 0, z  = 0, dc = 0;
    for (int i = 0; i < maxsteps; ++i)
    {
      // this version is a few % faster than using _Complex
      double zx = 0, zy = 0, dcx = 0, dcy = 0, cx = creal(c), cy = cimag(c);
      int k = 0;
      for (int p = 0; p < m; ++p)
      {
        double Zx = orbit[2 * k];
        double Zy = orbit[2 * k + 1];
        double Zzx = Zx + zx;
        double Zzy = Zy + zy;
        double Zz2 = Zzx * Zzx + Zzy * Zzy;
        double z2 = zx * zx + zy * zy;
        if (Zz2 < z2)
        {
          zx = Zzx;
          zy = Zzy;
          k = 0;
          Zx = 0;
          Zy = 0;
        }
        double Z2zx = 2 * Zx + zx;
        double Z2zy = 2 * Zy + zy;
        double dcxn = 2 * (Zzx * dcx - Zzy * dcy) + 1;
        double dcyn = 2 * (Zzx * dcy + Zzy * dcx);
        double zxn = Z2zx * zx - Z2zy * zy + cx;
        double zyn = Z2zx * zy + Z2zy * zx + cy;
        dcx = dcxn;
        dcy = dcyn;
        zx = zxn;
        zy = zyn;
        if (++k == length)
        {
          k = preperiod;
        }
      }
      z = zx + I * zy;
      dc = dcx + I * dcy;
      Z = orbit[2 * k] + I * orbit[2 * k + 1];
      double _Complex c_old = c;
      c -= (Z + z - target) / dc;
      if (c == c_old) break;
      if (! cisfinite(c)) break;
    }
    if (cisfinite(c))
    {
      ray->delta_c = c;
      ray->z = Z + z;
      ray->n = m;
      ray->d = dwell(ray->loger2, m, cabs2(ray->z));
      return m_stepped;
    }
    return m_failed;
  } else {
    double _Complex target[2] =
      { r * cexp(I * pi *  t     )
      , r * cexp(I * pi * (t + 1))
      };
    double _Complex c = ray->delta_c;
    double _Complex Z = 0, z  = 0, dc = 0;
    for (int i = 0; i < maxsteps; ++i)
    {
      // this version is a few % faster than using _Complex
      double zx = 0, zy = 0, dcx = 0, dcy = 0, cx = creal(c), cy = cimag(c);
      int k = 0;
      for (int p = 0; p < m; ++p)
      {
        double Zx = orbit[2 * k];
        double Zy = orbit[2 * k + 1];
        double Zzx = Zx + zx;
        double Zzy = Zy + zy;
        double Zz2 = Zzx * Zzx + Zzy * Zzy;
        double z2 = zx * zx + zy * zy;
        if (Zz2 < z2)
        {
          zx = Zzx;
          zy = Zzy;
          k = 0;
          Zx = 0;
          Zy = 0;
        }
        double Z2zx = 2 * Zx + zx;
        double Z2zy = 2 * Zy + zy;
        double dcxn = 2 * (Zzx * dcx - Zzy * dcy) + 1;
        double dcyn = 2 * (Zzx * dcy + Zzy * dcx);
        double zxn = Z2zx * zx - Z2zy * zy + cx;
        double zyn = Z2zx * zy + Z2zy * zx + cy;
        dcx = dcxn;
        dcy = dcyn;
        zx = zxn;
        zy = zyn;
        if (++k == length)
        {
          k = preperiod;
        }
      }
      z = zx + I * zy;
      dc = dcx + I * dcy;
      Z = orbit[2 * k] + I * orbit[2 * k + 1];
      double _Complex c_old = c;
      ray->z = Z + z;
      ray->bit = cabs(ray->z - target[1]) < cabs(ray->z - target[0]);
      c -= (ray->z - target[ray->bit]) / dc;
      if (c == c_old)
      {
        break;
      }
      if (c == c_old) break;
      if (! cisfinite(c)) break;
    }
    if (cisfinite(c))
    {
      ray->delta_c = c;
      ray->n = m;
      ray->d = dwell(ray->loger2, m, cabs2(ray->z));
      return m_stepped;
    }
    return m_failed;
  }
}

extern bool m_d_exray_out_perturbed_have_bit(const m_d_exray_out_perturbed *ray) {
  if (! ray) {
    return false;
  }
  return 0 <= ray->bit;
}

extern bool m_d_exray_out_perturbed_get_bit(const m_d_exray_out_perturbed *ray) {
  if (! ray) {
    return false;
  }
  return ray->bit;
}

extern double _Complex m_d_exray_out_perturbed_get(const m_d_exray_out_perturbed *ray) {
  if (! ray) {
    return 0;
  }
  return ray->delta_c;
}

extern char *m_d_exray_out_perturbed_do(const m_r_orbit_d *orbit, const double _Complex c, int sharpness, int maxdwell, double er, int maxsteps) {
  m_d_exray_out_perturbed *ray = m_d_exray_out_perturbed_new(orbit, c, sharpness, maxdwell, er);
  if (! ray) {
    return 0;
  }
  char *bits = malloc(maxdwell + 2);
  if (! bits) {
    m_d_exray_out_perturbed_delete(ray);
    return 0;
  }
  int n = 0;
  while (n <= maxdwell) {
    if (m_d_exray_out_perturbed_have_bit(ray)) {
      bits[n++] = '0' + m_d_exray_out_perturbed_get_bit(ray);
    }
    if (m_stepped != m_d_exray_out_perturbed_step(ray, maxsteps)) {
      break;
    }
  }
  bits[n] = 0;
  m_d_exray_out_perturbed_delete(ray);
  return bits;
}
