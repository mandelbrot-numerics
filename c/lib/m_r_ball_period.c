// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2021 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include <stdio.h>

struct m_r_ball_period {
  mpc_t c, z, dz;
  mpfr_t r, rdz, rz, rr, rrdz, Ei, t[3];
  int p;
};

extern m_r_ball_period *m_r_ball_period_new(const mpc_t center, const mpfr_t radius) {
  m_r_ball_period *ball = (m_r_ball_period *) malloc(sizeof(*ball));
  if (! ball) {
    return 0;
  }
  int bitsr = mpfr_get_prec(mpc_realref(center));
  int bitsi = mpfr_get_prec(mpc_imagref(center));
  int bits = bitsr > bitsi ? bitsr : bitsi;
  mpc_init2(ball->c, bits); mpc_set(ball->c, center, MPC_RNDNN);
  mpc_init2(ball->z, bits); mpc_set_ui(ball->z, 0, MPC_RNDNN);
  mpc_init2(ball->dz, 53); mpc_set_ui(ball->dz, 0, MPC_RNDNN);
  mpfr_init2(ball->r, 53); mpfr_set(ball->r, radius, MPFR_RNDU);
  mpfr_init2(ball->rdz, 53); mpfr_set_ui(ball->rdz, 0, MPFR_RNDN);
  mpfr_init2(ball->rz, 53); mpfr_set_ui(ball->rz, 0, MPFR_RNDN);
  mpfr_init2(ball->rr, 53); mpfr_set_ui(ball->rr, 0, MPFR_RNDN);
  mpfr_init2(ball->rrdz, 53); mpfr_set_ui(ball->rrdz, 0, MPFR_RNDN);
  mpfr_init2(ball->Ei, 53); mpfr_set_ui(ball->Ei, 0, MPFR_RNDN);
  mpfr_init2(ball->t[0], 53); mpfr_set_ui(ball->t[0], 0, MPFR_RNDN);
  mpfr_init2(ball->t[1], 53); mpfr_set_ui(ball->t[1], 0, MPFR_RNDN);
  mpfr_init2(ball->t[2], 53); mpfr_set_ui(ball->t[2], 0, MPFR_RNDN);
  ball->p = 0;
  m_r_ball_period_step(ball);
  return ball;
}

extern void m_r_ball_period_delete(m_r_ball_period *ball) {
  if (ball) {
    mpc_clear(ball->c);
    mpc_clear(ball->z);
    mpc_clear(ball->dz);
    mpfr_clear(ball->r);
    mpfr_clear(ball->rdz);
    mpfr_clear(ball->rz);
    mpfr_clear(ball->rr);
    mpfr_clear(ball->rrdz);
    mpfr_clear(ball->Ei);
    mpfr_clear(ball->t[0]);
    mpfr_clear(ball->t[1]);
    mpfr_clear(ball->t[2]);
    free(ball);
  }
}

extern bool m_r_ball_period_step(m_r_ball_period *ball) {
  if (! ball) {
    return false;
  }
  // Ei = rdz * rdz + (2 * rz + r * (2 * rdz + r * Ei)) * Ei;
  mpfr_mul(ball->t[0], ball->r, ball->Ei, MPFR_RNDU);
  mpfr_mul_2ui(ball->t[1], ball->rdz, 1, MPFR_RNDU);
  mpfr_add(ball->t[2], ball->t[0], ball->t[1], MPFR_RNDU);
  mpfr_mul(ball->t[0], ball->r, ball->t[2], MPFR_RNDU);
  mpfr_mul_2ui(ball->t[1], ball->rz, 1, MPFR_RNDU);
  mpfr_add(ball->t[2], ball->t[1], ball->t[0], MPFR_RNDU);
  mpfr_mul(ball->t[0], ball->t[2], ball->Ei, MPFR_RNDU);
  mpfr_sqr(ball->t[1], ball->rdz, MPFR_RNDU);
  mpfr_add(ball->Ei, ball->t[1], ball->t[0], MPFR_RNDU);
  // dz = 2 * z * dz + 1;
  mpc_mul(ball->dz, ball->z, ball->dz, MPC_RNDNN);
  mpfr_mul_2ui(mpc_realref(ball->dz), mpc_realref(ball->dz), 1, MPFR_RNDN);
  mpfr_mul_2ui(mpc_imagref(ball->dz), mpc_imagref(ball->dz), 1, MPFR_RNDN);
  mpfr_add_ui(mpc_realref(ball->dz), mpc_realref(ball->dz), 1, MPFR_RNDN);
  // z = z * z + c;
  mpc_sqr(ball->z, ball->z, MPC_RNDNN);
  mpc_add(ball->z, ball->z, ball->c, MPC_RNDNN);
  // rdz = cabs(dz);
  mpc_abs(ball->rdz, ball->dz, MPFR_RNDU);
  // rz = cabs(z);
  mpc_abs(ball->rz, ball->z, MPFR_RNDU);
  // rr = r * (rdz + r * Ei);
  mpfr_mul(ball->t[0], ball->r, ball->Ei, MPFR_RNDU);
  mpfr_add(ball->t[1], ball->rdz, ball->t[0], MPFR_RNDU);
  mpfr_mul(ball->rr, ball->r, ball->t[1], MPFR_RNDU);
  // retval = rz - rr <= 2;
  mpfr_sub(ball->t[0], ball->rz, ball->rr, MPFR_RNDD);
  mpfr_sub_ui(ball->t[1], ball->t[0], 2, MPFR_RNDD);
  bool retval = mpfr_sgn(ball->t[1]) <= 0;
  //  rrdz = r * (rdz - r * Ei)
  mpfr_mul(ball->t[0], ball->r, ball->Ei, MPFR_RNDU);
  mpfr_sub(ball->t[0], ball->rdz, ball->t[0], MPFR_RNDD);
  mpfr_mul(ball->rrdz, ball->r, ball->t[0], MPFR_RNDD);
  // p = p + 1
  ball->p = ball->p + 1;
  return retval;
}

extern bool m_r_ball_period_have_period(const m_r_ball_period *ball) {
  if (! ball) {
    return true;
  }
  // rz <= rr;
  return mpfr_lessequal_p(ball->rz, ball->rr);
}

extern bool m_r_ball_period_will_converge(const m_r_ball_period *ball) {
  if (! ball) {
    return false;
  }
  // safe to use Newton-Raphson without interval arithmetic when f is less than r f'
  return mpfr_lessequal_p(ball->rz, ball->rrdz);
}

extern int m_r_ball_period_get_period(const m_r_ball_period *ball) {
  if (! ball) {
    return 0;
  }
  return ball->p;
}

extern int m_r_ball_period_do(const mpc_t center, const mpfr_t radius, int maxperiod) {
  m_r_ball_period *ball = m_r_ball_period_new(center, radius);
  if (! ball) {
    return 0;
  }
  int period = 0;
  for (int i = 0; i < maxperiod; ++i) {
    if (m_r_ball_period_have_period(ball)) {
      period = m_r_ball_period_get_period(ball);
      if (! m_r_ball_period_will_converge(ball))
      {
        period = -period;
      }
      break;
    }
    if (! m_r_ball_period_step(ball)) {
      break;
    }
  }
  m_r_ball_period_delete(ball);
  return period;
}
