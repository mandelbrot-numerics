// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>

extern double _Complex m_d_size(double _Complex nucleus, int period) {
  double _Complex l = 1;
  double _Complex b = 1;
  double _Complex z = 0;
  for (int i = 1; i < period; ++i) {
    z = z * z + nucleus;
    l = 2 * z * l;
    b = b + 1 / l;
  }
  return 1 / (b * l * l);
}

extern double _Complex m_d_size_orbit(const m_r_orbit_d *orbit) {
  double _Complex l = 1;
  double _Complex b = 1;
  const double _Complex *z = m_r_orbit_d_get_z_ptr(orbit);
  int period = m_r_orbit_d_get_period(orbit);
  for (int i = 1; i < period; ++i) {
    l = 2 * z[i] * l;
    b = b + 1 / l;
  }
  return 1 / (b * l * l);
}
