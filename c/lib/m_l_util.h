// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef M_L_UTIL_H
#define M_L_UTIL_H 1

#include <complex.h>

static inline int sgnl(long double z) {
  if (z > 0) { return  1; }
  if (z < 0) { return -1; }
  return 0;
}

static inline long double cabsl2(long double _Complex z) {
  return creall(z) * creall(z) + cimagl(z) * cimagl(z);
}

static inline bool cisfinitel(long double _Complex z) {
  return isfinite(creall(z)) && isfinite(cimagl(z));
}

#endif
