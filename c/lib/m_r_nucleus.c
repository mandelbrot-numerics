// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include <stdio.h>

#define THREADING
//#undef THREADING
#if (!defined(__STDC_NO_THREADS__)) && (!defined(__STDC_NO_ATOMICS__))
#include <threads.h>
#include <stdatomic.h>
typedef int thrd_result_t;
#else
#if (!defined(__STDC_NO_ATOMICS__))
#include <sched.h>
#include <pthread.h>
#include <stdatomic.h>
typedef pthread_t thrd_t;
typedef void *thrd_result_t;
#define thrd_success 0
#define thrd_create(T,F,A) pthread_create(T,NULL,F,A)
#define thrd_join(T,R) pthread_join(*T,R)
#define thrd_yield() sched_yield()
#else
#undef THREADING
#endif
#endif

#ifdef THREADING

// centralized barrier from
// https://www.jlab.org/hpc/papers/hpcasia07.pdf

struct barrier_t
{
  atomic_int release;
  int padding1[63];
  atomic_int counter;
  int padding2[63];
  int num_thread;
  bool yielding;
};

void barrier_init(struct barrier_t *b, int num_thread, bool yielding)
{
  b->release = 0;
  b->counter = num_thread;
  b->num_thread = num_thread;
  b->yielding = yielding;
}

void barrier_wait(struct barrier_t *b)
{
  int flag = b->release;
  int count = --(b->counter);
  if (count == 0)
  {
    b->counter = b->num_thread;
    ++(b->release);
  }
  else
    if (b->yielding)
      while (flag == b->release)
        thrd_yield();
    else
      while (flag == b->release)
        ;
}

struct m_r_nucleus_naive_step_common_t
{
  struct barrier_t barrier;
  mpfr_t cr, ci, zr, zi, dr, di, zrdr, zidi, zidr, zrdi, zrzr, zizi, zrzi, d2, epsilon2;
  mpc_t z, dc, c_new, d;
  int period;
};

void m_r_nucleus_naive_step_common_init(struct m_r_nucleus_naive_step_common_t *m, mpfr_prec_t prec, int period, int ncpus)
{
  barrier_init(&m->barrier, 7, ncpus < 7);
  mpfr_inits2(prec, m->cr, m->ci, m->zr, m->zi, m->dr, m->di, m->zrdr, m->zidi, m->zidr, m->zrdi, m->zrzr, m->zizi, m->zrzi, m->d2, m->epsilon2, NULL);
  mpc_init2(m->z, prec);
  mpc_init2(m->dc, prec);
  mpc_init2(m->c_new, prec);
  mpc_init2(m->d, prec);
  mpfr_set_si(m->epsilon2, 2, MPFR_RNDN);
  mpfr_nextabove(m->epsilon2);
  mpfr_sub_si(m->epsilon2, m->epsilon2, 2, MPFR_RNDN);
  mpfr_sqr(m->epsilon2, m->epsilon2, MPFR_RNDN);
  m->period = period;
}

void m_r_nucleus_naive_step_common_clear(struct m_r_nucleus_naive_step_common_t *m)
{
  mpfr_clears(m->cr, m->ci, m->zr, m->zi, m->dr, m->di, m->zrdr, m->zidi, m->zidr, m->zrdi, m->zrzr, m->zizi, m->zrzi, m->d2, m->epsilon2, NULL);
  mpc_clear(m->z);
  mpc_clear(m->dc);
  mpc_clear(m->c_new);
  mpc_clear(m->d);
}

struct m_r_nucleus_naive_step_arg_t
{
  struct m_r_nucleus_naive_step_common_t *data;
  int id;
};

static thrd_result_t m_r_nucleus_naive_step_thread(void *arg0)
{
  struct m_r_nucleus_naive_step_arg_t *arg = arg0;
  struct m_r_nucleus_naive_step_common_t *m = arg->data;
  int t = arg->id;
  int period = m->period;
  for (int p = 0; p < period; ++p)
  {
    switch (t)
    {
      case 0: mpfr_mul(m->zrdr, m->zr, m->dr, MPFR_RNDN); break;
      case 1: mpfr_mul(m->zidi, m->zi, m->di, MPFR_RNDN); break;
      case 2: mpfr_mul(m->zidr, m->zi, m->dr, MPFR_RNDN); break;
      case 3: mpfr_mul(m->zrdi, m->zr, m->di, MPFR_RNDN); break;
      case 4: mpfr_sqr(m->zrzr, m->zr,        MPFR_RNDN); break;
      case 5: mpfr_sqr(m->zizi, m->zi,        MPFR_RNDN); break;
      case 6: mpfr_mul(m->zrzi, m->zr, m->zi, MPFR_RNDN); break;
    }
    barrier_wait(&m->barrier);
    switch (t)
    {
      case 0:
        mpfr_sub(m->dr, m->zrdr, m->zidi, MPFR_RNDN);
        mpfr_mul_2ui(m->dr, m->dr, 1, MPFR_RNDN);
        mpfr_add_ui(m->dr, m->dr, 1, MPFR_RNDN);
        break;
      case 1:
        mpfr_add(m->di, m->zidr, m->zrdi, MPFR_RNDN);
        mpfr_mul_2ui(m->di, m->di, 1, MPFR_RNDN);
        break;
      case 2:
        mpfr_sub(m->zr, m->zrzr, m->zizi, MPFR_RNDN);
        mpfr_add(m->zr, m->zr, m->cr, MPFR_RNDN);
        break;
      case 3:
        mpfr_mul_2ui(m->zi, m->zrzi, 1, MPFR_RNDN);
        mpfr_add(m->zi, m->zi, m->ci, MPFR_RNDN);
        break;
    }
    barrier_wait(&m->barrier);
    if ((t == 0) && ((p & 0xFFFF) == 0))
      fprintf(stderr, "%16d\r", p);
  }
  return 0;
}

static m_newton m_r_nucleus_naive_step_raw_threads(mpc_t c_out, const mpc_t c_guess, struct m_r_nucleus_naive_step_common_t *m)
{
  mpfr_set(m->cr, mpc_realref(c_guess), MPFR_RNDN);
  mpfr_set(m->ci, mpc_imagref(c_guess), MPFR_RNDN);
  mpfr_set_ui(m->zr, 0, MPFR_RNDN);
  mpfr_set_ui(m->zi, 0, MPFR_RNDN);
  mpfr_set_ui(m->dr, 0, MPFR_RNDN);
  mpfr_set_ui(m->di, 0, MPFR_RNDN);
  struct m_r_nucleus_naive_step_arg_t arg[7];
  thrd_t threads[7];
  int status[7];
  thrd_result_t result[7] = { 0, 0, 0, 0, 0, 0, 0 };
  bool ok = true;
  for (int t = 0; t < 7; ++t)
  {
    arg[t].data = m;
    arg[t].id = t;
    status[t] = thrd_create(&threads[t], m_r_nucleus_naive_step_thread, &arg[t]);
    ok = ok && status[t] == thrd_success;
  }
  for (int t = 0; t < 7; ++t)
  {
    if (status[t] == thrd_success)
      thrd_join(threads[t], &result[t]);
    ok = ok && !result[t];
  }
  if (! ok)
  {
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_failed;
  }
  mpfr_set(mpc_realref(m->dc), m->dr, MPFR_RNDN);
  mpfr_set(mpc_imagref(m->dc), m->di, MPFR_RNDN);
  mpc_norm(m->d2, m->dc, MPFR_RNDN);
  if (mpfr_lessequal_p(m->d2, m->epsilon2)) {
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_converged;
  }
  // c_new = c_guess - z / dc;
  mpfr_set(mpc_realref(m->z), m->zr, MPFR_RNDN);
  mpfr_set(mpc_imagref(m->z), m->zi, MPFR_RNDN);
  mpc_div(m->z, m->z, m->dc, MPC_RNDNN);
  mpc_sub(m->c_new, c_guess, m->z, MPC_RNDNN);
  // d = c_new - c_guess;
  mpc_sub(m->d, m->c_new, c_guess, MPC_RNDNN);
  mpc_norm(m->d2, m->d, MPFR_RNDN);
  if (mpfr_lessequal_p(m->d2, m->epsilon2)) {
    mpc_set(c_out, m->c_new, MPC_RNDNN);
    return m_converged;
  }
  if (mpfr_number_p(mpc_realref(m->d)) && mpfr_number_p(mpc_imagref(m->d))) {
    mpc_set(c_out, m->c_new, MPC_RNDNN);
    return m_stepped;
  } else {
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_failed;
  }
}
#endif

extern m_newton m_r_nucleus_naive_step_raw(mpc_t c_out, const mpc_t c_guess, int period, mpc_t z, mpc_t dc, mpc_t c_new, mpc_t d, mpfr_t d2, mpfr_t epsilon2) {
  // z = 0; dc = 0;
  mpc_set_si(z, 0, MPC_RNDNN);
  mpc_set_si(dc, 0, MPC_RNDNN);
  for (int i = 0; i < period; ++i) {
    // dc = 2 * z * dc + 1;
    mpc_mul(dc, z, dc, MPC_RNDNN);
    mpc_mul_2si(dc, dc, 1, MPC_RNDNN);
    mpc_add_ui(dc, dc, 1, MPC_RNDNN);
    // z = z * z + c_guess;
    mpc_sqr(z, z, MPC_RNDNN);
    mpc_add(z, z, c_guess, MPC_RNDNN);
  }
  mpc_norm(d2, dc, MPFR_RNDN);
  if (mpfr_lessequal_p(d2, epsilon2)) {
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_converged;
  }
  // c_new = c_guess - z / dc;
  mpc_div(z, z, dc, MPC_RNDNN);
  mpc_sub(c_new, c_guess, z, MPC_RNDNN);
  // d = c_new - c_guess;
  mpc_sub(d, c_new, c_guess, MPC_RNDNN);
  mpc_norm(d2, d, MPFR_RNDN);
  if (mpfr_lessequal_p(d2, epsilon2)) {
    mpc_set(c_out, c_new, MPC_RNDNN);
    return m_converged;
  }
  if (mpfr_number_p(mpc_realref(d)) && mpfr_number_p(mpc_imagref(d))) {
    mpc_set(c_out, c_new, MPC_RNDNN);
    return m_stepped;
  } else {
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_failed;
  }
}

extern m_newton m_r_nucleus_naive(mpc_t c_out, const mpc_t c_guess, int period, int maxsteps, int ncpus) {
  m_newton result = m_failed;
  // prec
  mpfr_prec_t precr, preci, prec;
  mpc_get_prec2(&precr, &preci, c_guess);
  prec = precr > preci ? precr : preci;
  // init
#ifdef THREADING
  if (ncpus > 1)
  {
    // c = c_guess
    mpc_t c;
    mpc_init2(c, prec);
    mpc_set(c, c_guess, MPC_RNDNN);
    {
      struct m_r_nucleus_naive_step_common_t common;
      m_r_nucleus_naive_step_common_init(&common, prec, period, ncpus);
      for (int i = 0; i < maxsteps; ++i) {
        fprintf(stderr, "pass\t%8d\n", i);
        if (m_stepped != (result = m_r_nucleus_naive_step_raw_threads(c, c, &common))) {
          break;
        }
      }
      m_r_nucleus_naive_step_common_clear(&common);
    }
    // c_out = c;
    mpc_set_prec(c_out, prec);
    mpc_set(c_out, c, MPC_RNDNN);
    mpc_clear(c);
  }
  else
#endif
  {
    (void) ncpus;
    mpc_t c, z, dc, c_new, d;
    mpfr_t d2, epsilon2;
    mpc_init2(c, prec);
    mpc_init2(z, prec);
    mpc_init2(dc, prec);
    mpc_init2(c_new, prec);
    mpc_init2(d, prec);
    mpfr_init2(d2, prec);
    mpfr_init2(epsilon2, prec);
    // epsilon
    mpfr_set_si(epsilon2, 2, MPFR_RNDN);
    mpfr_nextabove(epsilon2);
    mpfr_sub_si(epsilon2, epsilon2, 2, MPFR_RNDN);
    mpfr_sqr(epsilon2, epsilon2, MPFR_RNDN);
    // c = c_guess
    mpc_set(c, c_guess, MPC_RNDNN);
    for (int i = 0; i < maxsteps; ++i) {
      if (m_stepped != (result = m_r_nucleus_naive_step_raw(c, c, period, z, dc, c_new, d, d2, epsilon2))) {
        break;
      }
    }
    // c_out = c;
    mpc_set_prec(c_out, prec);
    mpc_set(c_out, c, MPC_RNDNN);
    // cleanup
    mpc_clear(c);
    mpc_clear(z);
    mpc_clear(dc);
    mpc_clear(c_new);
    mpc_clear(d);
    mpfr_clear(d2);
    mpfr_clear(epsilon2);
  }
  return result;
}

extern m_newton m_r_nucleus_step_raw(mpc_t c_out, const mpc_t c_guess, int period, mpfr_t epsilon2, mpc_t c_new, mpc_t z, mpc_t dc, mpc_t h, mpc_t dh, mpc_t f, mpc_t df)
{
  mpc_set_dc(z, 0, MPC_RNDNN);
  mpc_set_dc(dc, 0, MPC_RNDNN);
  mpc_set_dc(h, 1, MPC_RNDNN);
  mpc_set_dc(dh, 0, MPC_RNDNN);
  for (int i = 1; i <= period; ++i) {
    // dc = 2 * z * dc + 1;
    mpc_mul(dc, z, dc, MPC_RNDNN);
    mpc_mul_2si(dc, dc, 1, MPC_RNDNN);
    mpfr_add_d(mpc_realref(dc), mpc_realref(dc), 1, MPFR_RNDN);
    // z = z * z + c_guess;
    mpc_sqr(z, z, MPC_RNDNN);
    mpc_add(z, z, c_guess, MPC_RNDNN);
    // reject lower periods
    if (i < period && period % i == 0)
    {
      // h = h * z;
      mpc_mul(h, h, z, MPC_RNDNN);
      // dh = dh + dc / z;
      mpc_div(f, dc, z, MPC_RNDNN);
      mpc_add(dh, dh, f, MPC_RNDNN);
    }
  }
  // build function
  // dh = dh * h;
  mpc_mul(dh, dh, h, MPC_RNDNN);
  // df = dc * h - z * dh
  mpc_mul(dc, dc, h, MPC_RNDNN);
  mpc_mul(dh, z, dh, MPC_RNDNN);
  mpc_sub(df, dc, dh, MPC_RNDNN);
  // f = z / h;
  mpc_div(f, z, h, MPC_RNDNN);
  // df /= h * h
  mpc_sqr(h, h, MPC_RNDNN);
  mpc_div(df, df, h, MPC_RNDNN);
  // newton step
  // c_new = c_guess - f / df;
  mpc_div(f, f, df, MPC_RNDNN);
  mpc_sub(c_new, c_guess, f, MPC_RNDNN);
  // check convergence
  // f = c_new - c_guess;
  mpc_sub(f, c_new, c_guess, MPC_RNDNN);
  mpc_norm(mpc_realref(df), f, MPC_RNDNN);
  if (mpfr_lessequal_p(mpc_realref(df), epsilon2)) {
    mpc_set(c_out, c_new, MPC_RNDNN);
    return m_converged;
  }
  if (mpfr_number_p(mpc_realref(f)) && mpfr_number_p(mpc_imagref(f))) {
    mpc_set(c_out, c_new, MPC_RNDNN);
    return m_stepped;
  } else {
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_failed;
  }
}

extern m_newton m_r_nucleus(mpc_t c_out, const mpc_t c_guess, int period, int maxsteps, int ncpus) {
  m_newton result = m_failed;
  // prec
  mpfr_prec_t precr, preci, prec;
  mpc_get_prec2(&precr, &preci, c_guess);
  prec = precr > preci ? precr : preci;
  // init
  {
    (void) ncpus;
    mpc_t c, z, dc, c_new, h, dh, f, df;
    mpfr_t epsilon2;
    mpc_init2(c, prec);
    mpc_init2(z, prec);
    mpc_init2(dc, prec);
    mpc_init2(c_new, prec);
    mpc_init2(h, prec);
    mpc_init2(dh, prec);
    mpc_init2(f, prec);
    mpc_init2(df, prec);
    mpfr_init2(epsilon2, prec);
    // epsilon
    mpfr_set_si(epsilon2, 2, MPFR_RNDN);
    mpfr_nextabove(epsilon2);
    mpfr_sub_si(epsilon2, epsilon2, 2, MPFR_RNDN);
    mpfr_sqr(epsilon2, epsilon2, MPFR_RNDN);
    // c = c_guess
    mpc_set(c, c_guess, MPC_RNDNN);
    for (int i = 0; i < maxsteps; ++i)
      if (m_stepped != (result = m_r_nucleus_step_raw(c, c, period, epsilon2, c_new, z, dc, h, dh, f, df)))
        break;
    // c_out = c;
    mpc_set_prec(c_out, prec);
    mpc_set(c_out, c, MPC_RNDNN);
    // cleanup
    mpc_clear(c);
    mpc_clear(z);
    mpc_clear(dc);
    mpc_clear(c_new);
    mpc_clear(h);
    mpc_clear(dh);
    mpc_clear(f);
    mpc_clear(df);
    mpfr_clear(epsilon2);
  }
  return result;
}
