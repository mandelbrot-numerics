// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2020 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

/*
See appendix of <http://arxiv.org/abs/1706.04177v1>
*/

struct m_r_spider_path {
  int preperiod;
  int period;
  int sharpness;
  mpfr_prec_t prec;
  mpc_t x0; // 0
  mpc_t *x; // (preperiod + period) * sharpness
  mpc_t *xn; // (preperiod + period)
  int n; // t = n + (s + 0.5) / sharpness
  int s; // [0 .. sharpness)
};

static mpc_t *X(m_r_spider_path *s, int i, int j)
{
  if (i == 0) return &s->x0;
  else return s->x + (j * (s->preperiod + s->period) + (i - 1));
}

extern m_r_spider_path *m_r_spider_path_new(const mpq_t angle0, int preperiod, int period, int sharpness, mpfr_prec_t precision)
{
  m_r_spider_path *path = malloc(sizeof(*path));
  if (! path)
  {
    return 0;
  }
  path->preperiod = preperiod;
  path->period = period;
  path->sharpness = sharpness;
  path->prec = precision;
  path->x = malloc(sizeof(*path->x) * sharpness * (preperiod + period));
  if (! path->x)
  {
    free(path);
    return 0;
  }
  path->xn = malloc(sizeof(*path->x) * (preperiod + period));
  if (! path->xn)
  {
    free(path->x);
    free(path);
    return 0;
  }
  // path->x0 = 0;
  mpc_init2(path->x0, precision);
  mpc_set_dc(path->x0, 0, MPC_RNDNN);
  for (int i = 0; i < sharpness * (preperiod + period); ++i)
  {
    mpc_init2(path->x[i], precision);
  }
  for (int i = 0; i < preperiod + period; ++i)
  {
    mpc_init2(path->xn[i], precision);
  }
#if 0
  mpc_init2(path->tc, precision);
  mpfr_init2(path->tn, precision);
  mpfr_init2(path->tp, precision);
#endif
  mpq_t angle;
  mpq_init(angle);
  mpq_set(angle, angle0);
  mpq_t one;
  mpq_init(one);
  mpq_set_ui(one, 1, 1);
  mpfr_t twopi_;
  mpfr_init2(twopi_, precision);
  mpfr_const_pi(twopi_, MPFR_RNDN);
  mpfr_mul_2si(twopi_, twopi_, 1, MPFR_RNDN);
  mpc_t theta;
  mpc_init2(theta, precision);
  // x_j = exp(2 \pi i 2^{j-1} \theta)
  for (int i = 1; i <= preperiod + period; ++i)
  {
    // *X(path, i, 0) = cexp(twopi * I * mpq_get_d(angle));
    mpfr_set_d(mpc_realref(theta), 0, MPFR_RNDN);
    mpfr_set_q(mpc_imagref(theta), angle, MPFR_RNDN);
    mpfr_mul(mpc_imagref(theta), mpc_imagref(theta), twopi_, MPFR_RNDN);
    mpc_exp(*X(path, i, 0), theta, MPC_RNDNN);
    for (int j = 1; j < sharpness; ++j)
    {
      mpc_set(*X(path, i, j), *X(path, i, 0), MPC_RNDNN);
    }
    // double angle
    mpq_mul_2exp(angle, angle, 1);
    if (mpq_cmp_ui(angle, 1, 1) >= 0) {
      mpq_sub(angle, angle, one);
    }
  }
  mpq_clear(angle);
  mpq_clear(one);
  mpc_clear(theta);
  mpfr_clear(twopi_);
  for (int j = 0; j < sharpness; ++j)
  {
    double t = (j + 0.5) / sharpness;
    // *X(path, 1, j) *= t;
    mpc_t *x = X(path, 1, j);
    mpfr_mul_d(mpc_realref(*x), mpc_realref(*x), t, MPFR_RNDN);
    mpfr_mul_d(mpc_imagref(*x), mpc_imagref(*x), t, MPFR_RNDN);
    if (preperiod == 0)
    {
      // *X(path, period, j) *= (1 - t);
      x = X(path, period, j);
      mpfr_mul_d(mpc_realref(*x), mpc_realref(*x), 1 - t, MPFR_RNDN);
      mpfr_mul_d(mpc_imagref(*x), mpc_imagref(*x), 1 - t, MPFR_RNDN);
    }
  }
  path->n = 0;
  path->s = 0;
  return path;
}

extern void m_r_spider_path_delete(m_r_spider_path *path)
{
  if (! path)
  {
    return;
  }
  mpc_clear(path->x0);
#if 0
  mpc_clear(path->tc);
  mpfr_clear(path->tn);
  mpfr_clear(path->tp);
#endif
  if (path->x)
  {
    for (int i = 0; i < path->sharpness * (path->preperiod + path->period); ++i)
    {
      mpc_clear(path->x[i]);
    }
    free(path->x);
  }
  if (path->xn)
  {
    for (int i = 0; i < path->preperiod + path->period; ++i)
    {
      mpc_clear(path->xn[i]);
    }
    free(path->xn);
  }
  free(path);
}

extern void m_r_spider_path_step(m_r_spider_path *path, bool parallel)
{
  // next values
  int s0 = path->s;
  // for continuity check
  int s1 = (path->s - 1 + path->sharpness) % path->sharpness;
  // pull back path
  // NOTE parallel needs thread-local temporaries (tc, tn, tp)
  #pragma omp parallel for schedule(static) if(parallel)
  for (int i = 0; i < path->preperiod + path->period; ++i)
  {
    mpc_t tc;
    mpfr_t tn;
    mpfr_t tp;
    mpc_init2(tc, path->prec);
    mpfr_init2(tn, path->prec);
    mpfr_init2(tp, path->prec);
    int j = i + 1;
    if (j >= path->preperiod + path->period)
    {
      j -= path->period;
    }
    // x[i] = csqrt(*X(path, j + 1, s0) - *X(path, 1, s0));
    mpc_sub(path->xn[i], *X(path, j + 1, s0), *X(path, 1, s0), MPC_RNDNN);
    mpc_sqrt(path->xn[i], path->xn[i], MPC_RNDNN);
    // continuity
    mpc_t *y = X(path, i + 1, s1);
    // if (cabs2(-x[i] - y) < cabs2(x[i] - y))
    mpc_add(tc, path->xn[i], *y, MPC_RNDNN);
    mpc_norm(tn, tc, MPFR_RNDN);
    mpc_sub(tc, path->xn[i], *y, MPC_RNDNN);
    mpc_norm(tp, tc, MPFR_RNDN);
    if (mpfr_less_p(tn, tp))
    {
      // x[i] = -x[i];
      mpc_neg(path->xn[i], path->xn[i], MPC_RNDNN);
    }
    mpc_clear(tc);
    mpfr_clear(tn);
    mpfr_clear(tp);
  }
  // copy
  #pragma omp parallel for schedule(static) if(parallel)
  for (int i = 0; i < path->preperiod + path->period; ++i)
  {
    mpc_set(*X(path, i + 1, s0), path->xn[i], MPC_RNDNN);
  }
  path->s++;
  if (path->s == path->sharpness)
  {
    path->s = 0;
    path->n++;
  }
}

extern void m_r_spider_path_get(const m_r_spider_path *path, int i, mpc_t c)
{
  if (! path)
  {
    return;
  }
  mpc_t *x = X((m_r_spider_path *) path, i, path->s); // const cast
  mpc_set_prec(c, mpfr_get_prec(mpc_realref(*x)));
  mpc_set(c, *x, MPC_RNDNN);
}

extern void m_r_spider_path_do(const mpq_t angle, int preperiod, int period, int sharpness, int precision, int maxsteps, bool parallel, mpc_t c)
{
  m_r_spider_path *path = m_r_spider_path_new(angle, preperiod, period, sharpness, precision);
  if (! path)
  {
    return;
  }
  for (int i = 0; i < maxsteps; ++i)
  {
    m_r_spider_path_step(path, parallel);
  }
  m_r_spider_path_get(path, 1, c);
  m_r_spider_path_delete(path);
}
