// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2021 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*
Ball method using linear Taylor approximation.  The iterated ball
surrounds the origin at iteration P implies there is a period P nucleus
inside the original ball.
*/

#include <mandelbrot-numerics.h>
#include <stdio.h>

struct m_r_near_period {
  mpc_t c, z, dz;
  mpfr_t r2, z2, dz2, er2;
  int p;
};

extern m_r_near_period *m_r_near_period_new(const mpc_t center, const mpfr_t radius) {
  m_r_near_period *near = (m_r_near_period *) malloc(sizeof(*near));
  if (! near) {
    return 0;
  }
  int bitsr = mpfr_get_prec(mpc_realref(center));
  int bitsi = mpfr_get_prec(mpc_imagref(center));
  int bits = bitsr > bitsi ? bitsr : bitsi;
  mpc_init2(near->c, bits); mpc_set(near->c, center, MPC_RNDNN);
  mpc_init2(near->z, bits); mpc_set_ui(near->z, 0, MPC_RNDNN);
  mpc_init2(near->dz, 53); mpc_set_ui(near->dz, 0, MPC_RNDNN);
  mpfr_init2(near->r2, 53); mpfr_sqr(near->r2, radius, MPFR_RNDN);
  mpfr_init2(near->er2, 53); mpfr_set_ui(near->er2, 65536, MPFR_RNDN);
  mpfr_init2(near->z2, 53); mpfr_set_ui(near->z2, 0, MPFR_RNDN);
  mpfr_init2(near->dz2, 53); mpfr_set_ui(near->dz2, 0, MPFR_RNDN);
  near->p = 0;
  m_r_near_period_step(near);
  return near;
}

extern void m_r_near_period_delete(m_r_near_period *near) {
  if (near) {
    mpc_clear(near->c);
    mpc_clear(near->z);
    mpc_clear(near->dz);
    mpfr_clear(near->r2);
    mpfr_clear(near->z2);
    mpfr_clear(near->dz2);
    mpfr_clear(near->er2);
    free(near);
  }
}

extern bool m_r_near_period_step(m_r_near_period *near) {
  if (! near) {
    return false;
  }
  // dz = 2 * z * dz + 1
  mpc_mul(near->dz, near->z, near->dz, MPC_RNDNN);
  mpfr_mul_2ui(mpc_realref(near->dz), mpc_realref(near->dz), 1, MPFR_RNDN);
  mpfr_mul_2ui(mpc_imagref(near->dz), mpc_imagref(near->dz), 1, MPFR_RNDN);
  mpfr_add_ui(mpc_realref(near->dz), mpc_realref(near->dz), 1, MPFR_RNDN);
  // z = z * z + c
  mpc_sqr(near->z, near->z, MPC_RNDNN);
  mpc_add(near->z, near->z, near->c, MPC_RNDNN);
  // z2 = cnorm(z)
  mpc_norm(near->z2, near->z, MPFR_RNDN);
  // dz2 = cnorm(dz) * r2
  mpc_norm(near->dz2, near->dz, MPFR_RNDN);
  mpfr_mul(near->dz2, near->dz2, near->r2, MPFR_RNDN);
  // p = p + 1
  near->p = near->p + 1;
  // z2 <= er2
  return mpfr_lessequal_p(near->z2, near->er2);
}

extern bool m_r_near_period_have_period(const m_r_near_period *near) {
  if (! near) {
    return true;
  }
  /* $$ 0 \in z + z' B \equiv -z / z' \in B \equiv |z/z'| < R \equiv |z|^2 < |z'|^2 R^2 $$ */
  // z2 < dz2 * r2
  return mpfr_less_p(near->z2, near->dz2);
}

extern int m_r_near_period_get_period(const m_r_near_period *near) {
  if (! near) {
    return 0;
  }
  return near->p;
}

extern int m_r_near_period_do(const mpc_t center, const mpfr_t radius, int maxperiod) {
  m_r_near_period *near = m_r_near_period_new(center, radius);
  if (! near) {
    return 0;
  }
  int period = 0;
  for (int i = 0; i < maxperiod; ++i) {
    if (m_r_near_period_have_period(near)) {
      period = m_r_near_period_get_period(near);
      break;
    }
    if (! m_r_near_period_step(near)) {
      break;
    }
  }
  m_r_near_period_delete(near);
  return period;
}
