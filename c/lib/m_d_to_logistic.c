// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2018 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>

// https://en.wikipedia.org/wiki/Talk:Logistic_map#Very_old_discussions
extern double m_d_to_logistic(double c)
{
  return 1 + sqrt(1 - 4 * c);
}
