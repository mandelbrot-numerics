// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2021 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

// for strdup()
#define _POSIX_C_SOURCE 200809L

#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mandelbrot-numerics.h>
#include "m_d_util.h"

static int cmp_str(const void *a, const void *b)
{
  return strcmp(a, b);
}

extern void m_r_external_angles(int nrays, char **rays, const mpc_t center, int preperiod, int period, double radius, int sharpness)
{
  double er = 256;
  if ((! rays) || nrays <= 0 || period < 1)
  {
    return;
  }
  if (preperiod == 0 && nrays > 2)
  {
    return;
  }
  if (nrays == 2 && preperiod == 0 && period == 1)
  {
    rays[0] = strdup(".(0)");
    rays[1] = strdup(".(1)");
    return;
  }
  mpfr_prec_t precr = mpfr_get_prec(mpc_realref(center));
  mpfr_prec_t preci = mpfr_get_prec(mpc_imagref(center));
  mpfr_prec_t prec = precr > preci ? precr : preci;
  mpfr_t r;
  mpfr_init2(r, 53);
  if (preperiod == 0)
  {
    m_r_domain_size(r, center, period);
  }
  else
  {
    m_r_misiurewicz_size(r, center, preperiod + 1, period);
  }
  mpfr_mul_d(r, r, radius, MPFR_RNDN);
  int resolution = sharpness * nrays + 1;
  int *counts = malloc(resolution * sizeof(int));
  int maxiter = 16 * (preperiod + period);
  mpc_t probe, z;
  mpc_init2(probe, prec);
  mpc_init2(z, prec);
  mpfr_t mz, er2;
  mpfr_init2(mz, 53);
  mpfr_init2(er2, 53);
  mpfr_set_d(er2, er * er, MPFR_RNDN);
  for (int t = 0; t < resolution; ++t)
  {
    double a = 2 * pi * (t + 0.5) / resolution;
    double c = cos(a);
    double s = sin(a);
    // probe = nucleus + r * (c + I * s);
    mpc_set_d_d(probe, c, s, MPC_RNDNN);
    mpc_mul_fr(probe, probe, r, MPC_RNDNN);
    mpc_add(probe, probe, center, MPC_RNDNN);
    // z = 0
    mpc_set_d(z, 0, MPC_RNDNN);
    int count = maxiter;
    for (int i = 0; i < maxiter; ++i)
    {
      // z = z * z + probe;
      mpc_sqr(z, z, MPC_RNDNN);
      mpc_add(z, z, probe, MPC_RNDNN);
      // mz = cabs2(z);
      mpc_norm(mz, z, MPFR_RNDN);
      if (mpfr_greater_p(mz, er2))
      {
        count = i;
        break;
      }
    }
    counts[t] = count;
  }
  int nray = 0;
  while (1)
  {
    int mint = -1;
    int mincount = maxiter;
    for (int t = 0; t < resolution; ++t)
    {
      if (counts[t] < mincount)
      {
        mincount = counts[t];
        mint = t;
      }
    }
    if (mint == -1)
    {
      for (int t = 0; t < nrays; ++t)
      {
        if (rays[t])
        {
          free(rays[t]);
          rays[t] = 0;
        }
      }
      return;
    }
    double a = 2 * pi * (mint + 0.5) / resolution;
    double c = cos(a);
    double s = sin(a);
    // probe = nucleus + r * (c + I * s);
    mpc_set_d_d(probe, c, s, MPC_RNDNN);
    mpc_mul_fr(probe, probe, r, MPC_RNDNN);
    mpc_add(probe, probe, center, MPC_RNDNN);
    char *bits = m_r_exray_out_do(probe, 8, mincount * 2, er);
    if (bits)
    {
      int bitlen = strlen(bits);
      if (bitlen >= preperiod + period)
      {
        char *angle = malloc(1 + preperiod + 1 + period + 1 + 1);
        int k = 0;
        angle[k++] = '.';
        for (int i = 0; i < preperiod; ++i)
        {
          angle[k++] = bits[bitlen-1 - i];
        }
        angle[k++] = '(';
        for (int i = 0; i < period; ++i)
        {
          angle[k++] = bits[bitlen-1 - (i + preperiod)];
        }
        angle[k++] = ')';
        angle[k++] = 0;
        free(bits);
        bool duplicate = false;
        for (int i = 0; i < nray; ++i)
        {
          if (rays[i])
          {
            if (strcmp(rays[i], angle) == 0)
            {
              duplicate = true;
            }
          }
        }
        if (duplicate)
        {
          free(angle);
        }
        else
        {
          rays[nray++] = angle;
          if (nray == nrays)
          {
            qsort(rays, nrays, sizeof(*rays), cmp_str);
            return;
          }
        }
      }
    }
    counts[mint] = maxiter;
  }
}
