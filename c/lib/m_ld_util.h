// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef M_LD_UTIL_H
#define M_LD_UTIL_H 1

#include <complex.h>

static inline int sgnl(long double z) {
  if (z > 0) { return  1; }
  if (z < 0) { return -1; }
  return 0;
}

static inline long double cabs2l(long double _Complex z) {
  return creall(z) * creall(z) + cimagl(z) * cimagl(z);
}

static inline long double cabsmaxl(long double _Complex z) {
  return fmaxl(fabsl(creall(z)), fabsl(cimagl(z)));
}

static inline bool cisfinitel(long double _Complex z) {
  return isfinite(creall(z)) && isfinite(cimagl(z));
}

static const long double pil = 3.1415926535897932385L;
static const long double twopil = 6.283185307179586477L;

// last . takeWhile (\x -> 2 /= 2 + x) . iterate (/2) $ 1 :: LongDouble
static const long double epsilonl = 2.168404344971008868e-19L;

// epsilon^2
static const long double epsilon2l = 4.701977403289150032e-38L;

#endif
