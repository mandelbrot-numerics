// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

// for strdup()
#define _POSIX_C_SOURCE 200809L

#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mandelbrot-numerics.h>
#include "m_ld_util.h"

static int cmp_str(const void *a, const void *b)
{
  return strcmp(a, b);
}

extern void m_ld_external_angles_perturbed(int nrays, char **rays, const m_r_orbit_ld *orbit, int preperiod, int period, long double radius, int sharpness)
{
  int trace_sharpness = 4;
  int maxsteps = 8;
  long double er = 256;
  if ((! rays) || nrays <= 0 || period < 1 || preperiod < 0 || !orbit)
  {
    return;
  }
  if (preperiod == 0 && nrays > 2)
  {
    return;
  }
  if (nrays == 2 && preperiod == 0 && period == 1)
  {
    rays[0] = strdup(".(0)");
    rays[1] = strdup(".(1)");
    return;
  }
  long double r;
  if (preperiod == 0)
  {
    r = m_ld_domain_size_orbit(orbit);
  }
  else
  {
    r = m_ld_misiurewicz_size_orbit(orbit);
  }
  r *= radius;
  int resolution = sharpness * nrays + 1;
  int *counts = malloc(resolution * sizeof(int));
  int maxiter = 2 * (preperiod + period);
  int target_dwell = 100 + (preperiod + period); // FIXME check
  if (target_dwell > maxiter) target_dwell = maxiter;
  int deep = 0;
  do
  {
    r *= 2;
    deep = 0;
    for (int t = 0; t < resolution; ++t)
    {
      long double a = 2 * pil * (t + 0.5) / resolution;
      long double c = cosl(a);
      long double s = sinl(a);
      long double _Complex probe = r * (c + I * s);
      counts[t] = m_ld_dwell_perturbed(orbit, probe, er, maxiter);
      deep += counts[t] > target_dwell;
    }
  } while (deep == resolution);
  r /= 2;
  for (int t = 0; t < resolution; ++t)
  {
    long double a = 2 * pil * (t + 0.5) / resolution;
    long double c = cosl(a);
    long double s = sinl(a);
    long double _Complex probe = r * (c + I * s);
    counts[t] = m_ld_dwell_perturbed(orbit, probe, er, maxiter);
  }

  int nray = 0;
  while (1)
  {
    int mint = -1;
    int mincount = maxiter;
    for (int t = 0; t < resolution; ++t)
    {
      if (counts[t] < mincount)
      {
        mincount = counts[t];
        mint = t;
      }
    }
    if (mint == -1)
    {
      for (int t = 0; t < nrays; ++t)
      {
        if (rays[t])
        {
          free(rays[t]);
          rays[t] = 0;
        }
      }
      return;
    }
    long double a = 2 * pil * (mint + 0.5) / resolution;
    long double c = cosl(a);
    long double s = sinl(a);
    long double _Complex probe = r * (c + I * s);
    char *bits = m_ld_exray_out_perturbed_do(orbit, probe, trace_sharpness, mincount * 2, er, maxsteps);
    if (bits)
    {
      int bitlen = strlen(bits);
      if (bitlen >= preperiod + period)
      {
        char *angle = malloc(1 + preperiod + 1 + period + 1 + 1);
        int k = 0;
        angle[k++] = '.';
        for (int i = 0; i < preperiod; ++i)
        {
          angle[k++] = bits[bitlen-1 - i];
        }
        angle[k++] = '(';
        for (int i = 0; i < period; ++i)
        {
          angle[k++] = bits[bitlen-1 - (i + preperiod)];
        }
        angle[k++] = ')';
        angle[k++] = 0;
        free(bits);
        bool duplicate = false;
        for (int i = 0; i < nray; ++i)
        {
          if (rays[i])
          {
            if (strcmp(rays[i], angle) == 0)
            {
              duplicate = true;
            }
          }
        }
        if (duplicate)
        {
          free(angle);
        }
        else
        {
          rays[nray++] = angle;
          if (nray == nrays)
          {
            qsort(rays, nrays, sizeof(*rays), cmp_str);
            return;
          }
        }
      }
    }
    counts[mint] = maxiter;
  }
}
