// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>

extern int m_qd_dwell_perturbed(const m_r_orbit_qd *orbit, _Float128 _Complex c, _Float128 er, int maxiters) // FIXME TODO implement without _Complex
{
  const _Float128 _Complex *z_ptr = m_r_orbit_qd_get_z_ptr(orbit);
  const int preperiod = m_r_orbit_qd_get_preperiod(orbit);
  const int period = m_r_orbit_qd_get_period(orbit);
  _Float128 _Complex z = 0;
  int m = 0;
  for (int i = 0; i < maxiters; ++i)
  {
    _Float128 _Complex Z = z_ptr[m];
    if (cabsf128(Z + z) < cabsf128(z))
    {
      z = Z + z;
      m = 0;
      Z = z_ptr[m];
    }
    if (cabsf128(Z + z) > er)
    {
      return i;
    }
    z = (2 * Z + z) * z + c;
    if (++m == preperiod + period)
    {
      m = preperiod;
    }
  }
  return maxiters;
}
