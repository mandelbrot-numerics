// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2019 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"

struct m_d_equipotential {
  int count;
  double _Complex c;
  double _Complex d;
  double _Complex z;
};

extern m_d_equipotential *m_d_equipotential_new(const double _Complex c, int sharpness, int count) {
  m_d_equipotential *ray = malloc(sizeof(*ray));
  ray->count = count;
  ray->c = c;
  ray->d = cexp(I * twopi / sharpness);
  double _Complex z = 0;
  for (int i = 0; i < count; ++i)
  {
    z = z * z + c;
  }
  ray->z = z;
  return ray;
}

extern void m_d_equipotential_delete(m_d_equipotential *ray) {
  if (! ray) {
    return;
  }
  free(ray);
}

extern m_newton m_d_equipotential_step(m_d_equipotential *ray, int maxsteps) {
  ray->z *= ray->d;
  double _Complex target = ray->z;
  double _Complex c = ray->c;
  for (int i = 0; i < maxsteps; ++i) {
    double _Complex z = 0;
    double _Complex dc = 0;
    for (int p = 0; p < ray->count; ++p) {
      dc = 2 * z * dc + 1;
      z = z * z + c;
    }
    double _Complex c_new = c - (z - target) / dc;
    if (cisfinite(c_new)) {
      c = c_new;
    } else {
      break;
    }
  }
  if (cisfinite(c)) {
    ray->c = c;
    return m_stepped;
  } else {
    return m_failed;
  }
}

extern double _Complex m_d_equipotential_get(const m_d_equipotential *ray) {
  if (! ray) {
    return 0;
  }
  return ray->c;
}
