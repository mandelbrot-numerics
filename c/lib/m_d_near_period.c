// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2021 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*
Ball method using linear Taylor approximation.  The iterated ball
surrounds the origin at iteration P implies there is a period P nucleus
inside the original ball.
*/

#include <mandelbrot-numerics.h>
#include "m_d_util.h"
#include <stdio.h>

struct m_d_near_period {
  double _Complex c;
  double _Complex z;
  double _Complex dz;
  double r2;
  int p;
};

extern m_d_near_period *m_d_near_period_new(double _Complex center, double radius) {
  m_d_near_period *near = (m_d_near_period *) malloc(sizeof(*near));
  if (! near) {
    return 0;
  }
  near->c = center;
  near->r2 = radius * radius;
  near->z = 0;
  near->dz = 0;
  near->p = 0;
  m_d_near_period_step(near);
  return near;
}

extern void m_d_near_period_delete(m_d_near_period *near) {
  if (near) {
    free(near);
  }
}

extern bool m_d_near_period_step(m_d_near_period *near) {
  if (! near) {
    return false;
  }
  near->dz = 2 * near->z * near->dz + 1;
  near->z = near->z * near->z + near->c;
  near->p = near->p + 1;
  return isfinite(cabs2(near->z));
}

extern bool m_d_near_period_have_period(const m_d_near_period *near) {
  if (! near) {
    return true;
  }
  /* $$ 0 \in z + z' B \equiv -z / z' \in B \equiv |z/z'| < R \equiv |z|^2 < |z'|^2 R^2 $$ */
  return cabs2(near->z) < cabs2(near->dz) * near->r2;
}

extern int m_d_near_period_get_period(const m_d_near_period *near) {
  if (! near) {
    return 0;
  }
  return near->p;
}

extern int m_d_near_period_do(double _Complex center, double radius, int maxperiod) {
  m_d_near_period *near = m_d_near_period_new(center, radius);
  if (! near) {
    return 0;
  }
  int period = 0;
  for (int i = 0; i < maxperiod; ++i) {
    if (m_d_near_period_have_period(near)) {
      period = m_d_near_period_get_period(near);
      break;
    }
    if (! m_d_near_period_step(near)) {
      break;
    }
  }
  m_d_near_period_delete(near);
  return period;
}
