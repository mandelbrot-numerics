// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_qd_util.h"

#include <stdio.h>

static inline bool mpc_finite_p(const mpc_t z) {
  return mpfr_number_p(mpc_realref(z)) && mpfr_number_p(mpc_imagref(z));
}

static _Float128 dwell(_Float128 loger, int n, _Float128 zmag) {
  return n - log2f128(logf128(zmag) / loger);
}

struct m_qd_exray_out_perturbed
{
  const m_r_orbit_qd *orbit;
  int sharpness;
  _Float128 d;
  _Float128 er;
  _Float128 loger;
  _Float128 _Complex delta_c;
  _Float128 _Complex z;
  int n;
  int bit;
};

extern m_qd_exray_out_perturbed *m_qd_exray_out_perturbed_new(const m_r_orbit_qd *orbit, _Float128 _Complex endpoint, int sharpness, int maxdwell, _Float128 er)
{
  m_qd_exray_out_perturbed *ray = malloc(sizeof(*ray));
  if (! ray) {
    return 0;
  }
  ray->orbit = orbit;
  ray->delta_c = endpoint;
  ray->er = er;
  ray->loger = logf128(er);
  int n = 0;
  _Float128 _Complex z = 0;
  int m = 0;
  const int preperiod = m_r_orbit_qd_get_preperiod(ray->orbit);
  const int period = m_r_orbit_qd_get_period(ray->orbit);
  const _Float128 _Complex *z_ptr = m_r_orbit_qd_get_z_ptr(orbit);
  for (int i = 0; i < maxdwell; ++i) {
    _Float128 _Complex Z = z_ptr[m];
    if (cabsf128(Z + z) < cabsf128(z))
    {
      z = Z + z;
      m = 0;
      Z = z_ptr[m];
    }
    if (cabsf128(Z + z) > ray->er)
    {
      break;
    }
    z = (2 * Z + z) * z + ray->delta_c;
    if (++m == preperiod + period)
      m = preperiod;
    n = n + 1;
  }
  _Float128 _Complex Z = z_ptr[m];
  if (! (cabsf128(Z + z) > ray->er)) {
    free(ray);
    return 0;
  }
  ray->sharpness = sharpness;
  ray->z = Z + z;
  ray->d = dwell(ray->loger, n, cabsf128(ray->z));
  ray->n = n;
  ray->bit = -1;
  return ray;
}

extern void m_qd_exray_out_perturbed_delete(m_qd_exray_out_perturbed *ray)
{
  if (ray)
  {
    free(ray);
  }
}

extern m_newton m_qd_exray_out_perturbed_step(m_qd_exray_out_perturbed *ray, int maxsteps)
{
  if (! ray) {
    return m_failed;
  }
  ray->bit = -1;
  ray->d -= 1.0 / ray->sharpness;
  if (ray->d <= 0) {
    return m_converged;
  }
  int m = ceilf128(ray->d);
  _Float128 r = powf128(ray->er, powf128(2, m - ray->d));
  _Float128 a = cargf128(ray->z) / twopiq;
  _Float128 t = a - floorf128(a);
  const int preperiod = m_r_orbit_qd_get_preperiod(ray->orbit);
  const int period = m_r_orbit_qd_get_period(ray->orbit);
  const int length = preperiod + period;
  const _Float128 * restrict orbit = (const _Float128 * restrict) m_r_orbit_qd_get_z_ptr(ray->orbit);
  if (m == ray->n) {
    _Float128 _Complex target = r * cexpf128(I * twopiq *  t);
    _Float128 _Complex c = ray->delta_c;
    _Float128 _Complex Z = 0, z  = 0, dc = 0;
    for (int i = 0; i < maxsteps; ++i)
    {
      // this version is a few % faster than using _Complex
      _Float128 zx = 0, zy = 0, dcx = 0, dcy = 0, cx = crealf128(c), cy = cimagf128(c);
      int k = 0;
      for (int p = 0; p < m; ++p)
      {
        _Float128 Zx = orbit[2 * k];
        _Float128 Zy = orbit[2 * k + 1];
        _Float128 Zzx = Zx + zx;
        _Float128 Zzy = Zy + zy;
        _Float128 Zz2 = Zzx * Zzx + Zzy * Zzy;
        _Float128 z2 = zx * zx + zy * zy;
        if (Zz2 < z2)
        {
          zx = Zzx;
          zy = Zzy;
          k = 0;
          Zx = 0;
          Zy = 0;
        }
        _Float128 Z2zx = 2 * Zx + zx;
        _Float128 Z2zy = 2 * Zy + zy;
        _Float128 dcxn = 2 * (Zzx * dcx - Zzy * dcy) + 1;
        _Float128 dcyn = 2 * (Zzx * dcy + Zzy * dcx);
        _Float128 zxn = Z2zx * zx - Z2zy * zy + cx;
        _Float128 zyn = Z2zx * zy + Z2zy * zx + cy;
        dcx = dcxn;
        dcy = dcyn;
        zx = zxn;
        zy = zyn;
        if (++k == length)
        {
          k = preperiod;
        }
      }
      z = zx + I * zy;
      dc = dcx + I * dcy;
      Z = orbit[2 * k] + I * orbit[2 * k + 1];
      _Float128 _Complex c_old = c;
      c -= (Z + z - target) / dc;
      if (c == c_old) { fprintf(stderr, "ray stuck : c = %Le + I * %Le\n", (long double) crealf128(c), (long double) cimagf128(c)); break; }
      if (! cisfinitef128(c)) { fprintf(stderr, "ray failed : c = %Le + I * %Le\n", (long double) crealf128(c_old), (long double) cimagf128(c_old)); break; }
    }
    if (cisfinitef128(c))
    {
      ray->delta_c = c;
      ray->z = Z + z;
      ray->n = m;
      ray->d = dwell(ray->loger, m, cabsf128(ray->z));
      return m_stepped;
    }
    return m_failed;
  } else {
    _Float128 _Complex target[2] =
      { r * cexpf128(I * piq *  t     )
      , r * cexpf128(I * piq * (t + 1))
      };
    _Float128 _Complex c = ray->delta_c;
    _Float128 _Complex Z = 0, z  = 0, dc = 0;
    for (int i = 0; i < maxsteps; ++i)
    {
      // this version is a few % faster than using _Complex
      _Float128 zx = 0, zy = 0, dcx = 0, dcy = 0, cx = crealf128(c), cy = cimagf128(c);
      int k = 0;
      for (int p = 0; p < m; ++p)
      {
        _Float128 Zx = orbit[2 * k];
        _Float128 Zy = orbit[2 * k + 1];
        _Float128 Zzx = Zx + zx;
        _Float128 Zzy = Zy + zy;
        _Float128 Zz2 = Zzx * Zzx + Zzy * Zzy;
        _Float128 z2 = zx * zx + zy * zy;
        if (Zz2 < z2)
        {
          zx = Zzx;
          zy = Zzy;
          k = 0;
          Zx = 0;
          Zy = 0;
        }
        _Float128 Z2zx = 2 * Zx + zx;
        _Float128 Z2zy = 2 * Zy + zy;
        _Float128 dcxn = 2 * (Zzx * dcx - Zzy * dcy) + 1;
        _Float128 dcyn = 2 * (Zzx * dcy + Zzy * dcx);
        _Float128 zxn = Z2zx * zx - Z2zy * zy + cx;
        _Float128 zyn = Z2zx * zy + Z2zy * zx + cy;
        dcx = dcxn;
        dcy = dcyn;
        zx = zxn;
        zy = zyn;
        if (++k == length)
        {
          k = preperiod;
        }
      }
      z = zx + I * zy;
      dc = dcx + I * dcy;
      Z = orbit[2 * k] + I * orbit[2 * k + 1];
      _Float128 _Complex c_old = c;
      ray->z = Z + z;
      ray->bit = cabsf128(ray->z - target[1]) < cabsf128(ray->z - target[0]);
      c -= (ray->z - target[ray->bit]) / dc;
      if (c == c_old) { fprintf(stderr, "ray stuck : c = %Le + I * %Le\n", (long double) crealf128(c), (long double) cimagf128(c)); break; }
      if (! cisfinitef128(c)) { fprintf(stderr, "ray failed : c = %Le + I * %Le\n", (long double) crealf128(c_old), (long double) cimagf128(c_old)); break; }
    }
    if (cisfinitef128(c))
    {
      ray->delta_c = c;
      ray->n = m;
      ray->d = dwell(ray->loger, m, cabsf128(ray->z));
      return m_stepped;
    }
    return m_failed;
  }
}

extern bool m_qd_exray_out_perturbed_have_bit(const m_qd_exray_out_perturbed *ray) {
  if (! ray) {
    return false;
  }
  return 0 <= ray->bit;
}

extern bool m_qd_exray_out_perturbed_get_bit(const m_qd_exray_out_perturbed *ray) {
  if (! ray) {
    return false;
  }
 return ray->bit;
}

extern _Float128 _Complex m_qd_exray_out_perturbed_get(const m_qd_exray_out_perturbed *ray) {
  if (! ray) {
    return 0;
  }
  return ray->delta_c;
}

extern char *m_qd_exray_out_perturbed_do(const m_r_orbit_qd *orbit, const _Float128 _Complex c, int sharpness, int maxdwell, _Float128 er, int maxsteps) {
  m_qd_exray_out_perturbed *ray = m_qd_exray_out_perturbed_new(orbit, c, sharpness, maxdwell, er);
  if (! ray) {
    return 0;
  }
  char *bits = malloc(maxdwell + 2);
  if (! bits) {
    m_qd_exray_out_perturbed_delete(ray);
    return 0;
  }
  int n = 0;
  while (n <= maxdwell) {
    if (m_qd_exray_out_perturbed_have_bit(ray)) {
      bits[n++] = '0' + m_qd_exray_out_perturbed_get_bit(ray);
    }
    if (m_stepped != m_qd_exray_out_perturbed_step(ray, maxsteps)) {
      break;
    }
  }
  bits[n] = 0;
  m_qd_exray_out_perturbed_delete(ray);
  return bits;
}
