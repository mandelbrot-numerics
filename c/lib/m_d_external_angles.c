// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2019 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

// for strdup()
#define _POSIX_C_SOURCE 200809L

#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mandelbrot-numerics.h>
#include "m_d_util.h"

extern void m_d_external_angles(char **lo, char **hi, double _Complex nucleus, int period, double radius, int resolution)
{
  if ((! lo) || (! hi))
  {
    return;
  }
  if (period <= 1)
  {
    *lo = strdup(".(0)");
    *hi = strdup(".(1)");
    return;
  }
  double r = m_d_domain_size(nucleus, period);
  r *= radius;
  int *counts = malloc(resolution * sizeof(int));
  int maxiter = 16 * period;
  for (int t = 0; t < resolution; ++t)
  {
    double a = 2 * pi * (t + 0.5) / resolution;
    double c = cos(a);
    double s = sin(a);
    double _Complex probe = nucleus + r * (c + I * s);
    double _Complex z = 0;
    int count = maxiter;
    for (int i = 0; i < maxiter; ++i)
    {
      z = z * z + probe;
      double mz = cabs2(z);
      if (mz > 1024)
      {
        count = i;
        break;
      }
    }
    counts[t] = count;
  }
  char *first = 0;
  while (1)
  {
    int mint = -1;
    int mincount = maxiter;
    for (int t = 0; t < resolution; ++t)
    {
      if (counts[t] < mincount)
      {
        mincount = counts[t];
        mint = t;
      }
    }
    if (mint == -1)
    {
      if (first)
        free(first);
      *lo = 0;
      *hi = 0;
      return;
    }
    double a = 2 * pi * (mint + 0.5) / resolution;
    double c = cos(a);
    double s = sin(a);
    double _Complex probe = nucleus + r * (c + I * s);
    char *bits = m_d_exray_out_do(probe, 16, mincount * 2);
    int bitlen = strlen(bits);
    char *angle = malloc(period + 4);
    angle[0] = '.';
    angle[1] = '(';
    for (int i = 0; i < period; ++i)
    {
      angle[2 + i] = bits[bitlen-1 - i];
    }
    angle[2 + period] = ')';
    angle[3 + period] = 0;
    free(bits);
    if (first)
    {
      if (strcmp(first, angle) != 0)
      {
        if (strcmp(first, angle) < 0)
        {
          *lo = first;
          *hi = angle;
        }
        else
        {
          *lo = angle;
          *hi = first;
        }
        free(counts);
        return;
      }
      else
      {
        free(angle);
      }
    }
    else
    {
      first = angle;
      angle = 0;
    }
    counts[mint] = maxiter;
  }
}
