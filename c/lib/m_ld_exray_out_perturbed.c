// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_ld_util.h"

#include <stdio.h>

// BLA_FACTOR 2 - 64 is 30% slower than no BLA
#define USE_BLA
#define BLA_FACTOR 2

static inline bool mpc_finite_p(const mpc_t z) {
  return mpfr_number_p(mpc_realref(z)) && mpfr_number_p(mpc_imagref(z));
}

static long double dwell(long double loger, int n, long double zmag) {
  return n - log2l(logl(zmag) / loger);
}

struct m_ld_exray_out_perturbed
{
  const m_r_orbit_ld *orbit;
#ifdef USE_BLA
  m_ld_bla *bla;
#endif
  int sharpness;
  long double d;
  long double er;
  long double loger;
  long double _Complex delta_c;
  long double _Complex z;
  int n;
  int bit;
};

extern m_ld_exray_out_perturbed *m_ld_exray_out_perturbed_new(const m_r_orbit_ld *orbit, long double _Complex endpoint, int sharpness, int maxdwell, long double er)
{
  m_ld_exray_out_perturbed *ray = malloc(sizeof(*ray));
  if (! ray) {
    return 0;
  }
#ifdef USE_BLA
  ray->bla = m_ld_bla_new(orbit, 24, cabsmaxl(endpoint) * BLA_FACTOR);
  if (! ray->bla)
  {
    free(ray);
    return 0;
  }
#endif
  ray->orbit = orbit;
  ray->delta_c = endpoint;
  ray->er = er;
  ray->loger = logl(er);
  int n = 0;
  long double _Complex z = 0;
  int m = 0;
  const int preperiod = m_r_orbit_ld_get_preperiod(ray->orbit);
  const int period = m_r_orbit_ld_get_period(ray->orbit);
  const long double _Complex *z_ptr = m_r_orbit_ld_get_z_ptr(orbit);
  for (int i = 0; i < maxdwell; ++i) {
    long double _Complex Z = z_ptr[m];
    if (cabsmaxl(Z + z) < cabsmaxl(z))
    {
      z = Z + z;
      m = 0;
      Z = z_ptr[m];
    }
    if (cabs2l(Z + z) > ray->er * ray->er)
    {
      break;
    }
    z = (2 * Z + z) * z + ray->delta_c;
    if (++m == preperiod + period)
      m = preperiod;
    n = n + 1;
  }
  long double _Complex Z = z_ptr[m];
  if (! (cabs2l(Z + z) > ray->er * ray->er)) {
    free(ray);
    return 0;
  }
  ray->sharpness = sharpness;
  ray->z = Z + z;
  ray->d = dwell(ray->loger, n, cabsl(ray->z));
  ray->n = n;
fprintf(stderr, "n = %d\n", n);
  ray->bit = -1;
  return ray;
}

extern void m_ld_exray_out_perturbed_delete(m_ld_exray_out_perturbed *ray)
{
  if (ray)
  {
#ifdef USE_BLA
    m_ld_bla_delete(ray->bla);
#endif
    free(ray);
  }
}

extern m_newton m_ld_exray_out_perturbed_step(m_ld_exray_out_perturbed *ray, int maxsteps)
{
  if (! ray) {
    return m_failed;
  }
  ray->bit = -1;
  ray->d -= 1.0 / ray->sharpness;
  if (ray->d <= 0) {
    return m_converged;
  }
  int m = ceil(ray->d);
  long double r = powl(ray->er, powl(2, m - ray->d));
  long double a = cargl(ray->z) / twopil;
  long double t = a - floorl(a);
  const int preperiod = m_r_orbit_ld_get_preperiod(ray->orbit);
  const int period = m_r_orbit_ld_get_period(ray->orbit);
  const int length = preperiod + period;
  const long double * restrict orbit = (const long double * restrict) m_r_orbit_ld_get_z_ptr(ray->orbit);
  if (m == ray->n) {
    long double _Complex target = r * cexpl(I * twopil *  t);
    long double _Complex c = ray->delta_c;
    long double _Complex Z = 0, z  = 0, dc = 0;
    for (int i = 0; i < maxsteps; ++i)
    {
      int k = 0;
      int p = 0;
#ifdef USE_BLA
      z = 0;
      dc = 0;
      m_ld_bla_iterate_z_dc(ray->bla, 1ull << 48, m, c, &z, &dc, &p, &k);
      if (p != m)
      {
        fprintf(stderr, "%d %d %Le %Le %Le %Le\n", p, m, creall(c), cimagl(c), creall(z), cimagl(z));
        abort();
      }
#else
      // this version is a few % faster than using _Complex
      long double zx = 0, zy = 0, dcx = 0, dcy = 0, cx = creall(c), cy = cimagl(c);
      for (p = 0; p < m; ++p)
      {
        long double Zx = orbit[2 * k];
        long double Zy = orbit[2 * k + 1];
        long double Zzx = Zx + zx;
        long double Zzy = Zy + zy;
        long double Zz2 = Zzx * Zzx + Zzy * Zzy;
        long double z2 = zx * zx + zy * zy;
        if (Zz2 < z2)
        {
          zx = Zzx;
          zy = Zzy;
          k = 0;
          Zx = 0;
          Zy = 0;
        }
        long double Z2zx = 2 * Zx + zx;
        long double Z2zy = 2 * Zy + zy;
        long double dcxn = 2 * (Zzx * dcx - Zzy * dcy) + 1;
        long double dcyn = 2 * (Zzx * dcy + Zzy * dcx);
        long double zxn = Z2zx * zx - Z2zy * zy + cx;
        long double zyn = Z2zx * zy + Z2zy * zx + cy;
        dcx = dcxn;
        dcy = dcyn;
        zx = zxn;
        zy = zyn;
        if (++k == length)
        {
          k = preperiod;
        }
      }
      z = zx + I * zy;
      dc = dcx + I * dcy;
#endif
      Z = orbit[2 * k] + I * orbit[2 * k + 1];
      long double _Complex c_old = c;
      c -= (Z + z - target) / dc;
      if (c == c_old) { break; }
      if (! cisfinitel(c)) { fprintf(stderr, "ray failed : c = %Le + I * %Le\n", creall(c_old), cimagl(c_old)); break; }
    }
    if (cisfinitel(c))
    {
      ray->delta_c = c;
      ray->z = Z + z;
      ray->n = m;
      ray->d = dwell(ray->loger, m, cabsl(ray->z));
#ifdef USE_BLA
      if (cabsmaxl(c) > m_ld_bla_get_rc(ray->bla))
      {
        m_ld_bla_update(ray->bla, 24, cabsmaxl(c) * BLA_FACTOR);
      }
#endif
      return m_stepped;
    }
    return m_failed;
  } else {
    long double _Complex target[2] =
      { r * cexpl(I * pil *  t     )
      , r * cexpl(I * pil * (t + 1))
      };
    long double _Complex c = ray->delta_c;
    long double _Complex Z = 0, z  = 0, dc = 0;
    for (int i = 0; i < maxsteps; ++i)
    {
      int k = 0;
      int p = 0;
#ifdef USE_BLA
      z = 0;
      dc = 0;
      m_ld_bla_iterate_z_dc(ray->bla, 1ull << 48, m, c, &z, &dc, &p, &k);
      if (p != m)
      {
        fprintf(stderr, "%d %d %Le %Le %Le %Le\n", p, m, creall(c), cimagl(c), creall(z), cimagl(z));
        abort();
      }
#else
      // this version is a few % faster than using _Complex
      long double zx = 0, zy = 0, dcx = 0, dcy = 0, cx = creall(c), cy = cimagl(c);
      for (p = 0; p < m; ++p)
      {
        long double Zx = orbit[2 * k];
        long double Zy = orbit[2 * k + 1];
        long double Zzx = Zx + zx;
        long double Zzy = Zy + zy;
        long double Zz2 = Zzx * Zzx + Zzy * Zzy;
        long double z2 = zx * zx + zy * zy;
        if (Zz2 < z2)
        {
          zx = Zzx;
          zy = Zzy;
          k = 0;
          Zx = 0;
          Zy = 0;
        }
        long double Z2zx = 2 * Zx + zx;
        long double Z2zy = 2 * Zy + zy;
        long double dcxn = 2 * (Zzx * dcx - Zzy * dcy) + 1;
        long double dcyn = 2 * (Zzx * dcy + Zzy * dcx);
        long double zxn = Z2zx * zx - Z2zy * zy + cx;
        long double zyn = Z2zx * zy + Z2zy * zx + cy;
        dcx = dcxn;
        dcy = dcyn;
        zx = zxn;
        zy = zyn;
        if (++k == length)
        {
          k = preperiod;
        }
      }
      z = zx + I * zy;
      dc = dcx + I * dcy;
#endif
      Z = orbit[2 * k] + I * orbit[2 * k + 1];
      long double _Complex c_old = c;
      ray->z = Z + z;
      ray->bit = cabsmaxl(ray->z - target[1]) < cabsmaxl(ray->z - target[0]);
      c -= (ray->z - target[ray->bit]) / dc;
      if (c == c_old) { break; }
      if (! cisfinitel(c)) { fprintf(stderr, "ray failed : c = %Le + I * %Le\n", creall(c_old), cimagl(c_old)); break; }
    }
    if (cisfinitel(c))
    {
      ray->delta_c = c;
      ray->n = m;
      ray->d = dwell(ray->loger, m, cabsl(ray->z));
#ifdef USE_BLA
      if (cabsmaxl(c) > m_ld_bla_get_rc(ray->bla))
      {
        m_ld_bla_update(ray->bla, 24, cabsmaxl(c) * BLA_FACTOR);
      }
#endif
      return m_stepped;
    }
    return m_failed;
  }
}

extern bool m_ld_exray_out_perturbed_have_bit(const m_ld_exray_out_perturbed *ray) {
  if (! ray) {
    return false;
  }
  return 0 <= ray->bit;
}

extern bool m_ld_exray_out_perturbed_get_bit(const m_ld_exray_out_perturbed *ray) {
  if (! ray) {
    return false;
  }
 return ray->bit;
}

extern long double _Complex m_ld_exray_out_perturbed_get(const m_ld_exray_out_perturbed *ray) {
  if (! ray) {
    return 0;
  }
  return ray->delta_c;
}

extern char *m_ld_exray_out_perturbed_do(const m_r_orbit_ld *orbit, const long double _Complex c, int sharpness, int maxdwell, long double er, int maxsteps) {
  m_ld_exray_out_perturbed *ray = m_ld_exray_out_perturbed_new(orbit, c, sharpness, maxdwell, er);
  if (! ray) {
    return 0;
  }
  char *bits = malloc(maxdwell + 2);
  if (! bits) {
    m_ld_exray_out_perturbed_delete(ray);
    return 0;
  }
  int n = 0;
  while (n <= maxdwell) {
    if (m_ld_exray_out_perturbed_have_bit(ray)) {
      bits[n++] = '0' + m_ld_exray_out_perturbed_get_bit(ray);
    }
    if (m_stepped != m_ld_exray_out_perturbed_step(ray, maxsteps)) {
      break;
    }
  }
  bits[n] = 0;
  m_ld_exray_out_perturbed_delete(ray);
  return bits;
}
