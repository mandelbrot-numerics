// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_ld_util.h"

extern long double m_ld_domain_size(long double _Complex nucleus, int period)
{
  long double _Complex z = nucleus;
  long double _Complex dc = 1;
  long double zq2 = cabs2l(z);
  for (int q = 2; q <= period; ++q)
  {
    dc = 2 * z * dc + 1;
    z = z * z + nucleus;
    long double zp2 = cabs2l(z);
    if (q < period && zp2 < zq2)
      zq2 = zp2;
  }
  return sqrtl(zq2) / cabsl(dc);
}

extern long double m_ld_filtered_domain_size(long double _Complex nucleus, int period, m_period_filter_t *filter)
{
  long double _Complex z = 0;
  long double _Complex dc = 0;
  long double zq = 1.0/0.0;
  for (int q = 1; q <= period; ++q)
  {
    dc = 2 * z * dc + 1;
    z = z * z + nucleus;
    long double zp = cabsl(z);
    if (q < period && zp < zq && filter->accept(filter, q))
      zq = zp;
  }
  return zq / cabsl(dc);
}

extern long double m_ld_domain_size_orbit(const m_r_orbit_ld *orbit)
{
  const long double _Complex *z_ptr = m_r_orbit_ld_get_z_ptr(orbit);
  const int period = m_r_orbit_ld_get_period(orbit);
  long double _Complex z = z_ptr[1];
  long double _Complex dc = 1;
  long double zq2 = cabs2l(z);
  for (int q = 2; q <= period; ++q)
  {
    dc = 2 * z * dc + 1;
    if (q < period)
    {
      z = z_ptr[q];
      long double zp2 = cabs2l(z);
      if (q < period && zp2 < zq2)
        zq2 = zp2;
    }
  }
  return sqrtl(zq2) / cabsl(dc);
}
