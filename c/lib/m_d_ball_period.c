// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2021 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>
#include "m_d_util.h"
#include <stdio.h>

struct m_d_ball_period {
  double _Complex c;
  double _Complex z;
  double _Complex dz;
  double r, rdz, rz, rr, Ei;
  int p;
};

extern m_d_ball_period *m_d_ball_period_new(double _Complex center, double radius) {
  m_d_ball_period *ball = (m_d_ball_period *) malloc(sizeof(*ball));
  if (! ball) {
    return 0;
  }
  ball->c = center;
  ball->z = 0;
  ball->dz = 0;
  ball->r = radius;
  ball->rdz = 0;
  ball->rz = 0;
  ball->rr = 0;
  ball->Ei = 0;
  ball->p = 0;
  m_d_ball_period_step(ball);
  return ball;
}

extern void m_d_ball_period_delete(m_d_ball_period *ball) {
  if (ball) {
    free(ball);
  }
}

extern bool m_d_ball_period_step(m_d_ball_period *ball) {
  if (! ball) {
    return false;
  }
  ball->Ei = ball->rdz * ball->rdz + (2 * ball->rz + ball->r * (2 * ball->rdz + ball->r * ball->Ei)) * ball->Ei;
  ball->dz = 2 * ball->z * ball->dz + 1;
  ball->z = ball->z * ball->z + ball->c;
  ball->rdz = cabs(ball->dz);
  ball->rz = cabs(ball->z);
  ball->rr = ball->r * (ball->rdz + ball->r * ball->Ei);
  ball->p = ball->p + 1;
  return ball->rz - ball->rr <= 2;
}

extern bool m_d_ball_period_have_period(const m_d_ball_period *ball) {
  if (! ball) {
    return true;
  }
  return ball->rz <= ball->rr;
}

extern bool m_d_ball_period_will_converge(const m_d_ball_period *ball) {
  if (! ball) {
    return false;
  }
  // safe to use Newton-Raphson without interval arithmetic when f is less than r f'
  return ball->rz < ball->r * (ball->rdz - ball->r * ball->Ei);
}

extern int m_d_ball_period_get_period(const m_d_ball_period *ball) {
  if (! ball) {
    return 0;
  }
  return ball->p;
}

extern int m_d_ball_period_do(double _Complex center, double radius, int maxperiod) {
  m_d_ball_period *ball = m_d_ball_period_new(center, radius);
  if (! ball) {
    return 0;
  }
  int period = 0;
  for (int i = 0; i < maxperiod; ++i) {
    if (m_d_ball_period_have_period(ball)) {
      period = m_d_ball_period_get_period(ball);
      if (! m_d_ball_period_will_converge(ball))
      {
        period = -period;
      }
      break;
    }
    if (! m_d_ball_period_step(ball)) {
      break;
    }
  }
  m_d_ball_period_delete(ball);
  return period;
}
