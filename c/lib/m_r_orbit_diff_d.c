// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>

struct m_r_orbit_diff_d
{
  int preperiod;
  int period;
  int offset;
  mpc_t c;
  double _Complex *orbit;
  double _Complex *diff;
};

extern m_r_orbit_diff_d *m_r_orbit_diff_d_new(const mpc_t c, int preperiod, int period, int offset)
{
  m_r_orbit_diff_d *o = calloc(1, sizeof(*o));
  if (! o)
  {
    return 0;
  }
  o->preperiod = preperiod;
  o->period = period;
  o->offset = offset;
  o->orbit = malloc(sizeof(*o->orbit) * (preperiod + period));
  if (! o->orbit)
  {
    free(o);
    return 0;
  }
  o->diff = malloc(sizeof(*o->diff) * (preperiod + period));
  if (! o->diff)
  {
    free(o->orbit);
    free(o);
    return 0;
  }
  mpc_t z, w, d;
  mpc_init2(z, mpfr_get_prec(mpc_realref(c)));
  mpc_init2(w, mpfr_get_prec(mpc_realref(c)));
  mpc_init2(d, 53);
  mpc_set_dc(z, 0, MPC_RNDNN);
  mpc_set_dc(w, 0, MPC_RNDNN);
  for (int i = 0; i < offset; ++i)
  {
    mpc_sqr(z, z, MPC_RNDNN);
    mpc_add(z, z, c, MPC_RNDNN);
  }
  for (int i = 0; i < preperiod + period; ++i)
  {
    o->orbit[i] = mpc_get_dc(w, MPC_RNDNN);
    mpc_sub(d, z, w, MPC_RNDNN);
    o->diff[i] = mpc_get_dc(d, MPC_RNDNN);
    mpc_sqr(z, z, MPC_RNDNN);
    mpc_add(z, z, c, MPC_RNDNN);
    mpc_sqr(w, w, MPC_RNDNN);
    mpc_add(w, w, c, MPC_RNDNN);
  }
  mpc_clear(z);
  mpc_clear(w);
  mpc_clear(d);
  mpc_init2(o->c, mpfr_get_prec(mpc_realref(c)));
  mpc_set(o->c, c, MPC_RNDNN);
  return o;
}

extern void m_r_orbit_diff_d_delete(m_r_orbit_diff_d *o)
{
  if (o)
  {
    if (o->orbit)
    {
      free(o->orbit);
    }
    if (o->diff)
    {
      free(o->diff);
    }
    mpc_clear(o->c);
    free(o);
  }
}

extern int m_r_orbit_diff_d_get_preperiod(const m_r_orbit_diff_d *o)
{
  return o->preperiod;
}

extern int m_r_orbit_diff_d_get_period(const m_r_orbit_diff_d *o)
{
  return o->period;
}

extern int m_r_orbit_diff_d_get_offset(const m_r_orbit_diff_d *o)
{
  return o->offset;
}

extern void m_r_orbit_diff_d_get_c(const m_r_orbit_diff_d *o, mpc_t c)
{
  mpfr_set_prec(mpc_realref(c), mpfr_get_prec(mpc_realref(o->c)));
  mpfr_set_prec(mpc_imagref(c), mpfr_get_prec(mpc_imagref(o->c)));
  mpc_set(c, o->c, MPC_RNDNN);
}

extern double _Complex m_r_orbit_diff_d_get_z(const m_r_orbit_diff_d *o, int n)
{
  if (n >= o->preperiod)
  {
    n -= o->preperiod;
    n %= o->period;
    n += o->preperiod;
  }
  return o->orbit[n];
}

extern double _Complex m_r_orbit_diff_d_get_d(const m_r_orbit_diff_d *o, int n)
{
  if (n >= o->preperiod)
  {
    n -= o->preperiod;
    n %= o->period;
    n += o->preperiod;
  }
  return o->diff[n];
}

extern const double _Complex *m_r_orbit_diff_d_get_z_ptr(const m_r_orbit_diff_d *o)
{
  return o->orbit;
}

extern const double _Complex *m_r_orbit_diff_d_get_d_ptr(const m_r_orbit_diff_d *o)
{
  return o->diff;
}
