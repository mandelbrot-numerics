// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <mandelbrot-numerics.h>

extern int m_d_dwell_perturbed(const m_r_orbit_d *orbit, double _Complex c, double er, int maxiters) // FIXME TODO implement without _Complex
{
  const double _Complex *z_ptr = m_r_orbit_d_get_z_ptr(orbit);
  const int preperiod = m_r_orbit_d_get_preperiod(orbit);
  const int period = m_r_orbit_d_get_period(orbit);
  double _Complex z = 0;
  int m = 0;
  for (int i = 0; i < maxiters; ++i)
  {
    double _Complex Z = z_ptr[m];
    if (cabs(Z + z) < cabs(z))
    {
      z = Z + z;
      m = 0;
      Z = z_ptr[m];
    }
    if (cabs(Z + z) > er)
    {
      return i;
    }
    z = (2 * Z + z) * z + c;
    if (++m == preperiod + period)
    {
      m = preperiod;
    }
  }
  return maxiters;
}
