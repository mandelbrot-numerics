// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef MANDELBROT_NUMERICS_H
#define MANDELBROT_NUMERICS_H 1

#include <assert.h>
#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <gmp.h>
#include <mpfr.h>
#include <mpc.h>

#ifdef __cplusplus
extern "C"
{
#endif

/* ------------------------------------------------------------------ */
/* double precision: *_d_*() */

/* orbit for a high precision (pre)periodic point stored in double precision */

struct m_r_orbit_d;
typedef struct m_r_orbit_d m_r_orbit_d;
extern m_r_orbit_d *m_r_orbit_d_new(const mpc_t c, int preperiod, int period);
extern void m_r_orbit_d_delete(m_r_orbit_d *o);
extern int m_r_orbit_d_get_preperiod(const m_r_orbit_d *o);
extern int m_r_orbit_d_get_period(const m_r_orbit_d *o);
extern void m_r_orbit_d_get_c(const m_r_orbit_d *o, mpc_t c);
extern double _Complex m_r_orbit_d_get_z(const m_r_orbit_d *o, int n);
extern const double _Complex *m_r_orbit_d_get_z_ptr(const m_r_orbit_d *o);

/* differences at offset of orbit for a high precision (pre)periodic point stored in double precision */

struct m_r_orbit_diff_d;
typedef struct m_r_orbit_diff_d m_r_orbit_diff_d;
extern m_r_orbit_diff_d *m_r_orbit_diff_d_new(const mpc_t c, int preperiod, int period, int offset);
extern void m_r_orbit_diff_d_delete(m_r_orbit_diff_d *o);
extern int m_r_orbit_diff_d_get_preperiod(const m_r_orbit_diff_d *o);
extern int m_r_orbit_diff_d_get_period(const m_r_orbit_diff_d *o);
extern int m_r_orbit_diff_d_get_offset(const m_r_orbit_diff_d *o);
extern void m_r_orbit_diff_d_get_c(const m_r_orbit_diff_d *o, mpc_t c);
extern double _Complex m_r_orbit_diff_d_get_z(const m_r_orbit_diff_d *o, int n);
extern double _Complex m_r_orbit_diff_d_get_d(const m_r_orbit_diff_d *o, int n);
extern const double _Complex *m_r_orbit_diff_d_get_z_ptr(const m_r_orbit_diff_d *o);
extern const double _Complex *m_r_orbit_diff_d_get_d_ptr(const m_r_orbit_diff_d *o);

/* differences at offset of orbit for a high precision (pre)periodic point stored in long double precision */

struct m_r_orbit_diff_l;
typedef struct m_r_orbit_diff_l m_r_orbit_diff_l;
extern m_r_orbit_diff_l *m_r_orbit_diff_l_new(const mpc_t c, int preperiod, int period, int offset);
extern void m_r_orbit_diff_l_delete(m_r_orbit_diff_l *o);
extern int m_r_orbit_diff_l_get_preperiod(const m_r_orbit_diff_l *o);
extern int m_r_orbit_diff_l_get_period(const m_r_orbit_diff_l *o);
extern int m_r_orbit_diff_l_get_offset(const m_r_orbit_diff_l *o);
extern void m_r_orbit_diff_l_get_c(const m_r_orbit_diff_l *o, mpc_t c);
extern long double _Complex m_r_orbit_diff_l_get_z(const m_r_orbit_diff_l *o, int n);
extern long double _Complex m_r_orbit_diff_l_get_d(const m_r_orbit_diff_l *o, int n);
extern const long double _Complex *m_r_orbit_diff_l_get_z_ptr(const m_r_orbit_diff_l *o);
extern const long double _Complex *m_r_orbit_diff_l_get_d_ptr(const m_r_orbit_diff_l *o);

/* functions returning bool return true for success, false for failure */

struct m_d_mat2 {
  double _Complex a, b, c, d;
};
typedef struct m_d_mat2 m_d_mat2;

extern void m_d_mat2_set(m_d_mat2 *o, const m_d_mat2 *m);
extern void m_d_mat2_id(m_d_mat2 *o);
extern double _Complex m_d_mat2_tr(const m_d_mat2 *m);
extern double _Complex m_d_mat2_det(const m_d_mat2 *m);
extern void m_d_mat2_inv(m_d_mat2 *m1, const m_d_mat2 *m);
extern void m_d_mat2_mul(m_d_mat2 *o, const m_d_mat2 *l, const m_d_mat2 *r);
extern void m_d_mat2_diagonalize(m_d_mat2 *p, m_d_mat2 *d, m_d_mat2 *p1, const m_d_mat2 *m);
extern void m_d_mat2_moebius3(m_d_mat2 *m, double _Complex zero, double _Complex one, double _Complex infinity);

struct m_d_mat2_interp;
typedef struct m_d_mat2_interp m_d_mat2_interp;

extern m_d_mat2_interp *m_d_mat2_interp_new(void);
extern void m_d_mat2_interp_delete(m_d_mat2_interp *i);
extern void m_d_mat2_interp_init(m_d_mat2_interp *i, const m_d_mat2 *f, const m_d_mat2 *g);
extern void m_d_mat2_interp_do(m_d_mat2 *m, const m_d_mat2_interp *i, double t);

enum m_shape { m_cardioid, m_circle };
typedef enum m_shape m_shape;

enum m_newton { m_failed, m_stepped, m_converged };
typedef enum m_newton m_newton;

struct m_period_filter_t;
typedef struct m_period_filter_t m_period_filter_t;
typedef bool (m_period_filter_f)(m_period_filter_t *filter, int period);
struct m_period_filter_t
{
  m_period_filter_f *accept;
  m_period_filter_f *reject;
};

/* functions taking non-const mpq_t use them for output */
/* functions taking pointers to double _Complex use them for output */

extern m_newton m_d_nucleus_naive_step(double _Complex *c_out, double _Complex c_guess, int period);
extern m_newton m_d_nucleus_naive(double _Complex *c_out, double _Complex c_guess, int period, int maxsteps);

extern m_newton m_d_nucleus_step(double _Complex *c_out, double _Complex c_guess, int period);
extern m_newton m_d_nucleus(double _Complex *c_out, double _Complex c_guess, int period, int maxsteps);

extern m_newton m_d_misiurewicz_naive_step(double _Complex *c_out, double _Complex c_guess, int preperiod, int period);
extern m_newton m_d_misiurewicz_naive(double _Complex *c_out, double _Complex c_guess, int preperiod, int period, int maxsteps);

extern m_newton m_d_misiurewicz_step(double _Complex *c_out, double _Complex c_guess, int preperiod, int period);
extern m_newton m_d_misiurewicz(double _Complex *c_out, double _Complex c_guess, int preperiod, int period, int maxsteps);

extern m_newton m_d_attractor_step(double _Complex *z, double _Complex z_guess, double _Complex c, int period);
extern m_newton m_d_attractor(double _Complex *z, double _Complex z_guess, double _Complex c, int period, int maxsteps);

extern m_newton m_d_interior_step(double _Complex *z, double _Complex *c, double _Complex z_guess, double _Complex c_guess, double _Complex interior, int period);
extern m_newton m_d_interior(double _Complex *z, double _Complex *c, double _Complex z_guess, double _Complex c_guess, double _Complex interior, int period, int maxsteps);

extern m_newton m_d_domain_coord_step(double _Complex *c_out, double _Complex c_guess, double _Complex domain_coord, int loperiod, int hiperiod);
extern m_newton m_d_domain_coord(double _Complex *c_out, double _Complex c_guess, double _Complex domain_coord, int loperiod, int hiperiod, int maxsteps);

extern m_newton m_d_misiurewicz_coord_step(double _Complex *c_out, double _Complex c_guess, double _Complex misiurewicz_coord, int period, int lopreperiod, int hipreperiod);
extern m_newton m_d_misiurewicz_coord(double _Complex *c_out, double _Complex c_guess, double _Complex misiurewicz_coord, int period, int lopreperiod, int hipreperiod, int maxsteps);

extern int m_d_parent(mpq_t angle, double _Complex *root_out, double _Complex *parent_out, double _Complex nucleus, int period, int maxsteps);

extern bool m_d_interior_de(double *de_out, double _Complex *dz_out, double _Complex z, double _Complex c, int p, int steps);

extern double _Complex m_d_size(double _Complex nucleus, int period);
extern double _Complex m_d_size_orbit(const m_r_orbit_d *orbit);
extern double m_d_domain_size(double _Complex nucleus, int period);
extern double m_d_domain_size_orbit(const m_r_orbit_d *orbit);
extern double m_d_filtered_domain_size(double _Complex nucleus, int period, m_period_filter_t *filter);
extern double m_d_misiurewicz_size(double _Complex c, int preperiod, int period);
extern double m_d_misiurewicz_size_orbit(const m_r_orbit_d *orbit);
extern m_shape m_d_shape(double _Complex nucleus, int period);
extern double _Complex m_d_shape_estimate(double _Complex nucleus, int period);
extern double _Complex m_d_shape_estimate_orbit(const m_r_orbit_d *orbit);
extern m_shape m_d_shape_discriminant(double _Complex shape);

struct m_d_spider_path;
typedef struct m_d_spider_path m_d_spider_path;
extern m_d_spider_path *m_d_spider_path_new(const mpq_t angle0, int preperiod, int period, int sharpness);
extern void m_d_spider_path_delete(m_d_spider_path *path);
extern void m_d_spider_path_step(m_d_spider_path *path);
extern double _Complex m_d_spider_path_get(const m_d_spider_path *path, int i);
extern double _Complex m_d_spider_path_do(const mpq_t angle, int preperiod, int period, int sharpness, int maxstep);

struct m_d_exray_in;
typedef struct m_d_exray_in m_d_exray_in;
extern m_d_exray_in *m_d_exray_in_new(const mpq_t angle, int sharpness);
extern void m_d_exray_in_delete(m_d_exray_in *ray);
extern m_newton m_d_exray_in_step(m_d_exray_in *ray, int maxsteps);
extern double _Complex m_d_exray_in_get(const m_d_exray_in *ray);
extern double _Complex m_d_exray_in_do(const mpq_t angle, int sharpness, int maxsteps, int maxnewtonsteps);

struct m_d_exray_out;
typedef struct m_d_exray_out m_d_exray_out;
extern m_d_exray_out *m_d_exray_out_new(double _Complex c, int sharpness, int maxdwell);
extern void m_d_exray_out_delete(m_d_exray_out *ray);
extern m_newton m_d_exray_out_step(m_d_exray_out *ray);
extern bool m_d_exray_out_have_bit(const m_d_exray_out *ray);
extern bool m_d_exray_out_get_bit(const m_d_exray_out *ray);
extern double _Complex m_d_exray_out_get(const m_d_exray_out *ray);
extern char *m_d_exray_out_do(double _Complex c, int sharpness, int maxdwell);

struct m_d_equipotential;
typedef struct m_d_equipotential m_d_equipotential;
extern m_d_equipotential *m_d_equipotential_new(const double _Complex c, int sharpness, int count);
extern void m_d_equipotential_delete(m_d_equipotential *ray);
extern m_newton m_d_equipotential_step(m_d_equipotential *ray, int maxsteps);
extern double _Complex m_d_equipotential_get(const m_d_equipotential *ray);

struct m_d_box_period;
typedef struct m_d_box_period m_d_box_period;
extern m_d_box_period *m_d_box_period_new(double _Complex center, double radius);
extern void m_d_box_period_delete(m_d_box_period *box);
extern bool m_d_box_period_step(m_d_box_period *box);
extern bool m_d_box_period_have_period(const m_d_box_period *box);
extern int m_d_box_period_get_period(const m_d_box_period *box);
extern int m_d_box_period_do(double _Complex center, double radius, int maxperiod);

struct m_d_box_misiurewicz;
typedef struct m_d_box_misiurewicz m_d_box_misiurewicz;
extern m_d_box_misiurewicz *m_d_box_misiurewicz_new(double _Complex center, double radius);
extern void m_d_box_misiurewicz_delete(m_d_box_misiurewicz *box);
extern bool m_d_box_misiurewicz_step(m_d_box_misiurewicz *box);
extern bool m_d_box_misiurewicz_check_periods(m_d_box_misiurewicz *box, int maxperiod);
extern int m_d_box_misiurewicz_get_preperiod(const m_d_box_misiurewicz *box);
extern int m_d_box_misiurewicz_get_period(const m_d_box_misiurewicz *box);
extern bool m_d_box_misiurewicz_do(int *preperiod, int *period, double _Complex center, double radius, int maxpreperiod, int maxperiod);

struct m_d_ball_period;
typedef struct m_d_ball_period m_d_ball_period;
extern m_d_ball_period *m_d_ball_period_new(double _Complex center, double radius);
extern void m_d_ball_period_delete(m_d_ball_period *ball);
extern bool m_d_ball_period_step(m_d_ball_period *ball);
extern bool m_d_ball_period_have_period(const m_d_ball_period *ball);
extern bool m_d_ball_period_will_converge(const m_d_ball_period *ball);
extern int m_d_ball_period_get_period(const m_d_ball_period *ball);
extern int m_d_ball_period_do(double _Complex center, double radius, int maxperiod);

/* find (pre)period of nearby Misiurewicz point via ball method */

struct m_d_ball_misiurewicz;
typedef struct m_d_ball_misiurewicz m_d_ball_misiurewicz;
extern m_d_ball_misiurewicz *m_d_ball_misiurewicz_new(double _Complex center, double radius);
extern void m_d_ball_misiurewicz_delete(m_d_ball_misiurewicz *ball);
extern bool m_d_ball_misiurewicz_step(m_d_ball_misiurewicz *ball);
extern bool m_d_ball_misiurewicz_check_periods(m_d_ball_misiurewicz *ball, int maxperiod);
extern int m_d_ball_misiurewicz_get_period(const m_d_ball_misiurewicz *ball);
extern int m_d_ball_misiurewicz_get_preperiod(const m_d_ball_misiurewicz *ball);
extern bool m_d_ball_misiurewicz_do(int *preperiod, int *period, double _Complex center, double radius, int maxpreperiod, int maxperiod);

/* find (pre)period of nearby Misiurewicz point via ball method using orbit (period is orbit's offset) */

struct m_d_ball_misiurewicz_orbit;
typedef struct m_d_ball_misiurewicz_orbit m_d_ball_misiurewicz_orbit;
extern m_d_ball_misiurewicz_orbit *m_d_ball_misiurewicz_orbit_new(const m_r_orbit_diff_d *o, double radius);
extern void m_d_ball_misiurewicz_orbit_delete(m_d_ball_misiurewicz_orbit *ball);
extern bool m_d_ball_misiurewicz_orbit_step(m_d_ball_misiurewicz_orbit *ball);
extern bool m_d_ball_misiurewicz_orbit_check_periods(m_d_ball_misiurewicz_orbit *ball);
extern int m_d_ball_misiurewicz_orbit_get_period(const m_d_ball_misiurewicz_orbit *ball);
extern int m_d_ball_misiurewicz_orbit_get_preperiod(const m_d_ball_misiurewicz_orbit *ball);
extern bool m_d_ball_misiurewicz_orbit_do(int *preperiod, int *period, const m_r_orbit_diff_d *o, double radius, int maxpreperiod);

/* find preperiods of nearby Misiurewicz points using ball method; period is offset of orbit */

struct m_d_nearby_misiurewicz_orbit;
typedef struct m_d_nearby_misiurewicz_orbit m_d_nearby_misiurewicz_orbit;
extern m_d_nearby_misiurewicz_orbit *m_d_nearby_misiurewicz_orbit_new(const m_r_orbit_diff_d *orbit);
extern void m_d_nearby_misiurewicz_orbit_delete(m_d_nearby_misiurewicz_orbit *ball);
extern bool m_d_nearby_misiurewicz_orbit_step(m_d_nearby_misiurewicz_orbit *ball);
extern bool m_d_nearby_misiurewicz_orbit_check(m_d_nearby_misiurewicz_orbit *ball);
extern double m_d_nearby_misiurewicz_orbit_get_distance(const m_d_nearby_misiurewicz_orbit *ball);
extern double m_d_nearby_misiurewicz_orbit_get_size(const m_d_nearby_misiurewicz_orbit *ball);
extern int m_d_nearby_misiurewicz_orbit_get_period(const m_d_nearby_misiurewicz_orbit *ball);
extern int m_d_nearby_misiurewicz_orbit_get_preperiod(const m_d_nearby_misiurewicz_orbit *ball);

/* find preperiods of nearby Misiurewicz points using ball method; period is offset of orbit */

struct m_l_nearby_misiurewicz_orbit;
typedef struct m_l_nearby_misiurewicz_orbit m_l_nearby_misiurewicz_orbit;
extern m_l_nearby_misiurewicz_orbit *m_l_nearby_misiurewicz_orbit_new(const m_r_orbit_diff_l *orbit);
extern void m_l_nearby_misiurewicz_orbit_delete(m_l_nearby_misiurewicz_orbit *ball);
extern bool m_l_nearby_misiurewicz_orbit_step(m_l_nearby_misiurewicz_orbit *ball);
extern bool m_l_nearby_misiurewicz_orbit_check(m_l_nearby_misiurewicz_orbit *ball);
extern long double m_l_nearby_misiurewicz_orbit_get_distance(const m_l_nearby_misiurewicz_orbit *ball);
extern long double m_l_nearby_misiurewicz_orbit_get_size(const m_l_nearby_misiurewicz_orbit *ball);
extern int m_l_nearby_misiurewicz_orbit_get_period(const m_l_nearby_misiurewicz_orbit *ball);
extern int m_l_nearby_misiurewicz_orbit_get_preperiod(const m_l_nearby_misiurewicz_orbit *ball);

struct m_d_near_period;
typedef struct m_d_near_period m_d_near_period;
extern m_d_near_period *m_d_near_period_new(double _Complex center, double radius);
extern void m_d_near_period_delete(m_d_near_period *near);
extern bool m_d_near_period_step(m_d_near_period *near);
extern bool m_d_near_period_have_period(const m_d_near_period *near);
extern int m_d_near_period_get_period(const m_d_near_period *near);
extern int m_d_near_period_do(double _Complex center, double radius, int maxperiod);

extern void m_d_external_angles(char **lo, char **hi, double _Complex nucleus, int period, double radius, int resolution);
extern void m_d_external_angles_perturbed(int nrays, char **rays, const m_r_orbit_d *orbit, int preperiod, int period, double radius, int sharpness);

extern double m_d_from_logistic(double r);
extern double m_d_to_logistic(double c);

// use homotopy to find nucleus of period+1 near nucleus of period
struct m_d_nucleus_homotopy;
typedef struct m_d_nucleus_homotopy m_d_nucleus_homotopy;
extern m_d_nucleus_homotopy *m_d_nucleus_homotopy_new(double _Complex c, int period, int which);
extern void m_d_nucleus_homotopy_delete(m_d_nucleus_homotopy *path);
extern m_newton m_d_nucleus_homotopy_step(m_d_nucleus_homotopy *path);
extern double _Complex m_d_nucleus_homotopy_get(const m_d_nucleus_homotopy *path);
extern double _Complex m_d_nucleus_homotopy_do(double _Complex c, int period, int sign);

extern int m_d_dwell_perturbed(const m_r_orbit_d *orbit, double _Complex c, double er, int maxiters);

// trace dynamic ray in complement of filled-in Julia set
struct j_d_exray_in;
typedef struct j_d_exray_in j_d_exray_in;
extern j_d_exray_in *j_d_exray_in_new(double _Complex c, const mpq_t angle, int sharpness);
extern void j_d_exray_in_delete(j_d_exray_in *ray);
extern m_newton j_d_exray_in_step(j_d_exray_in *ray, int maxsteps);
extern double _Complex j_d_exray_in_get(const j_d_exray_in *ray);
extern double _Complex j_d_exray_in_do(double _Complex c, const mpq_t angle, int sharpness, int maxsteps, int maxnewtonsteps);

/* bilinear approximation acceleration */

struct m_d_bla;
typedef struct m_d_bla m_d_bla;
struct m_d_blas;
typedef struct m_d_blas m_d_blas;
extern m_d_blas *m_d_bla_new(const double _Complex *Z, int length);
extern void m_d_bla_delete(m_d_blas *state);
extern const m_d_bla *m_d_bla_lookup(const m_d_blas *B, int m, double rz, double rc);
extern int m_d_bla_apply(const m_d_bla *b, double _Complex c, double _Complex *z, double _Complex *dc);

/* ------------------------------------------------------------------ */
/* long double precision: *_ld_*() */
/* orbit for a high precision (pre)periodic point stored in long double precision */

struct m_r_orbit_ld;
typedef struct m_r_orbit_ld m_r_orbit_ld;
extern m_r_orbit_ld *m_r_orbit_ld_new(const mpc_t c, int preperiod, int period);
extern void m_r_orbit_ld_delete(m_r_orbit_ld *o);
extern int m_r_orbit_ld_get_preperiod(const m_r_orbit_ld *o);
extern int m_r_orbit_ld_get_period(const m_r_orbit_ld *o);
extern void m_r_orbit_ld_get_c(const m_r_orbit_ld *o, mpc_t c);
extern long double _Complex m_r_orbit_ld_get_z(const m_r_orbit_ld *o, int n);
extern const long double _Complex *m_r_orbit_ld_get_z_ptr(const m_r_orbit_ld *o);

struct m_ld_bla;
typedef struct m_ld_bla m_ld_bla;
m_ld_bla *m_ld_bla_new(const m_r_orbit_ld *orbit, int accuracy, long double _Complex rc);
void m_ld_bla_delete(m_ld_bla *bla);
void m_ld_bla_update(m_ld_bla *bla, int accuracy, long double rc);
bool m_ld_bla_iterate_z_dc(const m_ld_bla *bla, long double er, int N, long double _Complex c, long double _Complex *z, long double _Complex *dcp, int *np, int *mp);
long double m_ld_bla_get_rc(const m_ld_bla *bla);

extern int m_ld_dwell_perturbed(const m_r_orbit_ld *orbit, long double _Complex c, long double er, int maxiters);

extern long double m_ld_domain_size(long double _Complex nucleus, int period);
extern long double m_ld_domain_size_orbit(const m_r_orbit_ld *orbit);
extern long double m_ld_filtered_domain_size(long double _Complex nucleus, int period, m_period_filter_t *filter);

extern long double m_ld_misiurewicz_size(long double _Complex c, int preperiod, int period);
extern long double m_ld_misiurewicz_size_orbit(const m_r_orbit_ld *orbit);

struct m_ld_exray_out_perturbed;
typedef struct m_ld_exray_out_perturbed m_ld_exray_out_perturbed;
extern m_ld_exray_out_perturbed *m_ld_exray_out_perturbed_new(const m_r_orbit_ld *orbit, long double _Complex endpoint, int sharpness, int maxdwell, long double er);
extern void m_ld_exray_out_perturbed_delete(m_ld_exray_out_perturbed *ray);
extern m_newton m_ld_exray_out_perturbed_step(m_ld_exray_out_perturbed *ray, int maxsteps);
extern bool m_ld_exray_out_perturbed_have_bit(const m_ld_exray_out_perturbed *ray);
extern bool m_ld_exray_out_perturbed_get_bit(const m_ld_exray_out_perturbed *ray);
extern long double _Complex m_ld_exray_out_perturbed_get(const m_ld_exray_out_perturbed *ray);
extern char *m_ld_exray_out_perturbed_do(const m_r_orbit_ld *orbit, long double _Complex endpoint, int sharpness, int maxdwell, long double er, int maxsteps);

extern void m_ld_external_angles_perturbed(int nrays, char **rays, const m_r_orbit_ld *orbit, int preperiod, int period, long double radius, int sharpness);

/* ------------------------------------------------------------------ */
/* _Float128 precision: *_qd_*() */
/* orbit for a high precision (pre)periodic point stored in _Float128 precision */

struct m_r_orbit_qd;
typedef struct m_r_orbit_qd m_r_orbit_qd;
extern m_r_orbit_qd *m_r_orbit_qd_new(const mpc_t c, int preperiod, int period);
extern void m_r_orbit_qd_delete(m_r_orbit_qd *o);
extern int m_r_orbit_qd_get_preperiod(const m_r_orbit_qd *o);
extern int m_r_orbit_qd_get_period(const m_r_orbit_qd *o);
extern void m_r_orbit_qd_get_c(const m_r_orbit_qd *o, mpc_t c);
extern _Float128 _Complex m_r_orbit_qd_get_z(const m_r_orbit_qd *o, int n);
extern const _Float128 _Complex *m_r_orbit_qd_get_z_ptr(const m_r_orbit_qd *o);

/* bilinear approximation acceleration */

struct m_qd_bla;
typedef struct m_qd_bla m_qd_bla;
struct m_qd_blas;
typedef struct m_qd_blas m_qd_blas;
m_qd_bla *m_qd_bla_new(const m_r_orbit_qd *orbit, int accuracy, _Float128 _Complex rc);
void m_qd_bla_delete(m_qd_bla *bla);
void m_qd_bla_update(m_qd_bla *bla, int accuracy, _Float128 rc);
bool m_qd_bla_iterate_z_dc(const m_qd_bla *bla, _Float128 er, int N, _Float128 _Complex c, _Float128 _Complex *z, _Float128 _Complex *dcp, int *np, int *mp);
_Float128 m_qd_bla_get_rc(const m_qd_bla *bla);

extern int m_qd_dwell_perturbed(const m_r_orbit_qd *orbit, _Float128 _Complex c, _Float128 er, int maxiters);

extern _Float128 m_qd_domain_size(_Float128 _Complex nucleus, int period);
extern _Float128 m_qd_domain_size_orbit(const m_r_orbit_qd *orbit);
extern _Float128 m_qd_filtered_domain_size(_Float128 _Complex nucleus, int period, m_period_filter_t *filter);

extern _Float128 m_qd_misiurewicz_size(_Float128 _Complex c, int preperiod, int period);
extern _Float128 m_qd_misiurewicz_size_orbit(const m_r_orbit_qd *orbit);

struct m_qd_exray_out_perturbed;
typedef struct m_qd_exray_out_perturbed m_qd_exray_out_perturbed;
extern m_qd_exray_out_perturbed *m_qd_exray_out_perturbed_new(const m_r_orbit_qd *orbit, _Float128 _Complex endpoint, int sharpness, int maxdwell, _Float128 er);
extern void m_qd_exray_out_perturbed_delete(m_qd_exray_out_perturbed *ray);
extern m_newton m_qd_exray_out_perturbed_step(m_qd_exray_out_perturbed *ray, int maxsteps);
extern bool m_qd_exray_out_perturbed_have_bit(const m_qd_exray_out_perturbed *ray);
extern bool m_qd_exray_out_perturbed_get_bit(const m_qd_exray_out_perturbed *ray);
extern _Float128 _Complex m_qd_exray_out_perturbed_get(const m_qd_exray_out_perturbed *ray);
extern char *m_qd_exray_out_perturbed_do(const m_r_orbit_qd *orbit, _Float128 _Complex endpoint, int sharpness, int maxdwell, _Float128 er, int maxsteps);

extern void m_qd_external_angles_perturbed(int nrays, char **rays, const m_r_orbit_qd *orbit, int preperiod, int period, _Float128 radius, int sharpness);

/* ------------------------------------------------------------------ */
/* arbitrary precision: *_r_*() */
/* functions taking non-const mpq_t/mpfr_t/mpc_t use them for output, _raw also uses for temporaries */

extern m_newton m_r_nucleus_naive_step_raw(mpc_t c_out, const mpc_t c_guess, int period, mpc_t z, mpc_t dc, mpc_t c_new, mpc_t d, mpfr_t d2, mpfr_t epsilon2);
extern m_newton m_r_nucleus_naive_step(mpc_t c_out, const mpc_t c_guess, int period);
extern m_newton m_r_nucleus_naive(mpc_t c_out, const mpc_t c_guess, int period, int maxsteps, int ncpus);

extern m_newton m_r_nucleus_step_raw(mpc_t c_out, const mpc_t c_guess, int period, mpfr_t epsilon2, mpc_t z, mpc_t dc, mpc_t c_new, mpc_t h, mpc_t dh, mpc_t f, mpc_t df);
//extern m_newton m_r_nucleus_step(mpc_t c_out, const mpc_t c_guess, int period);
extern m_newton m_r_nucleus(mpc_t c_out, const mpc_t c_guess, int period, int maxsteps, int ncpus);

extern m_newton m_r_misiurewicz_naive_step_raw(mpc_t c_out, const mpc_t c_guess, int preperiod, int period, mpc_t z, mpc_t dc, mpc_t zp, mpc_t dcp, mpc_t c_new, mpc_t d, mpfr_t d2, mpfr_t epsilon2);
extern m_newton m_r_misiurewicz_naive_step(mpc_t c_out, const mpc_t c_guess, int preperiod, int period);
extern m_newton m_r_misiurewicz_naive(mpc_t c_out, const mpc_t c_guess, int preperiod, int period, int maxsteps);
extern m_newton m_r_misiurewicz_step_raw(mpc_t c_out, const mpc_t c_guess, int preperiod, int period, mpc_t z, mpc_t dc, mpc_t zp, mpc_t dcp, mpc_t f, mpc_t df, mpc_t g, mpc_t dg, mpc_t h, mpc_t dh, mpc_t k, mpc_t l, mpfr_t epsilon2);
// extern m_newton m_r_misiurewicz_step(mpc_t c_out, const mpc_t c_guess, int preperiod, int period, int maxsteps);
extern m_newton m_r_misiurewicz(mpc_t c_out, const mpc_t c_guess, int preperiod, int period, int maxsteps);

extern m_newton m_r_attractor_step_raw(mpc_t z_out, const mpc_t z_guess, const mpc_t c, int period, mpc_t z, mpc_t dz, mpc_t z_new, mpc_t d, mpfr_t d2, mpfr_t epsilon2);
extern m_newton m_r_attractor_step(mpc_t z_out, const mpc_t z_guess, const mpc_t c, int period);
extern m_newton m_r_attractor(mpc_t z_out, const mpc_t z_guess, const mpc_t c, int period, int maxsteps);

extern m_newton m_r_interior_step_raw(mpc_t z_out, mpc_t c_out, const mpc_t z_guess, const mpc_t c_guess, const mpc_t interior, int period, mpc_t z_new, mpc_t c_new, mpc_t c, mpc_t z, mpc_t dz, mpc_t dc, mpc_t dzdz, mpc_t dcdz, mpc_t det, mpc_t d, mpc_t dz1, mpfr_t d2z, mpfr_t d2c, mpfr_t epsilon2);
extern m_newton m_r_interior_step(mpc_t z_out, mpc_t c_out, const mpc_t z_guess, const mpc_t c_guess, const mpc_t interior, int period);
extern m_newton m_r_interior(mpc_t z_out, mpc_t c_out, const mpc_t z_guess, const mpc_t c_guess, const mpc_t interior, int period, int maxsteps);

extern m_newton m_r_domain_coord_step_raw(mpc_t c_out, const mpc_t c_guess, const mpc_t domain_coord, int loperiod, int hiperiod, mpc_t z, mpc_t dc, mpc_t zp, mpc_t dcp, mpc_t f, mpc_t df, mpc_t c_new, mpc_t d, mpfr_t d2, mpfr_t epsilon2);
extern m_newton m_r_domain_coord_step(mpc_t c_out, const mpc_t c_guess, const mpc_t domain_coord, int loperiod, int hiperiod);
extern m_newton m_r_domain_coord(mpc_t c_out, const mpc_t c_guess, const mpc_t domain_coord, int loperiod, int hiperiod, int maxsteps);
extern m_newton m_r_domain_coord_dynamic_step_raw(mpc_t c_out, const mpc_t c_guess, const mpc_t domain_coord, int hiperiod, mpc_t z, mpc_t dc, mpc_t zp, mpc_t dcp, mpc_t f, mpc_t df, mpc_t c_new, mpc_t d, mpfr_t d2, mpfr_t epsilon2);
extern m_newton m_r_domain_coord_dynamic_step(mpc_t c_out, const mpc_t c_guess, const mpc_t domain_coord, int hiperiod);
extern m_newton m_r_domain_coord_dynamic(mpc_t c_out, const mpc_t c_guess, const mpc_t domain_coord, int hiperiod, int maxsteps);

extern m_newton m_r_misiurewicz_coord_step_raw(mpc_t c_out, const mpc_t c_guess, const mpc_t misiurewicz_coord, int period, int lopreperiod, int hipreperiod, mpc_t z, mpc_t dc, mpc_t num, mpc_t dnum, mpc_t den, mpc_t dden, mpc_t f, mpc_t df, mpc_t c_new, mpc_t d, mpfr_t d2, mpfr_t epsilon2);
extern m_newton m_r_misiurewicz_coord_step(mpc_t c_out, const mpc_t c_guess, const mpc_t misiurewicz_coord, int period, int lopreperiod, int hipreperiod);
extern m_newton m_r_misiurewicz_coord(mpc_t c_out, const mpc_t c_guess, const mpc_t misiurewicz_coord, int period, int lopreperiod, int hipreperiod, int maxsteps);

extern int m_r_parent(mpq_t angle_out, mpc_t root_out, mpc_t parent_out, const mpc_t nucleus, int period, int maxsteps, int ncpus);

extern void m_r_size(mpc_t size, const mpc_t nucleus, int period);
extern int m_r_domain_size(mpfr_t size, const mpc_t nucleus, int period);
extern void m_r_misiurewicz_size(mpfr_t size, const mpc_t c, int preperiod, int period);
extern m_shape m_r_shape(const mpc_t nucleus, int period);
extern void m_r_shape_estimate(mpc_t shape, const mpc_t nucleus, int period);
extern m_shape m_r_shape_discriminant(const mpc_t shape);

struct m_r_spider_path;
typedef struct m_r_spider_path m_r_spider_path;
extern m_r_spider_path *m_r_spider_path_new(const mpq_t angle0, int preperiod, int period, int sharpness, mpfr_prec_t precision);
extern void m_r_spider_path_delete(m_r_spider_path *path);
extern void m_r_spider_path_step(m_r_spider_path *path, bool parallel);
extern void m_r_spider_path_get(const m_r_spider_path *path, int i, mpc_t c);
extern void m_r_spider_path_do(const mpq_t angle, int preperiod, int period, int sharpness, int precision, int maxsteps, bool parallel, mpc_t c);

struct m_r_exray_in;
typedef struct m_r_exray_in m_r_exray_in;
extern m_r_exray_in *m_r_exray_in_new(const mpq_t angle, int sharpness);
extern void m_r_exray_in_delete(m_r_exray_in *ray);
extern m_newton m_r_exray_in_step(m_r_exray_in *ray, int maxsteps);
extern void m_r_exray_in_get(const m_r_exray_in *ray, mpc_t c);

struct m_d_exray_in_perturbed;
typedef struct m_d_exray_in_perturbed m_d_exray_in_perturbed;
extern m_d_exray_in_perturbed *m_d_exray_in_perturbed_new(const m_r_orbit_d *orbit, const mpq_t angle, int sharpness);
extern void m_d_exray_in_perturbed_delete(m_d_exray_in_perturbed *ray);
extern m_newton m_d_exray_in_perturbed_step(m_d_exray_in_perturbed *ray, int maxsteps);
extern double _Complex m_d_exray_in_perturbed_get(const m_d_exray_in_perturbed *ray);
extern void m_d_exray_in_perturbed_rebase(m_d_exray_in_perturbed *ray, double _Complex delta_c, const m_r_orbit_d *orbit);

struct m_ld_exray_in_perturbed;
typedef struct m_ld_exray_in_perturbed m_ld_exray_in_perturbed;
extern m_ld_exray_in_perturbed *m_ld_exray_in_perturbed_new(const m_r_orbit_ld *orbit, const mpq_t angle, int sharpness);
extern void m_ld_exray_in_perturbed_delete(m_ld_exray_in_perturbed *ray);
extern m_newton m_ld_exray_in_perturbed_step(m_ld_exray_in_perturbed *ray, int maxsteps);
extern long double _Complex m_ld_exray_in_perturbed_get(const m_ld_exray_in_perturbed *ray);
extern m_ld_exray_in_perturbed * m_ld_exray_in_perturbed_rebase(m_ld_exray_in_perturbed *ray, long double _Complex delta_c, const m_r_orbit_ld *orbit);

struct m_qd_exray_in_perturbed;
typedef struct m_qd_exray_in_perturbed m_qd_exray_in_perturbed;
extern m_qd_exray_in_perturbed *m_qd_exray_in_perturbed_new(const m_r_orbit_qd *orbit, const mpq_t angle, int sharpness);
extern void m_qd_exray_in_perturbed_delete(m_qd_exray_in_perturbed *ray);
extern m_newton m_qd_exray_in_perturbed_step(m_qd_exray_in_perturbed *ray, int maxsteps);
extern _Float128 _Complex m_qd_exray_in_perturbed_get(const m_qd_exray_in_perturbed *ray);
extern m_qd_exray_in_perturbed * m_qd_exray_in_perturbed_rebase(m_qd_exray_in_perturbed *ray, _Float128 _Complex delta_c, const m_r_orbit_qd *orbit);

struct m_r_exray_out;
typedef struct m_r_exray_out m_r_exray_out;
extern m_r_exray_out *m_r_exray_out_new(const mpc_t c, int sharpness, int maxdwell, double er);
extern void m_r_exray_out_delete(m_r_exray_out *ray);
extern m_newton m_r_exray_out_step(m_r_exray_out *ray);
extern void m_r_exray_out_get(const m_r_exray_out *ray, mpc_t endpoint);
extern bool m_r_exray_out_have_bit(const m_r_exray_out *ray);
extern bool m_r_exray_out_get_bit(const m_r_exray_out *ray);
extern char *m_r_exray_out_do(const mpc_t c, int sharpness, int maxdwell, double er);

struct m_d_exray_out_perturbed;
typedef struct m_d_exray_out_perturbed m_d_exray_out_perturbed;
extern m_d_exray_out_perturbed *m_d_exray_out_perturbed_new(const m_r_orbit_d *orbit, double _Complex endpoint, int sharpness, int maxdwell, double er);
extern void m_d_exray_out_perturbed_delete(m_d_exray_out_perturbed *ray);
extern m_newton m_d_exray_out_perturbed_step(m_d_exray_out_perturbed *ray, int maxsteps);
extern bool m_d_exray_out_perturbed_have_bit(const m_d_exray_out_perturbed *ray);
extern bool m_d_exray_out_perturbed_get_bit(const m_d_exray_out_perturbed *ray);
extern double _Complex m_d_exray_out_perturbed_get(const m_d_exray_out_perturbed *ray);
extern char *m_d_exray_out_perturbed_do(const m_r_orbit_d *orbit, double _Complex endpoint, int sharpness, int maxdwell, double er, int maxsteps);

struct m_r_box_period;
typedef struct m_r_box_period m_r_box_period;
extern m_r_box_period *m_r_box_period_new(const mpc_t center, const mpfr_t radius);
extern void m_r_box_period_delete(m_r_box_period *box);
extern bool m_r_box_period_step(m_r_box_period *box);
extern bool m_r_box_period_have_period(const m_r_box_period *box);
extern int m_r_box_period_get_period(const m_r_box_period *box);
extern int m_r_box_period_do(const mpc_t center, const mpfr_t radius, int maxperiod);

struct m_r_box_misiurewicz;
typedef struct m_r_box_misiurewicz m_r_box_misiurewicz;
extern m_r_box_misiurewicz *m_r_box_misiurewicz_new(const mpc_t center, const mpfr_t radius);
extern void m_r_box_misiurewicz_delete(m_r_box_misiurewicz *box);
extern bool m_r_box_misiurewicz_step(m_r_box_misiurewicz *box);
extern bool m_r_box_misiurewicz_check_periods(m_r_box_misiurewicz *box, int maxperiod);
extern int m_r_box_misiurewicz_get_preperiod(const m_r_box_misiurewicz *box);
extern int m_r_box_misiurewicz_get_period(const m_r_box_misiurewicz *box);
extern bool m_r_box_misiurewicz_do(int *preperiod, int *period, const mpc_t center, const mpfr_t radius, int maxpreperiod, int maxperiod);

struct m_r_ball_period;
typedef struct m_r_ball_period m_r_ball_period;
extern m_r_ball_period *m_r_ball_period_new(const mpc_t center, const mpfr_t radius);
extern void m_r_ball_period_delete(m_r_ball_period *ball);
extern bool m_r_ball_period_step(m_r_ball_period *ball);
extern bool m_r_ball_period_have_period(const m_r_ball_period *ball);
extern bool m_r_ball_period_will_converge(const m_r_ball_period *ball);
extern int m_r_ball_period_get_period(const m_r_ball_period *ball);
extern int m_r_ball_period_do(const mpc_t center, const mpfr_t radius, int maxperiod);

struct m_r_near_period;
typedef struct m_r_near_period m_r_near_period;
extern m_r_near_period *m_r_near_period_new(const mpc_t center, const mpfr_t radius);
extern void m_r_near_period_delete(m_r_near_period *near);
extern bool m_r_near_period_step(m_r_near_period *near);
extern bool m_r_near_period_have_period(const m_r_near_period *near);
extern int m_r_near_period_get_period(const m_r_near_period *near);
extern int m_r_near_period_do(const mpc_t center, const mpfr_t radius, int maxperiod);

extern void m_r_external_angles(int nrays, char **rays, const mpc_t center, int preperiod, int period, double radius, int sharpness);

#ifdef __cplusplus
}
#endif

#endif
