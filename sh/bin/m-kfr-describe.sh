#!/bin/bash
for kfr in "$@"
do
  if [ ! -e "${kfr}.txt" ]
  then
    exponent="$(cat "${kfr}" | tr -d "\r," | grep ^Zoom: | sed "s/^Zoom: .*[eE]\(.*\)$/\1/")"
    re="$(cat "${kfr}" | tr -d "\r," | grep ^Re: | sed "s/Re: \(.*\)/\1/")"
    im="$(cat "${kfr}" | tr -d "\r," | grep ^Im: | sed "s/Im: \(.*\)/\1/")"
    period="$(cat "${kfr}" | tr -d "\r," | grep ^Period: | sed "s/Period: \(.*\)/\1/")"
    precision="$((exponent * 333 / 100))"
    m-describe "${precision}" "$period" "$period" "$re" "$im" 1 |
    tee "${kfr}.txt" |
    sed "s|^|${kfr%.kfr}: |"
  fi
done
