#!/bin/sh
prec=1024
cat <<EOF
program = "m-perturbator-gtk"
version = "v0.2-3-g57bae2f"
width = 1024
height = 1024
theme = "light"
colour = "low"
key = "none"
view.default = { precision = 53, re = "0", im = "0", radius = "2", iterations = 1024 }
annotations = [
EOF
while read prec re im p
do
  ds="$(m-domain-size "${prec}" "${re}" "${im}" "${p}")"
  s="$(m-size "${prec}" "${re}" "${im}" "${p}" | cut -d\  -f 1)"
  cat <<EOF
  { style = { rgb = [ 0.000000, 0.000000, 0.000000 ], fill_type = -1, line_type = 0 }, label = "${p}", type = "nucleus", precision = ${prec}, re = "${re}", im = "${im}", period = ${p}, domain_size = "${ds}", size = "${s}" },
EOF
done
cat <<EOF
]
EOF
