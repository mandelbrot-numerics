#!/bin/bash
maxperiod=10100100
for kfr in "$@"
do
  exponent="$(cat "${kfr}" | tr -d "\r," | grep ^Zoom: | sed "s/^Zoom: .*[eE]\(.*\)$/\1/")"
  re="$(cat "${kfr}" | tr -d "\r," | grep ^Re: | sed "s/Re: \(.*\)/\1/")"
  im="$(cat "${kfr}" | tr -d "\r," | grep ^Im: | sed "s/Im: \(.*\)/\1/")"
  period="$(cat "${kfr}" | tr -d "\r," | grep ^Period: | sed "s/Period: \(.*\)/\1/")"
  precision="$((exponent * 333 / 100))"
  if [ "x$period" = "x" ]
  then
    period="$(m-ball-period "$precision" "$re" "$im" "10e-$((exponent))" "$maxperiod")"
    if [ "x$period" = x ]
    then
      echo "$kfr FAILED $exponent"
    else
      echo "$kfr $period"
      echo -e "Period: ${period}\r" >> "${kfr}"
    fi
  fi
done
