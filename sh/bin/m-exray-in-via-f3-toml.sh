#!/bin/bash
stem="$1"
if [ "x${stem}" = "x" ]
then
  stem="m-exray-in-via"
fi
factor="$2"
if [ "x${factor}" = "x" ]
then
  factor=1
fi
while read new_period new_re new_im new_radius new_angle
do
  old_period="$period"
  period="$new_period"
  old_re="$re"
  re="$new_re"
  old_im="$im"
  im="$new_im"
  radius="${new_radius}"
  angle="${new_angle}"
done
zoom="$(runhugs <(echo "main = print (2 / (${radius} * ${factor}) :: Double)"))"
cat << EOF
location.real = "${old_re}"
location.imag = "${old_im}"
location.zoom = "${zoom}"
reference.period = ${old_period}
transform.rotate = ${angle}
bailout.iterations = 10100100
bailout.maximum_reference_iterations = 10100100
bailout.maximum_perturb_iterations = 5100
algorithm.lock_maximum_reference_iterations_to_period = true
image.width = 1920
image.height = 1080
image.subframes = 64
opencl.tile_width = 1920
opencl.tile_height = 1080
render.filename = "${stem}"
EOF
