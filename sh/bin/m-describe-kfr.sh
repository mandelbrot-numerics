#!/bin/bash
ME="$(dirname "$(readlink -e "${0}")")"
ncpus=1
for kfr in "$@"
do
  Zoom="$(cat "${kfr}" | tr -d "\r," | grep ^Zoom: | sed "s/^Zoom: \(.*\)$/\1/")"
  ZoomE="$(cat "${kfr}" | tr -d "\r," | grep ^Zoom: | sed "s/^Zoom: .*[eE]\(.*\)$/\1/")"
  precision="$((ZoomE * 333 / 100))"
  maxiters="$(cat "${kfr}" | tr -d "\r," | grep ^Iterations: | sed "s/Iterations: \(.*\)$/\1/")"
  if [ "x${maxiters}" = "x" ]
  then
    maxiters=100100
  fi
  maxperiod="${maxiters}"
  re="$(cat "${kfr}" | tr -d "\r," | grep ^Re: | sed "s/Re: \(.*\)/\1/")"
  im="$(cat "${kfr}" | tr -d "\r," | grep ^Im: | sed "s/Im: \(.*\)/\1/")"
  "${ME}/m-describe" "${precision}" "${maxperiod}" "${maxiters}" "${re}" "${im}" "${ncpus}" |
  cat
done
exit
  grep "the nucleus is" |
  head -n-1 |
  sed "s/^.*the nucleus is \([^ ]*\) to the .*$/\1/g" |
  ( cat && echo "$(ghc -e "Numeric.showEFloat Nothing (1 / ${Zoom} :: Numeric.ExpExtended.EDouble) \"\"" | tr "e" "E" | tr -d '"')" ) |
  cat -n |
  while read n radius
  do
    echo "$(basename "${kfr%.kfr}") ${n} ${radius}"
    zoom="$(ghc -e "Numeric.showEFloat Nothing (1 / ${radius} :: Numeric.ExpExtended.EDouble) \"\"" | tr "e" "E" | tr -d '"')"
    cat << EOF | sed "s/$/\r/g" > "$(basename "${kfr%.kfr}-$(printf %04d "${n}").kfr")"
Re: ${re}
Im: ${im}
Zoom: ${zoom}
Iterations: ${maxiters}
EOF
  done
done
