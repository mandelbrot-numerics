#!/bin/bash
j=12
k=8
for d in $(seq 2 17)
do
  n="$(( (1 << (d - 1)) - 1 ))"
  stem="olbaid-st-dms-001_mathr-${j}-$((printf %02d $d))-needle-${k}"
  (
    m-olbaid-st-001 "${j}" "${d}" "${n}" "${k}" &&
cat <<EOF
bailout.iterations = 10100100
bailout.maximum_reference_iterations = 10100100
bailout.maximum_perturb_iterations = 5100
algorithm.lock_maximum_reference_iterations_to_period = true
image.width = 1920
image.height = 1080
image.subframes = 64
opencl.tile_width = 1920
opencl.tile_height = 1080
render.filename = "${stem}"
EOF
  ) > "${stem}.f3.toml" &
done
wait
for d in $(seq 2 17)
do
  n="$(( (1 << (d - 1)) - 1 ))"
  stem="olbaid-st-dms-001_mathr-${j}-$((printf %02d $d))-needle-${k}"
  grep -q "location" "${stem}.f3.toml" &&
  (
    fraktaler-3 -P -b "${stem}.f3.toml"
    convert "${stem}.exr" -colorspace RGB -geometry 640x360 -colorspace sRGB "${stem}.png"
  )
done
ffmpeg -framerate 10 -i "olbaid-st-dms-001_mathr-${j}-%02d-needle-${k}.png" "olbaid-st-dms-001_mathr-${j}-needle-${k}.gif"

j=12
d=5
k=8
for n in $(seq 0 "$(( (1 << d) - 1 ))")
do
  stem="olbaid-st-dms-001_mathr-${j}-${d}-$(printf "%04d" "$n")-${k}"
  (
    m-olbaid-st-001 "${j}" "${d}" "${n}" "${k}" &&
cat <<EOF
bailout.iterations = 10100100
bailout.maximum_reference_iterations = 10100100
bailout.maximum_perturb_iterations = 5100
algorithm.lock_maximum_reference_iterations_to_period = true
image.width = 1920
image.height = 1080
image.subframes = 64
opencl.tile_width = 1920
opencl.tile_height = 1080
render.filename = "${stem}"
EOF
  ) > "${stem}.f3.toml" &
done
wait
for n in $(seq 0 "$(( (1 << d) - 1 ))")
do
  stem="olbaid-st-dms-001_mathr-${j}-${d}-$(printf "%04d" "$n")-${k}"
  grep -q "location" "${stem}.f3.toml" &&
  (
    fraktaler-3 -P -b "${stem}.f3.toml"
    convert "${stem}.exr" -colorspace RGB -geometry 640x360 -colorspace sRGB "${stem}.png"
  )
done
ffmpeg -framerate 10 -i "olbaid-st-dms-001_mathr-${j}-${d}-%04d-${k}.png" "olbaid-st-dms-001_mathr-${j}-${d}-spin-${k}.gif"
