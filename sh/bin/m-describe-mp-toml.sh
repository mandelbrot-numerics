#!/bin/bash
cat << EOF
program = "m-perturbator-gtk"
version = "v0.2"
width = 1280
height = 800
theme = "light"
colour = "low"
key = "none"
view.default = { precision = 53, re = "-7.5000000000000000e-01", im = "0.0000000000000000e+00", radius = "1.5000000000000000e+00" }
annotations = [
EOF
(
tr '\n' '@' |
sed "s/@@/\\n/g" |
sed "s|^- a period \([^ ]*\) [^@]*@  with nucleus at \([^ ]*\) [+] \([^ ]*\) i@  the component has size \([^ ]*\) and is pointing [^@]*@  the atom domain has size \([^@]*\)@.*$|\1 \2 \3 \4 \5|g" && echo
) |
while read period re im size domain_size
do
  exponent="$(echo "${size}" | sed "s/^.*e//g" | sed "s/-0*/-/g")"
  precision="$((24 - exponent * 333 / 100))"
  cat << EOF
  { style = { rgb = [ 0.000000, 0.000000, 0.000000 ], fill_type = -1, line_type = 0 }, label = "${period}", type = "nucleus", precision = ${precision}, re = "${re}", im = "${im}", period = ${period}, domain_size = "${domain_size}", size = "${size}" },
EOF
done
cat << EOF
]
EOF
