#!/bin/sh
sharpness="4"
depth="1"
lo="101010101010101010101010101010011"
hi="101010101010101010101010101010100"
period="33"
prec="24"

nucleusc="-1 0"
nucleusp="2"
nucleusprec="53"

echo "${nucleusprec} ${nucleusc} ${nucleusp}"

period="$((period + 1))"
lo="${lo}1"
hi="${hi}0"

endpoint="$(m-exray-in-perturbed "${nucleusprec}" ${nucleusc} 0 "${nucleusp}" ".(${lo})" "${sharpness}" "$((sharpness * period * depth))" "0")"
nucleusc="$(m-nucleus "${nucleusprec}" ${endpoint} "${period}" 64 1)"
nucleusp="${period}"

echo "${nucleusprec} ${nucleusc} ${nucleusp}"

while [ "${period}" -lt 9000 ]
do

  period="$((2 * period + 1))"
  lo="${lo}${lo}1"
  hi="${lo}${hi}0"

  period="$((period + 1))"
  lo="${lo}1"
  hi="${hi}0"

  prec="$((prec * 3 / 2))"

  endpoint="$(m-exray-in-perturbed "${nucleusprec}" ${nucleusc} 0 "${nucleusp}" ".(${lo})" "${sharpness}" "$((sharpness * period * depth))" "0")"
  nucleusc="$(m-nucleus "$((2 * prec + 24))" ${endpoint} "${period}" 64 1)"
  nucleusp="${period}"
  nucleusprec="$((2 * prec + 24))"

  echo "${nucleusprec} ${nucleusc} ${nucleusp}"

done
