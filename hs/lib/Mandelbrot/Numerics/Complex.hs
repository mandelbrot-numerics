{- |
Complex number operations useful in Mandelbrot set numeric algorithms.
-}
module Mandelbrot.Numerics.Complex
  ( module Data.Complex
  , Approx(..)
  , notFiniteRealFloat
  , bashZeroNumEq
  , approxEqRealFloat
  , converge'
  , converge
  , Square(..)
  , magnitude'
  , magnitudeSquared
  , loss
  ) where

import Data.Complex

import Mandelbrot.Numerics.Compat

class Approx t where
  finite :: t -> Bool
  finite = not . notFinite
  notFinite :: t -> Bool
  notFinite = not . finite
  bashZero :: t -> t
  approxEq :: Int -> t -> t -> Bool

instance Approx Double where
  notFinite = notFiniteRealFloat
  bashZero = bashZeroNumEq
  approxEq = approxEqRealFloat

notFiniteRealFloat :: RealFloat r => r -> Bool
notFiniteRealFloat x = isNaN x || isInfinite x

bashZeroNumEq :: (Num r, Eq r) => r -> r
bashZeroNumEq x
    | 1 + abs x == 1 = 0
    | otherwise = x

approxEqRealFloat :: (Approx r, RealFloat r) => Int -> r -> r -> Bool
approxEqRealFloat b x y
    | x == 0 && y == 0 = True
    | x >  0 && y >  0 = loss   x    y  > k
    | x <  0 && y <  0 = loss (-x) (-y) > k
    | otherwise = False
    where
      k = fromIntegral (floatDigits x - b)

loss :: RealFloat a => a -> a -> a
loss p q = negate . logBase' 2 $ abs (p - q) / max (abs p) (abs q)

closs :: (Square a, RealFloat a) => Complex a -> Complex a -> a
closs p q = (0.5 *) . negate . logBase' 2 $ magnitudeSquared (p - q) / max (magnitudeSquared p) (magnitudeSquared q)

logBase' :: RealFloat a => a -> a -> a
logBase' x y -- Hugs compat
  | y > 0 = logBase x y
  | y == 0 = -1/0
  | otherwise = 0/0

instance (Square t, RealFloat t, Approx t) => Approx (Complex t) where
  {-# SPECIALIZE instance Approx (Complex Double) #-}
  finite (x :+ y) = finite x && finite y
  bashZero (x :+ y) = bashZero x :+ bashZero y
  approxEq _ 0 0 = True
  approxEq b u@(x:+_) v = closs u v > fromIntegral (floatDigits x - b)

instance (Approx s, Approx t) => Approx (Pair s t) where
  {-# SPECIALIZE instance Approx (Pair (Complex Double) (Complex Double)) #-}
  finite (a :!: b) = finite a && finite b
  bashZero (a :!: b) = bashZero a :!: bashZero b
  approxEq b (u :!: v) (x :!: y) = approxEq b u x && approxEq b v y

converge'
  :: Approx t
  => Int -> Int -> [t] -> Maybe t
{-# SPECIALIZE converge' :: Int -> Int -> [Complex Double] -> Maybe (Complex Double) #-}
{-# SPECIALIZE converge' :: Int -> Int -> [Pair (Complex Double) (Complex Double)] -> Maybe (Pair (Complex Double) (Complex Double)) #-}
converge' b n = lastMay . trim . filter finite . take n
  where
    trim [] = []
    trim [_] = []
    trim (x:ys@(y:_))
      | approxEq b x y = [x,y]
      | otherwise = trim ys

converge
  :: (Approx t, Show t)
  => [t] -> t
converge xs = case converge' 2 64 xs of
  Just x -> x
  Nothing -> error $ show ("converge failed", take 64 xs)

class Num t => Square t where
  sqr :: t -> t
  sqr x = x * x
  double :: t -> t
  double x = x + x

instance Square Double

instance (RealFloat t, Square t) => Square (Complex t) where
  sqr (x :+ y) = (sqr x - sqr y) :+ double (x * y)
  {-# SPECIALIZE instance Square (Complex Double) #-}
  double (x :+ y) = double x :+ double y

magnitudeSquared :: (RealFloat r, Square r) => Complex r -> r
{-# SPECIALIZE magnitudeSquared :: Complex Double -> Double #-}
magnitudeSquared (x :+ y) = sqr x + sqr y

magnitude' :: (RealFloat r, Square r, Approx r) => Complex r -> r
{-# SPECIALIZE magnitude' :: Complex Double -> Double #-}
magnitude' z
  | finite z2 = sqrt z2
  | otherwise = 1 / 0
  where z2 = magnitudeSquared z
