{-# LANGUAGE FlexibleContexts #-}
module Mandelbrot.Numerics.BoxPeriod
  ( boxPeriod_
  , boxPeriod'
  , boxPeriod
  ) where

import Mandelbrot.Numerics.Complex
import Mandelbrot.Numerics.Progress

import Mandelbrot.Numerics.Compat

cross
  :: RealFloat r
  => Complex r -> Complex r -> r
cross (ax :+ ay) (bx :+ by) = ay * bx - ax * by

crossesPositiveRealAxis
  :: RealFloat r
  => Complex r -> Complex r -> Bool
crossesPositiveRealAxis a@(_:+ay) b@(_:+by)
  | signum ay /= signum by = s == t
  | otherwise = False
  where
    d@(_:+dy) = b - a
    s = signum dy
    t = signum (d `cross` a)

surroundsOrigin
  :: RealFloat r
  => Complex r -> Complex r -> Complex r -> Complex r -> Bool
surroundsOrigin a b c d
  = odd . length . filter id
  $ zipWith crossesPositiveRealAxis [a,b,c,d] [b,c,d,a]

boxPeriod_
  :: (Approx r, RealFloat r, Square r)
  => Complex r -> r -> Progress Int Int Int
boxPeriod_ c r = go 1 c0 c1 c2 c3
  where
    r' = negate r
    c0 = c + (r' :+ r')
    c1 = c + (r  :+ r')
    c2 = c + (r  :+ r )
    c3 = c + (r' :+ r )
    go p z0 z1 z2 z3
      | notFinite z0 = Failed p
      | notFinite z1 = Failed p
      | notFinite z2 = Failed p
      | notFinite z3 = Failed p
      | surroundsOrigin z0 z1 z2 z3 = Done p
      | otherwise = Continue p
          (go ! (p + 1) ! (sqr z0 + c0) ! (sqr z1 + c1) ! (sqr z2 + c2) ! (sqr z3 + c3))

boxPeriod'
  :: (Approx r, RealFloat r, Square r)
  => Complex r -> r -> Maybe Int
boxPeriod' c r = go (boxPeriod_ c r)
  where
    go (Done p) = Just p
    go (Failed _) = Nothing
    go (Continue _ b) = go b

boxPeriod
  :: (Show r, Approx r, RealFloat r, Square r)
  => Complex r -> r -> Int
boxPeriod c r = case boxPeriod' c r of
  Just p -> p
  Nothing -> error $ show ("boxPeriod failed", c, r)
