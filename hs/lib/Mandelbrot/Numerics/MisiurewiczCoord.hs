{-# LANGUAGE FlexibleContexts #-}
module Mandelbrot.Numerics.MisiurewiczCoord
  ( misiurewiczDomains
  , misiurewiczCoord
--  , atMisiurewiczCoord
  ) where

import Mandelbrot.Numerics.Complex

import Mandelbrot.Numerics.Compat

misiurewiczDomains
  :: (RealFloat r, Square r)
  => Int -> Int -> Complex r -> [Int]
{-# SPECIALIZE misiurewiczDomains :: Int -> Int -> Complex Double -> [Int] #-}
misiurewiczDomains maxn p c
  | 1 <= p && p <= maxn = go1 0 0
  | otherwise = []
  where
    -- compute p iterations
    go1 m z
      | m == p = go2 0 z 0 (1/0)
      | otherwise = go1 ! (m + 1) ! (sqr z + c)
      where
        go2 q u v best
          | q + m == maxn = []
          | magnitudeSquared u + magnitudeSquared v > 1000 = []
          | duv < best = q : (go2 ! (q + 1) ! (sqr u + c) ! (sqr v + c) ! duv)
          | otherwise  = go2 ! (q + 1) ! (sqr u + c) ! (sqr v + c) ! best
          where
            duv = magnitudeSquared (u - v)

misiurewiczCoord
  :: (RealFloat r, Square r)
  => Int -> Int -> Int -> Complex r -> Maybe (Complex r)
{-# SPECIALIZE misiurewiczCoord :: Int -> Int -> Int -> Complex Double -> Maybe (Complex Double) #-}
misiurewiczCoord r q p c
  | 0 <= r && r < q && 1 <= p = Just $ go1 0 0
  | otherwise = Nothing
  where
    -- compute r iterations
    go1 n z
      | n == r = go2 n z
      | otherwise = go1 ! (n + 1) ! (sqr z + c)
      where
        -- continue to q iterations
        go2 n' w
          | n' == q = (go3 0 w - w) / (go3 0 z - z)
          | otherwise = go2 ! (n' + 1) ! (sqr w + c)
          where
            -- compute p more iterations
            go3 m v
              | m == p = v
              | otherwise = go3 ! (m + 1) ! (sqr v + c)

{-
atMisiurewiczCoord
  :: (RealFloat r, Square r)
  => Int -> Int -> Complex r -> Complex r -> [Complex r]
{-# SPECIALIZE atDomainCoord :: Int -> Int -> Complex Double -> Complex Double -> [Complex Double] #-}
atDomainCoord q p g c = c : go1 0 0 0
  where
    go1 n z dz
      | n == q = go2 ! n ! z  !dz
      | otherwise = go1 ! (n + 1) ! (sqr z + c) ! (double (z * dz) + 1)
      where
        go2 n' w dw
          | n' == p = atDomainCoord q p g ! c'
          | otherwise = go2 ! (n' + 1) ! (sqr w + c) ! (double (w * dw) + 1)
          where
            c' = c - f / df
            f = w / z - g
            df = (dw * z - w * dz) / sqr z
-}
