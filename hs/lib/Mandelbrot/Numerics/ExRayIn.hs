module Mandelbrot.Numerics.ExRayIn
  ( ExRayIn()
  , mapExRayIn
  , newExRayIn
  , traceExRayIn
  , exRayIn_
  , exRayIn
  ) where

import Data.Fixed (mod')

import Mandelbrot.Numerics.Compat
import Mandelbrot.Numerics.Complex
import Mandelbrot.Numerics.Progress

data ExRayIn r = ExRayIn
  { angle :: !Rational
  , sharpness, j, k :: !Int
  , c :: !(Complex r)
  }
  deriving (Eq, Show)

exRayIn_
  :: (Approx r, Square r, RealFloat r)
  => Int -> Rational -> Progress (ExRayIn r) (Complex r) ()
exRayIn_ s q = traceExRayIn (newExRayIn s q)

exRayIn
  :: (Approx r, Square r, RealFloat r)
  => Int -> Rational -> [Complex r]
exRayIn s q = go (exRayIn_ s q)
  where
    go (Continue c' p) = c' : go p
    go _ = []

-- use for increasing precision on failure
mapExRayIn
  :: (RealFloat a, RealFloat b)
  => (a -> b) -> ExRayIn a -> ExRayIn b
mapExRayIn f x = x{ c = f (realPart (c x)) :+ f (imagPart (c x)) }

newExRayIn
  :: RealFloat r
  => Int -> Rational -> ExRayIn r
newExRayIn s q = ExRayIn
  { angle = q `mod'` 1
  , sharpness = s
  , k = 0
  , j = 0
  , c = realToFrac cx :+ realToFrac cy
  }
  where
    cx, cy :: Double
    cx :+ cy = mkPolar 65536 (2 * pi * fromRational q)

traceExRayIn
  :: (Approx r, Square r, RealFloat r)
  => ExRayIn r -> Progress (ExRayIn r) (Complex r) ()
traceExRayIn r
  | j r >= sharpness r = traceExRayIn r{ angle = doubleAngle (angle r), k = k r + 1, j = 0 }
  | otherwise = case converge' 4 64 (go (c r)) of
     Nothing -> Failed r
     Just c' -> if approxEq 4 (c r) c' then Failed r else Continue (c r) (traceExRayIn r{ j = j r + 1, c = c' })
  where
    radius :: Double
    radius = 65536 ** (0.5 ** ((fromIntegral (j r) + 0.5) / fromIntegral (sharpness r)))
    a :: Double
    a = 2 * pi * fromRational (angle r)
    tx :+ ty = mkPolar radius a
    target = realToFrac tx :+ realToFrac ty
    go cc = cc : (go ! go1 0 0 0)
      where
        go1 p z dc
          | p > k r = cc - (z - target) / dc
          | otherwise = go1 ! (p + 1) ! (sqr z + cc) ! (double (z * dc) + 1)

doubleAngle :: Rational -> Rational
doubleAngle q
  | q' >= 1 = q' - 1
  | otherwise = q'
  where
    q' = q * 2
