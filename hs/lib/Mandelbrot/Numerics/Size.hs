{-# LANGUAGE FlexibleContexts #-}
module Mandelbrot.Numerics.Size
  ( size
  ) where

import Mandelbrot.Numerics.Complex

import Mandelbrot.Numerics.Compat

size
  :: (RealFloat r, Square r)
  => Int -> Complex r -> Complex r
{-# SPECIALIZE size :: Int -> Complex Double -> Complex Double #-}
size p c
  | p == 1 = 1
  | otherwise = go 1 0 1 1
  where
    go q z b l
      | q == p = recip (b * sqr l)
      | otherwise = go ! q' ! z' ! b' ! l'
      where
        q' = q + 1
        z' = sqr z + c
        l' = double (z' * l)
        b' = b + recip l'
