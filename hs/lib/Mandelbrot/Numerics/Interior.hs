{-# LANGUAGE FlexibleContexts #-}
module Mandelbrot.Numerics.Interior
  ( interior_
  , interior'
  , interior
  , root_
  , root'
  , root
  , bond_
  , bond'
  , bond
  , Pair(..)
  ) where

import Mandelbrot.Numerics.Complex

import Mandelbrot.Numerics.Compat

interior_
  :: (Square c, Approx c, Fractional c)
  => Int -> c -> c -> c -> [Pair c c]
{-# SPECIALIZE interior_ :: Int -> Complex Double -> Complex Double -> Complex Double -> [Pair (Complex Double) (Complex Double)] #-}
interior_ p i z0 c0
  | p > 0 && finite i && finite z0 && finite c0 = go 0 z0 1 0 0 0
  | otherwise = []
  where
    go q z dz dc dzdz dcdz
      | q >= p = iz' `seq` ic' `seq` ((iz' :!: ic') : (interior_ p i ! iz' ! ic'))
      | otherwise =  go ! q' ! z' ! dz' ! dc' ! dzdz' ! dcdz'
      where
        det = (dz - 1) * dcdz - dc * dzdz;
        iz' = z0 - (dcdz * (z - z0) - dc * (dz - i)) / det;
        ic' = c0 - ((dz - 1) * (dz - i) - dzdz * (z - z0)) / det;
        q' = q + 1
        z' = sqr z + c0
        dz' = double (z * dz)
        dc' = double (z * dc) + 1
        dzdz' = double (z * dzdz + sqr dz)
        dcdz' = double (z * dcdz + dc * dz)

interior'
  :: (Square c, Approx c, Fractional c)
  => Int -> c -> c -> c -> Maybe (Pair c c)
{-# SPECIALIZE interior' :: Int -> Complex Double -> Complex Double -> Complex Double -> Maybe (Pair (Complex Double) (Complex Double)) #-}
interior' p i z0 c0 = converge' 8 256 $ interior_ p i z0 c0

interior
  :: (Show c, Square c, Approx c, Fractional c)
  => Int -> c -> c -> c -> Pair c c
{-# SPECIALIZE interior :: Int -> Complex Double -> Complex Double -> Complex Double -> Pair (Complex Double) (Complex Double) #-}
interior p i z0 c0 = case interior' p i z0 c0 of
  Just zc -> zc
  Nothing -> error $ show ("interior failed to converge", p, i, z0, c0)

root_
  :: (Square c, Approx c, Fractional c)
  => Int -> c -> [c]
root_ p c0 = [ c | _ :!: c <- interior_ p 1 0 c0 ]

root'
  :: (Square c, Approx c, Fractional c)
  => Int -> c -> Maybe c
root' p c0 = converge' 8 256 $ root_ p c0

root
  :: (Show c, Square c, Approx c, Fractional c)
  => Int -> c -> c
root p c0 = case root' p c0 of
  Just c -> c
  Nothing -> error $ show ("root failed to converge", p, c0)

bond_
  :: (RealFloat c, Square c, Approx c, Fractional c)
  => Int -> Rational -> Complex c -> [Complex c]
bond_ p t c0 = [ c | _ :!: c <- interior_ p (cis (2 * pi * fromRational t)) 0 c0 ]

bond'
  :: (RealFloat c, Square c, Approx c, Fractional c)
  => Int -> Rational -> Complex c -> Maybe (Complex c)
bond' p t c0 = converge' 2 64 $ bond_ p t c0

bond
  :: (Show c, RealFloat c, Square c, Approx c, Fractional c)
  => Int -> Rational -> Complex c -> Complex c
bond p t c0 = case bond' p t c0 of
  Just c -> c
  Nothing -> error $ show ("bond failed to converge", p, t, c0)
