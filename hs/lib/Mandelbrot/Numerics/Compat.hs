module Mandelbrot.Numerics.Compat where

infixl 0 !
(!) :: (a -> b) -> a -> b
f ! a = f $! a

data Pair a b = !a :!: !b
  deriving (Eq, Show, Read)

lastMay :: [a] -> Maybe a
lastMay [] = Nothing
lastMay xs = Just (last xs)
