{-# LANGUAGE FlexibleContexts #-}
module Mandelbrot.Numerics.DomainCoord
  ( domainCoord
  , atDomainCoord_
  , atDomainCoord'
  , atDomainCoord
  , tip'
  , tip
  ) where

import Mandelbrot.Numerics.Complex
import Mandelbrot.Numerics.Interior

import Mandelbrot.Numerics.Compat

domainCoord
  :: (RealFloat r, Square r)
  => Int -> Int -> Complex r -> Complex r
{-# SPECIALIZE domainCoord :: Int -> Int -> Complex Double -> Complex Double #-}
domainCoord q p c = go1 0 0
  where
    go1 n z
      | n == q = go2 n z
      | otherwise = go1 ! (n + 1) ! (sqr z + c)
      where
        go2 n' w
          | n' == p = w / z
          | otherwise = go2 ! (n' + 1) ! (sqr w + c)


atDomainCoord_
  :: (RealFloat r, Square r)
  => Int -> Int -> Complex r -> Complex r -> [Complex r]
{-# SPECIALIZE atDomainCoord_ :: Int -> Int -> Complex Double -> Complex Double -> [Complex Double] #-}
atDomainCoord_ q p g c = c : go1 0 0 0
  where
    go1 n z dz
      | n == q = go2 n z dz
      | otherwise = go1 ! (n + 1) ! (sqr z + c) ! (double (z * dz) + 1)
      where
        go2 n' w dw
          | n' == p = atDomainCoord_ q p g ! c'
          | otherwise = go2 ! (n' + 1) ! (sqr w + c) ! (double (w * dw) + 1)
          where
            c' = c - f / df
            f = w / z - g
            df = (dw * z - w * dz) / sqr z

atDomainCoord'
  :: (Approx r, RealFloat r, Square r)
  => Int -> Int -> Complex r -> Complex r -> Maybe (Complex r)
{-# SPECIALIZE atDomainCoord' :: Int -> Int -> Complex Double -> Complex Double -> Maybe (Complex Double) #-}
atDomainCoord' q p g c = converge' 2 64 (atDomainCoord_ q p g c)

atDomainCoord
  :: (Approx r, RealFloat r, Square r, Show r)
  => Int -> Int -> Complex r -> Complex r -> Complex r
{-# SPECIALIZE atDomainCoord :: Int -> Int -> Complex Double -> Complex Double -> Complex Double #-}
atDomainCoord q p g c = case atDomainCoord' q p g c of
  Just d -> d
  Nothing -> error $ show ("atDomainCoord failed", q, p, g, c)

tip'
  :: (Approx r, RealFloat r, Square r)
  => Int -> Int -> Complex r -> Maybe (Complex r)
tip' q p c = case bond' q (1/2) c of
  Just c' -> atDomainCoord' q p (-1) c'
  Nothing -> Nothing

tip
  :: (Show r, Approx r, RealFloat r, Square r)
  => Int -> Int -> Complex r -> Complex r
tip q p c = case tip' q p c of
  Just t -> t
  Nothing -> error $ show ("tip failed", q, p, c)
