module Mandelbrot.Numerics.Dual
  ( Dual()
  , unDual
  , derivative
  , variable
  , constant
  ) where

import Mandelbrot.Numerics.Complex (Square(..), Approx(..))

data Dual a = MkDual{ x, dx :: !a }
  deriving (Eq, Show)

instance Functor Dual where fmap f (MkDual x' dx') = MkDual (f x') (f dx')

unDual :: Dual a -> a
unDual = x

derivative :: Dual a -> a
derivative = dx

variable :: Num a => a -> Dual a
variable x' = MkDual x' 1

constant :: Num a => a -> Dual a
constant x' = MkDual x' 0

instance Num a => Num (Dual a) where
  a + b = MkDual (x a + x b) (dx a + dx b)
  a - b = MkDual (x a - x b) (dx a - dx b)
  a * b = MkDual (x a * x b) (x a * dx b + dx a * x b)
  negate a = MkDual (negate (x a)) (negate (dx a))
  fromInteger a = MkDual (fromInteger a) 0
  abs a = MkDual (abs (x a)) (signum (x a) * dx a)
  signum a = MkDual (signum (x a)) 0

instance (Square a, Fractional a) => Fractional (Dual a) where
  recip a = MkDual (recip (x a)) (negate (dx a) / sqr (x a))
  a / b = MkDual (x a / x b) ((dx a * x b - x a * dx b) / (sqr (x b)))
  fromRational a = MkDual (fromRational a) 0

instance (Square a, Floating a) => Floating (Dual a) where
  pi = constant pi
  exp a = MkDual e (dx a * e)
    where e = exp (x a)
  -- FIXME all the rest

instance Square a => Square (Dual a) where
  sqr a = MkDual (sqr (x a)) (double (x a * dx a))
  double a = MkDual (double (x a)) (double (dx a))

instance Approx t => Approx (Dual t) where
  finite d = finite (x d) && finite (dx d)
  notFinite d = notFinite (x d) || notFinite (dx d)
  bashZero d = MkDual (bashZero (x d)) (dx d)
  approxEq b u v = approxEq b (x u) (x v)
