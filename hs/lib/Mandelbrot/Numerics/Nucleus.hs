{-# LANGUAGE FlexibleContexts #-}
module Mandelbrot.Numerics.Nucleus
  ( nucleusNaive_
  , nucleusNaive'
  , nucleusNaive
  , nucleus_
  , nucleus'
  , nucleus
  ) where

import Mandelbrot.Numerics.Complex
import Mandelbrot.Numerics.Dual

import Mandelbrot.Numerics.Compat

nucleusNaive_
  :: (Approx r, RealFloat r, Square r)
  => Int -> Complex r -> [Complex r]
{-# SPECIALIZE nucleusNaive_ :: Int -> Complex Double -> [Complex Double] #-}
nucleusNaive_ p c0
  | p > 0 && finite c0 = go 0 0 0
  | otherwise = []
  where
    go i z dc
      | notFinite z || notFinite dc = []
      | i == p = c' `seq` c' : nucleus_ p c'
      | otherwise = go ! i' ! z' ! dc'
      where
        c' = c0 - z / dc
        i' = i + 1
        z' = sqr z + c0
        dc' = double (z * dc) + 1

nucleusNaive'
  :: (Approx r, RealFloat r, Square r)
  => Int -> Complex r -> Maybe (Complex r)
{-# SPECIALIZE nucleusNaive' :: Int -> Complex Double -> Maybe (Complex Double) #-}
nucleusNaive' p c0 = converge' 2 64 $ nucleusNaive_ p c0

nucleusNaive
  :: (Show r, Approx r, RealFloat r, Square r)
  => Int -> Complex r -> Complex r
{-# SPECIALIZE nucleusNaive :: Int -> Complex Double -> Complex Double #-}
nucleusNaive p c0 = case nucleusNaive' p c0 of
  Just n -> n
  Nothing -> error $ show ("nucleusNaive failed to converge", p, c0)

nucleus_
  :: (RealFloat r, Square r, Approx r)
  => Int -> Complex r -> [Complex r]
{-# SPECIALIZE nucleus_ :: Int -> Complex Double -> [Complex Double] #-}
nucleus_ p c0
  | p > 0 && finite c0 = go 0 0 1
  | otherwise = []
  where
    go i z h
      | i == p = c' `seq` c' : nucleus_ p c'
      | otherwise = go ! (i + 1) !  (sqr z + variable c0) ! h'
      where
        c' = c0 - unDual f / derivative f
        f = z / h
        h' | 0 < i && i < p && p `mod` i == 0 = h * z
           | otherwise = h

nucleus'
  :: (RealFloat r, Square r, Approx r)
  => Int -> Complex r -> Maybe (Complex r)
{-# SPECIALIZE nucleus' :: Int -> Complex Double -> Maybe (Complex Double) #-}
nucleus' p c0 = converge' 2 64 (nucleus_ p c0)

nucleus
  :: (Show r, RealFloat r, Square r, Approx r)
  => Int -> Complex r -> Complex r
{-# SPECIALIZE nucleus :: Int -> Complex Double -> Complex Double #-}
nucleus p c0 = case nucleus' p c0 of
  Just m -> m
  Nothing -> error $ show ("nucleus failed to converge", p, c0)
