module Mandelbrot.Numerics.Progress
  ( Progress(..)
  , skip
  ) where

data Progress f c d = Failed !f | Continue !c (Progress f c d) | Done !d
  deriving (Eq, Ord, Read, Show)

skip :: Int -> Progress f c d -> Progress f c d
skip n p
  | n <= 0 = p
  | otherwise = case p of
      Continue _ p' -> skip (n - 1) p'
      _ -> p
