{-# LANGUAGE FlexibleContexts #-}
module Mandelbrot.Numerics.Misiurewicz
  ( misiurewicz_
  , misiurewicz'
  , misiurewicz
  , misiurewiczNaive_
  , misiurewiczNaive'
  , misiurewiczNaive
  ) where

import Data.List
  ( foldl'
  )

import Mandelbrot.Numerics.Complex

import Mandelbrot.Numerics.Compat

misiurewiczNaive_
  :: (RealFloat r, Square r, Approx r)
  => Int -> Int -> Complex r -> [Complex r]
{-# SPECIALIZE misiurewiczNaive_ :: Int -> Int -> Complex Double -> [Complex Double] #-}
misiurewiczNaive_ pp p c0
  | pp > 0 && p > 0 && finite c0 = go 0 0 0 0 0
  | otherwise = []
  where
    go i zp dcp z dc
      | i == pp + p = c' `seq` c' : misiurewiczNaive_ pp p c'
      | i == pp = go ! (i + 1) ! z ! dc ! (sqr z + c0) ! (double (z * dc) + 1)
      | otherwise = go ! (i + 1) ! zp ! dcp ! (sqr z + c0) ! (double (z * dc) + 1)
      where
        c' = c0 - (z - zp) / (dc - dcp)

misiurewiczNaive'
  :: (RealFloat r, Square r, Approx r)
  => Int -> Int -> Complex r -> Maybe (Complex r)
{-# SPECIALIZE misiurewiczNaive' :: Int -> Int -> Complex Double -> Maybe (Complex Double) #-}
misiurewiczNaive' pp p c0 = converge' 2 64 (misiurewiczNaive_ pp p c0)

misiurewiczNaive
  :: (Show r, RealFloat r, Square r, Approx r)
  => Int -> Int -> Complex r -> Complex r
{-# SPECIALIZE misiurewiczNaive :: Int -> Int -> Complex Double -> Complex Double #-}
misiurewiczNaive pp p c0 = case misiurewiczNaive' pp p c0 of
  Just m -> m
  Nothing -> error $ show ("misiurewiczNaive failed to converge", pp, p, c0)

misiurewicz_
  :: (RealFloat r, Square r, Approx r)
  => Int -> Int -> Complex r -> [Complex r]
{-# SPECIALIZE misiurewicz_ :: Int -> Int -> Complex Double -> [Complex Double] #-}
misiurewicz_ pp p c0
  | pp > 0 && p > 0 && finite c0 = go 0 [] 0 0
  | otherwise = []
  where
    go i ps z dc
      | i == pp + p = c' `seq` c' : misiurewicz_ pp p c'
      | otherwise = go ! (i + 1) ! ((z:!:dc):ps) ! (sqr z + c0) ! (double (z * dc) + 1)
      where
        c' = c0 - f / df
        f = g / h
        df = (dg * h - g * dh) / (h * h)
        g = z - zp
        dg = dc - dcp
        zp :!: dcp = ps !! (p - 1)
        h :!: dh0 = foldl' prodsum (1 :!: 0) $ zipWith sub ps (drop p ps)
        dh = h * dh0
        sub (xp :!: xs) (yp :!: ys) = dp :!: ((xs - ys) / dp)
          where dp = xp - yp
        prodsum (xp :!: xs) (yp :!: ys) = (xp * yp) :!: (xs + ys)

misiurewicz'
  :: (RealFloat r, Square r, Approx r)
  => Int -> Int -> Complex r -> Maybe (Complex r)
{-# SPECIALIZE misiurewicz' :: Int -> Int -> Complex Double -> Maybe (Complex Double) #-}
misiurewicz' pp p c0 = converge' 2 64 (misiurewicz_ pp p c0)

misiurewicz
  :: (Show r, RealFloat r, Square r, Approx r)
  => Int -> Int -> Complex r -> Complex r
{-# SPECIALIZE misiurewicz :: Int -> Int -> Complex Double -> Complex Double #-}
misiurewicz pp p c0 = case misiurewicz' pp p c0 of
  Just m -> m
  Nothing -> error $ show ("misiurewicz failed to converge", pp, p, c0)
