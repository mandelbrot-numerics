{-# LANGUAGE FlexibleContexts #-}
module Mandelbrot.Numerics.Attractor
  ( attractor_
  , attractor'
  , attractor
  ) where

import Mandelbrot.Numerics.Complex
import Mandelbrot.Numerics.Dual

import Mandelbrot.Numerics.Compat

attractor_
  :: (RealFloat r, Square r, Approx r)
  => Int -> Complex r -> Complex r -> [Complex r]
{-# SPECIALIZE attractor_ :: Int -> Complex Double -> Complex Double -> [Complex Double] #-}
attractor_ p c0 z0
  | p < 1 || notFinite c0 || notFinite z0 = []
  | otherwise = go 0 1 (variable z0)
  where
    go i g z
      | notFinite z ||
        notFinite g = []
      | i == p = if finite z' then z' : attractor_ p c0 z' else []
      | otherwise = go ! (i + 1) ! g' ! (sqr z + constant c0)
      where
        f = z - variable z0
        g'
          | i == 0 = g
          | i == p = g
          | p `mod` i == 0 = g * f
          | otherwise = g
        h = f / g
        z' = z0 - unDual h / derivative h

{-
accept: z^p = z
reject: q < p, q | p, z^q = z
solve: (z^p - z) / prod (z^q - z) = 0
-}

attractor'
  :: (RealFloat r, Square r, Approx r)
  => Int -> Complex r -> Complex r -> Maybe (Complex r)
{-# SPECIALIZE attractor' :: Int -> Complex Double -> Complex Double -> Maybe (Complex Double) #-}
attractor' p c0 z0 = converge' 2 64 (attractor_ p c0 z0)

attractor
  :: (Show r, RealFloat r, Square r, Approx r)
  => Int -> Complex r -> Complex r -> Complex r
{-# SPECIALIZE attractor :: Int -> Complex Double -> Complex Double -> Complex Double #-}
attractor p c0 z0 = case attractor' p c0 z0 of
  Just z -> z
  Nothing -> error $ show ("attractor failed to converge", p, c0, z0)
