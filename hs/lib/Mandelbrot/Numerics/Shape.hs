{-# LANGUAGE FlexibleContexts #-}
module Mandelbrot.Numerics.Shape
  ( Shape(..)
  , shape'
  , shape
  ) where

import Mandelbrot.Numerics.Complex

import Mandelbrot.Numerics.Compat

data Shape = Cardioid | Circle
  deriving (Eq, Ord, Enum, Bounded, Read, Show)

shape'
  :: (RealFloat r, Square r, Approx r)
  => Int -> Complex r -> Maybe Shape
{-# SPECIALIZE shape' :: Int -> Complex Double -> Maybe Shape #-}
shape' p c
  | p < 1 || notFinite e = Nothing
  | magnitudeSquared e < magnitudeSquared (e - 1) = Just Cardioid
  | otherwise = Just Circle
  where
    e = go 1 c 1 1 0 0
    go i z dc dz dcdc dcdz
      | i == p = - (dcdc / (double dc) + dcdz / dz) / (dc * dz)
      | otherwise =
            go ! i' ! z' ! dc' ! dz' ! dcdc' ! dcdz'
      where
        i' = i + 1
        z' = sqr z + c
        dc' = double (z * dc) + 1
        dz' = double (z * dz)
        dcdc' = double (z * dcdc + sqr dc)
        dcdz' = double (z * dcdz + dc * dz)

shape
  :: (Show r, RealFloat r, Square r, Approx r)
  => Int -> Complex r -> Shape
{-# SPECIALIZE shape :: Int -> Complex Double -> Shape #-}
shape p c = case shape' p c of
  Just s -> s
  Nothing -> error $ show ("shape failed", p, c)
