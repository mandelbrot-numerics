{-# LANGUAGE FlexibleContexts #-}

import Data.Complex.Generic
import System.Environment (getArgs)
import System.Random

dot (x:+y) (u:+v) = x * u - y * v
mag2 z = dot z (conjugate z)

chunk :: Int -> [a] -> [[a]]
chunk n [] = []
chunk n zs = let (xs, ys) = splitAt n zs in xs : chunk n ys

exact = fmap toRational
inexact = fmap fromRational

corr :: [Complex Double] -> [Complex Double] -> Double
corr x y = ((/ fromIntegral (length x)) . sum $ zipWith dot x y) / (sqrt . (/ fromIntegral (length x)) . sum . map mag2 $ zipWith (*) x y)

test :: Int -> Int -> Complex Double -> (Complex Double, Complex Double)
test m n c =
  let c' = exact c
      z0':z1s' = drop m $ iterate (\z -> z^2 + c') 0
      z1':_    = drop n z1s'
      z0 :z1s  = drop m $ iterate (\z -> z^2 + c ) 0
      z1:_     = drop n z1s
      e0 = inexact (z0' - exact z0)
      e1 = inexact (z1' - exact z1)
  in  (e0, e1)

main :: IO ()
main = do
  [m, n] <- map read <$> getArgs
  g <- newStdGen
  let r = uncurry corr . unzip . take 100 . map (test m n) . filter (\c -> dot c c <= 4) . map (\[x, y] -> x :+ y) . chunk 2 . randomRs (-2, 2) $ g
  print r
