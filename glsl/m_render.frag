#donotrun

// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2020 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

struct m_mandelbrot
{
  int dwell;
  int atom_domain;
  int misiurewicz_domain;
  float smooth_dwell;
  float final_angle;
  float interior_distance_estimate;
  vec2 exterior_distance_estimate;
  vec2 atom_domain_coordinate;
  vec2 misiurewicz_domain_coordinate;
};

#define MANDELBROT \
  for (; i < Iterations;) \
  { \
    if (i >= MisiurewiczPeriod) \
    { \
      w = cSqr(w) + c; \
    } \
    z = cSqr(z) + cVar(c); \
    ++i; \
    if (dot(z.xy, z.xy) < dot(a, a)) \
    { \
      r.atom_domain = i; \
      r.atom_domain_coordinate = vec2(cDiv(z.xy, a)); \
      a = z.xy; \
      if (! (length(r.interior_distance_estimate) > 0.0)) \
      { \
        w0 = z.xy; \
        m_attractor(w0, w0, c, i, 16); \
        w1 = cVar(w0); \
        for (int j = 0; j < i; ++j) \
          w1 = cSqr(w1) + cConst(c); \
        if (dot(w1.zw, w1.zw) <= 1.0) \
          r.interior_distance_estimate = float(m_interior_de(w0, c, i) / pixel_spacing); \
      } \
    } \
    if (i > MisiurewiczPeriod && dot(z.xy - w, z.xy - w) < dot(m, m)) \
    { \
      r.misiurewicz_domain = i; \
      r.misiurewicz_domain_coordinate = vec2((z.xy - w) / length(m)); \
      m = z.xy - w; \
    } \
    if (! (dot(z.xy, z.xy) < EscapeRadius2)) break; \
  } \
  if (! (dot(z.xy, z.xy) < EscapeRadius2)) \
  { \
    r.dwell = i; \
    r.smooth_dwell = 1.0 - log2(log(length(vec2(z.xy))) / log(sqrt(EscapeRadius2))); \
    r.final_angle = fract(degrees(atan(float(z.y), float(z.x))) / 360.0); \
    r.exterior_distance_estimate = cDiv(vec2(z.xy) * log(length(vec2(z.xy))), vec2(z.zw * pixel_spacing)); \
  } \
  return r;

m_mandelbrot m_render(vec2 c, float pixel_spacing, int Iterations, float EscapeRadius2, int MisiurewiczPeriod)
{
  m_mandelbrot r = m_mandelbrot(0, 0, 0, 1.0 / 0.0, 0.0, 0.0, vec2(0.0 / 0.0), vec2(0.0 / 0.0), vec2(0.0 / 0.0));
  int i = 0;
  vec4 z = vec4(0.0);
  vec2 w = vec2(0.0);
  vec2 a = vec2(4.0, 0.0);
  vec2 m = vec2(4.0, 0.0);
  vec2 w0 = vec2(0.0);
  vec4 w1 = vec4(0.0);
  MANDELBROT
}

#if __VERSION__ >= 400

m_mandelbrot m_render(dvec2 c, double pixel_spacing, int Iterations, float EscapeRadius2, int MisiurewiczPeriod)
{
  m_mandelbrot r = m_mandelbrot(0, 0, 0, 1.0 / 0.0, 0.0, 0.0, vec2(0.0 / 0.0), vec2(0.0 / 0.0), vec2(0.0 / 0.0));
  int i = 0;
  dvec4 z = dvec4(0.0);
  dvec2 w = dvec2(0.0);
  dvec2 a = dvec2(4.0, 0.0);
  dvec2 m = dvec2(4.0, 0.0);
  dvec2 w0 = dvec2(0.0);
  dvec4 w1 = dvec4(0.0);
  MANDELBROT
}

#endif

#undef MANDELBROT

