#donotrun

// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2020 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#define M_MISIUREWICZ_FULL 1

int m_misiurewicz_simple_step(out vec2 c_out, vec2 c_guess, int preperiod, int period)
{
  vec4 z = vec4(0.0);
  vec4 zp = vec4(0.0);
  vec4 c = cVar(c_guess);
  for (int i = 0; i < preperiod + period; ++i)
  {
    if (i == preperiod)
    {
      zp = z;
    }
    z = cSqr(z) + c;
  }
  z -= zp;
  vec2 c_new = c_guess - cDiv(z.xy, z.zw);
  vec2 d = c_new - c_guess;
  if (dot(d, d) <= 1.0e-10)
  {
    c_out = c_new;
    return 1;
  }
  if (dot(d, d) < 1.0 / 0.0)
  {
    c_out = c_new;
    return 0;
  }
  else
  {
    c_out = c_guess;
    return -1;
  }
}

int m_misiurewicz_simple(out vec2 c_out, vec2 c_guess, int preperiod, int period, int maxsteps)
{
  int result = -1;
  vec2 c = c_guess;
  for (int i = 0; i < maxsteps; ++i)
  {
    if (0 != (result = m_misiurewicz_simple_step(c, c, preperiod, period)))
    {
      break;
    }
  }
  c_out = c;
  return result;
}

int m_misiurewicz_full_step(out vec2 c_out, vec2 c_guess, int preperiod, int period)
{
  // iteration
  vec4 z = vec4(0.0);
  vec4 zp = vec4(0.0);
  vec4 c = cVar(c_guess);
  for (int i = 0; i < period; ++i)
  {
    z = cSqr(z) + c;
  }
  vec2 h = vec2(1.0, 0.0);
  vec2 dh = vec2(0.0);
  for (int i = 0; i < preperiod; ++i)
  {
    // reject lower preperiods
    vec4 k = z - zp;
    h = cMul(h, k.xy);
    dh = dh + cDiv(k.zw, k.xy);
    // iterate
    z = cSqr(z) + c;
    zp = cSqr(zp) + c;
  }
  // build function
  dh = cMul(dh, h);
  vec4 g = z - zp;
  vec4 f = cDiv(g, vec4(h, dh));
  // newton step
  vec2 c_new = c_guess - cDiv(f.xy, f.zw);
  // check convergence
  vec2 d = c_new - c_guess;
  if (dot(d, d) <= 1.0e-10)
  {
    c_out = c_new;
    return 1;
  }
  if (dot(d, d) < 1.0 / 0.0)
  {
    c_out = c_new;
    return 0;
  }
  else
  {
    c_out = c_guess;
    return -1;
  }
}

int m_misiurewicz_full(out vec2 c_out, vec2 c_guess, int preperiod, int period, int maxsteps)
{
  int result = -1;
  vec2 c = c_guess;
  for (int i = 0; i < maxsteps; ++i)
  {
    if (0 != (result = m_misiurewicz_full_step(c, c, preperiod, period)))
    {
      break;
    }
  }
  c_out = c;
  return result;
}

#if __VERSION__ >= 400

int m_misiurewicz_simple_step(out dvec2 c_out, dvec2 c_guess, int preperiod, int period)
{
  dvec4 z = dvec4(0.0);
  dvec4 zp = dvec4(0.0);
  dvec4 c = cVar(c_guess);
  for (int i = 0; i < preperiod + period; ++i)
  {
    if (i == preperiod)
    {
      zp = z;
    }
    z = cSqr(z) + c;
  }
  z -= zp;
  dvec2 c_new = c_guess - cDiv(z.xy, z.zw);
  dvec2 d = c_new - c_guess;
  if (dot(d, d) <= 1.0e-24)
  {
    c_out = c_new;
    return 1;
  }
  if (dot(d, d) < 1.0 / 0.0)
  {
    c_out = c_new;
    return 0;
  }
  else
  {
    c_out = c_guess;
    return -1;
  }
}

int m_misiurewicz_simple(out dvec2 c_out, dvec2 c_guess, int preperiod, int period, int maxsteps)
{
  int result = -1;
  dvec2 c = c_guess;
  for (int i = 0; i < maxsteps; ++i)
  {
    if (0 != (result = m_misiurewicz_simple_step(c, c, preperiod, period)))
    {
      break;
    }
  }
  c_out = c;
  return result;
}

int m_misiurewicz_full_step(out dvec2 c_out, dvec2 c_guess, int preperiod, int period)
{
  // iteration
  dvec4 z = dvec4(0.0);
  dvec4 zp = dvec4(0.0);
  dvec4 c = cVar(c_guess);
  for (int i = 0; i < period; ++i)
  {
    z = cSqr(z) + c;
  }
  dvec2 h = dvec2(1.0, 0.0);
  dvec2 dh = dvec2(0.0);
  for (int i = 0; i < preperiod; ++i)
  {
    // reject lower preperiods
    dvec4 k = z - zp;
    h = cMul(h, k.xy);
    dh = dh + cDiv(k.zw, k.xy);
    // iterate
    z = cSqr(z) + c;
    zp = cSqr(zp) + c;
  }
  // build function
  dh = cMul(dh, h);
  dvec4 g = z - zp;
  dvec4 f = cDiv(g, h);
  // newton step
  dvec2 c_new = c_guess - cDiv(f.xy, f.zw);
  // check convergence
  dvec2 d = c_new - c_guess;
  if (dot(d, d) <= 1.0e-24)
  {
    c_out = c_new;
    return 1;
  }
  if (dot(d, d) < 1.0 / 0.0)
  {
    c_out = c_new;
    return 0;
  }
  else
  {
    c_out = c_guess;
    return -1;
  }
}

int m_misiurewicz_full(out dvec2 c_out, dvec2 c_guess, int preperiod, int period, int maxsteps)
{
  int result = -1;
  dvec2 c = c_guess;
  for (int i = 0; i < maxsteps; ++i)
  {
    if (0 != (result = m_misiurewicz_full_step(c, c, preperiod, period)))
    {
      break;
    }
  }
  c_out = c;
  return result;
}

#endif

#ifdef M_MISIUREWICZ_FULL
#define m_misiurewicz m_misiurewicz_full
#define m_misiurewicz_step m_misiurewicz_full_step
#else
#define m_misiurewicz m_misiurewicz_simple
#define m_misiurewicz_step m_misiurewicz_simple_step
#endif
