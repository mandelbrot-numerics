#donotrun

// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2020 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

float m_interior_de(vec2 w0, vec2 c, int period)
{
  vec2 z = w0;
  vec2 dz = vec2(1.0, 0.0);
  vec2 dc = vec2(0.0, 0.0);
  vec2 dzdz = vec2(0.0, 0.0);
  vec2 dcdz = vec2(0.0, 0.0);
  for (int i = 0; i < period; ++i)
  {
    dcdz = 2.0 * (cMul(z, dcdz) + cMul(dz, dc));
    dc = 2.0 * cMul(z, dc) + vec2(1.0, 0.0);
    dzdz = 2.0 * (cMul(z, dzdz) + cSqr(dz));
    dz = 2.0 * cMul(z, dz);
    z = cSqr(z) + c;
  }
  float d = (1.0 - dot(dz, dz)) / length(dcdz + cDiv(cMul(dzdz, dc), vec2(1.0, 0.0) - dz));
  return d;
}

#if __VERSION__ >= 400

double m_interior_de(dvec2 w0, dvec2 c, int period)
{
  dvec2 z = w0;
  dvec2 dz = dvec2(1.0, 0.0);
  dvec2 dc = dvec2(0.0, 0.0);
  dvec2 dzdz = dvec2(0.0, 0.0);
  dvec2 dcdz = dvec2(0.0, 0.0);
  for (int i = 0; i < period; ++i)
  {
    dcdz = 2.0 * (cMul(z, dcdz) + cMul(dz, dc));
    dc = 2.0 * cMul(z, dc) + vec2(1.0, 0.0);
    dzdz = 2.0 * (cMul(z, dzdz) + cSqr(dz));
    dz = 2.0 * cMul(z, dz);
    z = cSqr(z) + c;
  }
  double d = (1.0 - dot(dz, dz)) / length(dcdz + cDiv(cMul(dzdz, dc), dvec2(1.0, 0.0) - dz));
  return d;
}

#endif
