#donotrun

// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2020 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

int m_attractor_step(out vec2 z_out, vec2 z_guess, vec2 c, int period)
{
  vec4 w0 = cVar(z_guess);
  vec4 w = w0;
  for (int i = 0; i < period; ++i)
  {
    w = cSqr(w) + cConst(c);
  }
  w -= w0;
  vec2 z_new = z_guess - cDiv(w.xy, w.zw);
  vec2 d = z_new - z_guess;
  if (dot(d, d) <= 1.0e-10)
  {
    z_out = z_new;
    return 1;
  }
  if (dot(d, d) < 1.0 / 0.0)
  {
    z_out = z_new;
    return 0;
  }
  else
  {
    z_out = z_guess;
    return -1;
  }
}

int m_attractor(out vec2 z_out, vec2 z_guess, vec2 c, int period, int maxsteps)
{
  int result = -1;
  vec2 z = z_guess;
  for (int i = 0; i < maxsteps; ++i)
  {
    if (0 != (result = m_attractor_step(z, z, c, period)))
    {
      break;
    }
  }
  z_out = z;
  return result;
}

#if __VERSION__ >= 400

int m_attractor_step(out dvec2 z_out, dvec2 z_guess, dvec2 c, int period)
{
  dvec4 w0 = cVar(z_guess);
  dvec4 w = w0;
  for (int i = 0; i < period; ++i)
  {
    w = cSqr(w) + cConst(c);
  }
  w -= w0;
  dvec2 z_new = z_guess - cDiv(w.xy, w.zw);
  dvec2 d = z_new - z_guess;
  if (dot(d, d) <= 1.0e-24)
  {
    z_out = z_new;
    return 1;
  }
  if (dot(d, d) < 1.0 / 0.0)
  {
    z_out = z_new;
    return 0;
  }
  else
  {
    z_out = z_guess;
    return -1;
  }
}

int m_attractor(out dvec2 z_out, dvec2 z_guess, dvec2 c, int period, int maxsteps)
{
  int result = -1;
  dvec2 z = z_guess;
  for (int i = 0; i < maxsteps; ++i)
  {
    if (0 != (result = m_attractor_step(z, z, c, period)))
    {
      break;
    }
  }
  z_out = z;
  return result;
}

#endif
