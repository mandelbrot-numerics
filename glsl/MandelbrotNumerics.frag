#donotrun

// mandelbrot-numerics -- numerical algorithms related to the Mandelbrot set
// Copyright (C) 2015-2020 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include "Complex.frag"

#include "m_nucleus.frag"
#include "m_misiurewicz.frag"
#include "m_newton.frag"
#include "m_attractor.frag"
#include "m_interior_de.frag"
#include "m_render.frag"
