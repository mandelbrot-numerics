# Installing with Emscripten

See <https://stackoverflow.com/a/43583154/7459445> for instructions on setting
up a 32bit chroot for compiling emscripten, gmp, mpfr, mpc.  Then do this for
mandelbrot-numerics:

    git clone https://code.mathr.co.uk/mandelbrot-numerics.git
    cd mandelbrot-numerics
    make -C c/lib CC="emcc -I${HOME}/opt/include" prefix=${HOME}/opt -j 6
    make -C c/lib CC="emcc -I${HOME}/opt/include" prefix=${HOME}/opt install
    make -C c/bin CC="emcc -I${HOME}/opt/include" prefix=${HOME}/opt -j 6 js
    # test example:
    cd c/bin && nodejs m-nucleus.js 1000 -2 0 250 64
